#ifndef f7_H
#define f7_H

#include <QWidget>

namespace Ui {
class f7;
}

class f7 : public QWidget
{
    Q_OBJECT

public:
    explicit f7(QWidget *parent = 0);
    ~f7();

//private:
    Ui::f7 *ui;
};

#endif // f7_H
