#ifndef f14_H
#define f14_H

#include <QWidget>

namespace Ui {
class f14;
}

class f14 : public QWidget
{
    Q_OBJECT

public:
    explicit f14(QWidget *parent = 0);
    ~f14();

//private:
    Ui::f14 *ui;
};

#endif // f14_H
