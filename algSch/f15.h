#ifndef f15_H
#define f15_H

#include <QWidget>

namespace Ui {
class f15;
}

class f15 : public QWidget
{
    Q_OBJECT

public:
    explicit f15(QWidget *parent = 0);
    ~f15();

//private:
    Ui::f15 *ui;
};

#endif // f15_H
