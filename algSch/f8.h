#ifndef f8_H
#define f8_H

#include <QWidget>

namespace Ui {
class f8;
}

class f8 : public QWidget
{
    Q_OBJECT

public:
    explicit f8(QWidget *parent = 0);
    ~f8();

//private:
    Ui::f8 *ui;
};

#endif // f8_H
