#ifndef f12_H
#define f12_H

#include <QWidget>

namespace Ui {
class f12;
}

class f12 : public QWidget
{
    Q_OBJECT

public:
    explicit f12(QWidget *parent = 0);
    ~f12();

//private:
    Ui::f12 *ui;
};

#endif // f12_H
