#ifndef f16_H
#define f16_H

#include <QWidget>

namespace Ui {
class f16;
}

class f16 : public QWidget
{
    Q_OBJECT

public:
    explicit f16(QWidget *parent = 0);
    ~f16();

//private:
    Ui::f16 *ui;
};

#endif // f16_H
