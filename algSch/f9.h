#ifndef f9_H
#define f9_H

#include <QWidget>

namespace Ui {
class f9;
}

class f9 : public QWidget
{
    Q_OBJECT

public:
    explicit f9(QWidget *parent = 0);
    ~f9();

//private:
    Ui::f9 *ui;
};

#endif // f9_H
