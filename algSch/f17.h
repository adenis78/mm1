#ifndef F17_H
#define F17_H

#include <QWidget>

namespace Ui {
class f17;
}

class f17 : public QWidget
{
    Q_OBJECT

public:
    explicit f17(QWidget *parent = 0);
    ~f17();

private:
    Ui::f17 *ui;
};

#endif // F17_H
