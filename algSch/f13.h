#ifndef f13_H
#define f13_H

#include <QWidget>

namespace Ui {
class f13;
}

class f13 : public QWidget
{
    Q_OBJECT

public:
    explicit f13(QWidget *parent = 0);
    ~f13();

//private:
    Ui::f13 *ui;
};

#endif // f13_H
