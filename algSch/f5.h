#ifndef f5_H
#define f5_H

#include <QWidget>

namespace Ui {
class f5;
}

class f5 : public QWidget
{
    Q_OBJECT

public:
    explicit f5(QWidget *parent = 0);
    ~f5();

//private:
    Ui::f5 *ui;
};

#endif // f5_H
