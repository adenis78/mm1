#ifndef f11_H
#define f11_H

#include <QWidget>

namespace Ui {
class f11;
}

class f11 : public QWidget
{
    Q_OBJECT

public:
    explicit f11(QWidget *parent = 0);
    ~f11();

//private:
    Ui::f11 *ui;
};

#endif // f11_H
