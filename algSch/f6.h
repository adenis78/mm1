#ifndef f6_H
#define f6_H

#include <QWidget>

namespace Ui {
class f6;
}

class f6 : public QWidget
{
    Q_OBJECT

public:
    explicit f6(QWidget *parent = 0);
    ~f6();

//private:
    Ui::f6 *ui;
};

#endif // f6_H
