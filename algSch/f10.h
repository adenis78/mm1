#ifndef f10_H
#define f10_H

#include <QWidget>

namespace Ui {
class f10;
}

class f10 : public QWidget
{
    Q_OBJECT

public:
    explicit f10(QWidget *parent = 0);
    ~f10();

private:
    Ui::f10 *ui;
};

#endif // f10_H
