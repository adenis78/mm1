#ifndef SRAB_ISPR_ALG_153_H
#define SRAB_ISPR_ALG_153_H

// "срабатывание алгоритмов в 153" для подсветки выходов алгоритмов

//const PvkByteBit algAz1 = {pkc, 238,0 };
//const PvkByteBit algAz2 = {pkc, 238,1 };
//const PvkByteBit algAz3 = {pkc, 238,2 };
//const PvkByteBit algAz4 = {pkc, 238,3 };
//const PvkByteBit algAz5 = {pkc, 238,4 };
//const PvkByteBit algAz6 = {pkc, 238,5 };
//const PvkByteBit algAz7 = {pkc, 238,6 };
//const PvkByteBit algAz8 = {pkc, 238,7 };
//const PvkByteBit algAz9 = {pkc, 239,0 };
//const PvkByteBit algAz10 = {pkc, 239,1 };
//const PvkByteBit algAz11 = {pkc, 239,2 };
//const PvkByteBit algAz12 = {pkc, 239,3 };
//const PvkByteBit algAz13 = {pkc, 239,4 };
//const PvkByteBit algAz14 = {pkc, 239,5 };
//const PvkByteBit algAz15 = {pkc, 239,6 };
//const PvkByteBit algAz16 = {pkc, 239,7 };
//const PvkByteBit algAz17 = {pkc, 240,0 };
//const PvkByteBit algAz18 = {pkc, 240,1 };
//const PvkByteBit algAz19 = {pkc, 240,2 };
//const PvkByteBit algAz20 = {pkc, 240,3 };
//const PvkByteBit algAz21 = {pkc, 240,4 };
//const PvkByteBit algAz22 = {pkc, 240,5 };
//const PvkByteBit algAz23 = {pkc, 240,6 };
//const PvkByteBit algAz24 = {pkc, 240,7 };
//const PvkByteBit algAz25 = {pkc, 241,0 };
//const PvkByteBit algAz26 = {pkc, 241,1 };
//const PvkByteBit algAz27 = {pkc, 241,2 };
//const PvkByteBit algAz28 = {pkc, 241,3 };
//const PvkByteBit algAz29 = {pkc, 241,4 };
//const PvkByteBit algAz30 = {pkc, 241,5 };
//const PvkByteBit algAz31 = {pkc, 241,6 };

//const PvkByteBit algPz1_1 = {pkc, 72,0 };
//const PvkByteBit algPz1_2 = {pkc, 72,1 };
//const PvkByteBit algPz1_3 = {pkc, 72,2 };
//const PvkByteBit algPz1_4 = {pkc, 72,3 };
//const PvkByteBit algPz1_5 = {pkc, 72,4 };
//const PvkByteBit algPz1_6 = {pkc, 72,5 };
//const PvkByteBit algPz2_1 = {pkc, 72,6 };
//const PvkByteBit algPz2_2 = {pkc, 72,7 };
//const PvkByteBit algPz2_3 = {pkc, 73,0 };
//const PvkByteBit algPz2_4 = {pkc, 73,1 };
//const PvkByteBit algPz2_5 = {pkc, 73,2 };
//const PvkByteBit algUpz1 = {pkc, 73,3 };
//const PvkByteBit algUpz2 = {pkc, 73,4 };
//const PvkByteBit algUpz3 = {pkc, 73,5 };
//const PvkByteBit algUpz4 = {pkc, 73,6 };
//const PvkByteBit algUpz5 = {pkc, 73,7 };
//const PvkByteBit algPz1_7 = {pkc, 74,0}; //факт срабатывания схемы "≥2" для QL>400(выход алг. 17) ПЗ1
//const PvkByteBit algPz1_8 = {pkc, 74,1}; //факт срабатывания схемы "≥2" для DNBR<1,4(выход алг. 18) ПЗ1

// "исправность при авт.контроле в 153" для подсветки выходов алгоритмов
// теперь в ПКЦ184(185) !!
static const PvkByteBit isAz1 = {pkc, 238,0 };
static const PvkByteBit isAz2 = {pkc, 238,1 };
static const PvkByteBit isAz3 = {pkc, 238,2 };
static const PvkByteBit isAz4 = {pkc, 238,3 };
static const PvkByteBit isAz5 = {pkc, 238,4 };
static const PvkByteBit isAz6 = {pkc, 238,5 };
static const PvkByteBit isAz7 = {pkc, 238,6 };
static const PvkByteBit isAz8 = {pkc, 238,7 };
static const PvkByteBit isAz9 = {pkc, 239,0 };
static const PvkByteBit isAz10 = {pkc, 239,1 };
static const PvkByteBit isAz11 = {pkc, 239,2 };
static const PvkByteBit isAz12 = {pkc, 239,3 };
static const PvkByteBit isAz13 = {pkc, 239,4 };
static const PvkByteBit isAz14 = {pkc, 239,5 };
static const PvkByteBit isAz15 = {pkc, 239,6 };
static const PvkByteBit isAz16 = {pkc, 239,7 };
static const PvkByteBit isAz17 = {pkc, 240,0 };
static const PvkByteBit isAz18 = {pkc, 240,1 };
static const PvkByteBit isAz19 = {pkc, 240,2 };
static const PvkByteBit isAz20 = {pkc, 240,3 };
static const PvkByteBit isAz21 = {pkc, 240,4 };
static const PvkByteBit isAz22 = {pkc, 240,5 };
static const PvkByteBit isAz23 = {pkc, 240,6 };
static const PvkByteBit isAz24 = {pkc, 240,7 };
static const PvkByteBit isAz25 = {pkc, 241,0 };
static const PvkByteBit isAz26 = {pkc, 241,1 };
static const PvkByteBit isAz27 = {pkc, 241,2 };
static const PvkByteBit isAz28 = {pkc, 241,3 };
static const PvkByteBit isAz29 = {pkc, 241,4 };
static const PvkByteBit isAz30 = {pkc, 241,5 };
static const PvkByteBit isAz31 = {pkc, 241,6 };

static const PvkByteBit isPz1_1 = {pkc, 243,0 };
static const PvkByteBit isPz1_2 = {pkc, 243,1 };
static const PvkByteBit isPz1_3 = {pkc, 243,2 };
static const PvkByteBit isPz1_4 = {pkc, 243,3 };
static const PvkByteBit isPz1_5 = {pkc, 243,4 };
static const PvkByteBit isPz1_6 = {pkc, 243,5 };
static const PvkByteBit isPz2_1 = {pkc, 243,6 };
static const PvkByteBit isPz2_2 = {pkc, 243,7 };
static const PvkByteBit isPz2_3 = {pkc, 244,1 };
static const PvkByteBit isPz2_4 = {pkc, 244,2 };
static const PvkByteBit isPz2_5 = {pkc, 244,3 };
static const PvkByteBit isUpz1 = {pkc, 244,4 };
static const PvkByteBit isUpz2 = {pkc, 244,5 };
static const PvkByteBit isUpz3 = {pkc, 244,6 };
static const PvkByteBit isUpz4 = {pkc, 244,7 };
static const PvkByteBit isUpz5 = {pkc, 245,0 };
static const PvkByteBit isPz1_7 = {pkc, 245,1 }; //
static const PvkByteBit isPz1_8 = {pkc, 245,2 }; //


static const PvkByteBit alg153az_1 = {p153az, 270,0 };
static const PvkByteBit alg153az_2 = {p153az, 270,1 };
static const PvkByteBit alg153az_3 = {p153az, 270,2 };
static const PvkByteBit alg153az_4 = {p153az, 270,3 };
static const PvkByteBit alg153az_5 = {p153az, 270,4 };
static const PvkByteBit alg153az_6 = {p153az, 270,5 };
static const PvkByteBit alg153az_7 = {p153az, 270,6 };
static const PvkByteBit alg153az_8 = {p153az, 270,7 };
static const PvkByteBit alg153az_9 = {p153az, 271,0 };
static const PvkByteBit alg153az_10 = {p153az, 271,1 };
static const PvkByteBit alg153az_11 = {p153az, 271,2 };
static const PvkByteBit alg153az_12 = {p153az, 271,3 };
static const PvkByteBit alg153az_13 = {p153az, 271,4 };
static const PvkByteBit alg153az_14 = {p153az, 271,5 };
static const PvkByteBit alg153az_15 = {p153az, 271,6 };
static const PvkByteBit alg153az_16 = {p153az, 271,7 };
static const PvkByteBit alg153az_17 = {p153az, 272,0 };
static const PvkByteBit alg153az_18 = {p153az, 272,1 };
static const PvkByteBit alg153az_19 = {p153az, 272,2 };
static const PvkByteBit alg153az_20 = {p153az, 272,3 };
static const PvkByteBit alg153az_21 = {p153az, 272,4 };
static const PvkByteBit alg153az_22 = {p153az, 272,5 };
static const PvkByteBit alg153az_23 = {p153az, 272,6 };
static const PvkByteBit alg153az_24 = {p153az, 272,7 };
static const PvkByteBit alg153az_25 = {p153az, 273,0 };
static const PvkByteBit alg153az_26 = {p153az, 273,1 };
static const PvkByteBit alg153az_27 = {p153az, 273,2 };
static const PvkByteBit alg153az_28 = {p153az, 273,3 };
static const PvkByteBit alg153az_29 = {p153az, 273,4 };
static const PvkByteBit alg153az_30 = {p153az, 273,5 };
static const PvkByteBit alg153az_31 = {p153az, 273,6 };

static const PvkByteBit alg153pz1_1 = {p153pz, 72,0 };
static const PvkByteBit alg153pz1_2 = {p153pz, 72,1 };
static const PvkByteBit alg153pz1_3 = {p153pz, 72,2 };
static const PvkByteBit alg153pz1_4 = {p153pz, 72,3 };
static const PvkByteBit alg153pz1_5 = {p153pz, 72,4 };
static const PvkByteBit alg153pz1_6 = {p153pz, 72,5 };
static const PvkByteBit alg153pz2_1 = {p153pz, 72,6 };
static const PvkByteBit alg153pz2_2 = {p153pz, 72,7 };
static const PvkByteBit alg153pz2_3 = {p153pz, 73,0 };
static const PvkByteBit alg153pz2_4 = {p153pz, 73,1 };
static const PvkByteBit alg153pz2_5 = {p153pz, 73,2 };
static const PvkByteBit alg153upz_1 = {p153pz, 73,3 };
static const PvkByteBit alg153upz_2 = {p153pz, 73,4 };
static const PvkByteBit alg153upz_3 = {p153pz, 73,5 };
static const PvkByteBit alg153upz_4 = {p153pz, 73,6 };
static const PvkByteBit alg153upz_5 = {p153pz, 73,7 };
static const PvkByteBit alg153pz1_7 = {p153pz, 74,0}; //факт срабатывания схемы "≥2" для QL>400(выход алг. 17) ПЗ1
static const PvkByteBit alg153pz1_8 = {p153pz, 74,1}; //факт срабатывания схемы "≥2" для DNBR<1,4(выход алг. 18) ПЗ1

// "исправность при авт.контроле в 153" для подсветки выходов алгоритмов
// теперь в ПКЦ184(185) !!
//const PvkByteBit isp153az_1 = {p153az, 273,7 };
//const PvkByteBit isp153az_2 = {p153az, 274,0 };
//const PvkByteBit isp153az_3 = {p153az, 274,1 };
//const PvkByteBit isp153az_4 = {p153az, 274,2 };
//const PvkByteBit isp153az_5 = {p153az, 274,3 };
//const PvkByteBit isp153az_6 = {p153az, 274,4 };
//const PvkByteBit isp153az_7 = {p153az, 274,5 };
//const PvkByteBit isp153az_8 = {p153az, 274,6 };
//const PvkByteBit isp153az_9 = {p153az, 274,7 };
//const PvkByteBit isp153az_10 = {p153az, 275,0 };
//const PvkByteBit isp153az_11 = {p153az, 275,1 };
//const PvkByteBit isp153az_12 = {p153az, 275,2 };
//const PvkByteBit isp153az_13 = {p153az, 275,3 };
//const PvkByteBit isp153az_14 = {p153az, 275,4 };
//const PvkByteBit isp153az_15 = {p153az, 275,5 };
//const PvkByteBit isp153az_16 = {p153az, 275,6 };
//const PvkByteBit isp153az_17 = {p153az, 275,7 };
//const PvkByteBit isp153az_18 = {p153az, 276,0 };
//const PvkByteBit isp153az_19 = {p153az, 276,1 };
//const PvkByteBit isp153az_20 = {p153az, 276,2 };
//const PvkByteBit isp153az_21 = {p153az, 276,3 };
//const PvkByteBit isp153az_22 = {p153az, 276,4 };
//const PvkByteBit isp153az_23 = {p153az, 276,5 };
//const PvkByteBit isp153az_24 = {p153az, 276,6 };
//const PvkByteBit isp153az_25 = {p153az, 276,7 };
//const PvkByteBit isp153az_26 = {p153az, 277,0 };
//const PvkByteBit isp153az_27 = {p153az, 277,1 };
//const PvkByteBit isp153az_28 = {p153az, 277,2 };
//const PvkByteBit isp153az_29 = {p153az, 277,3 };
//const PvkByteBit isp153az_30 = {p153az, 277,4 };
//const PvkByteBit isp153az_31 = {p153az, 277,5 };
//const PvkByteBit isp153pz1_1 = {p153pz, 74,0 };
//const PvkByteBit isp153pz1_2 = {p153pz, 74,1 };
//const PvkByteBit isp153pz1_3 = {p153pz, 74,2 };
//const PvkByteBit isp153pz1_4 = {p153pz, 74,3 };
//const PvkByteBit isp153pz1_5 = {p153pz, 74,4 };
//const PvkByteBit isp153pz1_6 = {p153pz, 74,5 };
//const PvkByteBit isp153pz2_1 = {p153pz, 74,6 };
//const PvkByteBit isp153pz2_2 = {p153pz, 74,7 };
//const PvkByteBit isp153pz2_3 = {p153pz, 74,0 };
//const PvkByteBit isp153pz2_4 = {p153pz, 75,1 };
//const PvkByteBit isp153pz2_5 = {p153pz, 75,2 };
//const PvkByteBit isp153upz_1 = {p153pz, 75,3 };
//const PvkByteBit isp153upz_2 = {p153pz, 75,4 };
//const PvkByteBit isp153upz_3 = {p153pz, 75,5 };
//const PvkByteBit isp153upz_4 = {p153pz, 75,6 };
//const PvkByteBit isp153upz_5 = {p153pz, 75,6 };



#endif // SRAB_ISPR_ALG_153_H

