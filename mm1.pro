#-------------------------------------------------
#
# Project created by QtCreator 2015-08-11T15:57:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mm1
TEMPLATE = app

CONFIG += c++11

QMAKE_CXXFLAGS += -std=c++11 -pthread
#QMAKE_CXXFLAGS += -Wall -Wextra -Wno-missing-field-initializers -Werror
QMAKE_CXXFLAGS += -Wno-missing-field-initializers


QMAKE_LFLAGS += -Wl,--no-as-needed


QMAKE_CFLAGS +=  -pthread

LIBS += -pthread

SOURCES += main.cpp\
    dataTableModel.cpp \
    ParamTableModel.cpp \
    form3.cpp \
    shmemthread.cpp \
    ethtesttv.cpp \
    formula.cpp \
    mdlppndata.cpp \
    ppn.cpp \
    ethsend.cpp \
    referenceppn.cpp \
    dlgppn.cpp \
    ref.cpp \
    form3_labelcolor.cpp \
    form3_slideFailConnect.cpp \
    form3_slideDiscr.cpp \
    form3_ustav.cpp \
    form3_ppn.cpp \
    form3_slideDiscrTxt.cpp \
    form3_alg.cpp \
    algsignals.cpp \
    algSch/f1.cpp \
    algSch/f2.cpp \
    algSch/f3.cpp \
    algSch/f4.cpp \
    algSch/f5.cpp \
    algSch/f6.cpp \
    algSch/f7.cpp \
    algSch/f8.cpp \
    algSch/f9.cpp \
    algSch/f10.cpp \
    algSch/f11.cpp \
    algSch/f12.cpp \
    algSch/f13.cpp \
    algSch/f14.cpp \
    algSch/f15.cpp \
    algSch/f16.cpp \
    ethrecv.cpp \
    formtest.cpp \
    form3_events.cpp \
    algSch/f17.cpp \
    refalg.cpp \
    modbus/modbus.cpp \
    modbus/transmitthread.cpp \
    aop.cpp

HEADERS  += \
    dataTableModel.h \
    ParamTableModel.h \
    form3.h \
    shmemthread.h \
    packdata.h \
    ethtesttv.h \
    formula.h \
    mdlppndata.h \
    ppn.h \
    constants.h \
    referenceppn.h \
    dlgppn.h \
    ref.h \
    algsignals.h \
    pvk.h \
    algSch/f1.h \
    algSch/f2.h \
    algSch/f3.h \
    algSch/f4.h \
    algSch/f5.h \
    algSch/f6.h \
    algSch/f7.h \
    algSch/f8.h \
    algSch/f9.h \
    algSch/f10.h \
    algSch/f11.h \
    algSch/f12.h \
    algSch/f13.h \
    algSch/f14.h \
    algSch/f15.h \
    algSch/f16.h \
    eth.h \
    alg_fact_meq2.h \
    alg_outsignals.h \
    formtest.h \
    srab_ispr_alg_153.h \
    usts.h \
    parType.h \
    algSch/f17.h \
    refalg.h \
    modbus/modbus.h \
    algforms.h \
    modbus/transmitthread.h \
    im_blk_pkc.h \
    presets.h \
    p151_152.h \
    p153az.h \
    p153pz.h \
    p155_156.h \
    p157_158.h \
    pkc.h

FORMS    += \
    form3.ui \
    referenceppn.ui \
    dlgppn.ui \
    ref.ui \
    algSch/f1.ui \
    formtest.ui \
    algSch/f10.ui \
    algSch/f11.ui \
    algSch/f12.ui \
    algSch/f13.ui \
    algSch/f14.ui \
    algSch/f15.ui \
    algSch/f16.ui \
    algSch/f2.ui \
    algSch/f3.ui \
    algSch/f4.ui \
    algSch/f5.ui \
    algSch/f6.ui \
    algSch/f7.ui \
    algSch/f8.ui \
    algSch/f9.ui \
    algSch/f17.ui \
    refalg.ui

RESOURCES += \
    res1.qrc

DISTFILES += \
    1 \
    text
