#ifndef FORMULA_H
#define FORMULA_H
#include <QString>
#include "eth.h"
#include "pvk.h"
#include "parType.h"

//157/158 смещения для групп в пакете в байтах
const int G3 = 104; // группа 3
const int G1 = 0; // группа 1


extern unsigned char valData[ETH_SRC_NUM][ETH_DATA_MAX]; // в main.cpp

struct D2 { double d1; double d2;};


struct ByteBit { int byte; int bit; double sm; double diap; }; // байт, бит, еще смещение и диапазон для P

// 0, 1:  номер байта и бита для битов имитации
// 2, 3: смещение и предельное значение для значения вычисления параметра и уставок.
// Там где нули ( и не только там:) значит неважно, и данные берутся не отсюда

class Formula {
public:
    int im_pkc(int ix);

protected:
    double get_U (short int code3);
    double get_P (short int code3, double sm, double diap );
    double get_T (short int code1);
    double get_dT (short int code1);
    //struct D2 { double d1; double d2;};
    double get_F (short int code1);
    double get_R (short int code1);
    double get_I (short int code1);
public:
    D2 T(short int index);
    D2 dT(short int index);
    D2 P(short int index, double sm, double diap);
    D2 U(short int index);
    D2 I(short int index);
    D2 R(short int index);
    D2 F(short int index);

    //возвращает значение бита (1|0) с номером (bit) в байте с номером (byte) из массива
    int b(int pvk, int byte, int bit);
    int b(PvkByteBit pbb);
    int b(PvkByteBitSign pbb); // признак инверсии знаков ≤|≥ игнорируется
    int bs(PvkByteBitSign pbb); // признак инверсии знаков ≤|≥ учитывается
    int b_153az(int byte, int bit);
    int s_153az(int byte, int bit, int sign=0);
    int b_153pz(int byte, int bit);
    int s_153pz(int byte, int bit, int sign=0);

    QString ws16(int pvk,int n);
    QString wst16(int pvk,int n);

    // вх: 1)номер блока, 2) номер канала (т.е. смещение от начала - в 16-битн.словах!!!). Используется для 157-158
    // возвр.слово 16-бит
    unsigned  w(Pvk pvk,int n);

    // возвращает строку из двух значений через слэш с  указ. точностью после зпт
    QString vs(D2 d, int prec);

    // цвет в зависим. от битов имитации
    QString vi(ParType type, int ix , int prec);
    ParType ixToType(int ix);
    QString vj(int ix , int prec=2);

    // как vs, но для уставок (одно значение)
    QString us(double d, int prec);

    // знак уставки
    QString z(int v);
};

// адреса битов и начальный-конечные макс.значения (для расчета по формуле)
static const ByteBit bb[52] = {
    {	436	,	4, 0, 0 }, // 0
    {	436	,	5, 0, 0	}, // 1
    {	436	,	6, 0, 0	}, // 2
    {	436	,	7, 0, 0	}, // 3
    {	437	,	0, 0, 0	}, // 4
    {	437	,	1, 0, 0	}, // 5
    {	437	,	2, 0, 0	}, // 6
    {	437	,	3, 0, 0	}, // 7
    {	437	,	4, 0, 0	}, // 8
    {	437	,	5, 0, 0	}, // 9
    {	437	,	6, 0, 0	}, // 10
    {	437	,	7, 0, 0	}, // 11
    {	438	,	0, 0, 250 }, // 12
    {	438	,	1, 0, 100 }, // 13
    {	438	,	2, 0, 0	}, // 14
    {	438	,	3, 0, 0	}, // 15
    {	438	,	4, 0, 0	}, // 16
    {	438	,	5, 0, 0	}, // 17
    {	438	,	6, 0, 0	}, // 18
    {	438	,	7, 0, 0	}, // 19
    {	439	,	0, 0, 100	}, // 20 Р ТРОП ПГ1
    {	439	,	1, 0, 100	}, // 21 Р ТРОП ПГ2
    {	439	,	2, 0, 100	}, // 22 Р ТРОП ПГ3
    {	439	,	3, 0, 100	}, // 23 Р ТРОП ПГ4
    {	439	,	4, 0, 10	}, // 24 Pзкп тпн1
    {	439	,	5, 0, 10	}, // 25 Pзкп тпн2
    {	439	,	6, -0.5, 1 }, // 26 Pгаз
    {	439	,	7, 0, 6300}, // 27 Lкд
    {	440	,	0, 0, 10	}, // 28
    {	440	,	1, 0, 10	}, // 29
    {	440	,	2, 0, 10	}, // 30
    {	440	,	3, 0, 10	}, // 31
    {	440	,	4, 0, 16368	}, // 32
    {	440	,	5, 0, 16368	}, // 33
    {	440	,	6, 0, 16368	}, // 34
    {	440	,	7, 0, 16368	}, // 35
    {	441	,	0, 0, 4000	}, // 36 Lпг1
    {	441	,	1, 0, 4000	}, // 37 Lпг1
    {	441	,	2, 0, 4000	}, // 38 Lпг1
    {	441	,	3, 0, 4000	}, // 39 Lпг1
    {	441	,	4, 0, 0	}, // 40
    {	441	,	5, 0, 0	}, // 41
    {	441	,	6, 0, 0	}, // 42
    {	441	,	7, 0, 0	}, // 43
    {	442	,	0, 0, 0	}, // 44
    {	442	,	1, 0, 0	}, // 45
    {	442	,	2, 0, 0	}, // 46
    {	442	,	3, 0, 0	}, // 47
    {	442	,	4, 0, 0	}, // 48
    {	442	,	5, 0, 0	}, // 49
    {	442	,	6, 0, 0	}, // 50
    {	442	,	7, 0, 0	}  // 51
};

#endif // FORMULA_H
