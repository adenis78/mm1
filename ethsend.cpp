#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include "eth.h"

#define MY_DEST_MAC0	0x00
#define MY_DEST_MAC1	0x00
#define MY_DEST_MAC2	0x00
#define MY_DEST_MAC3	0x00
#define MY_DEST_MAC4	0x00
#define MY_DEST_MAC5	0x00

#define BUF_SIZ		2000

static int sockfd;
static struct ifreq if_idx;
static struct ifreq if_mac;
static int tx_len = 0;
static char sendbuf[BUF_SIZ];
/* non static!  */ struct ether_header *eh_send = (struct ether_header *) sendbuf;
//static struct iphdr *iph = (struct iphdr *) (sendbuf + sizeof(struct ether_header));
static struct sockaddr_ll socket_address;

int ethPackBuild() { // (int argc, char *argv[])
    extern char ifName[IFNAMSIZ];

    /* Open RAW socket to send on */
    if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
        perror("socket");
    }

    /* Get the index of the interface to send on */
    memset(&if_idx, 0, sizeof(struct ifreq));
    strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
    if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
        perror("SIOCGIFINDEX");
    /* Get the MAC address of the interface to send on */
    memset(&if_mac, 0, sizeof(struct ifreq));
    strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
    if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
        perror("SIOCGIFHWADDR");

    /* Construct the Ethernet header */
    memset(sendbuf, 0, BUF_SIZ);
    /* Ethernet header */
//    eh_send->ether_shost[0] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[0];
//    eh_send->ether_shost[1] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[1];
//    eh_send->ether_shost[2] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[2];
//    eh_send->ether_shost[3] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[3];
//    eh_send->ether_shost[4] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[4];
//    eh_send->ether_shost[5] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[5];

//    eh_send->ether_shost[0] = 0x00;
//    eh_send->ether_shost[1] = 0x12;
//    eh_send->ether_shost[2] = 0x17;
//    eh_send->ether_shost[3] = 0x33;
//    eh_send->ether_shost[4] = 0xC2;
//    eh_send->ether_shost[5] = 0xFC;

    eh_send->ether_shost[0] = 0xFC;
    eh_send->ether_shost[1] = 0xC2;
    eh_send->ether_shost[2] = 0x33;
    eh_send->ether_shost[3] = 0x17;
    eh_send->ether_shost[4] = 0x12;
    eh_send->ether_shost[5] = 0x00;




    eh_send->ether_dhost[0] = MY_DEST_MAC0;
    eh_send->ether_dhost[1] = MY_DEST_MAC1;
    eh_send->ether_dhost[2] = MY_DEST_MAC2;
    eh_send->ether_dhost[3] = MY_DEST_MAC3;
    eh_send->ether_dhost[4] = MY_DEST_MAC4;
    eh_send->ether_dhost[5] = MY_DEST_MAC5;
    /* Ethertype field */
    eh_send->ether_type = htons(ETH_P_IP);
    tx_len += sizeof(struct ether_header);

    /* Packet data */
//    sendbuf[tx_len++] = 0xde;
//    sendbuf[tx_len++] = 0xad;
//    sendbuf[tx_len++] = 0xbe;
//    sendbuf[tx_len++] = 0xef;

    int i=0;
    for(i=0; i<65; i++) {
        sendbuf[tx_len+i] = i;
    }
    tx_len+=i;

    /* Index of the network device */
    socket_address.sll_ifindex = if_idx.ifr_ifindex;
    /* Address length*/
    socket_address.sll_halen = ETH_ALEN;
    /* Destination MAC */
    socket_address.sll_addr[0] = MY_DEST_MAC0;
    socket_address.sll_addr[1] = MY_DEST_MAC1;
    socket_address.sll_addr[2] = MY_DEST_MAC2;
    socket_address.sll_addr[3] = MY_DEST_MAC3;
    socket_address.sll_addr[4] = MY_DEST_MAC4;
    socket_address.sll_addr[5] = MY_DEST_MAC5;


    return 0;
}
int ethPackSend() {
    /* Send packet */
    for(int i = 0; i<3; i++) {
        if (sendto(sockfd, sendbuf, tx_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0)
            printf("Send failed\n");
    }

}
