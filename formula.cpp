#include <QString>
#include "constants.h"
#include "formula.h"

double Formula::get_U (short code3) {
    double res = ((double)code3 - 655) * 31.492 / 2621 ;
    return res;
}

double Formula::get_P (short code3, double sm, double diap ) {
    double res = ((double)code3 - 655) * diap/2621 + sm ;
    return res;
}

double Formula::get_T (short code1) {
    double res = ((double)code1 - 655) * 400 / 2621;
    return res;
}

double Formula::get_dT (short code1) {
    double res = ((double)code1) * 400 / 2621 - 400;
    return res;
}

double Formula::get_F (short code1) {
    double res = ((double)code1 - 655) * 10 / 2621 + 45;
    return res;
}

//double Formula::get_R (short code1) {
//    double res = ((double)code1 - 655) * 39.1 / 2621 + 100;
//    return res;
//}

double Formula::get_R (short code) {
    double T = (code -655)*105./2621. - 5;
    double R = 100 * ( 1 + (T*3.969*0.001) - (T*T*5.841*0.0000001) );
    return R;
}


double Formula::get_I (short code1) {
    double res = ((double)code1 - 655) * 16 / 2621 + 4;
    return res;
}

D2 Formula::T(short index) {
    short v1 = w(p157_1, index);
    short v2 = w(p157_2, index);
    D2 r;
    r.d1 = get_T(v1);
    r.d2 = get_T(v2);
    return r;
}

D2 Formula::dT(short index) {
    short v1 = w(p157_1, index);
    short v2 = w(p157_2, index);
    D2 r;
    r.d1 = get_dT(v1);
    r.d2 = get_dT(v2);
    return r;
}


//    D2 P(short index, int sm, int diap) {
//        short v1 = w(p157_1, index);
//        short v2 = w(p157_2, index);
//        D2 r;
//        r.d1 = get_P(v1, sm, diap);
//        r.d2 = get_P(v2,  sm, diap);
//        return r;
//    }

D2 Formula::P(short index, double sm, double diap) {
    short v1 = w(p157_1, index);
    short v2 = w(p157_2, index);
    D2 r;
    r.d1 = get_P(v1, sm, diap);
    r.d2 = get_P(v2,  sm, diap);
    return r;
}


D2 Formula::U(short index) {
    short v1 = w(p157_1, index);
    short v2 = w(p157_2, index);
    D2 r;
    r.d1 = get_U(v1);
    r.d2 = get_U(v2);
    return r;
}
D2 Formula::I(short index) {
    short v1 = w(p157_1, index);
    short v2 = w(p157_2, index);
    D2 r;
    r.d1 = get_I(v1);
    r.d2 = get_I(v2);
    return r;
}

D2 Formula::R(short index) {
    short v1 = w(p157_1, index);
    short v2 = w(p157_2, index);
    D2 r;
    r.d1 = get_R(v1);
    r.d2 = get_R(v2);
    return r;
}
D2 Formula::F(short index) {
    short v1 = w(p157_1, index);
    short v2 = w(p157_2, index);
    D2 r;
    r.d1 = get_F(v1);
    r.d2 = get_F(v2);
    return r;
}

//возвращает значение бита (1|0) с номером (bit) в байте с номером (byte) из массива
int Formula::b(int pvk, int byte, int bit) {
    if(pvk == blank)
        return 0;
    return (valData[pvk][byte-1]>>bit) & 1;

}


int Formula::b(PvkByteBit pbb) {
    int res;
    if(pbb.pvk == blank)
        res = 0;
    else
        res = (valData[pbb.pvk][pbb.byte-1]>>pbb.bit) & 1;
    return res;
}

int Formula::b(PvkByteBitSign pbb) { // признак инверсии знаков ≤|≥ игнорируется
    int res;
    if(pbb.pvk == blank)
        res = 0;
    else
        res = (valData[pbb.pvk][pbb.byte-1]>>pbb.bit) & 1;
}

int Formula::bs(PvkByteBitSign pbb) { // признак инверсии знаков ≤|≥ учитывается
    int res=0;
    if(pbb.pvk == blank)
        return 0;
    else
        res = (valData[pbb.pvk][pbb.byte-1]>>pbb.bit) & 1;

    if(pbb.sign==0) // если ноль, оставить как есть, иначе наоборот
        return res?1:0;
    else
        return res?0:1;
}



int Formula::b_153az(int byte, int bit) {
    return b(p153az,byte,bit);
}

int Formula::s_153az(int byte, int bit, int sign) {
    int res = sign? b(p153az,byte,bit)?0:1 : b(p153az,byte,bit)?1:0;
    return res;
}

int Formula::b_153pz(int byte, int bit) {
    return b(p153pz,byte,bit);
}
int Formula::s_153pz(int byte, int bit, int sign) {
    int res = sign? b(p153pz,byte,bit)?0:1: b(p153pz,byte,bit)?1:0;
    return res;
}
QString Formula::ws16(int pvk,int n) {
    unsigned short w = *(( unsigned short*)(&valData[pvk][n]));
    return QString::number(w);
}
QString Formula::wst16(int pvk,int n) {
    unsigned short w = *(( unsigned short*)(&valData[pvk][n]));
    double d = ((double)w) / 100.;
    QString s = QString::number(d,'f',1);
    return s;
}


// вх: 1)номер блока, 2) номер канала (т.е. смещение от начала - в 16-битн.словах!!!). Используется для 157-158
// возвр.слово 16-бит
unsigned  Formula::w(Pvk pvk,int n) { // номер блока и номер канала - для 157/158
    unsigned short *w = (unsigned short*)valData[pvk];
    return w[n];
}

// возвращает строку из двух значений через слэш с  указ. точностью после зпт
QString Formula::vs(D2 d, int prec) {
    QString s= QString::number(d.d1,'f',prec);  //000.0
    s+= "/";
    s+= QString::number(d.d2,'f',prec);  //000.0";
    return s;
}

// вх: номер канала
// возвращает значение бита имитации ПКЦ, соотв. каналу
int Formula::im_pkc(int ix) {
    if(ix>=G3)
        ix-=G3; // на всякий случай

    int byte = 71+ix/8;
    int bit = ix % 8;
    byte--;
    int res= (valData[pkc][byte]>>bit) & 1;
    return res;
}

// цвет в зависим. от битов имитации
QString Formula::vi(ParType type, int ix , int prec) {
    D2 d;
    switch (type) {
    case tT:
        d = T(ix);
        break;
    case tP:
        d = P(ix, bb[ix-G3].sm,bb[ix-G3].diap );
        break;
    case tU:
        d = U(ix);
        break;
    case tI:
        d = I(ix);
        break;
    case tR:
        d = R(ix);
        break;
    case tF:
        d = F(ix);
        break;
    }
    if(ix>=G3)
        ix-=G3;

    QString res, s= QString::number(d.d1,'f',prec);  //000.0
    s+= "/";
    s+= QString::number(d.d2,'f',prec);  //000.0";
    int im;
    int byte  = bb[ix].byte;
    int bit =  bb[ix].bit;
    if(type == tU || type==tI || type == tR)
        im = im_pkc(ix);
    else  // само значение параметра
        im = b(p157_1, byte, bit);

    if(im)
        res = QString(" <html><head/><body><p><span style=\" color:#ff0000;\">%1</span></p></body></html>").arg(s);
    else
        res = QString(" <html><head/><body><span style=\"color:#000000;\">%1</span></body></html>").arg(s);
    return res;
}

ParType Formula::ixToType(int ix) {
    if(ix>=0 && ix<=11)
        return tT;
    else if(ix>=12 && ix<=15)
        return tP;
    else if (ix>=16 && ix<=19)
        return tF;
    else if (ix>=20)
        return tP;
}

//// vi без цвета
QString Formula::vj(int ix , int prec) {
    D2 d;
    if(ix > 100) {
        double res = ((double)(w(p153pz,ix-100)) - 655) * 400 / 2621;
        return QString::number(res,'f',prec);
    }

    ParType type = ixToType(ix);
    ix+=G3;
    switch (type) {
    case tT:
        d = T(ix);
        break;
    case tP:
        d = P(ix, bb[ix-G3].sm,bb[ix-G3].diap );
        break;
    case tU:
        d = U(ix);
        break;
    case tI:
        d = I(ix);
        break;
    case tR:
        d = R(ix);
        break;
    case tF:
        d = F(ix);
        break;
    }
    if(ix>=G3)
        ix-=G3;

    return QString::number(d.d1,'f',prec);  //000.0
}

// как vs, но для уставок (одно значение)
QString Formula::us(double d, int prec) {
    QString s= QString::number(d,'f',prec);  //000.0
    return s;
}

// знак уставки
QString Formula::z(int v) {
    if(v==0)
        return "≤";
    else return "≥";


}
