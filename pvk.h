#ifndef PVK
#define PVK

enum Pvk { p153az=0, p153pz=1,
           p151_1=2, p151_2=3,
           p157_1=4, p157_2=5,
           p155az1, p155az2, p155pz1, p155pz2,
           p08az, p08pz,
           pkc,
           blank=255 // отсутствие значения
         }; //  13(=ETH_SRC_NUM) + 1(blank) === 14

struct PvkByteBit { int pvk; int byte; int bit; };
struct PvkByteBitSign { int pvk; int byte; int bit; int sign; };

struct SrabImit { int flag; int imit; };

#endif // PVK

