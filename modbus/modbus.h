/* 
 * File:   Modbus.h
 * Author: diana
 *
 * Created on August 15, 2013, 2:21 PM
 */

#ifndef MODBUS_H
#define	MODBUS_H

#include <cstdio>
#include <cstdlib>
#include <cstring>

extern "C" {
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <pthread.h>
}
#include <string>

class serial_io {
private:
    char device_name[256]; // /dev/ttyS?
    int speed;
    int parity; //
    int bit_l;
    int stop_bit_number;

    struct termios saved_tty_settings; /* old serial port setting (restored on close) */
    struct termios tty_settings; /* new serial port setting */

    int fd;
    static bool verbose;

public:

    char * ttyDeviceName() {
        return device_name;
    };

    int getFd() {
        return fd;
    };
    serial_io();
    virtual ~serial_io();

    int line_open(
            const char *d_name,
            int _speed = 115200,
            int _parity = 0,
            int _bit_l = 8,
            int _stop_bit_number = 1
            );
    int line_close();

    serial_io(
            const char *d_name,
            int _speed = 115200,
            int _parity = 0,
            int _bit_l = 8,
            int _stop_bit_number = 1
            );

    int _read(const void *buff, int l);

    int _write(const void *buff, int l) {
        return write(fd, buff, l);
    };
    int get_byte_from_line(char &, const struct timeval);
}; /* class serial_io */

class modbus {
protected:

    const static int poly = 0xa001;

    char * out_message_buf;
    int out_message_buf_len;

    char * in_message_buf;
    int in_message_buf_len;

    unsigned short state;


    serial_io * sio;

    bool long_silence_in_line_flag;

    struct timeval tv;

    int wait_for_long_silence_in_line();

    int get_byte_from_line(char &c) {
        return sio->get_byte_from_line(c, tv);
    }

    int get_1stbyte_after_silence_in_line(char &);

    int max_wait_attempt; // = 10 000 ;

    int slave_mode_message_read(unsigned char);

public:

    char * ttyDeviceName() {
        return sio->ttyDeviceName();
    };

    char * getOutMessageBuf() {
        return out_message_buf;
    };

    int getOutMessageBufLength() {
        return out_message_buf_len;
    }

    char * getInMessageBuf() {
        return in_message_buf;
    };

    int getInMessageBufLength() {
        return in_message_buf_len;
    }



    static bool verbose;
    virtual ~modbus();

    modbus(serial_io * sio);

    modbus() {
    };


    static unsigned short crc16_rtu(char *, int);
    static const char * add_crc16_rtu(char *, int&);
    const char * add_crc16_rtu(int&);

    unsigned short crc16_rtu(int l) {
        return crc16_rtu(getOutMessageBuf(), l);
    }

    int master(
            int slave,
            int function,
            int adresse,
            int nbre,
            char *to,
            char *from);

    int slave(unsigned char);

    int createOutMessageBuff(int len = 256) {
        // @todo try catch emit
        out_message_buf = new char [len];
        out_message_buf_len = len;
        return len;
    }

    int freeOutMessageBuff() {
        // @todo try catch emit    
        if (out_message_buf != NULL) {
            delete [] out_message_buf;
            out_message_buf = NULL;
            out_message_buf_len = 0;
            return 0;
        } else {
            return 1;
        }

    }

    int createInMessageBuff(int len = 256) {

        in_message_buf = new char [len];
        in_message_buf_len = len;
        return 0;

    }

    int freeInMessageBuff() {
        if (in_message_buf != NULL) {
            delete [] in_message_buf;
            in_message_buf = NULL;
            return 0;
        } else {
            return 1;
        }

    }

    int message2stderr(int len);

    int echo_test(char slave, unsigned short test);

    int _write(int len) {
        return sio->_write(out_message_buf, len);
    };
    int _read(int len, int _timeout = 0);

private:
    static const char echo_test_func = 0x8;

};
#include <stdint.h>

class modbus_aop : public modbus {
public:
    int frame_send(uint16_t addr, short length, uint16_t *from);
    int frame_send(uint16_t addr, short length, uint16_t *from, uint16_t counter);

    modbus_aop(serial_io* sio) : modbus(sio), fake(false) {
        // @todo try catch emit
        //        if (!fake)modbus(sio);
        createOutMessageBuff();
    }

    modbus_aop(bool fake = true) : fake(fake) {

    }
private:

    static const char aop_func = 0x10;

    static const char broadcast_addr = 1;
    bool fake;
};

#endif	/* MODBUS_H */

