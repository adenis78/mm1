//#ifndef TRANSMITTHREAD_H
//#define TRANSMITTHREAD_H

//#include <QThread>
//#include "io-src/modbus.h"
//#include "io-src/dic_aps.h"
//#include "io-src/dic_afsz_ab.h"
//#include "io-src/dic_afsz.h"
////#include "io-src/dicafsz.h"
//#include <QMutex>

//#define di_len DicAfsz::dicrets_length
//#define an_len DicAfsz::analogs_length
//#define an_fr1_len DicAfsz::analogs_frame1_length

//class TransmitThread : public QThread {
//    Q_OBJECT
//public:
//    explicit TransmitThread(volatile bool *ready_flag,
//            QMutex *mutex, bool fake, char* dev, QObject *parent = 0);
//    ~TransmitThread();
//    void run();


//signals:

//private:
//    modbus_aop *aop;
//    uint16_t *di_arr;
//    uint16_t *an_arr;
//    bool enable;
//    QMutex *mutex;
//    volatile bool *ready_flag;
//    Dic *dic;
//    Dic *dic_out;
//    DicAfsz *dicAa;
//    DicAfszAb *dicAb;
////    DicAps *dicAps;
////    int boxNumber;
//    int size;

//};

//#endif // TRANSMITTHREAD_H
