/* 
 * File:   Modbus.cpp
 * Author: diana
 * 
 * Created on August 15, 2013, 2:21 PM
 */
#include <err.h>
#include <errno.h>
#include <iostream>
#include <QString>
#include <stdint.h>
#include <algorithm>
#include "modbus.h"

//#include <stdlib.h>
//typedef unsigned short UInt16;
//typedef unsigned char byte;
//using namespace std;
//
///* A utility function to reverse a string  */
//void reverse(char str[], int length)
//{
//    int start = 0;
//    int end = length -1;
//    while (start < end)
//    {
//        swap(*(str+start), *(str+end));
//        start++;
//        end--;
//    }
//}

//// Implementation of itoa()
//char* itoa(int num, char* str, int base)
//{
//    int i = 0;
//    bool isNegative = false;

//    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
//    if (num == 0)
//    {
//        str[i++] = '0';
//        str[i] = '\0';
//        return str;
//    }

//    // In standard itoa(), negative numbers are handled only with
//    // base 10. Otherwise numbers are considered unsigned.
//    if (num < 0 && base == 10)
//    {
//        isNegative = true;
//        num = -num;
//    }

//    // Process individual digits
//    while (num != 0)
//    {
//        int rem = num % base;
//        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
//        num = num/base;
//    }

//    // If number is negative, append '-'
//    if (isNegative)
//        str[i++] = '-';

//    str[i] = '\0'; // Append string terminator

//    // Reverse the string
//    reverse(str, i);

//    return str;
//}

//// Compute the MODBUS RTU CRC
//UInt16 ModRTU_CRC(byte* buf, int len) {
//  UInt16 crc = 0xFFFF;

//  for (int pos = 0; pos < len; pos++) {
//    crc ^= (UInt16)buf[pos];          // XOR byte into least sig. byte of crc

//    for (int i = 8; i != 0; i--) {    // Loop over each bit
//      if ((crc & 0x0001) != 0) {      // If the LSB is set
//        crc >>= 1;                    // Shift right and XOR 0xA001
//        crc ^= 0xA001;
//      }
//      else                            // Else LSB is not set
//        crc >>= 1;                    // Just shift right
//    }
//  }
//  // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
//  return crc;
//}


int serial_io::line_open
(
        const char *d_name,
        int _speed,
        int _parity,
        int _bit_l,
        int _stop_bit_number
        ) {
    strncpy(device_name, d_name, sizeof (device_name) - 1);

    speed = _speed;
    parity = _parity;
    bit_l = _bit_l;
    stop_bit_number = _stop_bit_number;

    fd = open(device_name, O_RDWR | O_NOCTTY | O_NONBLOCK | O_NDELAY);
    if (fd < 0) {
        // @todo try catch emit
        err(errno, "\nOpen device failure - %s", device_name);
        exit(-1);
    }
    /* save old port settings  */
    if (tcgetattr(fd, &saved_tty_settings) < 0) {
        // @todo try catch emit
        perror("Can't get terminal parameters ");
        exit(-1);
    }
    /* settings port */
    bzero(&tty_settings, sizeof (tty_settings));

    switch (speed) {
        case 0:
            tty_settings.c_cflag = B0;
            break;
        case 50:
            tty_settings.c_cflag = B50;
            break;
        case 75:
            tty_settings.c_cflag = B75;
            break;
        case 110:
            tty_settings.c_cflag = B110;
            break;
        case 134:
            tty_settings.c_cflag = B134;
            break;
        case 150:
            tty_settings.c_cflag = B150;
            break;
        case 200:
            tty_settings.c_cflag = B200;
            break;
        case 300:
            tty_settings.c_cflag = B300;
            break;
        case 600:
            tty_settings.c_cflag = B600;
            break;
        case 1200:
            tty_settings.c_cflag = B1200;
            break;
        case 1800:
            tty_settings.c_cflag = B1800;
            break;
        case 2400:
            tty_settings.c_cflag = B2400;
            break;
        case 4800:
            tty_settings.c_cflag = B4800;
            break;
        case 9600:
            tty_settings.c_cflag = B9600;
            break;
        case 19200:
            tty_settings.c_cflag = B19200;
            break;
        case 38400:
            tty_settings.c_cflag = B38400;
            break;
        case 57600:
            tty_settings.c_cflag = B57600;
            break;
        case 115200:
            tty_settings.c_cflag = B115200;
            break;
        case 230400:
            tty_settings.c_cflag = B230400;
            break;
        default:
            tty_settings.c_cflag = B9600;
            break;
    }

    switch (bit_l) {
        case 7:
            tty_settings.c_cflag = tty_settings.c_cflag | CS7;
            break;
        case 8:
        default:
            tty_settings.c_cflag = tty_settings.c_cflag | CS8;
            break;
    }
    switch (parity) {
        case 1:
            tty_settings.c_cflag = tty_settings.c_cflag | PARENB;
            //        tty_settings.c_iflag = ICRNL;
            break;
        case -1:
            tty_settings.c_cflag = tty_settings.c_cflag | PARENB | PARODD;
            //        tty_settings.c_iflag = ICRNL;
            break;
        case 0:
        default:
            //        tty_settings.c_iflag = IGNPAR | ICRNL;
            tty_settings.c_iflag = IGNPAR;
            //        tty_settings.c_iflag &= ~ICRNL;
            break;
    }
    if (stop_bit_number == 2)
        tty_settings.c_cflag |= CSTOPB;
    tty_settings.c_iflag &= ~ICRNL;

    tty_settings.c_cflag |= CLOCAL | CREAD;
    tty_settings.c_oflag = 0;

    tty_settings.c_lflag = 0; /*ICANON;*/
    tty_settings.c_cc[VMIN] = 1;
    tty_settings.c_cc[VTIME] = 0;

    if (tcsetattr(fd, TCSANOW, &tty_settings) < 0) {
        perror("Can't set terminal parameters ");
        exit(-1);
    }
    /* clean I & O device */
    tcflush(fd, TCIOFLUSH);

    if (verbose) {
        std::cout
                << "setting ok: \n"
                << "device    " << device_name << "\n"
                << "speed      " << speed << "\n"
                << "data bits  " << bit_l << "\n"
                << "stop bits  " << stop_bit_number << "\n"
                << "parity     " << parity << "\n"
                ;

    }
    return fd;

}

serial_io::serial_io() {
    fd = 0; // stdin 
    strcpy(device_name, "stdin as tty");
    /* save old port settings  */
    if (tcgetattr(fd, &saved_tty_settings) < 0) {
        perror("Can't get terminal parameters ");
        exit(-1);
    }
    /* settings port */

    tty_settings.c_iflag &= ~ICRNL;

    tty_settings.c_cflag |= CLOCAL | CREAD;
    tty_settings.c_oflag = 0;

    tty_settings.c_lflag = 0; /*ICANON;*/
    tty_settings.c_cc[VMIN] = 1;
    tty_settings.c_cc[VTIME] = 0;

    if (tcsetattr(fd, TCSANOW, &tty_settings) < 0) {
        perror("Can't set terminal parameters ");
        exit(-1);
    }

}

serial_io::serial_io(
        const char *d_name,
        int _speed,
        int _parity,
        int _bit_l,
        int _stop_bit_number
        ) {

    line_open(d_name, _speed, _parity, _bit_l, _stop_bit_number);
}

serial_io::~serial_io() {
    if (fd >= 0)
        line_close();
}

int serial_io::line_close() {
    if (fd >= 0) {
        tcsetattr(fd, TCSANOW, &saved_tty_settings);
        close(fd);
        return 0;
    } else {
        return 1;
    }
}

modbus::modbus(serial_io* _sio) :  max_wait_attempt(10000) {
    sio = _sio;
    long_silence_in_line_flag = false;

    tv.tv_sec = 0;
    tv.tv_usec = 1880;
}

modbus::~modbus() {
    freeOutMessageBuff();

}

/*static*/
unsigned short modbus::crc16_rtu(char *p, int len) {
    unsigned short int crc = 0xffff;

    while (len--) {
        crc ^= (*p++ & 0xff);
        for (int i = 0; i < 8; i++) {
            if (crc & 0x01)
                crc = (crc >> 1)^poly;
            else
                crc >>= 1;
        }
    }
    return crc;
}

/*static*/
const char * modbus::add_crc16_rtu(char *m, int& len) {
    unsigned short int crc = crc16_rtu(m, len);
    m[len++] = crc & 0xff;
    m[len++] = crc >> 8;
    return m;

}

const char * modbus::add_crc16_rtu(int& len) {
    char *m = getOutMessageBuf();
    unsigned short int crc = crc16_rtu(m, len);
   // unsigned short int crc1 = ModRTU_CRC((byte*)m,len);
    m[len++] = crc & 0xff;
    m[len++] = crc >> 8;
    return m;

}

/***
 * 
 * @param addr - внутренний адрес в АОП
 * @param length количество слов на отправку
 * @param from - указатель на массив данных
 */
int modbus_aop::frame_send(uint16_t addr, short length, uint16_t *from) {
    //to fake call
    if (fake) return length;
    if (length < (256 - 8) / 2) {

        union {

            struct {
                char lb;
                char hb;
            };
            uint16_t word;
        } w;


        int l = 0;
        char *c = out_message_buf;
        *c++ = broadcast_addr; // адрес устройства, куда посылаем
        l++;
        *c++ = aop_func; // вторая половина адреса 
        l++;
        w.word = addr; // адрес буфера 
        *c++ = w.hb;
        l++;
        *c++ = w.lb;
        l++;

        *c++ = 0;
        l++; // количество передаваемых слов данных в словах
        *c++ = (uint8_t) length;
        l++; // количество передаваемых слов данных в словах
        *c++ = (uint8_t) length * 2;
        l++; // колво данных в байтах 

        for (uint16_t* f = from, i = 0; i < length; f++, i++) {
            w.word = *f;
            //  нужно переставлять байты
            *c++ = w.hb; // меняем байты
            l++;
            *c++ = w.lb; // меняем байты
            l++;
        }
        add_crc16_rtu(l);
//        char *q = getOutMessageBuf() + l++;
//        *q = 'A';
        int r = _write(l);
        return r;
    }
    return 0;
}
bool serial_io::verbose = true;

int modbus::message2stderr(int len) {
    char *p = getOutMessageBuf();
    while (len--) {
        fprintf(stderr, "%02hhx ", *p++);
    }
    fprintf(stderr, "\n");
    return len;
}

/***
 * 
 * @param addr - внутренний адрес в АОП
 * @param length количество слов на отправку
 * @param from - указатель на массив данных
 * @param counter - счетчик для АОПа, прибавлением которого занимается вызывающий класс
 */
int modbus_aop::frame_send(uint16_t addr, short length, uint16_t *from, uint16_t counter) {
    //to fake call
    if (fake) return length;
    // инфа влезает в пакет со служебной инфой 
    if ((length + 1)< (256 - 8) / 2) {

        union {

            struct {
                char lb;
                char hb;
            };
            uint16_t word;
        } w;


        int l = 0;
        char *c = out_message_buf;
        *c++ = broadcast_addr; // адрес устройства, куда посылаем
        l++;
        *c++ = aop_func; // вторая половина адреса 
        l++;
        w.word = addr; // адрес буфера 
        *c++ = w.hb;
        l++;
        *c++ = w.lb;
        l++;

        *c++ = 0;
        l++; // количество передаваемых слов данных в словах
        *c++ = (uint8_t) (length + 1);
        l++; // количество передаваемых слов данных в словах
        *c++ = (uint8_t) (length + 1) * 2;
        l++; // колво данных в байтах 

        for (uint16_t* f = from, i = 0; i < length; f++, i++) {
            w.word = *f;
            //  нужно переставлять байты
            *c++ = w.hb; // меняем байты
            l++;
            *c++ = w.lb; // меняем байты
            l++;
        }
        ///из-за чего сыр-бор разгорелся:
        w.word = counter;
        *c++ = w.hb;
        l++;
        *c++ = w.lb; // меняем байты
        l++;
        // добавили счетчик в конец пакета
        add_crc16_rtu(l);

//        char *q = getOutMessageBuf() + l++;
//        *q = 'A';

//        unsigned char ch=0;
//        char s[16];
//        for(int i=0;i<20;i++) {
//            ch = (unsigned char)out_message_buf[i];
//            itoa((unsigned)ch,s,16);
//            std::cerr<<s<<",";
//        }
//        std::cerr<<std::endl;
        int r = _write(l);
        return r;
    }
    return 0;
}
