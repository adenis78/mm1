//#include "transmitthread.h"
//#include "mainwindow.h"
////#include "io-src/dic_aps.h"
////#include "io-src/dic_afsz.h"
////#include "io-src/dic_afsz_ab.h"
//#include "LinkLevel.h"
//#include <cstring>

//TransmitThread::TransmitThread(volatile bool *ready_flag,
//        QMutex *mutex,
//        bool fake,
//        char* dev,
//        QObject *parent) :
//QThread(parent),
//enable(true),
//mutex(mutex),
//ready_flag(ready_flag), dicAa(NULL), dicAb(NULL) {
//    // @todo добавить проверку что это parent - MainWindow

//    if (fake) {
//        if (dev)
//            aop = new modbus_aop(new serial_io(dev));
//        else aop = new modbus_aop();
//    } else
//        aop = new modbus_aop(new serial_io("/dev/ttyS2"));
//    dic = ((MainWindow*) parent)->getDic();
//    size = dic->getDataLength() * sizeof (uint16_t);

//    if (boxNumber <= 3 || (boxNumber >= 7 && boxNumber <= 9)) {
//        dicAa = new DicAfsz(0x100, fake);
//        dic_out = dicAa;
//    }
//    if ((boxNumber >= 4 && boxNumber <= 6) || (boxNumber >= 10 && boxNumber <= 12)) {
//        dicAb = new DicAfszAb(0x100, fake);
//        dic_out = dicAb;
//    }
//    if (boxNumber >= 13) {

//        dic_out = new DicAps(0x100, fake);
//    }
//}

//void TransmitThread::run() {
//    uint16_t counter = 0;
//    while (enable) {
//        //        printf("\ntransmit run");
//        if (*ready_flag) {
//            mutex->lock();
//            memcpy(dic_out->getArray(), dic->getArray(), size);
//            mutex->unlock();

//            if (boxNumber >= 13) {
//                // из АСП номер стойки не передается:
//                aop->frame_send(0, dic_out->getDataLength() - 1, dic_out->getArray() + 1, counter);
//                //                printf("\n aps frame has been send");
//                msleep(110);
//            } else {
//                if (boxNumber <= 3 || (boxNumber >= 7 && boxNumber <= 9)) {

//                    aop->frame_send(0, dicAa->discrets_length, dicAa->getDiscrets(), counter);
//                    msleep(25);
//                    aop->frame_send(100, dicAa->analogs_length, dicAa->getAnalogs(), counter);
//                    msleep(50);
//                }
//                if ((boxNumber >= 4 && boxNumber <= 6) || (boxNumber >= 10 && boxNumber <= 12)) {

//                    aop->frame_send(0, dicAb->discrets_length, dicAb->getDiscrets(), counter);
//                    msleep(25);
//                    aop->frame_send(100, dicAb->analogs_length, dicAb->getAnalogs(), counter);
//                    msleep(50);
//                }
//            }
//            counter++; // @todo инкремент при значении 0xffff даст 0. ничего страшного.
//            //            *ready_flag = false;
//        } else {
//            msleep(20);
//        }
//    }
//}

//TransmitThread::~TransmitThread() {
//    while (isRunning()) {
//        enable = false;
//        wait();
//    }
//}
