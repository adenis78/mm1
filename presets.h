#ifndef PRESETS_H
#define PRESETS_H


#if 1// 1==для загрузки в комплект

#define SHOW_MAIN 1
#define ETH_ON 1
#define TEST_AOP 0
#define RS422_READY 1
#define STOP_ON_TTY_ERRORS 0
#define IMIT 1
#define TEST_F11_KEY_ON 0

#else // для отладки

#define SHOW_MAIN 1
#define ETH_ON 0
#define TEST_AOP 1
#define RS422_READY 1
#define STOP_ON_TTY_ERRORS 0
#define IMIT 1 // включение отображения признаков имитации на мажоритарных эл-тах >=2 (1-2-3)
#define TEST_F11_KEY_ON 1
#endif




#endif // PRESETS_H
