/* функции для отрисовки слайдов алгоритмов */
#include <QDebug>
#include "form3.h"
#include "ui_form3.h"
#include <QRect>
#include <QPainter>
#include "formula.h"
#include "./algSch/f1.h"
#include "ui_f1.h"
#include "./algSch/f2.h"
#include "ui_f2.h"
#include "./algSch/f3.h"
#include "ui_f3.h"
#include "./algSch/f4.h"
#include "ui_f4.h"
#include "./algSch/f5.h"
#include "ui_f5.h"
#include "./algSch/f6.h"
#include "ui_f6.h"
#include "./algSch/f7.h"
#include "ui_f7.h"
#include "./algSch/f8.h"
#include "ui_f8.h"
#include "./algSch/f9.h"
#include "ui_f9.h"
#include "./algSch/f10.h"
#include "ui_f10.h"
#include "usts.h"
#include "parType.h"
#include "srab_ispr_alg_153.h"
#include "constants.h"
#include "im_blk_pkc.h"
#include "formulas.h"
#include "p151_152.h"
#include "p153az.h"
#include "p153pz.h"
#include "p155_156.h"
#include "p157_158.h"
#include "pkc.h"
#include "common_ops.h"

extern unsigned char valData[ETH_SRC_NUM][1600];
extern unsigned char oneOrTwo[ETH_SRC_NUM];

template <typename TYPE> void Form3::paintShak(TYPE *p) {
            lbx(p->shak11_srabZasch153, srAz(sa_ObobAz) );
            lbx(p->shak11_res155, bit(p155az1, p155_1az_Shak1_1Az + p155_res) );
            lby(p->shak11_imblk155, bit(p155az1, p155_1az_Shak1_1Az + p155_imit) || bit(p155az1, p155_1az_Shak1_1Az + p155_blk) );
            grnGr(p->shak11_ispr155, bit(p155az1, p155_1az_Shak1_1Az + p155_ispr) );

            lbx(p->shak12_srabZasch153, srAz(sa_ObobAz) );
            lbx(p->shak12_res155, bit(p155az2, p155_2az_Shak1_2Az + p155_res) );
            lby(p->shak12_imblk155, bit(p155az2, p155_2az_Shak1_2Az + p155_imit) || bit(p155az2, p155_2az_Shak1_2Az + p155_blk) );
            grnGr(p->shak12_ispr155, bit(p155az2, p155_2az_Shak1_2Az + p155_ispr) );

            lbx(p->shak21_srabZasch153, srAz(sa_ObobAz) );
            lbx(p->shak21_res155, bit(p155az1, p155_1az_Shak2_1Az + p155_res) );
            lby(p->shak21_imblk155, bit(p155az1, p155_1az_Shak2_1Az + p155_imit) || bit(p155az1, p155_1az_Shak2_1Az + p155_blk) );
            grnGr(p->shak21_ispr155, bit(p155az1, p155_1az_Shak2_1Az + p155_ispr) );

            lbx(p->shak22_srabZasch153, srAz(sa_ObobAz) );
            lbx(p->shak22_res155, bit(p155az2, p155_2az_Shak2_2Az + p155_res) );
            lby(p->shak22_imblk155, bit(p155az2, p155_2az_Shak2_2Az + p155_imit) || bit(p155az2, p155_2az_Shak2_2Az + p155_blk) );
            grnGr(p->shak22_ispr155, bit(p155az2, p155_2az_Shak2_2Az + p155_ispr) );

            if(oneOrTwo[pkc]==0) { // для 1 комплекта
                p->shak11_res155->setText("ШАК 1.1");
                p->shak12_res155->setText("ШАК 1.2");
                p->shak21_res155->setText("ШАК 2.1");
                p->shak22_res155->setText("ШАК 2.2");
            }
            else { // для 2 комплекта
                p->shak11_res155->setText("ШАК 3.1");
                p->shak12_res155->setText("ШАК 3.2");
                p->shak21_res155->setText("ШАК 4.1");
                p->shak22_res155->setText("ШАК 4.2");
            }



}

void Form3::paintAlgs() {
    int n =  algs->currentIndex();
    // жирная вертикальная черта
    //QString curPgStr =  QString(algs->currentWidget()->metaObject()->className());
    switch(n+1) {
        case 1: {
            Ui_f1* p = ((f1*)(fa[n]))->ui;
            // дискр входы
            // наличие сигнала на входе
            lbx( p->ip_i151_T_less10Az1, bit(p151_1, p151_in + p151_Aknp_T_less10Az1) );
            lbx( p->ip_i151_N_lessNzadAz1, bit(p151_1, p151_in + p151_Aknp_N_moreNustAz1) );
            // неисправность
            lb_ispLs( p->ip_ispr_T_less10Az1, bit(p151_1, p151_neisp + p151_Aknp_T_less10Az1), bit(p151_1, p151_sost + p151_Aknp_T_less10Az1) );
            lb_ispLs( p->ip_ispr_N_lessNzadAz1, bit(p151_1, p151_neisp + p151_Aknp_N_moreNustAz1), bit(p151_1, p151_sost + p151_Aknp_T_less10Az1) );
            // результирующий
            lbx( p->ip_res_T_less10Az1, bit(p151_1, p151_res + p151_Aknp_T_less10Az1) );
            lbx( p->ip_res_N_lessNzadAz1, bit(p151_1, p151_res + p151_Aknp_N_moreNustAz1) );
            // имитация и блокировка
            lby( p->ip_imblk_T_less10Az1, bit(p151_1, p151_imit + p151_Aknp_T_less10Az1) | bit(p151_1, p151_blk + p151_Aknp_T_less10Az1));
            lby( p->ip_imblk_N_lessNzadAz1, bit(p151_1, p151_imit + p151_Aknp_N_moreNustAz1) | bit(p151_1, p151_blk + p151_Aknp_N_moreNustAz1) );
            // входной в 153/154
            lbx( p->ip_i153_T_less10Az1, srAz(sa_T_less10az1_A) );
            lbx( p->ip_i153_N_lessNzadAz1 , srAz(sa_N_moreNaz1_A) );

            // аналогов.входы:
            // выход за диап
            p->out_Ppg1->setText(s2D1(ppn_Ppg1));
            p->out_Ppg2->setText(s2D1(ppn_Ppg2));
            p->out_Ppg3->setText(s2D1(ppn_Ppg3));
            p->out_Ppg4->setText(s2D1(ppn_Ppg4));
            p->out_dPpg1dt->setText(s2D1(ppn_dPpg1Dt));
            p->out_dPpg2dt->setText(s2D1(ppn_dPpg2Dt));
            p->out_dPpg3dt->setText(s2D1(ppn_dPpg3Dt));
            p->out_dPpg4dt->setText(s2D1(ppn_dPpg4Dt));
            // значения
            p->val_Ppg1->setText( getW153(tap_Ppg1, bit(p157_1, ppn_Ppg1) | bit(pkc, pkcAi_Ppg1)) );
            p->val_Ppg2->setText( getW153(tap_Ppg2, bit(p157_1, ppn_Ppg2) | bit(pkc, pkcAi_Ppg2))  );
            p->val_Ppg3->setText( getW153(tap_Ppg3, bit(p157_1, ppn_Ppg3) | bit(pkc, pkcAi_Ppg3)) );
            p->val_Ppg4->setText( getW153(tap_Ppg4, bit(p157_1, ppn_Ppg4) | bit(pkc, pkcAi_Ppg4))  );
            p->val_dPpg1dt->setText( getW153(tap_dPpg1Dt, bit(p157_1, ppn_dPpg1Dt) | bit(pkc, pkcAi_dPpg1dt))  );
            p->val_dPpg2dt->setText( getW153(tap_dPpg2Dt, bit(p157_1, ppn_dPpg2Dt) | bit(pkc, pkcAi_dPpg2dt))  );
            p->val_dPpg3dt->setText( getW153(tap_dPpg3Dt, bit(p157_1, ppn_dPpg3Dt) | bit(pkc, pkcAi_dPpg3dt))  );
            p->val_dPpg4dt->setText( getW153(tap_dPpg4Dt, bit(p157_1, ppn_dPpg4Dt) | bit(pkc, pkcAi_dPpg4dt))  );
            // уставки
            lbUst(p->u_Ppg1_55, srAz(sa_Ppg1_less55_00_A), getW153(tau_Ppg1_less55_00), less);
            lbUst(p->u_Ppg2_55, srAz(sa_Ppg2_less55_00_A), getW153(tau_Ppg2_less55_00), less);
            lbUst(p->u_Ppg3_55, srAz(sa_Ppg3_less55_00_A), getW153(tau_Ppg3_less55_00), less);
            lbUst(p->u_Ppg4_55, srAz(sa_Ppg4_less55_00_A), getW153(tau_Ppg4_less55_00), less);
            lbUst(p->u_Ppg1_80, srAz(sa_Ppg1_more80_00_A), getW153(tau_Ppg1_more80_00));
            lbUst(p->u_Ppg2_80, srAz(sa_Ppg2_more80_00_A), getW153(tau_Ppg2_more80_00));
            lbUst(p->u_Ppg3_80, srAz(sa_Ppg3_more80_00_A), getW153(tau_Ppg3_more80_00));
            lbUst(p->u_Ppg4_80, srAz(sa_Ppg4_more80_00_A), getW153(tau_Ppg4_more80_00));
            lbUst(p->u_dPpg1dt, srAz(sa_dPpg1Dt_more_0_500_A), getW153(tau_dPpg1Dt_more0_500));
            lbUst(p->u_dPpg2dt, srAz(sa_dPpg2Dt_more_0_500_A), getW153(tau_dPpg2Dt_more0_500));
            lbUst(p->u_dPpg3dt, srAz(sa_dPpg3Dt_more_0_500_A), getW153(tau_dPpg3Dt_more0_500));
            lbUst(p->u_dPpg4dt, srAz(sa_dPpg4Dt_more_0_500_A), getW153(tau_dPpg4Dt_more0_500));
            // входы >=2
            lbm(p->m_dPpg1dt_1, srM(sa_dPpg1Dt_more_0_500_A, pkcImAz_dPtropPg1Dt_more0_500) );
            lbm(p->m_dPpg1dt_2, srM(sa_dPpg1Dt_more_0_500_B, pkcImAz_dPtropPg1Dt_more0_500) );
            lbm(p->m_dPpg1dt_3, srM(sa_dPpg1Dt_more_0_500_C, pkcImAz_dPtropPg1Dt_more0_500) );
            lbm(p->m_dPpg1dt_m, srM(sa_dPpg1Dt_more_0_500_M));
            lbm(p->m_dPpg2dt_1, srM(sa_dPpg2Dt_more_0_500_A, pkcImAz_dPtropPg2Dt_more0_500 ));
            lbm(p->m_dPpg2dt_2, srM(sa_dPpg2Dt_more_0_500_B, pkcImAz_dPtropPg2Dt_more0_500 ));
            lbm(p->m_dPpg2dt_3, srM(sa_dPpg2Dt_more_0_500_C, pkcImAz_dPtropPg2Dt_more0_500 ));
            lbm(p->m_dPpg2dt_m, srM(sa_dPpg2Dt_more_0_500_M));
            lbm(p->m_dPpg3dt_1, srM(sa_dPpg3Dt_more_0_500_A, pkcImAz_dPtropPg3Dt_more0_500 ));
            lbm(p->m_dPpg3dt_2, srM(sa_dPpg3Dt_more_0_500_B, pkcImAz_dPtropPg3Dt_more0_500 ));
            lbm(p->m_dPpg3dt_3, srM(sa_dPpg3Dt_more_0_500_C, pkcImAz_dPtropPg3Dt_more0_500 ));
            lbm(p->m_dPpg3dt_m, srM(sa_dPpg3Dt_more_0_500_M));
            lbm(p->m_dPpg4dt_1, srM(sa_dPpg4Dt_more_0_500_A, pkcImAz_dPtropPg4Dt_more0_500 ));
            lbm(p->m_dPpg4dt_2, srM(sa_dPpg4Dt_more_0_500_B, pkcImAz_dPtropPg4Dt_more0_500 ));
            lbm(p->m_dPpg4dt_3, srM(sa_dPpg4Dt_more_0_500_C, pkcImAz_dPtropPg4Dt_more0_500 ));
            lbm(p->m_dPpg4dt_m, srM(sa_dPpg4Dt_more_0_500_M ));
            lbm(p->m_Ppg1_55_1, srM(sa_Ppg1_less55_00_A, pkcImAz_PtropPg1_less55_00));
            lbm(p->m_Ppg1_55_2, srM(sa_Ppg1_less55_00_B, pkcImAz_PtropPg1_less55_00));
            lbm(p->m_Ppg1_55_3, srM(sa_Ppg1_less55_00_C, pkcImAz_PtropPg1_less55_00));
            lbm(p->m_Ppg1_55_m, srM(sa_Ppg1_less55_00_M));
            lbm(p->m_Ppg2_55_1, srM(sa_Ppg2_less55_00_A, pkcImAz_PtropPg2_less55_00));
            lbm(p->m_Ppg2_55_2, srM(sa_Ppg2_less55_00_B, pkcImAz_PtropPg2_less55_00));
            lbm(p->m_Ppg2_55_3, srM(sa_Ppg2_less55_00_C, pkcImAz_PtropPg2_less55_00));
            lbm(p->m_Ppg2_55_m, srM(sa_Ppg2_less55_00_M));
            lbm(p->m_Ppg3_55_1, srM(sa_Ppg3_less55_00_A, pkcImAz_PtropPg3_less55_00));
            lbm(p->m_Ppg3_55_2, srM(sa_Ppg3_less55_00_B, pkcImAz_PtropPg3_less55_00));
            lbm(p->m_Ppg3_55_3, srM(sa_Ppg3_less55_00_C, pkcImAz_PtropPg3_less55_00));
            lbm(p->m_Ppg3_55_m, srM(sa_Ppg3_less55_00_M));
            lbm(p->m_Ppg4_55_1, srM(sa_Ppg4_less55_00_A, pkcImAz_PtropPg4_less55_00));
            lbm(p->m_Ppg4_55_2, srM(sa_Ppg4_less55_00_B, pkcImAz_PtropPg4_less55_00));
            lbm(p->m_Ppg4_55_3, srM(sa_Ppg4_less55_00_C, pkcImAz_PtropPg4_less55_00));
            lbm(p->m_Ppg4_55_m, srM(sa_Ppg4_less55_00_M));
            lbm(p->m_Ppg1_80_1, srM(sa_Ppg1_more80_00_A, pkcImAz_Ppg1_more80_00));
            lbm(p->m_Ppg1_80_2, srM(sa_Ppg1_more80_00_B, pkcImAz_Ppg1_more80_00));
            lbm(p->m_Ppg1_80_3, srM(sa_Ppg1_more80_00_C, pkcImAz_Ppg1_more80_00));
            lbm(p->m_Ppg1_80_m, srM(sa_Ppg1_more80_00_M));
            lbm(p->m_Ppg2_80_1, srM(sa_Ppg2_more80_00_A, pkcImAz_Ppg2_more80_00));
            lbm(p->m_Ppg2_80_2, srM(sa_Ppg2_more80_00_B, pkcImAz_Ppg2_more80_00));
            lbm(p->m_Ppg2_80_3, srM(sa_Ppg2_more80_00_C, pkcImAz_Ppg2_more80_00));
            lbm(p->m_Ppg2_80_m, srM(sa_Ppg2_more80_00_M));
            lbm(p->m_Ppg3_80_1, srM(sa_Ppg3_more80_00_A, pkcImAz_Ppg3_more80_00));
            lbm(p->m_Ppg3_80_2, srM(sa_Ppg3_more80_00_B, pkcImAz_Ppg3_more80_00));
            lbm(p->m_Ppg3_80_3, srM(sa_Ppg3_more80_00_C, pkcImAz_Ppg3_more80_00));
            lbm(p->m_Ppg3_80_m, srM(sa_Ppg3_more80_00_M));
            lbm(p->m_Ppg4_80_1, srM(sa_Ppg4_more80_00_A, pkcImAz_Ppg4_more80_00));
            lbm(p->m_Ppg4_80_2, srM(sa_Ppg4_more80_00_B, pkcImAz_Ppg4_more80_00));
            lbm(p->m_Ppg4_80_3, srM(sa_Ppg4_more80_00_C, pkcImAz_Ppg4_more80_00));
            lbm(p->m_Ppg4_80_m, srM(sa_Ppg4_more80_00_M));
            lbm(p->m_T10_1, srM(sa_T_less10az1_A, pkcImAz_Aknp_T_less10_Az1));
            lbm(p->m_T10_2, srM(sa_T_less10az1_B, pkcImAz_Aknp_T_less10_Az1));
            lbm(p->m_T10_3, srM(sa_T_less10az1_C, pkcImAz_Aknp_T_less10_Az1));
            lbm(p->m_T10_m, srM(sa_T_less10az1_M));
            lbm(p->m_NmoreN_1, srM(sa_N_moreNaz1_A, pkcImAz_Aknp_N_moreN_Az1));
            lbm(p->m_NmoreN_2, srM(sa_N_moreNaz1_B, pkcImAz_Aknp_N_moreN_Az1));
            lbm(p->m_NmoreN_3, srM(sa_N_moreNaz1_C, pkcImAz_Aknp_N_moreN_Az1));
            lbm(p->m_NmoreN_m, srM(sa_N_moreNaz1_M));


            // подсветка логич.эл-тов
            lbx(p->Ppg_and1, srAzPz(sa_p1_a1_25) );
            lbx(p->Ppg_and2, srAzPz(sa_p2_a1_25) );
            lbx(p->Ppg_and3, srAzPz(sa_p3_a1_25) );
            lbx(p->Ppg_and4, srAzPz(sa_p4_a1_25) );
            lbx(p->Ppg_or1, srAzPz(sa_algAz1_25) );
            lbx(p->Ppg_or2, srAzPz(sa_algAz1_28) );
            // выходы
            lbx(p->opAz1_25_srab153, srAz(sa_algAz1_25)); // сраб алгоритма в 153
            grnGr(p->opAz1_25_isprPkc, bitD(pkcNeiprStart,pkc_na_az25) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_28_srab153, srAz(sa_algAz1_28)); // сраб алгоритма в 153
            grnGr(p->opAz1_28_isprPkc, bitD(pkcNeiprStart,pkc_na_az28) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_1_srab153, srAz(sa_algAz1_1)); // сраб алгоритма в 153
            grnGr(p->opAz1_1_isprPkc, bitD(pkcNeiprStart, pkc_na_az1) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_2_srab153, srAz(sa_algAz1_2)); // сраб алгоритма в 153
            grnGr(p->opAz1_2_isprPkc, bitD(pkcNeiprStart,pkc_na_az2) ); // испр при авт контроле в ПКЦ

            // ШАК
            paintShak(p);

            break;
        }
        case 2: {
            Ui_f2* p = ((f2*)(fa[n]))->ui;
            // дискр входы
            lbx( p->ip_i151_N_more75, bit(p151_1, p151_in + p151_Aknp_N_more75) ); // наличие сигнала на входе
            lb_ispLs(p->ip_ispr_N_more75, bit(p151_1, p151_neisp + p151_Aknp_N_more75), bit(p151_1, p151_sost + p151_Aknp_N_more75) ); // неисправность
            lbx( p->ip_res_N_more75, bit(p151_1, p151_res + p151_Aknp_N_more75) ); // результирующий
            lby( p->ip_imblk_N_more75, bit(p151_1, p151_imit + p151_Aknp_N_more75) | bit(p151_1, p151_blk + p151_Aknp_N_more75)); // имитация и блокировка
            lbx( p->ip_i153_N_more75, srAz(sa_N_more75_A) ); // входной в 153/154

            // выход за диап
            p->out_dPreakDt->setText(s2D1(ppn_dPreakDt));
            p->out_Preak->setText(s2D1(ppn_Preak));
            p->out_Tgn1->setText(s2D2(ppn_tp_gn1, ppn_tsp_gn1));
            p->out_Tgn2->setText(s2D2(ppn_tp_gn2, ppn_tsp_gn2));
            p->out_Tgn3->setText(s2D2(ppn_tp_gn3, ppn_tsp_gn3));
            p->out_Tgn4->setText(s2D2(ppn_tp_gn4, ppn_tsp_gn4));
            // значения
            p->val_dPreakDt->setText( getW153(tap_dPreakDt, bit(p157_1, ppn_dPreakDt) | bit(pkc, pkcAi_dPreakDt)) );
            p->val_Preak->setText( getW153(tap_Preak, bit(p157_1, ppn_Ppg1) | bit(pkc, pkcAi_Preak )) );
            p->val_Tgn1->setText( getW153(tap_Tgn1,bit(p157_1, ppn_tp_gn1) | bit(p157_2, ppn_tsp_gn1) | bit(pkc, pkcAi_Tgn1Tep) | bit(pkc, pkcAi_Tgn1Tsp)) );
            p->val_Tgn2->setText( getW153(tap_Tgn2,bit(p157_1, ppn_tp_gn2) | bit(p157_2, ppn_tsp_gn2) | bit(pkc, pkcAi_Tgn2Tep) | bit(pkc, pkcAi_Tgn2Tsp)) );
            p->val_Tgn3->setText( getW153(tap_Tgn3,bit(p157_1, ppn_tp_gn3) | bit(p157_2, ppn_tsp_gn3) | bit(pkc, pkcAi_Tgn3Tep) | bit(pkc, pkcAi_Tgn3Tsp)) );
            p->val_Tgn4->setText( getW153(tap_Tgn4,bit(p157_1, ppn_tp_gn4) | bit(p157_2, ppn_tsp_gn4) | bit(pkc, pkcAi_Tgn4Tep) | bit(pkc, pkcAi_Tgn4Tsp)) );

            // уставки
            //lbUst(p->u_dPreakDt, srAz(sa_dPreakDt_more0_500_A), getW153(tau_Ppg1_less55_00), less);
            lbUst(p->u_dPreakDt, srAz(sa_dPreakDt_more0_500_A), getW153(tau_dPreakDt_less0_500), less);

            lbUst(p->u_Preak_less140, srAz(sa_Preak_less140_0_A), getW153(tau_Preak_less140_0), less);
            lbUst(p->u_Preak_less150, srAz(sa_Preak_less150_0_A), getW153(tau_Preak_less150_0), less);
            lbUst(p->u_Preak_less153, srAz(sa_Preak_less153_0_A), getW153(tau_Preak_less153_0), less);
            lbUst(p->u_Preak_more180, srAz(sa_Preak_more180_A), getW153(tau_Preak_more180_0), more);
            lbUst(p->u_Preak_more150, srAz(sa_Preak_more150_0_A), getW153(tau_Preak_more150_0), more);
            lbUst(p->u_Tgn1_less268, srAz(sa_Tgn1_less268_0_A), getW153(tau_Tgn1_less268_0), less);
            lbUst(p->u_Tgn1_more260, srAz(sa_Tgn1_more260_0_A), getW153(tau_Tgn1_more260_0), more);
            lbUst(p->u_Tgn2_less268, srAz(sa_Tgn2_less268_0_A), getW153(tau_Tgn2_less268_0), less);
            lbUst(p->u_Tgn2_more260, srAz(sa_Tgn2_more260_0_A), getW153(tau_Tgn2_more260_0), more);
            lbUst(p->u_Tgn3_less268, srAz(sa_Tgn3_less268_0_A), getW153(tau_Tgn3_less268_0), less);
            lbUst(p->u_Tgn3_more260, srAz(sa_Tgn3_more260_0_A), getW153(tau_Tgn3_more260_0), more);
            lbUst(p->u_Tgn4_less268, srAz(sa_Tgn4_less268_0_A), getW153(tau_Tgn4_less268_0), less);
            lbUst(p->u_Tgn4_more260, srAz(sa_Tgn4_more260_0_A), getW153(tau_Tgn4_more260_0), more);
            // входы >=2
            lbm(p->m_N_more75_1, srM(sa_N_more75_A, pkcImAz_Aknp_N_more75));
            lbm(p->m_N_more75_2, srM(sa_N_more75_B, pkcImAz_Aknp_N_more75));
            lbm(p->m_N_more75_3, srM(sa_N_more75_C, pkcImAz_Aknp_N_more75));
            lbm(p->m_N_more75_m, srM(sa_N_more75_M));

            lbm(p->m_dPreakDt_a, srM(sa_dPreakDt_more0_500_A, pkcImAz_dPreakDt_more0_500));
            lbm(p->m_dPreakDt_b, srM(sa_dPreakDt_more0_500_B, pkcImAz_dPreakDt_more0_500));
            lbm(p->m_dPreakDt_c, srM(sa_dPreakDt_more0_500_C, pkcImAz_dPreakDt_more0_500));
            lbm(p->m_dPreakDt_m, srM(sa_dPreakDt_more0_500_M));
            lbm(p->m_Preak_more180_1, srM(sa_Preak_more180_A, pkcImAz_Preak_more180_0));
            lbm(p->m_Preak_more180_2, srM(sa_Preak_more180_B, pkcImAz_Preak_more180_0));
            lbm(p->m_Preak_more180_3, srM(sa_Preak_more180_C, pkcImAz_Preak_more180_0));
            lbm(p->m_Preak_more180_m, srM(sa_Preak_more180_M));
            lbm(p->m_Preak_less153_1, srM(sa_Preak_less153_0_A, pkcImAz_Preak_less153_0 ));
            lbm(p->m_Preak_less153_2, srM(sa_Preak_less153_0_B, pkcImAz_Preak_less153_0));
            lbm(p->m_Preak_less153_3, srM(sa_Preak_less153_0_C, pkcImAz_Preak_less153_0));
            lbm(p->m_Preak_less153_m, srM(sa_Preak_less153_0_M));
            lbm(p->m_Preak_less150_1, srM(sa_Preak_less150_0_A, pkcImAz_Preak_less150_0));
            lbm(p->m_Preak_less150_2, srM(sa_Preak_less150_0_B, pkcImAz_Preak_less150_0));
            lbm(p->m_Preak_less150_3, srM(sa_Preak_less150_0_C, pkcImAz_Preak_less150_0));
            lbm(p->m_Preak_less150_m, srM(sa_Preak_less150_0_M));
            lbm(p->m_Preak_more150_1, srM(sa_Preak_more150_0_A, pkcImAz_Preak_more150_0));
            lbm(p->m_Preak_more150_2, srM(sa_Preak_more150_0_B, pkcImAz_Preak_more150_0));
            lbm(p->m_Preak_more150_3, srM(sa_Preak_more150_0_C, pkcImAz_Preak_more150_0));
            lbm(p->m_Preak_more150_m, srM(sa_Preak_more150_0_M));
            lbm(p->m_Preak_less140_1, srM(sa_Preak_less140_0_A, pkcImAz_Preak_less140_0));
            lbm(p->m_Preak_less140_2, srM(sa_Preak_less140_0_B, pkcImAz_Preak_less140_0));
            lbm(p->m_Preak_less140_3, srM(sa_Preak_less140_0_C, pkcImAz_Preak_less140_0));
            lbm(p->m_Preak_less140_m, srM(sa_Preak_less140_0_M));

            lbm(p->m_Tgn1_less268_1, srM(sa_Tgn1_less268_0_A, pkcImAz_Tgn1_less268_0));
            lbm(p->m_Tgn1_less268_2, srM(sa_Tgn1_less268_0_B, pkcImAz_Tgn1_less268_0));
            lbm(p->m_Tgn1_less268_3, srM(sa_Tgn1_less268_0_C, pkcImAz_Tgn1_less268_0));
            lbm(p->m_Tgn1_less268_m, srM(sa_Tgn1_less268_0_M));
            lbm(p->m_Tgn1_more260_1, srM(sa_Tgn1_more260_0_A, pkcImAz_Tgn1_more260_0));
            lbm(p->m_Tgn1_more260_2, srM(sa_Tgn1_more260_0_B, pkcImAz_Tgn1_more260_0));
            lbm(p->m_Tgn1_more260_3, srM(sa_Tgn1_more260_0_C, pkcImAz_Tgn1_more260_0));
            lbm(p->m_Tgn1_more260_m, srM(sa_Tgn1_more260_0_M));

            lbm(p->m_Tgn2_less268_1, srM(sa_Tgn2_less268_0_A, pkcImAz_Tgn2_less268_0));
            lbm(p->m_Tgn2_less268_2, srM(sa_Tgn2_less268_0_B, pkcImAz_Tgn2_less268_0));
            lbm(p->m_Tgn2_less268_3, srM(sa_Tgn2_less268_0_C, pkcImAz_Tgn2_less268_0));
            lbm(p->m_Tgn2_less268_m, srM(sa_Tgn2_less268_0_M));
            lbm(p->m_Tgn2_more260_1, srM(sa_Tgn2_more260_0_A, pkcImAz_Tgn2_more260_0));
            lbm(p->m_Tgn2_more260_2, srM(sa_Tgn2_more260_0_B, pkcImAz_Tgn2_more260_0));
            lbm(p->m_Tgn2_more260_3, srM(sa_Tgn2_more260_0_C, pkcImAz_Tgn2_more260_0));
            lbm(p->m_Tgn2_more260_m, srM(sa_Tgn2_more260_0_M));

            lbm(p->m_Tgn3_less268_1, srM(sa_Tgn3_less268_0_A, pkcImAz_Tgn3_less268_0));
            lbm(p->m_Tgn3_less268_2, srM(sa_Tgn3_less268_0_B, pkcImAz_Tgn3_less268_0));
            lbm(p->m_Tgn3_less268_3, srM(sa_Tgn3_less268_0_C, pkcImAz_Tgn3_less268_0));
            lbm(p->m_Tgn3_less268_m, srM(sa_Tgn3_less268_0_M));
            lbm(p->m_Tgn3_more260_1, srM(sa_Tgn3_more260_0_A, pkcImAz_Tgn3_more260_0));
            lbm(p->m_Tgn3_more260_2, srM(sa_Tgn3_more260_0_B, pkcImAz_Tgn3_more260_0));
            lbm(p->m_Tgn3_more260_3, srM(sa_Tgn3_more260_0_C, pkcImAz_Tgn3_more260_0));
            lbm(p->m_Tgn3_more260_m, srM(sa_Tgn3_more260_0_M));

            lbm(p->m_Tgn4_less268_1, srM(sa_Tgn4_less268_0_A, pkcImAz_Tgn4_less268_0));
            lbm(p->m_Tgn4_less268_2, srM(sa_Tgn4_less268_0_B, pkcImAz_Tgn4_less268_0));
            lbm(p->m_Tgn4_less268_3, srM(sa_Tgn4_less268_0_C, pkcImAz_Tgn4_less268_0));
            lbm(p->m_Tgn4_less268_m, srM(sa_Tgn4_less268_0_M));
            lbm(p->m_Tgn4_more260_1, srM(sa_Tgn4_more260_0_A, pkcImAz_Tgn4_more260_0));
            lbm(p->m_Tgn4_more260_2, srM(sa_Tgn4_more260_0_B, pkcImAz_Tgn4_more260_0));
            lbm(p->m_Tgn4_more260_3, srM(sa_Tgn4_more260_0_C, pkcImAz_Tgn4_more260_0));
            lbm(p->m_Tgn4_more260_m, srM(sa_Tgn4_more260_0_M));

            // Значения таймеров
            p->t_t1->setText(tmAz(tat_Timer1s2));
            p->t_t2->setText(tmAz(tat_Timer2s2));
            p->t_t3->setText(tmAz(tat_Timer3s2));
            p->t_t4->setText(tmAz(tat_Timer4s2));
            // подсветка логич.эл-тов и таймеров
            lbx(p->and_N_more75, srAzPz(sa_algAz1_6) );
            lbx(p->and_dPreakDt, srAzPz(sa_algAz1_3) );
            lbx(p->and_Tgn1, srAzPz(sa_p1_a1_5) );
            lbx(p->and_Preak_Tgn1, srAzPz(sa_p2_a1_5) );
            lbx(p->and_Tgn2, srAzPz(sa_p3_a1_5) );
            lbx(p->and_Preak_Tgn2, srAzPz(sa_p4_a1_5) );
            lbx(p->and_Tgn3, srAzPz(sa_p5_a1_5) );
            lbx(p->and_Preak_Tgn3, srAzPz(sa_p6_a1_5) );
            lbx(p->and_Tgn4, srAzPz(sa_p7_a1_5) );
            lbx(p->and_Preak_Tgn4, srAzPz(sa_p8_a1_5) );

            // входы R и S триггеров, повторяют пред.привязки
            lbx(p->rs_r1, srAzPz(sa_p1_a1_5) );
            lbx(p->rs_s1, srAzPz(sa_p2_a1_5) );
            lbx(p->rs_r2, srAzPz(sa_p3_a1_5) );
            lbx(p->rs_s2, srAzPz(sa_p4_a1_5) );
            lbx(p->rs_r3, srAzPz(sa_p5_a1_5) );
            lbx(p->rs_s3, srAzPz(sa_p6_a1_5) );
            lbx(p->rs_r4, srAzPz(sa_p7_a1_5) );
            lbx(p->rs_s4, srAzPz(sa_p8_a1_5) );
            // выходы триггеров
            lbx(p->rs_t1, srAzPz(sa_p9_a1_5) );
            lbx(p->rs_t2, srAzPz(sa_p10_a1_5) );
            lbx(p->rs_t3, srAzPz(sa_p11_a1_5) );
            lbx(p->rs_t4, srAzPz(sa_p12_a1_5) );
            // подсветка таймеров после триггеров
            lbx(p->t_t1, srAzPz(sa_p13_a1_5) );
            lbx(p->t_t2, srAzPz(sa_p14_a1_5) );
            lbx(p->t_t3, srAzPz(sa_p15_a1_5) );
            lbx(p->t_t4, srAzPz(sa_p16_a1_5) );
            // лог.И после триггеров
            lbx(p->and_t1, srAzPz(sa_p17_a1_5) );
            lbx(p->and_t2, srAzPz(sa_p18_a1_5) );
            lbx(p->and_t3, srAzPz(sa_p19_a1_5) );
            lbx(p->and_t4, srAzPz(sa_p20_a1_5) );
            lbx(p->or_Az1_5, srAzPz(sa_algAz1_5) );

            // выходы
            lbx(p->opAz1_6_srab153, srAz(sa_algAz1_6) ); // сраб алгоритма в 153
            grnGr(p->opAz1_6_isprPkc, bitD(pkcNeiprStart,pkc_na_az6) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_3_srab153, srAz(sa_algAz1_3) ); // сраб алгоритма в 153
            grnGr(p->opAz1_3_isprPkc, bitD(pkcNeiprStart,pkc_na_az3) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_24_srab153, srAz(sa_algAz1_24) ); // сраб алгоритма в 153
            grnGr(p->opAz1_24_isprPkc, bitD(pkcNeiprStart,pkc_na_az24) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_5_srab153, srAz(sa_algAz1_5) ); // сраб алгоритма в 153
            grnGr(p->opAz1_5_isprPkc, bitD(pkcNeiprStart,pkc_na_az5) ); // испр при авт контроле в ПКЦ

            // ШАК
            paintShak(p);

            break;
        }
        case 3: {
            Ui_f3* p = ((f3*)(fa[n]))->ui;

            // дискр входы
            lbx( p->ip_i151_N_more5, bit(p151_1, p151_in + p151_Aknp_N_more5) ); // наличие сигнала на входе
            lb_ispLs(p->ip_ispr_N_more5, bit(p151_1, p151_neisp + p151_Aknp_N_more5), bit(p151_1, p151_sost + p151_Aknp_N_more5) ); // неисправность
            lbx( p->ip_res_N_more5, bit(p151_1, p151_res + p151_Aknp_N_more5) ); // результирующий
            lby( p->ip_imblk_N_more5, bit(p151_1, p151_imit + p151_Aknp_N_more5) | bit(p151_1, p151_blk + p151_Aknp_N_more5)); // имитация и блокировка
            lbx( p->ip_i153_N_more5, srAz(sa_N_more5_A) ); // входной в 153/154

            // выход за диап
            p->out_Hkd->setText(s2D1(ppn_Lkd));
            p->out_dPgcn1->setText(s2D1(ppn_dPgcn1));
            p->out_dPgcn2->setText(s2D1(ppn_dPgcn2));
            p->out_dPgcn3->setText(s2D1(ppn_dPgcn3));
            p->out_dPgcn4->setText(s2D1(ppn_dPgcn4));
            // значения
            p->val_Hkd->setText( getW153(tap_Lkd, bit(pkc, pkcAi_Hkd) ) );
            p->val_dPgcn1->setText( getW153(tap_dPgcn1, bit(p157_1,ppn_dPgcn1) | bit(pkc, pkcAi_dPgcn1) ) );
            p->val_dPgcn2->setText( getW153(tap_dPgcn2, bit(p157_1,ppn_dPgcn2) | bit(pkc, pkcAi_dPgcn2) ) );
            p->val_dPgcn3->setText( getW153(tap_dPgcn3, bit(p157_1,ppn_dPgcn3) | bit(pkc, pkcAi_dPgcn3) ) );
            p->val_dPgcn4->setText( getW153(tap_dPgcn4, bit(p157_1,ppn_dPgcn4) | bit(pkc, pkcAi_dPgcn4) ) );
            // уставки
            lbUst(p->u_Hkd_less200, srAz(sa_Lkd_less200_A), getW153(tau_Lkd_less200), less);
            //lbUst(p->u_neispHkd, srAz(sa_Lkd_neispr_A), "", less); //необычный случай
            lbUst(p->u_dPgcn1_more8, srAz(sa_dPgn1_more8_00_A), getW153(tau_dPgcn1_more8_00), more);
            lbUst(p->u_dPgcn1_less4, srAz(sa_dPgn1_less4_00_A), getW153(tau_dPgcn1_less4_00), less);
            lbUst(p->u_dPgcn1_less2_5, srAz(sa_dPgn1_less2_50_A), getW153(tau_dPgcn1_less2_50), less);

            lbUst(p->u_dPgcn2_more8, srAz(sa_dPgn2_more8_00_A), getW153(tau_dPgcn2_more8_00), more);
            lbUst(p->u_dPgcn2_less4, srAz(sa_dPgn2_less4_00_A), getW153(tau_dPgcn2_less4_00), less);
            lbUst(p->u_dPgcn2_less2_5, srAz(sa_dPgn2_less2_50_A), getW153(tau_dPgcn2_less2_50), less);

            lbUst(p->u_dPgcn3_more8, srAz(sa_dPgn3_more8_00_A), getW153(tau_dPgcn3_more8_00), more);
            lbUst(p->u_dPgcn3_less4, srAz(sa_dPgn3_less4_00_A), getW153(tau_dPgcn3_less4_00), less);
            lbUst(p->u_dPgcn3_less2_5, srAz(sa_dPgn3_less2_50_A), getW153(tau_dPgcn3_less2_50), less);

            lbUst(p->u_dPgcn4_more8, srAz(sa_dPgn4_more8_00_A), getW153(tau_dPgcn4_more8_00), more);
            lbUst(p->u_dPgcn4_less4, srAz(sa_dPgn4_less4_00_A), getW153(tau_dPgcn4_less4_00), less);
            lbUst(p->u_dPgcn4_less2_5, srAz(sa_dPgn4_less2_50_A), getW153(tau_dPgcn4_less2_50), less);

            // входы >=2
            lbm(p->m_N_more5_1, srM(sa_N_more5_A, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_2, srM(sa_N_more5_B, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_3, srM(sa_N_more5_C, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_m, srM(sa_N_more5_M));

            lbm(p->m_Hkd_less200_1, srM(sa_Lkd_less200_A, pkcImAz_Hkd_less200));
            lbm(p->m_Hkd_less200_2, srM(sa_Lkd_less200_B, pkcImAz_Hkd_less200));
            lbm(p->m_Hkd_less200_3, srM(sa_Lkd_less200_C, pkcImAz_Hkd_less200));
            lbm(p->m_Hkd_less200_m, srM(sa_Lkd_less200_M));

            lbm(p->m_neisp_Hkd_1, srM(sa_Lkd_neispr_A, pkcImAz_Afsz_neisp_Hkd));
            lbm(p->m_neisp_Hkd_2, srM(sa_Lkd_neispr_B, pkcImAz_Afsz_neisp_Hkd));
            lbm(p->m_neisp_Hkd_3, srM(sa_Lkd_neispr_C, pkcImAz_Afsz_neisp_Hkd));
            lbm(p->m_neispHkd_m, srM(sa_Lkd_neispr_M));

            lbm(p->m_Pgcn1_more8_1, srM(sa_dPgn1_more8_00_A, pkcImAz_dPgcn1_more8_00));
            lbm(p->m_Pgcn1_more8_2, srM(sa_dPgn1_more8_00_B, pkcImAz_dPgcn1_more8_00));
            lbm(p->m_Pgcn1_more8_3, srM(sa_dPgn1_more8_00_C, pkcImAz_dPgcn1_more8_00));
            lbm(p->m_dPgcn1_more8_m, srM(sa_dPgn1_more8_00_M));

            lbm(p->m_Pgcn1_less4_1, srM(sa_dPgn1_less4_00_A, pkcImAz_dPgcn1_less4_00));
            lbm(p->m_Pgcn1_less4_2, srM(sa_dPgn1_less4_00_B, pkcImAz_dPgcn1_less4_00));
            lbm(p->m_Pgcn1_less4_3, srM(sa_dPgn1_less4_00_C, pkcImAz_dPgcn1_less4_00));
            lbm(p->m_dPgcn1_less4_m, srM(sa_dPgn1_less4_00_M));

            lbm(p->m_Pgcn1_less2_5_1, srM(sa_dPgn1_less2_50_A, pkcImAz_dPgcn1_less2_50));
            lbm(p->m_Pgcn1_less2_5_2, srM(sa_dPgn1_less2_50_B, pkcImAz_dPgcn1_less2_50));
            lbm(p->m_Pgcn1_less2_5_3, srM(sa_dPgn1_less2_50_C, pkcImAz_dPgcn1_less2_50));
            lbm(p->m_dPgcn1_less2_5_m, srM(sa_dPgn1_less2_50_M));

            lbm(p->m_Pgcn2_more8_1, srM(sa_dPgn2_more8_00_A, pkcImAz_dPgcn2_more8_00));
            lbm(p->m_Pgcn2_more8_2, srM(sa_dPgn2_more8_00_B, pkcImAz_dPgcn2_more8_00));
            lbm(p->m_Pgcn2_more8_3, srM(sa_dPgn2_more8_00_C, pkcImAz_dPgcn2_more8_00));
            lbm(p->m_dPgcn2_more8_m, srM(sa_dPgn2_more8_00_M));

            lbm(p->m_Pgcn2_less4_1, srM(sa_dPgn2_less4_00_A, pkcImAz_dPgcn2_less4_00));
            lbm(p->m_Pgcn2_less4_2, srM(sa_dPgn2_less4_00_B, pkcImAz_dPgcn2_less4_00));
            lbm(p->m_Pgcn2_less4_3, srM(sa_dPgn2_less4_00_C, pkcImAz_dPgcn2_less4_00));
            lbm(p->m_dPgcn2_less4_m, srM(sa_dPgn2_less4_00_M));

            lbm(p->m_Pgcn2_less2_5_1, srM(sa_dPgn2_less2_50_A, pkcImAz_dPgcn2_less2_50));
            lbm(p->m_Pgcn2_less2_5_2, srM(sa_dPgn2_less2_50_B, pkcImAz_dPgcn2_less2_50));
            lbm(p->m_Pgcn2_less2_5_3, srM(sa_dPgn2_less2_50_C, pkcImAz_dPgcn2_less2_50));
            lbm(p->m_dPgcn2_less2_5_m, srM(sa_dPgn2_less2_50_M));

            lbm(p->m_Pgcn3_more8_1, srM(sa_dPgn3_more8_00_A, pkcImAz_dPgcn3_more8_00));
            lbm(p->m_Pgcn3_more8_2, srM(sa_dPgn3_more8_00_B, pkcImAz_dPgcn3_more8_00));
            lbm(p->m_Pgcn3_more8_3, srM(sa_dPgn3_more8_00_C, pkcImAz_dPgcn3_more8_00));
            lbm(p->m_dPgcn3_more8_m, srM(sa_dPgn3_more8_00_M));

            lbm(p->m_Pgcn3_less4_1, srM(sa_dPgn3_less4_00_A, pkcImAz_dPgcn3_less4_00));
            lbm(p->m_Pgcn3_less4_2, srM(sa_dPgn3_less4_00_B, pkcImAz_dPgcn3_less4_00));
            lbm(p->m_Pgcn3_less4_3, srM(sa_dPgn3_less4_00_C, pkcImAz_dPgcn3_less4_00));
            lbm(p->m_dPgcn3_less4_m, srM(sa_dPgn3_less4_00_M));

            lbm(p->m_Pgcn3_less2_5_1, srM(sa_dPgn3_less2_50_A, pkcImAz_dPgcn3_less2_50));
            lbm(p->m_Pgcn3_less2_5_2, srM(sa_dPgn3_less2_50_B, pkcImAz_dPgcn3_less2_50));
            lbm(p->m_Pgcn3_less2_5_3, srM(sa_dPgn3_less2_50_C, pkcImAz_dPgcn3_less2_50));
            lbm(p->m_dPgcn3_less2_5_m, srM(sa_dPgn3_less2_50_M));

            lbm(p->m_Pgcn4_more8_1, srM(sa_dPgn4_more8_00_A, pkcImAz_dPgcn4_more8_00));
            lbm(p->m_Pgcn4_more8_2, srM(sa_dPgn4_more8_00_B, pkcImAz_dPgcn4_more8_00));
            lbm(p->m_Pgcn4_more8_3, srM(sa_dPgn4_more8_00_C, pkcImAz_dPgcn4_more8_00));
            lbm(p->m_dPgcn4_more8_m, srM(sa_dPgn4_more8_00_M));

            lbm(p->m_Pgcn4_less4_1, srM(sa_dPgn4_less4_00_A, pkcImAz_dPgcn4_less4_00));
            lbm(p->m_Pgcn4_less4_2, srM(sa_dPgn4_less4_00_B, pkcImAz_dPgcn4_less4_00));
            lbm(p->m_Pgcn4_less4_3, srM(sa_dPgn4_less4_00_C, pkcImAz_dPgcn4_less4_00));
            lbm(p->m_dPgcn4_less4_m, srM(sa_dPgn4_less4_00_M));

            lbm(p->m_Pgcn4_less2_5_1, srM(sa_dPgn4_less2_50_A, pkcImAz_dPgcn4_less2_50));
            lbm(p->m_Pgcn4_less2_5_2, srM(sa_dPgn4_less2_50_B, pkcImAz_dPgcn4_less2_50));
            lbm(p->m_Pgcn4_less2_5_3, srM(sa_dPgn4_less2_50_C, pkcImAz_dPgcn4_less2_50));
            lbm(p->m_dPgcn4_less2_5_m, srM(sa_dPgn4_less2_50_M));

            // Значения таймеров
            p->t_dPgcn1->setText(tmAz(tat_Timer1s3));
            p->t_dPgcn2->setText(tmAz(tat_Timer2s3));
            p->t_dPgcn3->setText(tmAz(tat_Timer3s3));
            p->t_dPgcn4->setText(tmAz(tat_Timer4s3));
            // подсветка логич.эл-тов и таймеров
            lbx(p->and_N_more5, srAzPz(sa_p_a1_26) );
            lbx(p->or_Hkd_less200, srAzPz(sa_algAz1_26) );

            lbx(p->t_dPgcn1, srAzPz(sa_dPgn1_less4_00_M) );
            lbx(p->t_dPgcn2, srAzPz(sa_dPgn2_less4_00_M) );
            lbx(p->t_dPgcn3, srAzPz(sa_dPgn3_less4_00_M) );
            lbx(p->t_dPgcn4, srAzPz(sa_dPgn4_less4_00_M) );

            lbx(p->and_dPgcn1, srAzPz(sa_algAz1_11) );
            lbx(p->and_dPgcn2, srAzPz(sa_algAz1_12) );
            lbx(p->and_dPgcn3, srAzPz(sa_algAz1_13) );
            lbx(p->and_dPgcn4, srAzPz(sa_algAz1_14) );

            // выходы
            lbx(p->opAz1_26_srab153, srAz(sa_algAz1_26) ); // сраб алгоритма в 153
            grnGr(p->opAz1_26_isprPkc, bitD(pkcNeiprStart,pkc_na_az26) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_7_srab153, srAz(sa_algAz1_7) ); // сраб алгоритма в 153
            grnGr(p->opAz1_7_isprPkc, bitD(pkcNeiprStart,pkc_na_az7) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_11_srab153, srAz(sa_algAz1_11) ); // сраб алгоритма в 153
            grnGr(p->opAz1_11_isprPkc, bitD(pkcNeiprStart,pkc_na_az11 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_8_srab153, srAz(sa_algAz1_8) ); // сраб алгоритма в 153
            grnGr(p->opAz1_8_isprPkc, bitD(pkcNeiprStart,pkc_na_az8 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_12_srab153, srAz(sa_algAz1_12) ); // сраб алгоритма в 153
            grnGr(p->opAz1_12_isprPkc, bitD(pkcNeiprStart,pkc_na_az12 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_9_srab153, srAz(sa_algAz1_9) ); // сраб алгоритма в 153
            grnGr(p->opAz1_9_isprPkc, bitD(pkcNeiprStart,pkc_na_az9 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_13_srab153, srAz(sa_algAz1_13) ); // сраб алгоритма в 153
            grnGr(p->opAz1_13_isprPkc, bitD(pkcNeiprStart,pkc_na_az13 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_10_srab153, srAz(sa_algAz1_10) ); // сраб алгоритма в 153
            grnGr(p->opAz1_10_isprPkc, bitD(pkcNeiprStart,pkc_na_az10 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_14_srab153, srAz(sa_algAz1_14) ); // сраб алгоритма в 153
            grnGr(p->opAz1_14_isprPkc, bitD(pkcNeiprStart,pkc_na_az14 ) ); // испр при авт контроле в ПКЦ

            // ШАК
            paintShak(p);

            break;
        }
        case 4: {
            Ui_f4* p = ((f4*)(fa[n]))->ui;
            // дискр входы
            lbx( p->ip_i151_N_more5, bit(p151_1, p151_in + p151_Aknp_N_more5) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_N_more5, bit(p151_1, p151_neisp + p151_Aknp_N_more5), bit(p151_1, p151_sost + p151_Aknp_N_more5) ); // неисправность
            lbx( p->ip_res_N_more5, bit(p151_1, p151_res + p151_Aknp_N_more5) ); // результирующий
            lby( p->ip_imblk_N_more5, bit(p151_1, p151_imit + p151_Aknp_N_more5) | bit(p151_1, p151_blk + p151_Aknp_N_more5)); // имитация и блокировка
            lbx( p->ip_i153_N_more5, srAz(sa_N_more5_A) ); // входной в 153/154

            lbx( p->ip_i151_neispAKNP, bit(p151_1, p151_in + p151_Aknp_Neisp) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_neispAKNP, bit(p151_1, p151_neisp + p151_Aknp_Neisp), bit(p151_1, p151_sost + p151_Aknp_Neisp) ); // неисправность
            lbx( p->ip_res_neispAKNP, bit(p151_1, p151_res + p151_Aknp_Neisp) ); // результирующий
            lby( p->ip_imblk_neispAKNP, bit(p151_1, p151_imit + p151_Aknp_Neisp) | bit(p151_1, p151_blk + p151_Aknp_Neisp)); // имитация и блокировка
            lbx( p->ip_i153_neispAKNP, srAz(sa_Neispr_A) ); // входной в 153/154

            // выход за диап
            p->out_Lpg1->setText(s2D1(ppn_Lpg1));
            p->out_Lpg2->setText(s2D1(ppn_Lpg2));
            p->out_Lpg3->setText(s2D1(ppn_Lpg3));
            p->out_Lpg4->setText(s2D1(ppn_Lpg4));

//            p->out_MedGcn1->setText(s2D1(ppn_MedGcn1));
//            p->out_MedGcn2->setText(s2D1(ppn_MedGcn2));
//            p->out_MedGcn3->setText(s2D1(ppn_MedGcn3));
//            p->out_MedGcn4->setText(s2D1(ppn_MedGcn4));
            // значения
            p->val_Lpg1->setText( getW153(tap_Lpg1, bit(p157_1, ppn_Lpg1) | bit(pkc, pkcAi_Hpg1) ) );
            p->val_Lpg2->setText( getW153(tap_Lpg2, bit(p157_1, ppn_Lpg2) | bit(pkc, pkcAi_Hpg2) ) );
            p->val_Lpg3->setText( getW153(tap_Lpg3, bit(p157_1, ppn_Lpg3) | bit(pkc, pkcAi_Hpg3) ) );
            p->val_Lpg4->setText( getW153(tap_Lpg4, bit(p157_1, ppn_Lpg4) | bit(pkc, pkcAi_Hpg4) ) );

//            p->val_MedGcn1->setText( getW153(tap_MedGcn1, bit(p157_1, ppn_MedGcn1) | bit(pkc, pkcAi_MedGcn1) ) );
//            p->val_MedGcn2->setText( getW153(tap_MedGcn2, bit(p157_1, ppn_MedGcn2) | bit(pkc, pkcAi_MedGcn2) ) );
//            p->val_MedGcn3->setText( getW153(tap_MedGcn3, bit(p157_1, ppn_MedGcn3) | bit(pkc, pkcAi_MedGcn3) ) );
//            p->val_MedGcn4->setText( getW153(tap_MedGcn4, bit(p157_1, ppn_MedGcn4) | bit(pkc, pkcAi_MedGcn4) ) );
            // уставки
            lbUst(p->u_Lpg1_less1600, srAz(sa_Lpg1_less1600_A), getW153(tau_Lpg1_less1600), less);
            lbUst(p->u_Lpg2_less1600, srAz(sa_Lpg2_less1600_A), getW153(tau_Lpg2_less1600), less);
            lbUst(p->u_Lpg3_less1600, srAz(sa_Lpg3_less1600_A), getW153(tau_Lpg3_less1600), less);
            lbUst(p->u_Lpg4_less1600, srAz(sa_Lpg4_less1600_A), getW153(tau_Lpg4_less1600), less);

//            lbUst(p->u_MedGcn1_more4600, srAz(sa_MedGcn1_more4600_A), getW153(tau_MedGcn1_more4600), more);
//            lbUst(p->u_MedGcn2_more4600, srAz(sa_MedGcn2_more4600_A), getW153(tau_MedGcn2_more4600), more);
//            lbUst(p->u_MedGcn3_more4600, srAz(sa_MedGcn3_more4600_A), getW153(tau_MedGcn3_more4600), more);
//            lbUst(p->u_MedGcn4_more4600, srAz(sa_MedGcn4_more4600_A), getW153(tau_MedGcn4_more4600), more);
            //lbUst(p->u_neispLpg1, srAz(sa_Lpg1_neispr_A), getW153(emp), more);

            // входы >=2
            lbm(p->m_N_more5_1, srM(sa_N_more5_A, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_2, srM(sa_N_more5_B, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_3, srM(sa_N_more5_C, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_m, srM(sa_N_more5_M));

            lbm(p->m_Lpg1_1, srM(sa_Lpg1_less1600_A, pkcImAz_Lpg1_less1600));
            lbm(p->m_Lpg1_2, srM(sa_Lpg1_less1600_B, pkcImAz_Lpg1_less1600));
            lbm(p->m_Lpg1_3, srM(sa_Lpg1_less1600_C, pkcImAz_Lpg1_less1600));
            lbm(p->m_Lpg1_m, srM(sa_Lpg1_less1600_M));

            lbm(p->m_neispLpg1_1, srM(sa_Lpg1_neispr_A, pkcImAz_Afsz_neisprLpg1));
            lbm(p->m_neispLpg1_2, srM(sa_Lpg1_neispr_B, pkcImAz_Afsz_neisprLpg1));
            lbm(p->m_neispLpg1_3, srM(sa_Lpg1_neispr_C, pkcImAz_Afsz_neisprLpg1));
            lbm(p->m_neispLpg1_m, srM(sa_Lpg1_neispr_M));

//            lbm(p->m_MedGcn1_1, srM(sa_MedGcn1_more4600_A, pkcImAz_MedGcn1_more4600));
//            lbm(p->m_MedGcn1_2, srM(sa_MedGcn1_more4600_B, pkcImAz_MedGcn1_more4600));
//            lbm(p->m_MedGcn1_3, srM(sa_MedGcn1_more4600_C, pkcImAz_MedGcn1_more4600));
//            lbm(p->m_MedGcn1_m, srM(sa_MedGcn1_more4600_M));

            lbm(p->m_Lpg2_1, srM(sa_Lpg2_less1600_A, pkcImAz_Lpg2_less1600));
            lbm(p->m_Lpg2_2, srM(sa_Lpg2_less1600_B, pkcImAz_Lpg2_less1600));
            lbm(p->m_Lpg2_3, srM(sa_Lpg2_less1600_C, pkcImAz_Lpg2_less1600));
            lbm(p->m_Lpg2_m, srM(sa_Lpg2_less1600_M));

            lbm(p->m_neispLpg2_1, srM(sa_Lpg2_neispr_A, pkcImAz_Afsz_neisprLpg2));
            lbm(p->m_neispLpg2_2, srM(sa_Lpg2_neispr_B, pkcImAz_Afsz_neisprLpg2));
            lbm(p->m_neispLpg2_3, srM(sa_Lpg2_neispr_C, pkcImAz_Afsz_neisprLpg2));
            lbm(p->m_neispLpg2_m, srM(sa_Lpg2_neispr_M));

//            lbm(p->m_MedGcn2_1, srM(sa_MedGcn2_more4600_A, pkcImAz_MedGcn2_more4600));
//            lbm(p->m_MedGcn2_2, srM(sa_MedGcn2_more4600_B, pkcImAz_MedGcn2_more4600));
//            lbm(p->m_MedGcn2_3, srM(sa_MedGcn2_more4600_C, pkcImAz_MedGcn2_more4600));
//            lbm(p->m_MedGcn2_m, srM(sa_MedGcn2_more4600_M));

            lbm(p->m_Lpg3_1, srM(sa_Lpg3_less1600_A, pkcImAz_Lpg3_less1600));
            lbm(p->m_Lpg3_2, srM(sa_Lpg3_less1600_B, pkcImAz_Lpg3_less1600));
            lbm(p->m_Lpg3_3, srM(sa_Lpg3_less1600_C, pkcImAz_Lpg3_less1600));
            lbm(p->m_Lpg3_m, srM(sa_Lpg3_less1600_M));

            lbm(p->m_neispLpg3_1, srM(sa_Lpg3_neispr_A, pkcImAz_Afsz_neisprLpg3));
            lbm(p->m_neispLpg3_2, srM(sa_Lpg3_neispr_B, pkcImAz_Afsz_neisprLpg3));
            lbm(p->m_neispLpg3_3, srM(sa_Lpg3_neispr_C, pkcImAz_Afsz_neisprLpg3));
            lbm(p->m_neispLpg3_m, srM(sa_Lpg3_neispr_M));

//            lbm(p->m_MedGcn3_1, srM(sa_MedGcn3_more4600_A, pkcImAz_MedGcn3_more4600));
//            lbm(p->m_MedGcn3_2, srM(sa_MedGcn3_more4600_B, pkcImAz_MedGcn3_more4600));
//            lbm(p->m_MedGcn3_3, srM(sa_MedGcn3_more4600_C, pkcImAz_MedGcn3_more4600));
//            lbm(p->m_MedGcn3_m, srM(sa_MedGcn3_more4600_M));

            lbm(p->m_Lpg4_1, srM(sa_Lpg4_less1600_A, pkcImAz_Lpg4_less1600));
            lbm(p->m_Lpg4_2, srM(sa_Lpg4_less1600_B, pkcImAz_Lpg4_less1600));
            lbm(p->m_Lpg4_3, srM(sa_Lpg4_less1600_C, pkcImAz_Lpg4_less1600));
            lbm(p->m_Lpg4_m, srM(sa_Lpg4_less1600_M));

            lbm(p->m_neispLpg4_1, srM(sa_Lpg4_neispr_A, pkcImAz_Afsz_neisprLpg4));
            lbm(p->m_neispLpg4_2, srM(sa_Lpg4_neispr_B, pkcImAz_Afsz_neisprLpg4));
            lbm(p->m_neispLpg4_3, srM(sa_Lpg4_neispr_C, pkcImAz_Afsz_neisprLpg4));
            lbm(p->m_neispLpg4_m, srM(sa_Lpg4_neispr_M));

//            lbm(p->m_MedGcn4_1, srM(sa_MedGcn4_more4600_A, pkcImAz_MedGcn4_more4600));
//            lbm(p->m_MedGcn4_2, srM(sa_MedGcn4_more4600_B, pkcImAz_MedGcn4_more4600));
//            lbm(p->m_MedGcn4_3, srM(sa_MedGcn4_more4600_C, pkcImAz_MedGcn4_more4600));
//            lbm(p->m_MedGcn4_m, srM(sa_MedGcn4_more4600_M));

            lbm(p->m_neispAKNP_1, srM(sa_Neispr_A, pkcImAz_Aknp_Neisp));
            lbm(p->m_neispAKNP_2, srM(sa_Neispr_B, pkcImAz_Aknp_Neisp));
            lbm(p->m_neispAKNP_3, srM(sa_Neispr_C, pkcImAz_Aknp_Neisp));
            lbm(p->m_neispAKNP_m, srM(sa_Neispr_M));

            // подсветка логич.эл-тов
            lbx(p->and_MedGcn1, srAzPz(sa_p_a1_15) );
            lbx(p->and_MedGcn2, srAzPz(sa_p_a1_16) );
            lbx(p->and_MedGcn3, srAzPz(sa_p_a1_17) );
            lbx(p->and_MedGcn4, srAzPz(sa_p_a1_18) );

            lbx(p->or_Lpg1, srAzPz(sa_algAz1_15) );
            lbx(p->or_Lpg2, srAzPz(sa_algAz1_16) );
            lbx(p->or_Lpg3, srAzPz(sa_algAz1_17) );
            lbx(p->or_Lpg4, srAzPz(sa_algAz1_18) );

            // выходные
            lbx(p->opAz1_15_srab153, srAz(sa_algAz1_15) ); // сраб алгоритма в 153
            grnGr(p->opAz1_15_isprPkc, bitD(pkcNeiprStart,pkc_na_az15 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_16_srab153, srAz(sa_algAz1_16) ); // сраб алгоритма в 153
            grnGr(p->opAz1_16_isprPkc, bitD(pkcNeiprStart,pkc_na_az16 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_17_srab153, srAz(sa_algAz1_17) ); // сраб алгоритма в 153
            grnGr(p->opAz1_17_isprPkc, bitD(pkcNeiprStart,pkc_na_az17 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_18_srab153, srAz(sa_algAz1_18) ); // сраб алгоритма в 153
            grnGr(p->opAz1_18_isprPkc, bitD(pkcNeiprStart,pkc_na_az18 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_31_srab153, srAz(sa_algAz1_31) ); // сраб алгоритма в 153
            grnGr(p->opAz1_31_isprPkc, bitD(pkcNeiprStart,pkc_na_az31 ) ); // испр при авт контроле в ПКЦ

            // ШАК
            paintShak(p);

            break;
         }
         case 5: {
            Ui_f5* p = ((f5*)(fa[n]))->ui;

            // дискр входы
            lbx( p->ip_i151_N_more5, bit(p151_1, p151_in + p151_Aknp_N_more5) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_N_more5, bit(p151_1, p151_neisp + p151_Aknp_N_more5), bit(p151_1, p151_sost + p151_Aknp_N_more5)  ); // неисправность
            lbx( p->ip_res_N_more5, bit(p151_1, p151_res + p151_Aknp_N_more5) ); // результирующий
            lby( p->ip_imblk_N_more5, bit(p151_1, p151_imit + p151_Aknp_N_more5) | bit(p151_1, p151_blk + p151_Aknp_N_more5)); // имитация и блокировка
            lbx( p->ip_i153_N_more5, srAz(sa_N_more5_A) ); // входной в 153/154

            lbx( p->ip_i151_N_more30, bit(p151_1, p151_in + p151_Aknp_N_more30) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_N_more30, bit(p151_1, p151_neisp + p151_Aknp_N_more30),  bit(p151_1, p151_sost + p151_Aknp_N_more30) ); // неисправность
            lbx( p->ip_res_N_more30, bit(p151_1, p151_res + p151_Aknp_N_more30) ); // результирующий
            lby( p->ip_imblk_N_more30, bit(p151_1, p151_imit + p151_Aknp_N_more30) | bit(p151_1, p151_blk + p151_Aknp_N_more30)); // имитация и блокировка
            lbx( p->ip_i153_N_more30, srAz(sa_N_more30_A) ); // входной в 153/154

            lbx( p->ip_i151_N_more75, bit(p151_1, p151_in + p151_Aknp_N_more75) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_N_more75, bit(p151_1, p151_neisp + p151_Aknp_N_more75), bit(p151_1, p151_sost + p151_Aknp_N_more75) ); // неисправность
            lbx( p->ip_res_N_more75, bit(p151_1, p151_res + p151_Aknp_N_more75) ); // результирующий
            lby( p->ip_imblk_N_more75, bit(p151_1, p151_imit + p151_Aknp_N_more75) | bit(p151_1, p151_blk + p151_Aknp_N_more75)); // имитация и блокировка
            lbx( p->ip_i153_N_more75, srAz(sa_N_more75_A) ); // входной в 153/154

            lbx( p->ip_i151_potPit1Shp30, bit(p151_2, p151_in + p151_Shp30_PotPit1) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_potPit1Shp30, bit(p151_2, p151_neisp + p151_Shp30_PotPit1), bit(p151_2, p151_sost + p151_Shp30_PotPit1) ); // неисправность
            lbx( p->ip_res_potPit1Shp30, bit(p151_2, p151_res + p151_Shp30_PotPit1) ); // результирующий
            lby( p->ip_imblk_potPit1Shp30, bit(p151_2, p151_imit + p151_Shp30_PotPit1) | bit(p151_2, p151_blk + p151_Shp30_PotPit1)); // имитация и блокировка
            lbx( p->ip_i153_potPit1Shp30, srAz(sa_PotPit1_A) ); // входной в 153/154

            lbx( p->ip_i151_potPit2Shp30, bit(p151_2, p151_in + p151_Shp30_PotPit2) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_potPit2Shp30, bit(p151_2, p151_neisp + p151_Shp30_PotPit2), bit(p151_2, p151_sost + p151_Shp30_PotPit2) ); // неисправность
            lbx( p->ip_res_potPit2Shp30, bit(p151_2, p151_res + p151_Shp30_PotPit2) ); // результирующий
            lby( p->ip_imblk_potPit2Shp30, bit(p151_2, p151_imit + p151_Shp30_PotPit2) | bit(p151_2, p151_blk + p151_Shp30_PotPit2)); // имитация и блокировка
            lbx( p->ip_i153_potPit2Shp30, srAz(sa_PotPit2_A) ); // входной в 153/154

            lbx( p->ip_i151_potPitRtzo, bit(p151_2, p151_in + p151_Rtzo_PotPitSborok) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_potPitRtzo, bit(p151_2, p151_neisp + p151_Rtzo_PotPitSborok), bit(p151_2, p151_sost + p151_Rtzo_PotPitSborok) ); // неисправность
            lbx( p->ip_res_potPitRtzo, bit(p151_2, p151_res + p151_Rtzo_PotPitSborok) ); // результирующий
            lby( p->ip_imblk_potPitRtzo, bit(p151_2, p151_imit + p151_Rtzo_PotPitSborok) | bit(p151_2, p151_blk + p151_Rtzo_PotPitSborok)); // имитация и блокировка
            lbx( p->ip_i153_potPitRtzo, srAz(sa_PotPitSborokRtzo_A) ); // входной в 153/154

            // выход за диап
            p->out_MedGcn1->setText(s2D1(ppn_MedGcn1));
            p->out_MedGcn2->setText(s2D1(ppn_MedGcn2));
            p->out_MedGcn3->setText(s2D1(ppn_MedGcn3));
            p->out_MedGcn4->setText(s2D1(ppn_MedGcn4));
            p->out_PzkpTpn1->setText(s2D1(ppn_PzkpTpn1));
            p->out_PzkpTpn2->setText(s2D1(ppn_PzkpTpn2));
            p->out_Pobol->setText(s2D1(ppn_Pgaz06));
            p->out_Tobol->setText(s2D1(ppn_tsp_Tobol));
            // значения
            p->val_MedGcn1->setText( getW153(tap_MedGcn1, bit(p157_1, ppn_MedGcn1) | bit(pkc, pkcAi_MedGcn1) ) );
            p->val_MedGcn2->setText( getW153(tap_MedGcn2, bit(p157_1, ppn_MedGcn2) | bit(pkc, pkcAi_MedGcn2) ) );
            p->val_MedGcn3->setText( getW153(tap_MedGcn3, bit(p157_1, ppn_MedGcn3) | bit(pkc, pkcAi_MedGcn3) ) );
            p->val_MedGcn4->setText( getW153(tap_MedGcn4, bit(p157_1, ppn_MedGcn4) | bit(pkc, pkcAi_MedGcn4) ) );
            p->val_PzkpTpn1->setText( getW153(tap_PzkpTpn1, bit(p157_1, ppn_PzkpTpn1) | bit(pkc, pkcAi_PzkpTpn1) ) );
            p->val_PzkpTpn2->setText( getW153(tap_PzkpTpn2, bit(p157_1, ppn_PzkpTpn2) | bit(pkc, pkcAi_PzkpTpn2) ) );
            p->val_Pobol->setText( getW153(tap_Pobol, bit(p157_1, ppn_Pgaz06) | bit(pkc, pkcAi_Pobol) ) );
            p->val_Tobol->setText( getW153(tap_Tobol, bit(p157_1, ppn_tsp_Tobol)| bit(pkc, pkcAi_Tobol) ) );
            // уставки
            lbUst(p->u_MedGcn1_less3000, srAz(sa_MedGcn1_less3000_A), getW153(tau_MedGcn1_less3000), less);
            lbUst(p->u_MedGcn2_less3000, srAz(sa_MedGcn2_less3000_A), getW153(tau_MedGcn2_less3000), less);
            lbUst(p->u_MedGcn3_less3000, srAz(sa_MedGcn3_less3000_A), getW153(tau_MedGcn3_less3000), less);
            lbUst(p->u_MedGcn4_less3000, srAz(sa_MedGcn4_less3000_A), getW153(tau_MedGcn4_less3000), less);
            lbUst(p->u_PzkpTpn1_less0_500, srAz(sa_PzkpTpn1_less_0_500_A), getW153(tau_PzkpTpn1_less0_500), less);
            lbUst(p->u_PzkpTpn2_less0_500, srAz(sa_PzkpTpn2_less_0_500_A), getW153(tau_PzkpTpn2_less0_500), less);
            lbUst(p->u_Pobol_more0_700, srAz(sa_Pobol_more7000_A), getW153(tau_Pobol_more0_70), more);
            lbUst(p->u_Tobol_more90, srAz(sa_Tobol_more90_0_A), getW153(tau_Tobol_more90_0), more);

            // входы >=2
            lbm(p->m_N_more5_1, srM(sa_N_more5_A, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_2, srM(sa_N_more5_B, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_3, srM(sa_N_more5_C, pkcImAz_Aknp_N_more5));
            lbm(p->m_N_more5_m, srM(sa_N_more5_M));

            lbm(p->m_N_more30_1, srM(sa_N_more30_A, pkcImAz_Aknp_N_more30));
            lbm(p->m_N_more30_2, srM(sa_N_more30_B, pkcImAz_Aknp_N_more30));
            lbm(p->m_N_more30_3, srM(sa_N_more30_C, pkcImAz_Aknp_N_more30));
            lbm(p->m_N_more30_m, srM(sa_N_more30_M));

            lbm(p->m_N_more75_1, srM(sa_N_more75_A, pkcImAz_Aknp_N_more75));
            lbm(p->m_N_more75_2, srM(sa_N_more75_B, pkcImAz_Aknp_N_more75));
            lbm(p->m_N_more75_3, srM(sa_N_more75_C, pkcImAz_Aknp_N_more75));
            lbm(p->m_N_more75_m, srM(sa_N_more75_M));

            lbm(p->m_MedGcn1_1, srM(sa_MedGcn1_less3000_A, pkcImAz_MedGcn1_less3000));
            lbm(p->m_MedGcn1_2, srM(sa_MedGcn1_less3000_B, pkcImAz_MedGcn1_less3000));
            lbm(p->m_MedGcn1_3, srM(sa_MedGcn1_less3000_C, pkcImAz_MedGcn1_less3000));
            lbm(p->m_MedGcn1_m, srM(sa_MedGcn1_less3000_M));

            lbm(p->m_MedGcn2_1, srM(sa_MedGcn2_less3000_A, pkcImAz_MedGcn2_less3000));
            lbm(p->m_MedGcn2_2, srM(sa_MedGcn2_less3000_B, pkcImAz_MedGcn2_less3000));
            lbm(p->m_MedGcn2_3, srM(sa_MedGcn2_less3000_C, pkcImAz_MedGcn2_less3000));
            lbm(p->m_MedGcn2_m, srM(sa_MedGcn2_less3000_M));

            lbm(p->m_MedGcn3_1, srM(sa_MedGcn3_less3000_A, pkcImAz_MedGcn3_less3000));
            lbm(p->m_MedGcn3_2, srM(sa_MedGcn3_less3000_B, pkcImAz_MedGcn3_less3000));
            lbm(p->m_MedGcn3_3, srM(sa_MedGcn3_less3000_C, pkcImAz_MedGcn3_less3000));
            lbm(p->m_MedGcn3_m, srM(sa_MedGcn3_less3000_M));

            lbm(p->m_MedGcn4_1, srM(sa_MedGcn4_less3000_A, pkcImAz_MedGcn4_less3000));
            lbm(p->m_MedGcn4_2, srM(sa_MedGcn4_less3000_B, pkcImAz_MedGcn4_less3000));
            lbm(p->m_MedGcn4_3, srM(sa_MedGcn4_less3000_C, pkcImAz_MedGcn4_less3000));
            lbm(p->m_MedGcn4_m, srM(sa_MedGcn4_less3000_M));

            lbm(p->m_PzkpTpn1_1, srM(sa_PzkpTpn1_less_0_500_A, pkcImAz_PzkpTpn1_less0_500));
            lbm(p->m_PzkpTpn1_2, srM(sa_PzkpTpn1_less_0_500_B, pkcImAz_PzkpTpn1_less0_500));
            lbm(p->m_PzkpTpn1_3, srM(sa_PzkpTpn1_less_0_500_C, pkcImAz_PzkpTpn1_less0_500));
            lbm(p->m_PzkpTpn1_m, srM(sa_PzkpTpn1_less_0_500_M));

            lbm(p->m_PzkpTpn2_1, srM(sa_PzkpTpn2_less_0_500_A, pkcImAz_PzkpTpn2_less0_500));
            lbm(p->m_PzkpTpn2_2, srM(sa_PzkpTpn2_less_0_500_B, pkcImAz_PzkpTpn2_less0_500));
            lbm(p->m_PzkpTpn2_3, srM(sa_PzkpTpn2_less_0_500_C, pkcImAz_PzkpTpn2_less0_500));
            lbm(p->m_PzkpTpn2_m, srM(sa_PzkpTpn2_less_0_500_M));

            lbm(p->m_Tobol_1, srM(sa_Tobol_more90_0_A, pkcImAz_Tobol_more90_0));
            lbm(p->m_Tobol_2, srM(sa_Tobol_more90_0_B, pkcImAz_Tobol_more90_0));
            lbm(p->m_Tobol_3, srM(sa_Tobol_more90_0_C, pkcImAz_Tobol_more90_0));
            lbm(p->m_Tobol_m, srM(sa_Tobol_more90_0_M));

            lbm(p->m_Pobol_1, srM(sa_Pobol_more7000_A, pkcImAz_Pobol_more0_7000));
            lbm(p->m_Pobol_2, srM(sa_Pobol_more7000_B, pkcImAz_Pobol_more0_7000));
            lbm(p->m_Pobol_3, srM(sa_Pobol_more7000_C, pkcImAz_Pobol_more0_7000));
            lbm(p->m_Pobol_m, srM(sa_Pobol_more7000_M));

            lbm(p->m_PotPit1Shp30_1, srM(sa_PotPit1_A, pkcImAz_ShP30_PotPit1));
            lbm(p->m_PotPit1Shp30_2, srM(sa_PotPit1_B, pkcImAz_ShP30_PotPit1));
            lbm(p->m_PotPit1Shp30_3, srM(sa_PotPit1_C, pkcImAz_ShP30_PotPit1));
            lbm(p->m_PotPit1Shp30_m, srM(sa_PotPit1_M));

            lbm(p->m_PotPit2Shp30_1, srM(sa_PotPit2_A, pkcImAz_ShP30_PotPit2));
            lbm(p->m_PotPit2Shp30_2, srM(sa_PotPit2_B, pkcImAz_ShP30_PotPit2));
            lbm(p->m_PotPit2Shp30_3, srM(sa_PotPit2_C, pkcImAz_ShP30_PotPit2));
            lbm(p->m_PotPit2Shp30_m, srM(sa_PotPit2_M));

            lbm(p->m_PotPit1Rtzo_1, srM(sa_PotPitSborokRtzo_A, pkcImAz_PotPitSborokPT30));
            lbm(p->m_PotPit1Rtzo_2, srM(sa_PotPitSborokRtzo_B, pkcImAz_PotPitSborokPT30));
            lbm(p->m_PotPit1Rtzo_3, srM(sa_PotPitSborokRtzo_C, pkcImAz_PotPitSborokPT30));
            lbm(p->m_PotPit1Rtzo_m, srM(sa_PotPitSborokRtzo_M));

            // Значения таймеров
            p->t_medGcn1_1->setText(tmAz(tat_Timer1_1s5));
            p->t_medGcn1_2->setText(tmAz(tat_Timer1_2s5));
            p->t_medGcn2_1->setText(tmAz(tat_Timer2_1s5));
            p->t_medGcn2_2->setText(tmAz(tat_Timer2_2s5));
            p->t_medGcn3_1->setText(tmAz(tat_Timer3_1s5));
            p->t_medGcn3_2->setText(tmAz(tat_Timer3_2s5));
            p->t_medGcn4_1->setText(tmAz(tat_Timer4_1s5));
            p->t_medGcn4_2->setText(tmAz(tat_Timer4_2s5));
            p->t_zkpTpn->setText(tmAz(tat_Timer5s5));

            // подсветка логич.эл-тов и таймеров
            lbx(p->and_zkpTpn, srAzPz(sa_p1_a1_29) );
            lbx(p->and_N_more30, srAzPz(sa_p2_a1_29) );
            lbx(p->t_zkpTpn, srAzPz(sa_p2_a1_29) );

            lbx(p->and_potPit, srAzPz(sa_algAz1_30) );
            lbx(p->and_potPit, srAzPz(sa_algAz1_17) );
            lbx(p->t_medGcn1_1, srAzPz(sa_p1_a1_20) );
            lbx(p->t_medGcn2_1, srAzPz(sa_p2_a1_20) );
            lbx(p->t_medGcn3_1, srAzPz(sa_p3_a1_20) );
            lbx(p->t_medGcn4_1, srAzPz(sa_p4_a1_20) );
            lbx(p->threeFour, srAzPz(sa_p5_a1_20) );
            lbx(p->and_N_more5, srAzPz(sa_algAz1_20) );

            lbx(p->t_medGcn1_2, srAzPz(sa_p1_a1_21) );
            lbx(p->t_medGcn2_2, srAzPz(sa_p2_a1_21) );
            lbx(p->t_medGcn3_2, srAzPz(sa_p3_a1_21) );
            lbx(p->t_medGcn4_2, srAzPz(sa_p4_a1_21) );
            lbx(p->or_medGcn, srAzPz(sa_p5_a1_21) );
            lbx(p->and_N_more75, srAzPz(sa_algAz1_21) );

            lbx(p->and_potPit, srAzPz(sa_p_a1_30) );
            lbx(p->or_potPit1Rtzo, srAzPz(sa_algAz1_30) );




            // выходные
            lbx(p->opAz1_20_srab153, srAz(sa_algAz1_20) ); // сраб алгоритма в 153
            grnGr(p->opAz1_20_isprPkc, bitD(pkcNeiprStart,pkc_na_az20 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_21_srab153, srAz(sa_algAz1_21) ); // сраб алгоритма в 153
            grnGr(p->opAz1_21_isprPkc, bitD(pkcNeiprStart,pkc_na_az21 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_29_srab153, srAz(sa_algAz1_29) ); // сраб алгоритма в 153
            grnGr(p->opAz1_29_isprPkc, bitD(pkcNeiprStart,pkc_na_az29 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_22_srab153, srAz(sa_algAz1_22) ); // сраб алгоритма в 153
            grnGr(p->opAz1_22_isprPkc, bitD(pkcNeiprStart,pkc_na_az22 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_23_srab153, srAz(sa_algAz1_23) ); // сраб алгоритма в 153
            grnGr(p->opAz1_23_isprPkc, bitD(pkcNeiprStart,pkc_na_az23 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz1_30_srab153, srAz(sa_algAz1_30) ); // сраб алгоритма в 153
            grnGr(p->opAz1_30_isprPkc, bitD(pkcNeiprStart,pkc_na_az30 ) ); // испр при авт контроле в ПКЦ

            // ШАК
            paintShak(p);

            break;
        }
        case 6: {
           Ui_f6* p = ((f6*)(fa[n]))->ui;

           // дискр входы
           lbx( p->ip_i151_potPitSuz, bit(p151_1, p151_in + p151_Shp6_PotPitSuz) ); // наличие сигнала на входе
           lb_ispLs( p->ip_ispr_potPitSuz, bit(p151_1, p151_neisp + p151_Shp6_PotPitSuz), bit(p151_1, p151_sost + p151_Shp6_PotPitSuz) ); // неисправность
           lbx( p->ip_res_potPitSuz, bit(p151_1, p151_res + p151_Shp6_PotPitSuz) ); // результирующий
           lby( p->ip_imblk_potPitSuz, bit(p151_1, p151_imit + p151_Shp6_PotPitSuz) | bit(p151_1, p151_blk + p151_Shp6_PotPitSuz)); // имитация и блокировка
           lbx( p->ip_i153_potPitSuz, srAz(sa_PotPitSuz_A) ); // входной в 153/154

           lbx( p->ip_i151_seism, bit(p151_2, p151_in + p151_Sd_SeismVozdMore4) ); // наличие сигнала на входе
           lb_ispLs( p->ip_ispr_seism, bit(p151_2, p151_neisp + p151_Sd_SeismVozdMore4), bit(p151_2, p151_sost + p151_Sd_SeismVozdMore4) ); // неисправность
           lbx( p->ip_res_seism, bit(p151_2, p151_res + p151_Sd_SeismVozdMore4) ); // результирующий
           lby( p->ip_imblk_seism, bit(p151_2, p151_imit + p151_Sd_SeismVozdMore4) | bit(p151_2, p151_blk + p151_Sd_SeismVozdMore4)); // имитация и блокировка
           lbx( p->ip_i153_seism, srAz(sa_seism_A) ); // входной в 153/154

           // выход за диап
           p->out_dPreak->setText(s2D1(ppn_dPreak));
           p->out_Tgn1->setText(s2D2(ppn_tp_gn1, ppn_tsp_gn1));
           p->out_Tgn2->setText(s2D2(ppn_tp_gn2, ppn_tsp_gn2));
           p->out_Tgn3->setText(s2D2(ppn_tp_gn3, ppn_tsp_gn3));
           p->out_Tgn4->setText(s2D2(ppn_tp_gn4, ppn_tsp_gn4));
           // значения
           p->val_dPreak->setText( getW153(tap_dPreak, bit(p157_1, ppn_dPreak) | bit(pkc, pkcAi_dPreak) ) );
           p->val_Tgn1->setText( getW153(tap_Tgn1, bit(p157_1, ppn_tp_gn1) | bit(p157_1, ppn_tsp_gn1) | bit(pkc, pkcAi_Tgn1Tep) | bit(pkc, pkcAi_Tgn1Tsp) ) );
           p->val_Tgn2->setText( getW153(tap_Tgn2, bit(p157_1, ppn_tp_gn1) | bit(p157_1, ppn_tsp_gn2) | bit(pkc, pkcAi_Tgn2Tep) | bit(pkc, pkcAi_Tgn2Tsp) ) );
           p->val_Tgn3->setText( getW153(tap_Tgn3, bit(p157_1, ppn_tp_gn1) | bit(p157_1, ppn_tsp_gn3) | bit(pkc, pkcAi_Tgn3Tep) | bit(pkc, pkcAi_Tgn3Tsp) ) );
           p->val_Tgn4->setText( getW153(tap_Tgn4, bit(p157_1, ppn_tp_gn1) | bit(p157_1, ppn_tsp_gn4) | bit(pkc, pkcAi_Tgn4Tep) | bit(pkc, pkcAi_Tgn4Tsp) ) );

           // уставки
           lbUst(p->u_dPreak_more4_5, srAz(sa_dPreak_more4_500_A), getW153(tau_dPreak_more4_500), more);
           lbUst(p->u_Tgn1_more330, srAz(sa_Tgn1_more330_0_A), getW153(tau_Tgn1_more330_0), more);
           lbUst(p->u_Tgn2_more330, srAz(sa_Tgn2_more330_0_A), getW153(tau_Tgn2_more330_0), more);
           lbUst(p->u_Tgn3_more330, srAz(sa_Tgn3_more330_0_A), getW153(tau_Tgn3_more330_0), more);
           lbUst(p->u_Tgn4_more330, srAz(sa_Tgn4_more330_0_A), getW153(tau_Tgn4_more330_0), more);

           // входы >=2
           lbm(p->m_dPreak_1, srM(sa_dPreak_more4_500_A, pkcImAz_dPreak_more4_500));
           lbm(p->m_dPreak_2, srM(sa_dPreak_more4_500_B, pkcImAz_dPreak_more4_500));
           lbm(p->m_dPreak_3, srM(sa_dPreak_more4_500_C, pkcImAz_dPreak_more4_500));
           lbm(p->m_dPreak_m, srM(sa_dPreak_more4_500_M));

           lbm(p->m_potPitSuz_1, srM(sa_PotPitSuz_A, pkcImAz_ShP6_PotPitSuz));
           lbm(p->m_potPitSuz_2, srM(sa_PotPitSuz_B, pkcImAz_ShP6_PotPitSuz));
           lbm(p->m_potPitSuz_3, srM(sa_PotPitSuz_C, pkcImAz_ShP6_PotPitSuz));
           lbm(p->m_potPitSuz_m, srM(sa_PotPitSuz_M));

           lbm(p->m_Seismika_more4_1, srM(sa_seism_A, pkcImAz_SeismVozd_more4));
           lbm(p->m_Seismika_more4_2, srM(sa_seism_B, pkcImAz_SeismVozd_more4));
           lbm(p->m_Seismika_more4_3, srM(sa_seism_C, pkcImAz_SeismVozd_more4));
           lbm(p->m_Seismika_more4_m, srM(sa_seism_M));

           lbm(p->m_Tgn1_more330_1, srM(sa_Tgn1_more330_0_A, pkcImAz_Tgn1_more330_0));
           lbm(p->m_Tgn1_more330_2, srM(sa_Tgn1_more330_0_B, pkcImAz_Tgn1_more330_0));
           lbm(p->m_Tgn1_more330_3, srM(sa_Tgn1_more330_0_C, pkcImAz_Tgn1_more330_0));
           lbm(p->m_Tgn1_more330_m, srM(sa_Tgn1_more330_0_M));

           lbm(p->m_Tgn2_more330_1, srM(sa_Tgn2_more330_0_A, pkcImAz_Tgn2_more330_0));
           lbm(p->m_Tgn2_more330_2, srM(sa_Tgn2_more330_0_B, pkcImAz_Tgn2_more330_0));
           lbm(p->m_Tgn2_more330_3, srM(sa_Tgn2_more330_0_C, pkcImAz_Tgn2_more330_0));
           lbm(p->m_Tgn2_more330_m, srM(sa_Tgn2_more330_0_M));

           lbm(p->m_Tgn3_more330_1, srM(sa_Tgn3_more330_0_A, pkcImAz_Tgn3_more330_0));
           lbm(p->m_Tgn3_more330_2, srM(sa_Tgn3_more330_0_B, pkcImAz_Tgn3_more330_0));
           lbm(p->m_Tgn3_more330_3, srM(sa_Tgn3_more330_0_C, pkcImAz_Tgn3_more330_0));
           lbm(p->m_Tgn3_more330_m, srM(sa_Tgn3_more330_0_M));

           lbm(p->m_Tgn4_more330_1, srM(sa_Tgn4_more330_0_A, pkcImAz_Tgn4_more330_0));
           lbm(p->m_Tgn4_more330_2, srM(sa_Tgn4_more330_0_B, pkcImAz_Tgn4_more330_0));
           lbm(p->m_Tgn4_more330_3, srM(sa_Tgn4_more330_0_C, pkcImAz_Tgn4_more330_0));
           lbm(p->m_Tgn4_more330_m, srM(sa_Tgn4_more330_0_M));

           // Значения таймеров
           p->t_potPit->setText(tmAz(tat_Timer1s6));
           // посдветка логич.эл-тов и таймеров
           lbx(p->t_potPit, srAzPz(sa_algAz1_19) );

           lbx(p->or_Tgrcn, srAzPz(sa_algAz1_32) );

           lbx(p->opAz1_4_srab153, srAz(sa_algAz1_4) ); // сраб алгоритма в 153
           grnGr(p->opAz1_4_isprPkc, bitD(pkcNeiprStart,pkc_na_az4 ) ); // испр при авт контроле в ПКЦ

           lbx(p->opAz1_19_srab153, srAz(sa_algAz1_19) ); // сраб алгоритма в 153
           grnGr(p->opAz1_19_isprPkc, bitD(pkcNeiprStart,pkc_na_az19 ) ); // испр при авт контроле в ПКЦ

           lbx(p->opAz1_27_srab153, srAz(sa_algAz1_27) ); // сраб алгоритма в 153
           grnGr(p->opAz1_27_isprPkc, bitD(pkcNeiprStart,pkc_na_az27 ) ); // испр при авт контроле в ПКЦ

           lbx(p->opAz1_32_srab153, srAz(sa_algAz1_32) ); // сраб алгоритма в 153
           grnGr(p->opAz1_32_isprPkc, bitD(pkcNeiprStart,pkc_na_az32 ) ); // испр при авт контроле в ПКЦ

           // ШАК
           paintShak(p);

           break;
       }
       case 7: {
           //Ui_f7* p = ((f7*)(fa[n]))->ui;
           // выход за диап
           //p->out_dPreak->setText(s2D1(ppn_MedGcn1));
           // значения

           //p->val_Tgn1->setText( getW153(tap_Tgn1) );

           // уставки
           //lbUst(p->u_Tgn1_more330, srAz(sa_Tgn1_more330_0_A), getW153(tau_Tgn1_more330_0), more);

           // входы >=2
           //lbx(p->m_dPreak_1, srAzPz(sa_dPreak_more4_500_A));

           // логич.эл-ты и таймеры
           //lbx(p->t_potPit, srAzPz(sa_algAz1_32) );

           // ШАК
           //paintShak(p);

           break;
       }
       case 8: {
            Ui_f8* p = ((f8*)(fa[n]))->ui;

            // дискр входы:
            // наличие сигнала на входе
            lbx( p->ip_i151_T_less20Az3, bit(p151_1, p151_in + p151_Aknp_T_less20Az3) );
            lbx( p->ip_i151_N_lessNzadAz3, bit(p151_1, p151_in + p151_Aknp_N_moreNustAz3) );
            // неисправность
            lb_ispLs( p->ip_ispr_T_less20Az3, bit(p151_1, p151_neisp + p151_Aknp_T_less20Az3), bit(p151_1, p151_sost + p151_Aknp_T_less20Az3) );
            lb_ispLs( p->ip_ispr_N_lessNzadAz3, bit(p151_1, p151_neisp + p151_Aknp_N_moreNustAz3), bit(p151_1, p151_sost + p151_Aknp_N_moreNustAz3) );
            // результирующий
            lbx( p->ip_res_T_less20Az3, bit(p151_1, p151_res + p151_Aknp_T_less20Az3) );
            lbx( p->ip_res_N_lessNzadAz3, bit(p151_1, p151_res + p151_Aknp_N_moreNustAz3) );
            // имитация и блокировка
            lby( p->ip_imblk_T_less20Az3, bit(p151_1, p151_imit + p151_Aknp_T_less20Az3) | bit(p151_1, p151_blk + p151_Aknp_T_less20Az3));
            lby( p->ip_imblk_N_lessNzadAz3, bit(p151_1, p151_imit + p151_Aknp_N_moreNustAz3) | bit(p151_1, p151_blk + p151_Aknp_N_moreNustAz3) );
            // входной в 153/154
            lbx( p->ip_i153_T_less20Az3, srPz(sp_T_less20_az3_A) );
            lbx( p->ip_i153_N_lessNzadAz3 , srPz(sp_N_moreNust_az3_A) );

            lbx( p->ip_i151_Rom, bit(p151_2, p151_in + p151_Arom_ObobRazgr) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_Rom, bit(p151_2, p151_neisp + p151_Arom_ObobRazgr), bit(p151_2, p151_sost + p151_Arom_ObobRazgr) ); // неисправность
            lbx( p->ip_res_Rom, bit(p151_2, p151_res + p151_Arom_ObobRazgr) ); // результирующий
            lby( p->ip_imblk_Rom, bit(p151_2, p151_imit + p151_Arom_ObobRazgr) | bit(p151_2, p151_blk + p151_Arom_ObobRazgr)); // имитация и блокировка
            lbx( p->ip_i153_Rom, srPz(sp_Arom_OR_A) ); // входной в 153/154

            // выход за диап
            p->out_Preak->setText(s2D1(ppn_Preak));
            p->out_Tgn1->setText(s2D2(ppn_tp_gn1, ppn_tsp_gn1));
            p->out_Tgn2->setText(s2D2(ppn_tp_gn2, ppn_tsp_gn2));
            p->out_Tgn3->setText(s2D2(ppn_tp_gn3, ppn_tsp_gn3));
            p->out_Tgn4->setText(s2D2(ppn_tp_gn4, ppn_tsp_gn4));
            p->out_Pgpk->setText(s2D1(ppn_Pgpk));
            // значения
            p->val_Preak->setText( getW153(tpp_Preak, bit(p157_1, ppn_Ppg1) | bit(pkc, pkcAi_Preak )) );
            p->val_Tgn1->setText( getW153(tpp_Tgn1,bit(p157_1, ppn_tp_gn1) | bit(p157_1, ppn_tsp_gn1) | bit(pkc, pkcAi_Tgn1Tep) | bit(pkc, pkcAi_Tgn1Tsp)) );
            p->val_Tgn2->setText( getW153(tpp_Tgn2,bit(p157_1, ppn_tp_gn2) | bit(p157_1, ppn_tsp_gn2) | bit(pkc, pkcAi_Tgn2Tep) | bit(pkc, pkcAi_Tgn2Tsp)) );
            p->val_Tgn3->setText( getW153(tpp_Tgn3,bit(p157_1, ppn_tp_gn3) | bit(p157_1, ppn_tsp_gn3) | bit(pkc, pkcAi_Tgn3Tep) | bit(pkc, pkcAi_Tgn3Tsp)) );
            p->val_Tgn4->setText( getW153(tpp_Tgn4,bit(p157_1, ppn_tp_gn4) | bit(p157_1, ppn_tsp_gn4) | bit(pkc, pkcAi_Tgn4Tep) | bit(pkc, pkcAi_Tgn4Tsp)) );

            p->val_Pgpk->setText( getW153(tpp_Pgpk, bit(p157_1, ppn_Pgpk) | bit(pkc, pkcAi_Pgpk)) );
            // уставки
            lbUst(p->u_Preak_more175, srPz(sp_Preak_more175_0_A), getW153(tpu_Preak_more175_0), more);
            lbUst(p->u_Tgn1_more327, srPz(sp_Tgn1_more327_0_A), getW153(tpu_Tgn1_more327_0), more);
            lbUst(p->u_Tgn2_more327, srPz(sp_Tgn2_more327_0_A), getW153(tpu_Tgn2_more327_0), more);
            lbUst(p->u_Tgn3_more327, srPz(sp_Tgn3_more327_0_A), getW153(tpu_Tgn3_more327_0), more);
            lbUst(p->u_Tgn4_more327, srPz(sp_Tgn4_more327_0_A), getW153(tpu_Tgn4_more327_0), more);
            lbUst(p->u_Pgpk_more70, srPz(sp_Pgpk_more70_0_A), getW153(tpu_Pgpk_more70_0), more);

            // входы >=2
            lbm(p->m_T_less20_1, srM(sp_T_less20_az3_A, pkcImPz_Aknp_T_less20_Az3));
            lbm(p->m_T_less20_2, srM(sp_T_less20_az3_B, pkcImPz_Aknp_T_less20_Az3));
            lbm(p->m_T_less20_3, srM(sp_T_less20_az3_C, pkcImPz_Aknp_T_less20_Az3));
            lbm(p->m_T_less20_m, srM(sp_T_less20_az3_M));

            lbm(p->m_N_lessN_1, srM(sp_N_moreNust_az3_A, pkcImPz_Aknp_Nrd_moreNust_Az3));
            lbm(p->m_N_lessN_2, srM(sp_N_moreNust_az3_B, pkcImPz_Aknp_Nrd_moreNust_Az3));
            lbm(p->m_N_lessN_3, srM(sp_N_moreNust_az3_C, pkcImPz_Aknp_Nrd_moreNust_Az3));
            lbm(p->m_N_lessN_m, srM(sp_N_moreNust_az3_M));

            lbm(p->m_Preak_more175_1,  srM(sp_Preak_more175_0_A, pkcImPz_Preak_more175_0));
            lbm(p->m_Preak_more175_2,  srM(sp_Preak_more175_0_B, pkcImPz_Preak_more175_0));
            lbm(p->m_Preak_more175_3,  srM(sp_Preak_more175_0_C, pkcImPz_Preak_more175_0));
            lbm(p->m_Preak_more175_m,  srM(sp_Preak_more175_0_M));

            lbm(p->m_Tgn1_more327_1,  srM(sp_Tgn1_more327_0_A, pkcImPz_Tgn1_more327_0 ));
            lbm(p->m_Tgn1_more327_2,  srM(sp_Tgn1_more327_0_B, pkcImPz_Tgn1_more327_0));
            lbm(p->m_Tgn1_more327_3,  srM(sp_Tgn1_more327_0_C, pkcImPz_Tgn1_more327_0));
            lbm(p->m_Tgn1_more327_m,  srM(sp_Tgn1_more327_0_M));

            lbm(p->m_Tgn2_more327_1,  srM(sp_Tgn2_more327_0_A, pkcImPz_Tgn2_more327_0));
            lbm(p->m_Tgn2_more327_2,  srM(sp_Tgn2_more327_0_B, pkcImPz_Tgn2_more327_0));
            lbm(p->m_Tgn2_more327_3,  srM(sp_Tgn2_more327_0_C, pkcImPz_Tgn2_more327_0));
            lbm(p->m_Tgn2_more327_m,  srM(sp_Tgn2_more327_0_M));

            lbm(p->m_Tgn3_more327_1,  srM(sp_Tgn3_more327_0_A, pkcImPz_Tgn3_more327_0));
            lbm(p->m_Tgn3_more327_2,  srM(sp_Tgn3_more327_0_B, pkcImPz_Tgn3_more327_0));
            lbm(p->m_Tgn3_more327_3,  srM(sp_Tgn3_more327_0_C, pkcImPz_Tgn3_more327_0));
            lbm(p->m_Tgn3_more327_m,  srM(sp_Tgn3_more327_0_M));

            lbm(p->m_Tgn4_more327_1,  srM(sp_Tgn4_more327_0_A, pkcImPz_Tgn4_more327_0));
            lbm(p->m_Tgn4_more327_2,  srM(sp_Tgn4_more327_0_B, pkcImPz_Tgn4_more327_0));
            lbm(p->m_Tgn4_more327_3,  srM(sp_Tgn4_more327_0_C, pkcImPz_Tgn4_more327_0));
            lbm(p->m_Tgn4_more327_m,  srM(sp_Tgn4_more327_0_M));

            lbm(p->m_Pgpk_more70_1,  srM(sp_Pgpk_more70_0_A, pkcImPz_Pgpk_more70_00));
            lbm(p->m_Pgpk_more70_2,  srM(sp_Pgpk_more70_0_B, pkcImPz_Pgpk_more70_00));
            lbm(p->m_Pgpk_more70_3,  srM(sp_Pgpk_more70_0_C, pkcImPz_Pgpk_more70_00));
            lbm(p->m_Pgpk_more70_m,  srM(sp_Pgpk_more70_0_M));

            lbm(p->m_Rom_1,  srM(sp_Arom_OR_A, pkcImPz_Arom_ObobRazgr));
            lbm(p->m_Rom_2,  srM(sp_Arom_OR_B, pkcImPz_Arom_ObobRazgr));
            lbm(p->m_Rom_3,  srM(sp_Arom_OR_C, pkcImPz_Arom_ObobRazgr));
            lbm(p->m_Rom_m,  srM(sp_Arom_OR_M));

            // подсветка логич.эл-тов и таймеров
            lbx(p->or_Tgrcn, srAzPz(sp_algAz3_4) );

            // выходные
            lbx(p->opAz3_1_srab153, srPz(sp_algAz3_1) ); // сраб алгоритма в 153
            grnGr(p->opAz3_1_isprPkc, bitD(pkcNeiprStart,pkc_na_pz6 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz3_2_srab153, srPz(sp_algAz3_2) ); // сраб алгоритма в 153
            grnGr(p->opAz3_2_isprPkc, bitD(pkcNeiprStart,pkc_na_pz7 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz3_3_srab153, srPz(sp_algAz3_3) ); // сраб алгоритма в 153
            grnGr(p->opAz3_3_isprPkc, bitD(pkcNeiprStart,pkc_na_pz8 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz3_4_srab153, srPz(sp_algAz3_4) ); // сраб алгоритма в 153
            grnGr(p->opAz3_4_isprPkc, bitD(pkcNeiprStart,pkc_na_pz9 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz3_5_srab153, srPz(sp_algAz3_5) ); // сраб алгоритма в 153
            grnGr(p->opAz3_5_isprPkc, bitD(pkcNeiprStart,pkc_na_pz10 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opAz3_6_srab153, srPz(sp_algAz3_6) ); // сраб алгоритма в 153
            grnGr(p->opAz3_6_isprPkc, bitD(pkcNeiprStart,pkc_na_pz11 ) ); // испр при авт контроле в ПКЦ

            // шак
            lbx(p->shak11_srabZasch153, srPz(sp_obobAz3) );
            lbx(p->shak11_res155, bit(p155pz1, p155_1pz_Shak1_1Az3 + p155_res) );
            lby(p->shak11_imblk155, bit(p155pz1, p155_1pz_Shak1_1Az3 + p155_imit) || bit(p155pz1, p155_1pz_Shak1_1Az3 + p155_blk) );
            grnGr(p->shak11_ispr155, bit(p155pz1, p155_1pz_Shak1_1Az3 + p155_ispr) );

            lbx(p->shak12_srabZasch153, srPz(sp_obobAz3) );
            lbx(p->shak12_res155, bit(p155pz2, p155_2pz_Shak1_2Az3 + p155_res) );
            lby(p->shak12_imblk155, bit(p155pz2, p155_2pz_Shak1_2Az3 + p155_imit) || bit(p155pz2, p155_1pz_Shak1_1Az3 + p155_blk) );
            grnGr(p->shak12_ispr155, bit(p155pz2, p155_2pz_Shak1_2Az3 + p155_ispr) );

            lbx(p->shak21_srabZasch153, srPz(sp_obobAz3) );
            lbx(p->shak21_res155, bit(p155pz1, p155_1pz_Shak2_1Az3 + p155_res) );
            lby(p->shak21_imblk155, bit(p155pz1, p155_1pz_Shak2_1Az3 + p155_imit) || bit(p155pz1, p155_1pz_Shak2_1Az3 + p155_blk) );
            grnGr(p->shak21_ispr155, bit(p155pz1, p155_1pz_Shak2_1Az3 + p155_ispr) );

            lbx(p->shak22_srabZasch153, srPz(sp_obobAz3) );
            lbx(p->shak22_res155, bit(p155pz2, p155_2pz_Shak2_2Az3 + p155_res) );
            lby(p->shak22_imblk155, bit(p155pz2, p155_2pz_Shak2_2Az3 + p155_imit) || bit(p155pz2, p155_2pz_Shak2_2Az3 + p155_blk) );
            grnGr(p->shak22_ispr155, bit(p155pz2, p155_2pz_Shak2_2Az3 + p155_ispr) );

            if(oneOrTwo[pkc]==0) { // для 1 комплекта
                p->shak11_res155->setText("ШАК 1.1");
                p->shak12_res155->setText("ШАК 1.2");
                p->shak21_res155->setText("ШАК 2.1");
                p->shak22_res155->setText("ШАК 2.2");
            }
            else { // для 2 комплекта
                p->shak11_res155->setText("ШАК 3.1");
                p->shak12_res155->setText("ШАК 3.2");
                p->shak21_res155->setText("ШАК 4.1");
                p->shak22_res155->setText("ШАК 4.2");
            }


            break;
        }
        case 9: {
            // дискр входы:
             Ui_f9* p = ((f9*)(fa[n]))->ui;

             lbx( p->ip_i151_T_less20Az4, bit(p151_1, p151_in + p151_Aknp_T_less20Az4) );
             lb_ispLs( p->ip_ispr_T_less20Az4, bit(p151_1, p151_neisp + p151_Aknp_T_less20Az4), bit(p151_1, p151_sost + p151_Aknp_T_less20Az4) );// неисправность
             lbx( p->ip_res_T_less20Az4, bit(p151_1, p151_res + p151_Aknp_T_less20Az4) );// результирующий
             lby( p->ip_imblk_T_less20Az4, bit(p151_1, p151_imit + p151_Aknp_T_less20Az4) | bit(p151_1, p151_blk + p151_Aknp_T_less20Az4));// имитация и блокировка
             lbx( p->ip_i153_T_less20Az4, srPz(sp_T_less20_az4_A) );// входной в 153/154

             lbx( p->ip_i151_PadOr, bit(p151_1, p151_in + p151_Shlos_PadenieOr) );
             lb_ispLs( p->ip_ispr_PadOr, bit(p151_1, p151_neisp + p151_Shlos_PadenieOr), bit(p151_1, p151_sost + p151_Shlos_PadenieOr) );// неисправность
             lbx( p->ip_res_PadOr, bit(p151_1, p151_res + p151_Shlos_PadenieOr) );// результирующий
             lby( p->ip_imblk_PadOr, bit(p151_1, p151_imit + p151_Shlos_PadenieOr) | bit(p151_1, p151_blk + p151_Shlos_PadenieOr));// имитация и блокировка
             lbx( p->ip_i153_PadOr, srPz(sp_padenieOR_A) );// входной в 153/154

             // выход за диап
             p->out_Preak->setText(s2D1(ppn_Preak));
             p->out_Tgn1->setText(s2D2(ppn_tp_gn1, ppn_tsp_gn1));
             p->out_Tgn2->setText(s2D2(ppn_tp_gn2, ppn_tsp_gn2));
             p->out_Tgn3->setText(s2D2(ppn_tp_gn3, ppn_tsp_gn3));
             p->out_Tgn4->setText(s2D2(ppn_tp_gn4, ppn_tsp_gn4));
             // значения
             p->val_Preak->setText( getW153(tpp_Preak, bit(p157_1, ppn_Ppg1) | bit(pkc, pkcAi_Preak )) );
             p->val_Tgn1->setText( getW153(tpp_Tgn1,bit(p157_1, ppn_tp_gn1) | bit(p157_1, ppn_tsp_gn1) | bit(pkc, pkcAi_Tgn1Tep) | bit(pkc, pkcAi_Tgn1Tsp)) );
             p->val_Tgn2->setText( getW153(tpp_Tgn2,bit(p157_1, ppn_tp_gn2) | bit(p157_1, ppn_tsp_gn2) | bit(pkc, pkcAi_Tgn2Tep) | bit(pkc, pkcAi_Tgn2Tsp)) );
             p->val_Tgn3->setText( getW153(tpp_Tgn3,bit(p157_1, ppn_tp_gn3) | bit(p157_1, ppn_tsp_gn3) | bit(pkc, pkcAi_Tgn3Tep) | bit(pkc, pkcAi_Tgn3Tsp)) );
             p->val_Tgn4->setText( getW153(tpp_Tgn4,bit(p157_1, ppn_tp_gn4) | bit(p157_1, ppn_tsp_gn4) | bit(pkc, pkcAi_Tgn4Tep) | bit(pkc, pkcAi_Tgn4Tsp)) );

             // уставки
             lbUst(p->u_Preak_more165, srPz(sp_Preak_more165_0_A), getW153(tpu_Preak_more165_0), more);
             lbUst(p->u_Tgn1_more324, srPz(sp_Tgn1_more324_0_A), getW153(tpu_Tgn1_more324_0), more);
             lbUst(p->u_Tgn2_more324, srPz(sp_Tgn2_more324_0_A), getW153(tpu_Tgn2_more324_0), more);
             lbUst(p->u_Tgn3_more324, srPz(sp_Tgn3_more324_0_A), getW153(tpu_Tgn3_more324_0), more);
             lbUst(p->u_Tgn4_more324, srPz(sp_Tgn4_more324_0_A), getW153(tpu_Tgn4_more324_0), more);

             // входы >=2
             lbm(p->m_T_less20az4_1, srM(sp_T_less20_az4_A, pkcImPz_T_less20_Az4));
             lbm(p->m_T_less20az4_2, srM(sp_T_less20_az4_B, pkcImPz_T_less20_Az4));
             lbm(p->m_T_less20az4_3, srM(sp_T_less20_az4_C, pkcImPz_T_less20_Az4));
             lbm(p->m_T_less20az4_m, srM(sp_T_less20_az4_M));

             lbm(p->m_Preak_more165_1,  srM(sp_Preak_more165_0_A, pkcImPz_Preak_more165_0));
             lbm(p->m_Preak_more165_2,  srM(sp_Preak_more165_0_B, pkcImPz_Preak_more165_0));
             lbm(p->m_Preak_more165_3,  srM(sp_Preak_more165_0_C, pkcImPz_Preak_more165_0));
             lbm(p->m_Preak_more165_m,  srM(sp_Preak_more165_0_M));

             lbm(p->m_Tgn1_more324_1,  srM(sp_Tgn1_more324_0_A, pkcImPz_Tgn1_more324_0));
             lbm(p->m_Tgn1_more324_2,  srM(sp_Tgn1_more324_0_B, pkcImPz_Tgn1_more324_0));
             lbm(p->m_Tgn1_more324_3,  srM(sp_Tgn1_more324_0_C, pkcImPz_Tgn1_more324_0));
             lbm(p->m_Tgn1_more324_m,  srM(sp_Tgn1_more324_0_M));

             lbm(p->m_Tgn2_more324_1,  srM(sp_Tgn2_more324_0_A, pkcImPz_Tgn2_more324_0));
             lbm(p->m_Tgn2_more324_2,  srM(sp_Tgn2_more324_0_B, pkcImPz_Tgn2_more324_0));
             lbm(p->m_Tgn2_more324_3,  srM(sp_Tgn2_more324_0_C, pkcImPz_Tgn2_more324_0));
             lbm(p->m_Tgn2_more324_m,  srM(sp_Tgn2_more324_0_M));

             lbm(p->m_Tgn3_more324_1,  srM(sp_Tgn3_more324_0_A, pkcImPz_Tgn3_more324_0));
             lbm(p->m_Tgn3_more324_2,  srM(sp_Tgn3_more324_0_B, pkcImPz_Tgn3_more324_0));
             lbm(p->m_Tgn3_more324_3,  srM(sp_Tgn3_more324_0_C, pkcImPz_Tgn3_more324_0));
             lbm(p->m_Tgn3_more324_m,  srM(sp_Tgn3_more324_0_M));

             lbm(p->m_Tgn4_more324_1,  srM(sp_Tgn4_more324_0_A, pkcImPz_Tgn4_more324_0));
             lbm(p->m_Tgn4_more324_2,  srM(sp_Tgn4_more324_0_B, pkcImPz_Tgn4_more324_0));
             lbm(p->m_Tgn4_more324_3,  srM(sp_Tgn4_more324_0_C, pkcImPz_Tgn4_more324_0));
             lbm(p->m_Tgn4_more324_m,  srM(sp_Tgn4_more324_0_M));

             lbm(p->m_padenieOR_1,  srM(sp_padenieOR_A, pkcImPz_PadenieOr));
             lbm(p->m_padenieOR_2,  srM(sp_padenieOR_B, pkcImPz_PadenieOr));
             lbm(p->m_padenieOR_3,  srM(sp_padenieOR_C, pkcImPz_PadenieOr));
             lbm(p->m_padenieOR_m,  srM(sp_padenieOR_M));

             // логич.эл-ты и таймеры
             lbx(p->or_Tgrcn, srAzPz(sp_algAz4_4) );

             // выходные
             lbx(p->opAz4_1_srab153, srPz(sp_algAz4_1) ); // сраб алгоритма в 153
             grnGr(p->opAz4_1_isprPkc, bitD(pkcNeiprStart,pkc_na_pz11 ) ); // испр при авт контроле в ПКЦ

             lbx(p->opAz4_2_srab153, srPz(sp_algAz4_2) ); // сраб алгоритма в 153
             grnGr(p->opAz4_2_isprPkc, bitD(pkcNeiprStart,pkc_na_pz12 ) ); // испр при авт контроле в ПКЦ

             lbx(p->opAz4_3_srab153, srPz(sp_algAz4_3) ); // сраб алгоритма в 153
             grnGr(p->opAz4_3_isprPkc, bitD(pkcNeiprStart,pkc_na_pz13 ) ); // испр при авт контроле в ПКЦ

             lbx(p->opAz4_4_srab153, srPz(sp_algAz4_4) ); // сраб алгоритма в 153
             grnGr(p->opAz4_4_isprPkc, bitD(pkcNeiprStart,pkc_na_pz14 ) ); // испр при авт контроле в ПКЦ

             // ШАК
             lbx(p->shak11_srabZasch153, srPz(sp_obobAz4) );
             lbx(p->shak11_res155, bit(p155pz1, p155_1pz_Shak1_1Az4 + p155_res) );
             lby(p->shak11_imblk155, bit(p155pz1, p155_1pz_Shak1_1Az4 + p155_imit) || bit(p155pz1, p155_1pz_Shak1_1Az4 + p155_blk) );
             grnGr(p->shak11_ispr155, bit(p155pz1, p155_1pz_Shak1_1Az4 + p155_ispr) );

             lbx(p->shak12_srabZasch153, srPz(sp_obobAz4) );
             lbx(p->shak12_res155, bit(p155pz2, p155_2pz_Shak1_2Az4 + p155_res) );
             lby(p->shak12_imblk155, bit(p155pz2, p155_2pz_Shak1_2Az4 + p155_imit) || bit(p155pz2, p155_2pz_Shak1_2Az4 + p155_blk) );
             grnGr(p->shak12_ispr155, bit(p155pz2, p155_2pz_Shak1_2Az4 + p155_ispr) );

             lbx(p->shak21_srabZasch153, srPz(sp_obobAz4) );
             lbx(p->shak21_res155, bit(p155pz1, p155_1pz_Shak2_1Az4 + p155_res) );
             lby(p->shak21_imblk155, bit(p155pz1, p155_1pz_Shak2_1Az4 + p155_imit) || bit(p155pz1, p155_1pz_Shak2_1Az4 + p155_blk) );
             grnGr(p->shak21_ispr155, bit(p155pz1, p155_1pz_Shak2_1Az4 + p155_ispr) );

             lbx(p->shak22_srabZasch153, srPz(sp_obobAz4) );
             lbx(p->shak22_res155, bit(p155pz2, p155_2pz_Shak2_2Az4 + p155_res) );
             lby(p->shak22_imblk155, bit(p155pz2, p155_2pz_Shak2_2Az4 + p155_imit) || bit(p155pz2, p155_2pz_Shak2_2Az4 + p155_blk) );
             grnGr(p->shak22_ispr155, bit(p155pz2, p155_2pz_Shak2_2Az4 + p155_ispr) );

             if(oneOrTwo[pkc]==0) { // для 1 комплекта
                 p->shak11_res155->setText("ШАК 1.1");
                 p->shak12_res155->setText("ШАК 1.2");
                 p->shak21_res155->setText("ШАК 2.1");
                 p->shak22_res155->setText("ШАК 2.2");
             }
             else { // для 2 комплекта
                 p->shak11_res155->setText("ШАК 3.1");
                 p->shak12_res155->setText("ШАК 3.2");
                 p->shak21_res155->setText("ШАК 4.1");
                 p->shak22_res155->setText("ШАК 4.2");
             }


             break;
        }
        case 10: {
            Ui_f10* p = ((f10*)(fa[n]))->ui;
            lbx( p->ip_i151_N_more75, bit(p151_1, p151_in + p151_Aknp_N_more75) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_N_more75, bit(p151_1, p151_neisp + p151_Aknp_N_more75), bit(p151_1, p151_sost + p151_Aknp_N_more75) ); // неисправность
            lbx( p->ip_res_N_more75, bit(p151_1, p151_res + p151_Aknp_N_more75) ); // результирующий
            lby( p->ip_imblk_N_more75, bit(p151_1, p151_imit + p151_Aknp_N_more75) | bit(p151_1, p151_blk + p151_Aknp_N_more75)); // имитация и блокировка
            lbx( p->ip_i153_N_more75, srPz(sp_N_more75_A) ); // входной в 153/154

            lbx( p->ip_i151_ZakrSk1, bit(p151_2, p151_in + p151_Shk2_Zakr1sk) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_ZakrSk1, bit(p151_2, p151_neisp + p151_Shk2_Zakr1sk), bit(p151_2, p151_sost + p151_Shk2_Zakr1sk) ); // неисправность
            lbx( p->ip_res_ZakrSk1, bit(p151_2, p151_res + p151_Shk2_Zakr1sk) ); // результирующий
            lby( p->ip_imblk_ZakrSk1, bit(p151_2, p151_imit + p151_Shk2_Zakr1sk) | bit(p151_2, p151_blk + p151_Shk2_Zakr1sk)); // имитация и блокировка
            lbx( p->ip_i153_ZakrSk1, srPz(sp_OTO_zakrSk1_A) ); // входной в 153/154

            lbx( p->ip_i151_ZakrSk2, bit(p151_2, p151_in + p151_Shk2_Zakr2sk) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_ZakrSk2, bit(p151_2, p151_neisp + p151_Shk2_Zakr2sk), bit(p151_2, p151_sost + p151_Shk2_Zakr2sk) ); // неисправность
            lbx( p->ip_res_ZakrSk2, bit(p151_2, p151_res + p151_Shk2_Zakr2sk) ); // результирующий
            lby( p->ip_imblk_ZakrSk2, bit(p151_2, p151_imit + p151_Shk2_Zakr2sk) | bit(p151_2, p151_blk + p151_Shk2_Zakr2sk)); // имитация и блокировка
            lbx( p->ip_i153_ZakrSk2, srPz(sp_OTO_zakrSk2_A) ); // входной в 153/154

            lbx( p->ip_i151_ZakrSk3, bit(p151_2, p151_in + p151_Shk2_Zakr3sk) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_ZakrSk3, bit(p151_2, p151_neisp + p151_Shk2_Zakr3sk), bit(p151_2, p151_sost + p151_Shk2_Zakr3sk) ); // неисправность
            lbx( p->ip_res_ZakrSk3, bit(p151_2, p151_res + p151_Shk2_Zakr3sk) ); // результирующий
            lby( p->ip_imblk_ZakrSk3, bit(p151_2, p151_imit + p151_Shk2_Zakr3sk) | bit(p151_2, p151_blk + p151_Shk2_Zakr3sk)); // имитация и блокировка
            lbx( p->ip_i153_ZakrSk3, srPz(sp_OTO_zakrSk3_A) ); // входной в 153/154

            lbx( p->ip_i151_ZakrSk4, bit(p151_2, p151_in + p151_Shk2_Zakr4sk) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_ZakrSk4, bit(p151_2, p151_neisp + p151_Shk2_Zakr4sk), bit(p151_2, p151_sost + p151_Shk2_Zakr4sk) ); // неисправность
            lbx( p->ip_res_ZakrSk4, bit(p151_2, p151_res + p151_Shk2_Zakr4sk) ); // результирующий
            lby( p->ip_imblk_ZakrSk4, bit(p151_2, p151_imit + p151_Shk2_Zakr4sk) | bit(p151_2, p151_blk + p151_Shk2_Zakr4sk)); // имитация и блокировка
            lbx( p->ip_i153_ZakrSk4, srPz(sp_OTO_zakrSk4_A) ); // входной в 153/154

            lbx( p->ip_i151_OtklEb, bit(p151_2, p151_in + p151_RSchg_OtklEb) ); // наличие сигнала на входе
            lb_ispLs( p->ip_ispr_OtklEb, bit(p151_2, p151_neisp + p151_RSchg_OtklEb), bit(p151_2, p151_sost + p151_RSchg_OtklEb) ); // неисправность
            lbx( p->ip_res_OtklEb, bit(p151_2, p151_res + p151_RSchg_OtklEb) ); // результирующий
            lby( p->ip_imblk_OtklEb, bit(p151_2, p151_imit + p151_RSchg_OtklEb) | bit(p151_2, p151_blk + p151_RSchg_OtklEb)); // имитация и блокировка
            lbx( p->ip_i153_OtklEb, srPz(sp_OTO_otkl_EB_A) ); // входной в 153/154

            // выход за диап
            p->out_MedGcn1->setText(s2D1(ppn_MedGcn1));
            p->out_MedGcn2->setText(s2D1(ppn_MedGcn2));
            p->out_MedGcn3->setText(s2D1(ppn_MedGcn3));
            p->out_MedGcn4->setText(s2D1(ppn_MedGcn4));
            p->out_PzkpTpn1->setText(s2D1(ppn_PzkpTpn1));
            p->out_PzkpTpn2->setText(s2D1(ppn_PzkpTpn2));
            // значения
            p->val_MedGcn1->setText( getW153(tpp_MedGcn1) );
            p->val_MedGcn2->setText( getW153(tpp_MedGcn2) );
            p->val_MedGcn3->setText( getW153(tpp_MedGcn3) );
            p->val_MedGcn4->setText( getW153(tpp_MedGcn4) );
            p->val_PzkpTpn1->setText( getW153(tpp_PzkpTpn1) );
            p->val_PzkpTpn2->setText( getW153(tpp_PzkpTpn2) );

            // значения таймеров
            p->t_MedGcn1->setText(tmPz(tpt_Timer1));
            p->t_MedGcn2->setText(tmPz(tpt_Timer2));
            p->t_MedGcn3->setText(tmPz(tpt_Timer3));
            p->t_MedGcn4->setText(tmPz(tpt_Timer4));
            p->t_PzkpTpn1 ->setText(tmPz(tpt_Timer5));
            p->t_PzkpTpn2->setText(tmPz(tpt_Timer6));

            // уставки
            lbUst(p->u_MedGcn1_less3000, srPz(sp_MedGcn1_less3000_A), getW153(tpu_MedGcn1_less3000), less);
            lbUst(p->u_MedGcn2_less3000, srPz(sp_MedGcn2_less3000_A), getW153(tpu_MedGcn2_less3000), less);
            lbUst(p->u_MedGcn3_less3000, srPz(sp_MedGcn3_less3000_A), getW153(tpu_MedGcn3_less3000), less);
            lbUst(p->u_MedGcn4_less3000, srPz(sp_MedGcn4_less3000_A), getW153(tpu_MedGcn4_less3000), less);
            lbUst(p->u_PzkpTpn1_less0_400, srPz(sp_PzkpTpn1_less400_0_A), getW153(tpu_PzkpTpn1_less0_400), less);
            lbUst(p->u_PzkpTpn2_less0_400, srPz(sp_PzkpTpn2_less400_0_A), getW153(tpu_PzkpTpn2_less0_400), less);

            // входы >=2
            lbm(p->m_N_more75_1, srM(sp_N_more75_A, pkcImPz_Aknp_N_more75));
            lbm(p->m_N_more75_2, srM(sp_N_more75_B, pkcImPz_Aknp_N_more75));
            lbm(p->m_N_more75_3, srM(sp_N_more75_C, pkcImPz_Aknp_N_more75));
            lbm(p->m_N_more75_m, srM(sp_N_more75_M));

            lbm(p->m_MedGcn1_1, srM(sp_MedGcn1_less3000_A, pkcImPz_MedGcn1_less3000));
            lbm(p->m_MedGcn1_2, srM(sp_MedGcn1_less3000_B, pkcImPz_MedGcn1_less3000));
            lbm(p->m_MedGcn1_3, srM(sp_MedGcn1_less3000_C, pkcImPz_MedGcn1_less3000));
            lbm(p->m_MedGcn1_m, srM(sp_MedGcn1_less3000_M));

            lbm(p->m_MedGcn2_1, srM(sp_MedGcn2_less3000_A, pkcImPz_MedGcn2_less3000));
            lbm(p->m_MedGcn2_2, srM(sp_MedGcn2_less3000_B, pkcImPz_MedGcn2_less3000));
            lbm(p->m_MedGcn2_3, srM(sp_MedGcn2_less3000_C, pkcImPz_MedGcn2_less3000));
            lbm(p->m_MedGcn2_m, srM(sp_MedGcn2_less3000_M));

            lbm(p->m_MedGcn3_1, srM(sp_MedGcn3_less3000_A, pkcImPz_MedGcn3_less3000));
            lbm(p->m_MedGcn3_2, srM(sp_MedGcn3_less3000_B, pkcImPz_MedGcn3_less3000));
            lbm(p->m_MedGcn3_3, srM(sp_MedGcn3_less3000_C, pkcImPz_MedGcn3_less3000));
            lbm(p->m_MedGcn3_m, srM(sp_MedGcn3_less3000_M));

            lbm(p->m_MedGcn4_1, srM(sp_MedGcn4_less3000_A, pkcImPz_MedGcn4_less3000));
            lbm(p->m_MedGcn4_2, srM(sp_MedGcn4_less3000_B, pkcImPz_MedGcn4_less3000));
            lbm(p->m_MedGcn4_3, srM(sp_MedGcn4_less3000_C, pkcImPz_MedGcn4_less3000));
            lbm(p->m_MedGcn4_m, srM(sp_MedGcn4_less3000_M));

            lbm(p->m_PzkpTpn1_1, srM(sp_PzkpTpn1_less400_0_A, pkcImPz_PzkpTpn1_less0_400));
            lbm(p->m_PzkpTpn1_2, srM(sp_PzkpTpn1_less400_0_B, pkcImPz_PzkpTpn1_less0_400));
            lbm(p->m_PzkpTpn1_3, srM(sp_PzkpTpn1_less400_0_C, pkcImPz_PzkpTpn1_less0_400));
            lbm(p->m_PzkpTpn1_m, srM(sp_PzkpTpn1_less400_0_M));

            lbm(p->m_PzkpTpn2_1, srM(sp_PzkpTpn2_less400_0_A, pkcImPz_PzkpTpn2_less0_400));
            lbm(p->m_PzkpTpn2_2, srM(sp_PzkpTpn2_less400_0_B, pkcImPz_PzkpTpn2_less0_400));
            lbm(p->m_PzkpTpn2_3, srM(sp_PzkpTpn2_less400_0_C, pkcImPz_PzkpTpn2_less0_400));
            lbm(p->m_PzkpTpn2_m, srM(sp_PzkpTpn2_less400_0_M));

            lbm(p->m_zakrSk1_1, srM(sp_OTO_zakrSk1_A, pkcImPz_ZakrSk1));
            lbm(p->m_zakrSk1_2, srM(sp_OTO_zakrSk1_B, pkcImPz_ZakrSk1));
            lbm(p->m_zakrSk1_3, srM(sp_OTO_zakrSk1_C, pkcImPz_ZakrSk1));
            lbm(p->m_zakrSk1_m, srM(sp_OTO_zakrSk1_M));

            lbm(p->m_zakrSk2_1, srM(sp_OTO_zakrSk2_A, pkcImPz_ZakrSk2));
            lbm(p->m_zakrSk2_2, srM(sp_OTO_zakrSk2_B, pkcImPz_ZakrSk2));
            lbm(p->m_zakrSk2_3, srM(sp_OTO_zakrSk2_C, pkcImPz_ZakrSk2));
            lbm(p->m_zakrSk2_m, srM(sp_OTO_zakrSk2_M));

            lbm(p->m_zakrSk3_1, srM(sp_OTO_zakrSk3_A, pkcImPz_ZakrSk3));
            lbm(p->m_zakrSk3_2, srM(sp_OTO_zakrSk3_B, pkcImPz_ZakrSk3));
            lbm(p->m_zakrSk3_3, srM(sp_OTO_zakrSk3_C, pkcImPz_ZakrSk3));
            lbm(p->m_zakrSk3_m, srM(sp_OTO_zakrSk3_M));

            lbm(p->m_zakrSk4_1, srM(sp_OTO_zakrSk4_A, pkcImPz_ZakrSk4));
            lbm(p->m_zakrSk4_2, srM(sp_OTO_zakrSk4_B, pkcImPz_ZakrSk4));
            lbm(p->m_zakrSk4_3, srM(sp_OTO_zakrSk4_C, pkcImPz_ZakrSk4));
            lbm(p->m_zakrSk4_m, srM(sp_OTO_zakrSk4_M));

            lbm(p->m_otklEb_1, srM(sp_OTO_otkl_EB_A, pkcImPz_OtklEb));
            lbm(p->m_otklEb_2, srM(sp_OTO_otkl_EB_B, pkcImPz_OtklEb));
            lbm(p->m_otklEb_3, srM(sp_OTO_otkl_EB_C, pkcImPz_OtklEb));
            lbm(p->m_otklEb_m, srM(sp_OTO_otkl_EB_M));


            // логич.эл-ты и таймеры
            lbx(p->t_MedGcn1, srAzPz(sp_pt1Urb1) );
            lbx(p->t_MedGcn2, srAzPz(sp_pt2Urb1) );
            lbx(p->t_MedGcn3, srAzPz(sp_pt3Urb1) );
            lbx(p->t_MedGcn4, srAzPz(sp_pt4Urb1) );

            lbx(p->or_Tgrcn, srAzPz(sp_pt5Urb1) );
            lbx(p->and_N_more75, srAzPz(sp_algUrb1) );

            lbx(p->t_PzkpTpn1, srAzPz(sp_pt1Urb2) );
            lbx(p->t_PzkpTpn2, srAzPz(sp_pt2Urb2) );
            lbx(p->or_Pzkp, srAzPz(sp_pt3Urb2) );
            lbx(p->and_PzkpTpn, srAzPz(sp_algUrb2) );

            lbx(p->twoFour, srAzPz(sp_ptUrb3) );
            lbx(p->and_zakrSk1, srAzPz(sp_algUrb3) );

            lbx(p->and_otklEb, srAzPz(sp_algUrb4) );

            // выходные

            lbx(p->opUrb1_srab153, srPz(sp_algUrb1) ); // сраб алгоритма в 153
            grnGr(p->opUrb1_isprPkc, bitD(pkcNeiprStart,pkc_na_pz7 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opUrb2_srab153, srPz(sp_algUrb2) ); // сраб алгоритма в 153
            grnGr(p->opUrb2_isprPkc, bitD(pkcNeiprStart,pkc_na_pz8 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opUrb3_srab153, srPz(sp_algUrb3) ); // сраб алгоритма в 153
            grnGr(p->opUrb3_isprPkc, bitD(pkcNeiprStart,pkc_na_pz9 ) ); // испр при авт контроле в ПКЦ

            lbx(p->opUrb4_srab153, srPz(sp_algUrb4) ); // сраб алгоритма в 153
            grnGr(p->opUrb4_isprPkc, bitD(pkcNeiprStart,pkc_na_pz10 ) ); // испр при авт контроле в ПКЦ

            // ШАК
            lbx(p->shak11_srabZasch153, srPz(sp_obobUrb) );
            lbx(p->shak11_res155, bit(p155pz1, p155_1pz_Shak1_1Urb + p155_res) );
            lby(p->shak11_imblk155, bit(p155pz1, p155_1pz_Shak1_1Urb + p155_imit) || bit(p155pz1, p155_1pz_Shak1_1Urb + p155_blk) );
            grnGr(p->shak11_ispr155, bit(p155pz1, p155_1pz_Shak1_1Urb + p155_ispr) );

            lbx(p->shak12_srabZasch153, srPz(sp_obobUrb) );
            lbx(p->shak12_res155, bit(p155pz2, p155_2pz_Shak1_2Urb + p155_res) );
            lby(p->shak12_imblk155, bit(p155pz2, p155_2pz_Shak1_2Urb + p155_imit) || bit(p155pz2, p155_2pz_Shak1_2Urb + p155_blk) );
            grnGr(p->shak12_ispr155, bit(p155pz2, p155_2pz_Shak1_2Urb + p155_ispr) );

            lbx(p->shak21_srabZasch153, srPz(sp_obobUrb) );
            lbx(p->shak21_res155, bit(p155pz1, p155_1pz_Shak2_1Urb + p155_res) );
            lby(p->shak21_imblk155, bit(p155pz1, p155_1pz_Shak2_1Urb + p155_imit) || bit(p155pz1, p155_1pz_Shak2_1Urb + p155_blk) );
            grnGr(p->shak21_ispr155, bit(p155pz1, p155_1pz_Shak2_1Urb + p155_ispr) );

            lbx(p->shak22_srabZasch153, srPz(sp_obobUrb) );
            lbx(p->shak22_res155, bit(p155pz2, p155_2pz_Shak2_2Urb + p155_res) );
            lby(p->shak22_imblk155, bit(p155pz2, p155_2pz_Shak2_2Urb + p155_imit) || bit(p155pz2, p155_2pz_Shak2_2Urb + p155_blk) );
            grnGr(p->shak22_ispr155, bit(p155pz2, p155_2pz_Shak2_2Urb + p155_ispr) );

            if(oneOrTwo[pkc]==0) { // для 1 комплекта
                p->shak11_res155->setText("ШАК 1.1");
                p->shak12_res155->setText("ШАК 1.2");
                p->shak21_res155->setText("ШАК 2.1");
                p->shak22_res155->setText("ШАК 2.2");
            }
            else { // для 2 комплекта
                p->shak11_res155->setText("ШАК 3.1");
                p->shak12_res155->setText("ШАК 3.2");
                p->shak21_res155->setText("ШАК 4.1");
                p->shak22_res155->setText("ШАК 4.2");
            }


            break;
        }
   }
}


