#ifndef FORM3_H
#define FORM3_H

#include <QWidget>
#include <QStackedWidget>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include "dataTableModel.h"
#include "ethtesttv.h"
#include <QString>
#include <QTimer>
#include <QLabel>
#include <QListWidget>
#include "formula.h"
//#include "dlgppn.h"
#include "ref.h"
#include "referenceppn.h"
#include "algsignals.h"
#include "algforms.h"
#define PpnPages 6;
#define PPnLabelPerPageMax 50

#include "./algSch/f1.h"
#include "./algSch/f2.h"
#include "./algSch/f3.h"
#include "./algSch/f4.h"
#include "./algSch/f5.h"
#include "./algSch/f6.h"
#include "./algSch/f7.h"
#include "./algSch/f8.h"
#include "./algSch/f9.h"
#include "./algSch/f10.h"
#include "./algSch/f11.h"
#include "./algSch/f12.h"
#include "./algSch/f13.h"
#include "./algSch/f14.h"
#include "./algSch/f15.h"
#include "./algSch/f16.h"
#include "./algSch/f17.h"
#include "refalg.h"


namespace Ui {
class Form3;
}

class Form3 : public QWidget, public Formula
{
    Q_OBJECT

private:
    int lastMousePosX, lastMousePosY;
    int dig[4], digCnt;
    static const int lbTblMAX = 24;
    QLabel *lbTbl[lbTblMAX]; // для таблицы

    QListWidget *lstAlg, *lstPorog, *lstSig;

public:

    QWidget *fa[AlgSlidesN]; //массив указателей для создания виджетов со слайдами алгоритмов
    QStackedWidget* algs;// схемы алгоритмов


    QLabel *lbFrameNum;

    explicit Form3(QWidget *parent = 0);
    ~Form3();
    bool eventFilter(QObject *target, QEvent *event);

    DataTableModel *mdlDataBus;
    EthTestTV *ethTestTV;

    // для желтой рамки
    void imit(QFrame *lb);

    // вспомогательные функции для отрисовки лэйблов и линий
    void ln(QFrame *lb, int redFlag, int yellowFlag);

    void ln2(QFrame *lb1, QFrame *lb2, int redFlag, int yellowFlag);

    void ln3(QFrame *lb1, QFrame *lb2,QFrame *lb3, int redFlag, int yellowFlag);

    void lb(QLabel *lb, int flag);
    void lbx(QLabel *lb, int flag); // для эл-тов И, ИЛИ и т.д. на сх.алг.

    void cl(QLabel *lb, int b1, int b2);

    void lb(QLabel *lb, QColor color);

    void red(QLabel *lb);

    void black(QLabel *lb);

    void pbColor(QPushButton *pb, bool color);

    void pbColor(QPushButton *pb, QColor color);

    void l1(QLabel *lb, int b);
    void l2(QLabel *lb, int b);
    // закрашенный треугольник (0-зеленый, 1-красный)
    void triangle(QPainter *painter, int up, int x, int y, PvkByteBit pbb);

    //extern unsigned char *ar;
    //unsigned char ar[1536*3];

    void setPage (int n); // выбрать страницу,  с ППН или алг, или главн
    void setPpnPage (int n); // выбрать страницу ППН
    void setAlgPage (int n); // выбрать слайд с алгоритмами по номеру выходного сигнала алгоритма

    // обобщенный слайд
    void commonSlide();

    // визуализация входов алгоритмов
    void inputs();

    // таблица ППН, отрисовка выходов за границу измерений (стрелок вверх-вниз)
    void ppn();

    // таблица ППН, тек.значения (и цвет)
    void ppn1();

    // реальн.знач.уставок
    void ustav();

    void fail_E();

    // для отрисовки вертикального текста
    void drawVTxt(QPainter *painter, const QPoint& p, const QString& str);
    // трехвходовое болье или равно двух
    void drawMoreOrEqTwo(QPainter &painter, int x, int y, int h, Ss& ss, int i=0);

    // отрисовка вх.дискр сигналов для схем алгоритмов (??)
    void i1(Ss &ss, int x, int y, int w, int h);
    void o1(OutSignals &ss, bool pinsLeft=0);

    void srabAlg();
    void prevPorog();
    void discrSig();

    enum DevType { in151, out155az, out155pz, pca };

    // отрисовка слайдов дискр сигналов
    void paintEvent1(DevType type);
    void paintEvent(QPaintEvent* e);

    int timerId;

    void createAnalogLabels();

  protected:
      void timerEvent(QTimerEvent *event);

private slots:
      void on_dial_valueChanged(int value);
      void onHideFrameNumbers();
      void onHide2Digit();
      //void on_verticalScrollBar_rangeChanged(int min, int max);

private:
    void keyPressEvent(QKeyEvent * ev);
    //void mouseMoveEvent(QMouseEvent * ev);
    QTimer *timer, *tmFrameNumber;
    bool hideFrameNumbersTimerEnabled, hide2DigitTimerEnabled;

    void timerBell();
    Ref *ref;
    referencePpn *ppnRef;
    RefAlg *refAlg;
    int ix; // индекс (номер) текущего слайда
    int discrSlideCurr; // номер текущего экрана (0..3) на слайде ИК

public:
    Ui::Form3 *ui;

};

#endif // FORM3_H
