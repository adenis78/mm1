//EthRecv::EthRecv(){}
//EthRecv::~EthRecv(){}

#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <thread>
#include <mutex>
#include <chrono>         // std::chrono::seconds
#include <iostream>
#include <QDebug>
#include "eth.h"
#include "presets.h"

std::mutex pkClrMutex;

#define ETHER_TYPE	0xFFFF

extern unsigned char valData[ETH_SRC_NUM][1600];
extern char ethArgName[IFNAMSIZ];
unsigned tBuf[400];

unsigned char ethErrFlag[ETH_SRC_NUM_MAX];
unsigned char lastCnt[ETH_SRC_NUM_MAX];
unsigned char cntPk[ETH_SRC_NUM_MAX];
unsigned int errCnt[ETH_SRC_NUM_MAX];
unsigned int ethPkClearFlag[ETH_SRC_NUM_MAX],ethPkClearFlagLast[ETH_SRC_NUM_MAX]; // признаки обнуления пакетов
unsigned const char ethCode[ETH_SRC_NUM_MAX] = { 0x8,0xA,0xC,0xE,
                              0x10,0x12,0x14,0x16,
                              0x18,0x1A,0x1C,0x1E,
                              0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,
                              0x28,0x2A,0x2C,0x2E,
                              0x30,0x34,0x36
                            };
unsigned const char adrToIndex[ETH_SRC_NUM_MAX] = { 0,1,0,1,
                                              2,3,2,3,
                                              4,5,4,5,
                                              6,7,8,9,
                                              6,7,8,9,
                                              10,11,10,11,
                                              12,12,12};

unsigned const char adrToCod[ETH_SRC_NUM_MAX] = { 0,1,100,101,
                                              2,3,102,103,
                                              4,5,104,105,
                                              6,7,8,9,
                                              106,107,108,109,
                                              10,11,110,111,
                                              12,112,212};
unsigned char oneOrTwo[ETH_SRC_NUM] = { 0,0,0,0, 0,0,0,0, 0,0,0,0, 0};


// номера пакетов в массиве пакетов для АФСЗ:
//p153az=0, p153pz=1,
//p151_1=2, p151_2=3,
//p157_1=4, p157_2=5,
//p155az1=6, p155az2=7, p155pz1=8, p155pz2=9,
//p08az=10, p08pz=1,
//pkc=12,
//blank=255 // отсутствие значения

unsigned const long long srcMac64 = 0x000000121733c2fc;
unsigned long long *srcMac64Ptr = (unsigned long long*)   (((ether_header *) tBuf) ->ether_shost);

void ethPackClear(int ix) {
    pkClrMutex.lock();

    memset(valData[ix], 0xffffffff, 1500-1);
    ethErrFlag[ix]=1;

    pkClrMutex.unlock();
}


extern char ifName[IFNAMSIZ];
int rackN=0; // номер стойки (фактически, № канала: 0, 1 или 2 )

int channelNumber;
extern struct ether_header *eh_send;

void ethRead() {
    ether_header *eh = (ether_header *) tBuf;
    int sockfd;
    int sockopt;
    ssize_t numbytes;
    struct ifreq ifopts;	// set promiscuous mode

    int i;
    unsigned char id, uz;

    for(i=0;i<ETH_SRC_NUM;i++)
        errCnt[i]=lastCnt[i]= cntPk[i]= ethPkClearFlag[i] = 0;

    for(i=0;i<ETH_SRC_NUM;i++)
        ethPkClearFlag[i] = ~0;


    // Open PF_PACKET socket, listening for EtherType ETHER_TYPE
    if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1) {
        perror("listener: socket");
        //return -1;
    }

    // Set interface to promiscuous mode - do we need to do this every time?
    strncpy(ifopts.ifr_name, ifName, IFNAMSIZ-1);
    ioctl(sockfd, SIOCGIFFLAGS, &ifopts);
    ifopts.ifr_flags |= IFF_PROMISC;
    ioctl(sockfd, SIOCSIFFLAGS, &ifopts);
    // Allow the socket to be reused - incase connection is closed prematurely
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof sockopt) == -1) {
        perror("setsockopt");
        //close(sockfd);
        exit(EXIT_FAILURE);
    }
    // Bind to device
    if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, ifName, IFNAMSIZ-1) == -1)	{
        perror("SO_BINDTODEVICE, since you've to correct program arg (#eth interface name)... ");
        //close(sockfd);
        exit(EXIT_FAILURE);
    }
    for(;;) {
        //printf("listener: Waiting to recvfrom...\n");
        numbytes = recvfrom(sockfd, tBuf, 1514 , 0, NULL, NULL); // TODO остальные поля проверить, тип эзернета

        // отбросить, если первые 5 байт адреса источника не от наших блоков
//        int macSrcByteCnt;
//        for(macSrcByteCnt=0;macSrcByteCnt<5;macSrcByteCnt++)
//            if(eh->ether_shost[macSrcByteCnt] != srcMac[macSrcByteCnt] )
//                break;
//        if(macSrcByteCnt<5)
//            continue;

        if( (*srcMac64Ptr & 0x000000ffffffffff) != srcMac64 )
            continue;

        //printf("listener: got packet %lu bytes\n", numbytes);

        //int pkN = idToN(eh->ether_shost[5]);
        //if(pkN<0) continue;

        //cntPk[pkN] = * (  (int*) ( &((unsigned char*)tBuf)[14+1])  );

        // *** подсчет ошибок ***
        id = eh->ether_shost[5];

        uz = id >> 2; // сдвинутые на 2 бита вправо

        if(uz>=0x20 && uz<=0x27) // кроме 155, 156
            uz &= 0xff;
        else
            uz &= 0xfe;

        // ищем соответстве MAC-адреса индексу пакета чтобы переписать в этот пакет по нужному индексу
        for(i=0;i<ETH_SRC_NUM_MAX;i++) {
            if(uz == ethCode[i]) {
                int cod = adrToCod[i];
                i = adrToIndex[i];

                if(i==12) { // если это pkc, переписываем младшие 2 бит = номер канала в 2 мл бита адреса передаваемых пакетов, чтобы коммутатор норм.пропускал пакеты
                    eh_send->ether_shost[5] &= 0xfc;
                    eh_send->ether_shost[5] |= eh->ether_shost[5] & 0x3;
                    rackN = eh->ether_shost[5] & 0x3;

                }
                if(cod>=100) {
                    if(cod>200) {
                        oneOrTwo[i] = 2;
                    }
                    else {
                        oneOrTwo[i] = 1;
                    }
                }
                else {
                    oneOrTwo[i] = 0;
                }
            }
            else continue;
            cntPk[i] =  ((unsigned char*)tBuf)[14] ;
            if(lastCnt[i] ==255) {
                if (0 != cntPk[i]) {
                    errCnt[i]++;
                    ethErrFlag[i]=1;
                 }
                 else
                    ethErrFlag[i]=0;
            }
            else {
                if (lastCnt[i]+1 != cntPk[i]) {
                    errCnt[i]++;
                    ethErrFlag[i]=1;
                }
                else
                    ethErrFlag[i]=0;
            }

            lastCnt[i] = cntPk[i];
            break;

        }


       //qDebug()<<cntPk[i]<<QString(" : ")<<QString(i)<<QString("\n");



        // копируем данные, без eth-заголовка и без номера пакета в 0м байте
        // TODO переделать чтобы переписывать сразу на свое место
        //int pkN = idToN(eh->ether_shost[5]);

        pkClrMutex.lock();
        memcpy(valData[i], (unsigned char*)eh + 15, numbytes-15);
        pkClrMutex.unlock();

//        printf("%x%x%x%x%x%x\n", eh->ether_shost[0], eh->ether_shost[1], eh->ether_shost[2],
//             eh->ether_shost[3], eh->ether_shost[4], eh->ether_shost[5]);
//        printf("%x%x%x%x%x%x\n", eh->ether_dhost[0], eh->ether_dhost[1], eh->ether_dhost[2],
//             eh->ether_dhost[3], eh->ether_dhost[4], eh->ether_dhost[5]);
//        // Print packet
//        printf("\tData:");
//        for (int i=0; i<numbytes; i++) printf("%02x:", tBuf[i]);
//        printf("\n");

        //std::this_thread::sleep_for(std::chrono::milliseconds(50));

    } // for
    //close(sockfd);
    //return ret;
}

