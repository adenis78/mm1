#ifndef INPUTSIGNALS
#define INPUTSIGNALS
#include <QString>
#include "pvk.h"
#include "eth.h"
#include "usts.h"
#include "im_blk_pkc.h"

// количество слайдов с алгоритмами
#define AlgSlidesN 17

enum SigType {SigInD=0, SigInA1=1,SigInA2=2, SigInA3=3};

#include "p157_158.h"
#include "pkc.h"

/*в 151-152 есть группы сигналов:
    1. вход
    2. сост лин связи
    3. исправность входа
    4. результат
    5. имитация
    6. блок
    7. вход, находится в 153-154 */
// № группы сигнала ( дискр.сигнал)
enum SigGrp { in=0, sost=24, ispr=48, res = 72, im=96, blk=120 /*, in153*/ }; // *24 = дискр.сигнал 151-152

#define PVK151_2    24
#define PZ153   ETH_DATA_MAX

//struct LimOut {
//    int  up; // индекс выход за верхний предел
//    int down; // индекс выход за нижний предел
//};

// структура, описывающая индексы "выход за пределы измерений" вверх и вниз соотв.
struct OutOfLimits { PvkByteBit up; PvkByteBit down; };

struct Imit {PvkByteBit im157; PvkByteBit imPkc; };
struct Ss {

//        int vhod; // индекс (смещение от 1) дискр.вх.сигнал в 151
//        int ispr; // индекс исправность в 151
//        int res; // индекс результир. сигнал в 151
//        int imit; // индекс признак имитации/блк в 151

    // 1. общие для цифр и аналог сигналов:
    SigType type; // тип входа (цифровой=0, аналоговый с одной уставкой=1, с двумя=2 и т.д.)

    // 2. название (то, что будет написано в прямоугольнике)
    QString name[7]; // строки с обознач. сигналов и уставок

    // 3. для дискр: индекс состояние дискр.входа. номер сигнала вход1...вход24, см. interfaces.ods, 151-152
    // 3. для аналог: индекс тек. параметра в 153.
    int sig;

    InImitBlkAz maj[2];// номер мажоритара (0..95) для имитации|блокировки для максимум ДВУХ мажоритарн.эл-тов, отобр как желтая рамка в маленькаих квадратах с цифрами 1,2,3 мажоритаров >=2. Потом надо вычислить адрес (путь смещения на константу) для 2,3 входа и для блокировки


    // далее только для аналог.:
    // 4. индексы (адреса смещ.) факт сраб. уставки в 153-154
    PvkByteBitSign fact[2][3]; //(всегда три стойки/сигнала, одна или две уставки)
    // 5. индексы (адреса смещ.) подсветка "≥2"
    PvkByteBit meq2[2];

    // 6. адреса и параметры тек. значений уставок (уставок максимум может быть 2 шт для одного значения). И адрес факт сраб.уставки
    Usts ust[2];

    // 7. для аналог. сигналов: количество элементов в out[]
    int outN;
    // 8. выходы за диапазон измерений вверх или вниз
    // они же + смещение = биты имитации 157-158 ("в имитации измеритель")
    OutDiap outDiap;

    //    OutOfLimits out[3];
    //    Imit imit;
    // биты имитации ПКЦ
    int imPkc;

};

struct OutSignals {
    int x,y;
    Pvk pvk;
    int chan; // номер канала (1..24)
    QString text; // АЗ №1 или ПЗ№2

    PvkByteBit srab153; // сраб алг.защиты в 153/154
    //PvkByteBit ispr153; // исправность при авт.контроле в 153/154
    PvkByteBit isprPkc; // исправность алгоритма (в ПКЦ184/185)
};

#endif // INPUTSIGNALS

