#ifndef P151_152_H
#define P151_152_H

enum P151_Grp {
    p151_in=0,
    p151_sost=24,
    p151_neisp=24*2,
    p151_res=24*3,
    p151_imit=24*4,
    p151_blk=24*5,
};

enum P151_1_24Ch {
p151_Aknp_N_more5,        // АКНП: N ≥ 5 %
p151_Aknp_N_more30,       // АКНП: N ≥ 30 %
p151_Aknp_N_more75,       // АКНП: N ≥ 75 %
p151_Aknp_N_moreNustAz1,  // АКНП: N ≥ N УСТ. АЗ-1
p151_Aknp_N_moreNustAz3,  //АКНП: N ≥ N УСТ. АЗ-3
p151_res1,           //РЕЗЕРВ
p151_Aknp_T_less10Az1,//АКНП: T ≤ 10 с АЗ-1
p151_Aknp_T_less20Az3,//АКНП: T ≤ 20 с АЗ-3
p151_Aknp_T_less20Az4,//АКНП: T ≤ 20 с АЗ-4
p151_Aknp_Neisp,//АКНП: НЕИСПРАВНОСТЬ
p151_res2,//РЕЗЕРВ
p151_res3,//РЕЗЕРВ
p151_res4,//РЕЗЕРВ
p151_res5,//РЕЗЕРВ
p151_res6,//РЕЗЕРВ
p151_res7,//РЕЗЕРВ
p151_Shp6_PotPitSuz,//ШП6: ПОТ. ПИТ. СУЗ
p151_Shlos_PadenieOr,//ШЛОС: ПАДЕНИЕ ОР
p151_Shvkk_oprob,//ШВКК: ОПРОБОВАНИЕ
p151_res8,//РЕЗЕРВ
p151_res9,//РЕЗЕРВ
p151_res10,//РЕЗЕРВ
P151_res11,//РЕЗЕРВ
p151_Afsz_OtkrDveri,//АФСЗ: ОТКР. ДВЕРИ
};

enum P151_2_24Ch {
p151_Sd_Neisp,//    "СД: НЕИСПРАВНОСТЬ",
p151_Sd_SeismVozdMore4,//    "CД: СЕЙСМИЧ.ВОЗД ≥ 4 БАЛЛ.",
p151_res12,//    "РЕЗЕРВ",
p151_res13,//    "РЕЗЕРВ",
p151_Arom_ObobRazgr,//    "АРОМ: ОБОБЩ.РАЗГРУЗКА",
p151_RSchg_OtklEb,//    "РЩГ: ОТКЛ. ЭБ",
p151_res14,//    "РЕЗЕРВ", //"    : ВЫКЛ.ГЕНЕР.",
p151_Shk2_Zakr1sk,//    "ШК2: ЗАКР. 1 СК.",
p151_Shk2_Zakr2sk,//    "ШК2: ЗАКР. 2 СК.",
p151_Shk2_Zakr3sk,//    "ШК2: ЗАКР. 3 СК.",
p151_Shk2_Zakr4sk,//    "ШК2: ЗАКР. 4 СК.",
p151_Svrk_QLMore448Az1,//    "СВРК: QL ≥ 448 Вт*см¯¹ АЗ-1",
p151_Svrk_DnbrLess1_35Az1,//    "СВРК: DNBR ≤ 1,35 АЗ1",
p151_Svrk_QLMore400Az3,//    "СВРК: QL ≥ 400 Вт*см¯¹ АЗ-3",
p151_Svrk_DnbrLess1_40Az3,//    "СВРК: DNBR ≤ 1,40 АЗ-3",
p151_Svrk_QLMore350Az4,//    "СВРК: QL ≥ 350 Вт*см¯¹ АЗ-4",
p151_Svrk_DnbrLess1_45Az4, //    "СВРК: DNBR ≤ 1,45 АЗ-4",
p151_res15,//    "РЕЗЕРВ",
p151_res16,//    "РЕЗЕРВ",
p151_Shkb_Zakr2,//    "ШКБ: ЗАКР. 2 ОК ЦСД || КЭН-2",
p151_Shp30_PotPit1,//    "ШП30: ПОТ. ПИТ. 1",
p151_Shp30_PotPit2,//    "ШП30: ПОТ. ПИТ. 2",
p151_Rtzo_PotPitSborok,//    "РТ3О: ПОТ. ПИТ. СБОРОК",
p151_res17//    "РЕЗЕРВ"
};

#endif // P151_152_H

