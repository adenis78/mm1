#ifndef P157_158
#define P157_158



// выходы за диапазон измерений вверх или вниз
// они же + смещение = биты имитации ("в имитации измеритель")
enum  OutDiap   {
ppn_tp_gn1, //ТП гн 1, мВ
ppn_tp_gn2, //ТП гн 2, мВ
ppn_tsp_gn1, //ТСП гн 1, Ом
ppn_tsp_gn2, //ТСП гн 2, Ом
ppn_tp_gn3, //ТП гн 3, мВ
ppn_tp_gn4, //ТП гн 4, мВ
ppn_tsp_gn3, //ТСП гн 3, Ом
ppn_tsp_gn4, //ТСП гн 4, Ом
ppn_tp_hn1, //ТП хн 1, мВ
ppn_tp_hn2, //ТП хн 2, мВ
ppn_tsp_hn1,//ТСП хн 1, Ом
ppn_tsp_hn2,//ТСП хн 2, Ом
ppn_Preak,//Р РЕАК, мА
ppn_Pgpk,//Р ГПК, мА
ppn_dPreak,//ΔР РЕАК, мА
ppn_dPreakDt,//dР РЕАК/dt, мА
ppn_FpitGcn1,//F ПИТ ГЦН 1, мА
ppn_FpitGcn2,//F ПИТ ГЦН 2, мА
ppn_FpitGcn3,//F ПИТ ГЦН 3, мА
ppn_FpitGcn4,//F ПИТ ГЦН 4, мА
ppn_Ppg1,//Р ТРОП ПГ 1, мА
ppn_Ppg2,//Р ТРОП ПГ 2, мА
ppn_Ppg3,//Р ТРОП ПГ 3, мА
ppn_Ppg4,//Р ТРОП ПГ 4, мА
ppn_PzkpTpn1,//Р ЗКП ТПН 1, мА
ppn_PzkpTpn2,//Р ЗКП ТПН 2, мА
ppn_Pgaz06,//Р ГА306/1, мА
ppn_Lkd,//L КД, мА
ppn_dPgcn1,//ΔP ГЦН1, мА
ppn_dPgcn2,//ΔP ГЦН2, мА
ppn_dPgcn3,//ΔP ГЦН3, мА
ppn_dPgcn4,//ΔP ГЦН4, мА
ppn_MedGcn1,//МЭД ГЦН1, мА
ppn_MedGcn2,//МЭД ГЦН2, мА
ppn_MedGcn3,//МЭД ГЦН3, мА
ppn_MedGcn4,//МЭД ГЦН4, мА
ppn_Lpg1,//L ПГ 1, мА
ppn_Lpg2,//L ПГ 2, мА
ppn_Lpg3,//L ПГ 3, мА
ppn_Lpg4,//L ПГ 4, мА
ppn_dPpg1Dt,//dР ПГ 1/dt, мА
ppn_dPpg2Dt,//dР ПГ 2/dt, мА
ppn_dPpg3Dt,//dР ПГ 3/dt, мА
ppn_dPpg4Dt,//dР ПГ 4/dt, мА
ppn_res1,//резерв
ppn_res2,//резерв
ppn_res3,//резерв
ppn_res4,//резерв
ppn_tp1_res,//ТП 1 резерв, мВ
ppn_tp2_res,//ТП 2 резерв, мВ
ppn_tsp1_res,//ТСП 1 резерв, Ом
ppn_tsp_Tobol,//ТСП Т обол., Ом
};

//enum Ch157 { // для Ростов-4
//ppn_tp_gn1, //ТП ГРЧН1[T гн 1]	0
//ppn_tp_gn4,//ТП ГРЧН4[T гн 4]	1
//ppn_tsp_gn14,//ТСП ГРЧН1,4	2
//ppn_tp_res_ch3,//ТСП резерв	3
//ppn_tp_gn2,//ТП ГРЧН2[T гн 2]	4
//ppn_tp_gn3,//ТП ГРЧН3[T гн 3]	5
//ppn_tsp_gn23,//ТСП ГРЧН2,3	6
//ppn_tap_res,//ТСП резерв	7
//ppn_tp_hn,//ТП ХН	8
//ppn_tp_res_ch9,//ТП резерв	9
//ppn_tsp_hn,//ТСП ХН	10
//ppn_tsp_res,//ТСП резерв	11
//ppn_Preak,//Р РЕАК, кгс/см²	12
//ppn_Pgpk,//Ргпк, кгс/см²	13
//ppn_dPreak,//резерв (Р-Р РЕАК, кгс/см²)[Ts реак]	14
//ppn_dPreakDt,//резерв (dР РЕАК, кгс/см²)	15
//ppn_FpitGcn1,//F ПИТ ГЦН1, Гц	16
//ppn_FpitGcn2,//F ПИТ ГЦН2, Гц	17
//ppn_FpitGcn3,//F ПИТ ГЦН3, Гц	18
//ppn_FpitGcn4,//F ПИТ ГЦН4, Гц	19
//ppn_PtropPg1,//Р ТРОП ПГ1, кгс/см²	20
//ppn_PtropPg2,//Р ТРОП ПГ2, кгс/см²	21
//ppn_PtropPg3,//Р ТРОП ПГ3, кгс/см²	22
//ppn_PtropPg4,//Р ТРОП ПГ4, кгс/см²	23
//ppn_PzkpTpn1,//Р ЗКП ТПН1, кгс/см²	24
//ppn_PzkpTpn2,//Р ЗКП ТПН2, кгс/см²	25
//ppn_Pgaz06,//Р ГАЗ06/1, кгс/см²	26
//ppn_Lkd,//L КД, см	27
//ppn_dPgcn1,//Р-Р ГЦН1, кгс/см²	28
//ppn_dPgcn2,//Р-Р ГЦН2, кгс/см²	29
//ppn_dPgcn3,//Р-Р ГЦН3, кгс/см²	30
//ppn_dPgcn4,//Р-Р ГЦН4, кгс/см²	31
//ppn_MedGcn1,//МЭД ГЦН1, кВт	32
//ppn_MedGcn2,//МЭД ГЦН2, кВт	33
//ppn_MedGcn3,//МЭД ГЦН3, кВт	34
//ppn_MedGcn4,//МЭД ГЦН4, кВт	35
//ppn_Lpg1,//L ПГ1, см	36
//ppn_Lpg2,//L ПГ2, см	37
//ppn_Lpg3,//L ПГ3, см	38
//ppn_Lpg4,//L ПГ4, см	39
//ppn_dPtropPg1,//резерв (dР ТРОП ПГ1, кгс/см²)[Ts ПГ1]	40
//ppn_dPtropPg2,//резерв (dР ТРОП ПГ2, кгс/см²)[Ts ПГ2]	41
//ppn_dPtropPg3,//резерв (dР ТРОП ПГ3, кгс/см²)[Ts ПГ3]	42
//ppn_dPtropPg4,//резерв (dР ТРОП ПГ4, кгс/см²)[Ts ПГ4]	43
//res_ch44,//резерв	44
//res_ch45,//резерв	45
//res_ch46,//резерв	46
//res_ch47,//резерв	47
//res_tp_res_ch48,//ТП резерв	48
//res_tp_res_ch49,//ТП резерв	49
//res_tp_res_ch50,//ТСП резерв	50
//res_tp_res_ch51,//ТСП резерв	51
//};


//имитация в руч из 157 (в авт из ПКЦ)
//ТП гн 1, мВ
//ТП гн 2, мВ
//ТСП гн 1, Ом
//ТСП гн 2, Ом
//ТП гн 3, мВ
//ТП гн 4, мВ
//ТСП гн 3, Ом
//ТСП гн 4, Ом
//ТП хн 1, мВ
//ТП хн 2, мВ
//ТСП хн 1, Ом
//ТСП хн 2, Ом
//Р РЕАК, мА
//Р ГПК, мА
//ΔР РЕАК, мА
//dР РЕАК/dt, мА
//F ПИТ ГЦН 1, мА
//F ПИТ ГЦН 2, мА
//F ПИТ ГЦН 3, мА
//F ПИТ ГЦН 4, мА
//Р ТРОП ПГ 1, мА
//Р ТРОП ПГ 2, мА
//Р ТРОП ПГ 3, мА
//Р ТРОП ПГ 4, мА
//Р ЗКП ТПН 1, мА
//Р ЗКП ТПН 2, мА
//Р ГА306/1, мА
//L КД, мА
//ΔP ГЦН1, мА
//ΔP ГЦН2, мА
//ΔP ГЦН3, мА
//ΔP ГЦН4, мА
//МЭД ГЦН1, мА
//МЭД ГЦН2, мА
//МЭД ГЦН3, мА
//МЭД ГЦН4, мА
//L ПГ 1, мА
//L ПГ 2, мА
//L ПГ 3, мА
//L ПГ 4, мА
//dР ПГ 1/dt, мА
//dР ПГ 2/dt, мА
//dР ПГ 3/dt, мА
//dР ПГ 4/dt, мА
//резерв
//резерв
//резерв
//резерв
//ТП 1 резерв, мВ
//ТП 2 резерв, мВ
//ТСП 1 резерв, Ом
//ТСП Т обол., Ом


#endif // P157_158

