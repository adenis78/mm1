#ifndef RefAlg_H
#define RefAlg_H
#include <QKeyEvent>
#include <QWidget>

namespace Ui {
class RefAlg;
}

class RefAlg : public QWidget
{
    Q_OBJECT

public:
    explicit RefAlg(QWidget *parent = 0);
    ~RefAlg();

    int dig[2];
    int digCnt;

    void keyPressEvent(QKeyEvent * ev);
    void paintEvent(QPaintEvent* e);
private:
    Ui::RefAlg *ui;
};

#endif // RefAlg_H
