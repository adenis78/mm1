#include "formtest.h"
#include "ui_formtest.h"
#include "eth.h"
extern unsigned char valData[ETH_SRC_NUM][1600];


FormTest::FormTest(QWidget *parent) :
    QWidget(parent), ui(new Ui::FormTest) {
    ui->setupUi(this);
    setGeometry(800,0,330,250);

    btGrp = new QButtonGroup(this);
    btGrp->addButton(ui->ck_0,0);
    btGrp->addButton(ui->ck_1,1);
    btGrp->addButton(ui->ck_2,2);
    btGrp->addButton(ui->ck_3,3);
    btGrp->addButton(ui->ck_4,4);
    btGrp->addButton(ui->ck_5,5);
    btGrp->addButton(ui->ck_6,6);
    btGrp->addButton(ui->ck_7,7);
}

FormTest::~FormTest() {
    delete btGrp;

    delete ui;

}

int FormTest::getBits() {
//    btGrp->
}


// обработчик кнопки Установить
void FormTest::set() {
    setReset(true);
}
// обработчик кнопки Сбросить
void FormTest::reset() {
    setReset(false);
}


void FormTest::setReset(bool setreset) {
    unsigned char v;
    int pvk = ui->cb_pvk->currentIndex();
    int addr = ui->sb_addr->value() -1;
    int bit, bits=0;
    if(!ui->ck_wordOrByte->isChecked()) { // адрес = байт
        if(!ui->ck_mask->isChecked()) { // уст-сброс отдельного бита
            int cid = btGrp->checkedId();
            if(cid>=0) {
                bit = 1<<cid;
                if(setreset)
                    v = valData[pvk][addr] = valData[pvk][addr] | bit;
                else
                    v = valData[pvk][addr] = valData[pvk][addr] & ~bit;
            }
        }
        else { // установка-очистка по битовой маске
            bits |= ui->ck_0->isChecked()?1:0;
            bits |= ui->ck_1->isChecked()?2:0;
            bits |= ui->ck_2->isChecked()?4:0;
            bits |= ui->ck_3->isChecked()?8:0;
            bits |= ui->ck_4->isChecked()?16:0;
            bits |= ui->ck_5->isChecked()?32:0;
            bits |= ui->ck_6->isChecked()?64:0;
            bits |= ui->ck_6->isChecked()?128:0;
            if(setreset)
                v = valData[pvk][addr] = valData[pvk][addr] | bits;
            else
                v = valData[pvk][addr] = valData[pvk][addr] & ~bits;
        }
    }
    else { // адрес = слово
        int val = ui->sb_value->value();
        short *adr16 =  ((short*)(valData[pvk]))+addr-1;

        *adr16 = val;
    }
    return;
}



void FormTest::on_pb_set_pressed() {
    set();
}


void FormTest::on_pb_reset_pressed() {
    reset();
}


void FormTest::refreshV() {
    int pvk = ui->cb_pvk->currentIndex();
    int addr = ui->sb_addr->value();

    if(ui->ck_wordOrByte->isChecked()) { // адрес = слово
        if (addr<0 || addr>750)
            return;

        short *adr16 =  ((short*)(valData[pvk])) + addr;
        ui->lb_hex->setText( "=0x" + QString::number(*adr16, 16) );
        ui->lb_dec->setText( "=" + QString::number(*adr16, 10) );
    }
    else {
        if (addr<0 || addr>1500)
            return;
        ui->lb_hex->setText( "= 0x" + QString::number( valData[pvk][addr], 16 ) );
        ui->lb_dec->setText( "=" + QString::number( valData[pvk][addr], 10 ) );
    }
}

void FormTest::on_ck_mask_stateChanged(int arg1) {
    //btGrp->setExclusive( ui->ck_mask->isChecked() );

    if(!ui->ck_mask->isChecked()) {
        btGrp->addButton(ui->ck_0,0);
        btGrp->addButton(ui->ck_1,1);
        btGrp->addButton(ui->ck_2,2);
        btGrp->addButton(ui->ck_3,3);
        btGrp->addButton(ui->ck_4,4);
        btGrp->addButton(ui->ck_5,5);
        btGrp->addButton(ui->ck_6,6);
        btGrp->addButton(ui->ck_7,7);
    }
    else {
        btGrp->removeButton(ui->ck_0);
        btGrp->removeButton(ui->ck_1);
        btGrp->removeButton(ui->ck_2);
        btGrp->removeButton(ui->ck_3);
        btGrp->removeButton(ui->ck_4);
        btGrp->removeButton(ui->ck_5);
        btGrp->removeButton(ui->ck_6);
        btGrp->removeButton(ui->ck_7);
    }


}
