// оглавление для слайдов с алгоритмами

#include <QPainter>
#include "refalg.h"
#include "ui_refalg.h"
#include "form3.h"
extern Form3 *mf;

RefAlg::RefAlg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RefAlg)
{
    ui->setupUi(this);

    setWindowFlags(Qt::FramelessWindowHint);
    //setModal(true);
    setWindowModality(Qt::WindowModal);
    digCnt=0;
}

RefAlg::~RefAlg()
{
    delete ui;
}

void RefAlg::keyPressEvent(QKeyEvent * ev) {

    int ix, pg, d = ev->key();
    //if(d==Qt::Key_Escape || d==Qt::Key_Enter)
    d-= Qt::Key_0;
    if(d >= 0 && d <= 9) { // двузначный номер слайда
        if (digCnt==0) {
            if(d>=0 && d<=4 ) // первая цифра (ст.разряд)
            dig[digCnt] = d;
           digCnt++;
            ui->lb_chN->setText(QString::number(d));
        }
        else {
            dig[digCnt] = d;
              ui->lb_chN->setText( QString::number(dig[0] *10)
                               + QString::number(dig[1]));
              digCnt=0;
              ix = dig[0] * 10 + dig[1];
//              if(ix!=2 && ix!=3 && ix!=6 && ix!=7 && ix!=9 && ix!=10 && ix!=14 &&ix!=15 ) {
//                  if(ix<12)
//                      pg=0;
//                  else if(ix<24)
//                      pg=1;
//                  else if(ix<36)
//                      pg=2;
//                  else if(ix<40)
//                      pg=3;
//                  else if(ix<44)
//                      pg=4;
//                  else if(ix<48)
//                      pg=5;

//              }
              mf->setAlgPage(ix);
              hide();
              mf->show();
              mf->setFocus();
        }
    }
    else {
        hide();
        mf->show();
        mf->setFocus();
    }
}

const int AlgAzNumber=31;

QString strAlgAz[AlgAzNumber] = {
    "Р РЕАК ≤ 150 кгс/см2 & N ≥ 75%",  // АЗ №1
    "ОТКЛ 1/4 & N ≥ 75%", // АЗ №2
    "ОТКЛ 3/4 & N ≥ 5%", // АЗ №3
    "L ПВ ПГ 1 ≤ -65 см", // АЗ №4
    "L ПВ ПГ 2 ≤ -65 см", // АЗ №5
    "L ПВ ПГ 3 ≤ -65 см", // АЗ №6
    "L ПВ ПГ 4 ≤ -65 см", // АЗ №7
    "L КД ≤ 460 см", // АЗ №8
    "Р РЕАК ≥ 178.5 кгс/см2", // АЗ №9
    "Р ГАЗ06/1 ≥ 0.306 кгс/см2", // АЗ №10
    "ПОТЕРЯ ПИТАНИЯ СУЗ", // АЗ №11
    "ИЗМ. Р-Р ГЦН 1", // АЗ №12
    "ИЗМ. Р-Р ГЦН 2", // АЗ №13
    "ИЗМ. Р-Р ГЦН 3", // АЗ №14
    "ИЗМ. Р-Р ГЦН 4", // АЗ №15
    "Р ПГ1 ≤ 50.0 кгс/cм2 & T НАС 1K ≥ 75°C", // АЗ №16
    "Р ПГ2 ≤ 50.0 кгс/cм2 & T НАС 1K ≥ 75°C", // АЗ №17
    "Р ПГ3 ≤ 50.0 кгс/cм2 & T НАС 1K ≥ 75°C", // АЗ №18
    "Р ПГ4 ≤ 50.0 кгс/cм2 & T НАС 1K ≥ 75°C", // АЗ №19
    "Р ТРОП ПГ ≥ 80 кгс/см2", //АЗ №20
    "Т ГРЧН ≥ +8°C", // АЗ №21
    "F 3/4 ГЦН ≤ 46 Гц", //АЗ №22
    "Т НАС 1К - Т MAX ГРЧН 1 ≤ 10°C", //АЗ №23
    "Т НАС 1К - Т MAX ГРЧН 2 ≤ 10°C", //АЗ №24
    "Т НАС 1К - Т MAX ГРЧН 3 ≤ 10°C", //АЗ №25
    "Т НАС 1К - Т MAX ГРЧН 4 ≤ 10°C", //АЗ №26
    "2 ТПН ВЫКЛ & N ≥ 40%", //АЗ №27
    "T ≤ 10c АЗ", //АЗ №28
    "N ≥ N УСТ.АЗ", //АЗ №29
    "СЕЙСМИЧ.ВОЗД ≥ 6 баллов", //АЗ №30
    "Р РЕАК ≤ 140 кгс/см2" //АЗ №31
};

const int AlgPzNumber = 18;
QString strAlgPz[AlgPzNumber]  = {
    "Tgn > 325",   // (выход алг. 1)
    "Р а.з. >170,3 кгс",  // (выход алг. 2)
    "Р гпк >70 кгс",      // (выход алг. 3)
    "T(пд.рд.) < 20 c",//(выход алг. 4)
    "N(пд.рд.) > уст.",//(выход алг. 5)
    "Разгрузка",//(выход алг. 6)
    "N(пд.рд.) > 10/15АЗ", // (выход алг. 7)
    "Р а.з. >165,2 кгс", // (выход алг. 8)
    "Падение одного ОР", // (выход алг. 9)
    "QL>350", // (выход алг. 10)
    "DNBR<1,45", // (выход алг. 11)
    "N>75&Откл. Гцн", // (выход алг. 12)
    "N>75&Откл. ЭБ", // (выход алг. 13)
    "N>75&Откл. Генератора", // (выход алг. 14)
    "N>75&P тпн < 0,408", // (выход алг. 15)
    "N>75&Закрыты более 1 СК", // (выход алг. 16)
    "QL>400(выход алг. 17)", // // (выход алг. 17)
    "DNBR<1,4(выход алг. 18)", // // (выход алг. 18)

};


void RefAlg::paintEvent(QPaintEvent* e) {
    int i,j;
    int x=0;
    QPainter p(this);
    QFont oldfont, font = p.font();
    oldfont = font;
//    font.setPixelSize(28);
//    p.setFont(font);
//    //p.drawText(QPoint(175, 28), "Выходы алгоритмов");
//    p.setFont(oldfont);

    font = p.font();
    oldfont = font;
    font.setPixelSize(13);
    p.setFont(font);

//    p.drawText(QPoint(30, 70), "Алгоритмы АЗ:");
//    p.drawText(QPoint(500, 70), "Алгоритмы ПЗ1, ПЗ2, УПЗ:");


    p.setFont(oldfont);

    // гориз.линия сверху
    //p.drawLine(0,50, 799, 50);

    // вертикальные линии
    p.drawLine(400, 0, 400, 599);
    p.drawLine(799, 120, 799, 120+20*24);

    // табл.СЛЕВА: горизонтальные линии, текст
    //QRect rect(300, 120+2, 10, 15);
    QRect rect(300,40+2, 10, 15);
    int flag;
    int tmp=0;
    for(i=0;i<AlgAzNumber;i++) {
//        p.drawLine(0, 120+20*i, 400, 120+20*i);
//        p.drawText(QPoint(5, 120+20*i + 15), QString::number(i+1) + ".");
//        p.drawText(QPoint(25, 120+20*i + 15), strAlgAz[i]);
        p.drawLine(0, 40+18*i, 400, 40+18*i);
        p.drawText(QPoint(5, 38+18*i + 18), QString::number(i+1) + ".");
        p.drawText(QPoint(25, 38+18*i + 18), strAlgAz[i]);
    }
    p.drawLine(0, 40+18*(i), 400, 40+18*(i));

    // табл.СПРАВА: горизонтальные линии, текст
    rect.moveLeft(698);
    for(i=0;i<AlgPzNumber;i++) {
        //i1(x,y+ i*h + 1,w,h);
//        p.drawLine(400, 120+20*i, 799, 120+20*i);
//        p.drawText(QPoint(5+400, 120+20*i + 15), QString::number(i+1+31) + ".");
//        p.drawText(QPoint(25+400, 120+20*i + 15), strAlgPz[i]);
        p.drawLine(400, 76+18*i, 799, 76+18*i);
        p.drawText(QPoint(5+400, 72+18*i + 18), QString::number(i+1+31) + ".");
        p.drawText(QPoint(25+400, 72+18*i + 18), strAlgPz[i]);
    }
    p.drawLine(400, 76+18*(i), 799, 76+18*(i));
}
