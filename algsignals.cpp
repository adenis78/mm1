#include "algsignals.h"
#include "pvk.h"
#include "constants.h"
#include "alg_fact_meq2.h"
#include "srab_ispr_alg_153.h"
#include "usts.h"
#include "im_blk_pkc.h"

//// количество входов на каждом из алг.слайдов
int algSlideInputsN[AlgSlidesN] = {8,6,6,6,
                                   6,6,7,6,
                                   4,4,5,9,
                                   5,7,7,4,2};

// АЗ: смещения в 2байтных словах для тек.знач.параметров <153>
const int a_Paz = 0; //текущее значение параметра Р а.з.
const int a_Ngcn1 = 1; //текущее значение параметра N гцн 1
const int a_Ngcn2 = 2; //текущее значение параметра N гцн 2
const int a_Ngcn3 = 3; //текущее значение параметра N гцн 3
const int a_Ngcn4 = 4; //текущее значение параметра N гцн 4
const int a_Lpg1 = 5; //текущее значение параметра L пг 1
const int a_Lpg2 = 6; //текущее значение параметра L пг 2
const int a_Lpg3 = 7; //текущее значение параметра L пг 3
const int a_Lpg4 = 8; //текущее значение параметра L пг 4
const int a_Lkd = 9; //текущее значение параметра L кд
const int a_Pgaz = 10; //текущее значение параметра Р под.об., то же что и ГАЗ06
const int a_dPgcn1 = 11; //текущее значение параметра ΔР гцн 1
const int a_dPgcn2 = 12; //текущее значение параметра ΔР гцн 2
const int a_dPgcn3 = 13; //текущее значение параметра ΔР гцн 3
const int a_dPgcn4 = 14; //текущее значение параметра ΔР гцн 4
const int a_Ppg1 = 15; //текущее значение параметра Ppg1
const int a_Ppg2 = 16; //текущее значение параметра Ppg2
const int a_Ppg3 = 17; //текущее значение параметра Ppg3
const int a_Ppg4 = 18; //текущее значение параметра Ppg4
const int a_Tgn1 = 19; //текущее значение параметра Tgn1
const int a_Tgn2 = 20; //текущее значение параметраTgn2
const int a_Tgn3 = 21; //текущее значение параметраTgn3
const int a_Tgn4 = 22; //текущее значение параметраTgn4
const int a_Fgcn1 = 23; //текущее значение параметра F гцн 1
const int a_Fgcn2 = 24; //текущее значение параметра F гцн 2
const int a_Fgcn3 = 25; //текущее значение параметра F гцн 3
const int a_Fgcn4 = 26; //текущее значение параметра F гцн 4
const int a_Ptpn1 = 27; //текущее значение параметра P тпн 1
const int a_Ptpn2 = 28; //текущее значение параметра P тпн 2
const int a_Tsaz = 29; //текущее значение параметра Ts а.з.
const int a_Tsppg1 = 30; //текущее значение параметра Ts п.пг. 1
const int a_Tsppg2 = 31; //текущее значение параметра Ts п.пг. 2
const int a_Tsppg3 = 32; //текущее значение параметра Ts п.пг. 3
const int a_Tsppg4 = 33; //текущее значение параметра Ts п.пг. 4
const int a_Tsaz_Tsppg1 = 34; //текущее значение параметра Ts а.з. - Ts п.пг. 1
const int a_Tsaz_Tsppg2 = 35; //текущее значение параметра Ts а.з. - Ts п.пг. 2
const int a_Tsaz_Tsppg3 = 36; //текущее значение параметра Ts а.з. - Ts п.пг. 3
const int a_Tsaz_Tsppg4 = 37; //текущее значение параметра Ts а.з. - Ts п.пг. 4
const int a_Tsaz_Tsgn1 = 38; //текущее значение параметра Ts а.з. -Tgn1
const int a_Tsaz_Tsgn2 = 39; //текущее значение параметра Ts а.з. -Tgn2
const int a_Tsaz_Tsgn3 = 40; //текущее значение параметра Ts а.з. -Tgn3
const int a_Tsaz_Tsgn4 = 41; //текущее значение параметра Ts а.з. -Tgn4

// ПЗ: текущее значение параметраTgn1
const int p_Tgn1 = PZ153 + 0; // текущее значение параметраTgn1
const int p_Tgn2 = PZ153 + 1; // текущее значение параметраTgn2
const int p_Tgn3 = PZ153 + 2; // текущее значение параметраTgn3
const int p_Tgn4 = PZ153 + 3; // текущее значение параметраTgn4
const int p_Paz = PZ153 + 4; // текущее значение параметра Р а.з.
const int p_Pgpk = PZ153 + 5; // текущее значение параметра Р гпк
const int p_Ngcn1 = PZ153 + 6; // текущее значение параметра N гцн 1
const int p_Ngcn2 = PZ153 + 7; // текущее значение параметра N гцн 2
const int p_Ngcn3 = PZ153 + 8; // текущее значение параметра N гцн 3
const int p_Ngcn4 = PZ153 + 9; // текущее значение параметра N гцн 4
const int p_Ntpn1 = PZ153 + 10; // текущее значение параметра P тпн 1
const int p_Ntpn2 = PZ153 + 11; // текущее значение параметра P тпн 2


const int DISP_UST153AZ = 42;
const int DISP_UST153PZ = 12;

const SmD smdForT = {0,400,1};
const SmD smdFor_dT = {0,400,1};

const Usts ust_Paz_l150 = { p153az, DISP_UST153AZ + 0, tP, {0,250,1}, faz_Paz_less150_A  };     //Р а.з. <150 кгс
const Usts ust_Paz_m178_5 = {p153az, DISP_UST153AZ + 1,  tP, {0,250,1}, faz_Paz_more178_5_A };   //Р а.з. >178,5 кгс
const Usts ust_Paz_l140 = {p153az, DISP_UST153AZ + 2, tP, {0,250,1}, faz_Paz_less140_A  };     //Р а.з. <140 кгс
const Usts ust_Paz_m150 = {p153az, DISP_UST153AZ + 3, tP, {0,250,1}, faz_Paz_more150_A  }; //Р а.з. >150 кгс
const Usts ust_Ngcn1_l3000 = {p153az, DISP_UST153AZ + 4, tP, {0,16368,0}, faz_Ngcn1_less_3000_A }; //N гцн 1 < 3000 кВт
const Usts ust_Ngcn2_l3000 = {p153az, DISP_UST153AZ + 5, tP, {0,16368,0}, faz_Ngcn2_less_3000_A }; //N гцн 2 < 3000 кВт
const Usts ust_Ngcn3_l3000 = {p153az, DISP_UST153AZ + 6, tP, {0,16368,0}, faz_Ngcn3_less_3000_A }; //N гцн 3 < 3000 кВт
const Usts ust_Ngcn4_l3000 = {p153az, DISP_UST153AZ + 7, tP, {0,16368,0}, faz_Ngcn4_less_3000_A }; //N гцн 4 < 3000 кВт
const Usts ust_Lpg1_l175 = { p153az, DISP_UST153AZ + 8, tP, {0,400,1}, faz_Lpg1_less_175_A };    //L пг 1 < 175 см
const Usts ust_Lpg2_l175 = { p153az, DISP_UST153AZ + 9, tP, {0,400,1}, faz_Lpg2_less_175_A  };    //L пг 2 < 175 см
const Usts ust_Lpg3_l175 = { p153az, DISP_UST153AZ + 10, tP, {0,400,1}, faz_Lpg3_less_175_A  };   //L пг 3 < 175 см
const Usts ust_Lpg4_l175 = { p153az, DISP_UST153AZ + 11, tP, {0,400,1}, faz_Lpg4_less_175_A  };   //L пг 4 < 175 см
const Usts ust_Lkd_l460 = { p153az, DISP_UST153AZ + 12, tP, {0,630,0}, faz_L_kd_less_460_A  };    //L кд < 460 см
const Usts ust_Ppod_m0_306 = { p153az, DISP_UST153AZ + 13, tP, {-0.5,1,4}, faz_Pgaz06_more0_306_A };  //Р под.об. > 0,306 кгс
const Usts ust_dPgcn1_l4_0 = { p153az, DISP_UST153AZ + 14, tP, {0,10,3}, faz_dPgcn1_less_4_0_A }; //ΔР гцн 1 < 4,0 кгс
const Usts ust_dPgcn2_l4_0 = { p153az, DISP_UST153AZ + 15, tP, {0,10,3}, faz_dPgcn2_less_4_0_B }; //ΔР гцн 2 < 4,0 кгс
const Usts ust_dPgcn3_l4_0 = { p153az, DISP_UST153AZ + 16, tP, {0,10,3}, faz_dPgcn3_less_4_0_C }; //ΔР гцн 3 < 4,0 кгс
const Usts ust_dPgcn4_l4_0 = { p153az, DISP_UST153AZ + 17, tP, {0,10,3}, faz_dPgcn4_less_4_0_A }; //ΔР гцн 4 < 4,0 кгс
const Usts ust_dPgcn1_l2_5 = { p153az, DISP_UST153AZ + 18, tP, {0,10,3}, faz_dPgcn1_less_2_5_A }; //ΔР гцн 1 < 2,5 кгс
const Usts ust_dPgcn2_l2_5 = { p153az, DISP_UST153AZ + 19, tP, {0,10,3}, faz_dPgcn2_less_2_5_A }; //ΔР гцн 2 < 2,5 кгс
const Usts ust_dPgcn3_l2_5 = { p153az, DISP_UST153AZ + 20, tP, {0,10,3}, faz_dPgcn3_less_2_5_A }; //ΔР гцн 3 < 2,5 кгс
const Usts ust_dPgcn4_l2_5 = { p153az, DISP_UST153AZ + 21, tP, {0,10,3}, faz_dPgcn4_less_2_5_A }; //ΔР гцн 4 < 2,5 кгс
const Usts ust_Ppg1_l50 = { p153az, DISP_UST153AZ + 22, tP, {0,100,2}, faz_Ppg1_less50_A }; //Ppg1 < 50 кгс
const Usts ust_Ppg2_l50 = { p153az, DISP_UST153AZ + 23, tP, {0,100,2}, faz_Ppg2_less50_A }; //Ppg1 < 50 кгс
const Usts ust_Ppg3_l50 = { p153az, DISP_UST153AZ + 24, tP, {0,100,2}, faz_Ppg3_less50_A }; //Ppg1 < 50 кгс
const Usts ust_Ppg4_l50 = { p153az, DISP_UST153AZ + 25, tP, {0,100,2}, faz_Ppg4_less50_A }; //Ppg1 < 50 кгс
const Usts ust_Ppg1_m80 = { p153az, DISP_UST153AZ + 26, tP, {0,100,2}, faz_Ppg1_more80_A }; //Ppg1 < 80 кгс
const Usts ust_Ppg2_m80 = { p153az, DISP_UST153AZ + 27, tP, {0,100,2}, faz_Ppg2_more80_A }; //Ppg2 < 80 кгс
const Usts ust_Ppg3_m80 = { p153az, DISP_UST153AZ + 28, tP, {0,100,2}, faz_Ppg3_more80_A }; //Ppg3 < 80 кгс
const Usts ust_Ppg4_m80 = { p153az, DISP_UST153AZ + 29, tP, {0,100,2}, faz_Ppg4_more80_A }; //Ppg4 < 80 кгс
const Usts ust_Tgn1_m200 = { p153az, DISP_UST153AZ + 30, tT,smdForT,faz_Tgn1_more200_C_A }; //Tgn1 > 200 C
const Usts ust_Tgn2_m200 = { p153az, DISP_UST153AZ + 31, tT,smdForT,faz_Tgn2_more200_C_A }; //Tgn2 > 200 C
const Usts ust_Tgn3_m200 = { p153az, DISP_UST153AZ + 32, tT,smdForT,faz_Tgn3_more200_C_A }; //Tgn3 > 200 C
const Usts ust_Tgn4_m200 = { p153az, DISP_UST153AZ + 33, tT,smdForT,faz_Tgn4_more200_C_A }; //Tgn4 > 200 C
const Usts ust_Tgn1_m330 = { p153az, DISP_UST153AZ + 34, tT,smdForT,faz_Tgn1_more330_C_A }; //Tgn1 > 330 C
const Usts ust_Tgn2_m330 = { p153az, DISP_UST153AZ + 35, tT,smdForT,faz_Tgn2_more330_C_A }; //Tgn2 > 330 C
const Usts ust_Tgn3_m330 = { p153az, DISP_UST153AZ + 36, tT,smdForT,faz_Tgn3_more330_C_A }; //Tgn3 > 330 C
const Usts ust_Tgn4_m330 = { p153az, DISP_UST153AZ + 37, tT,smdForT,faz_Tgn4_more330_C_A }; //Tgn4 > 330 C
const Usts ust_Tgn1_m260 = { p153az, DISP_UST153AZ + 38, tT,smdForT,faz_Tgn1_more260_C_A }; //Tgn1 > 260 C
const Usts ust_Tgn2_m260 = { p153az, DISP_UST153AZ + 39, tT,smdForT,faz_Tgn2_more260_C_A }; //Tgn2 > 260 C
const Usts ust_Tgn3_m260 = { p153az, DISP_UST153AZ + 40, tT,smdForT,faz_Tgn3_more260_C_A }; //Tgn3 > 260 C
const Usts ust_Tgn4_m260 = { p153az, DISP_UST153AZ + 41, tT,smdForT,faz_Tgn4_more260_C_A }; //Tgn4 > 260 C
const Usts ust_Tgn1_l268 = { p153az, DISP_UST153AZ + 42, tT,smdForT,faz_Tgn1_less_268_C_A }; //Tgn1 < 268 C
const Usts ust_Tgn2_l268 = { p153az, DISP_UST153AZ + 43, tT,smdForT,faz_Tgn2_less_268_C_A }; //Tgn2 < 268 C
const Usts ust_Tgn3_l268 = { p153az, DISP_UST153AZ + 44, tT,smdForT,faz_Tgn3_less_268_C_A }; //Tgn3 < 268 C
const Usts ust_Tgn4_l268 = { p153az, DISP_UST153AZ + 45, tT,smdForT,faz_Tgn4_less_268_C_A }; //Tgn4 < 268 C
const Usts ust_Fgcn1_l46 = { p153az, DISP_UST153AZ + 46, tF,{45,10,2},faz_Fgcn1_less_46_A }; //F гцн 1 < 46 Гц
const Usts ust_Fgcn2_l46 = { p153az, DISP_UST153AZ + 47, tF,{45,10,2},faz_Fgcn2_less_46_A }; //F гцн 2 < 46 Гц
const Usts ust_Fgcn3_l46 = { p153az, DISP_UST153AZ + 48, tF,{45,10,2},faz_Fgcn3_less_46_A }; //F гцн 3 < 46 Гц
const Usts ust_Fgcn4_l46 = { p153az, DISP_UST153AZ + 49, tF,{45,10,2},faz_Fgcn4_less_46_A }; //F гцн 4 < 46 Гц
const Usts ust_Ptpn1_l0_408 = { p153az, DISP_UST153AZ + 50, tP, {0,10,3},faz_Ptpn1_less_0_408_A }; //P тпн 1 < 0,408 кгс
const Usts ust_Ptpn2_l0_408 = { p153az, DISP_UST153AZ + 51, tP, {0,10,3},faz_Ptpn2_less_0_408_A }; //P тпн 2 < 0,408 кгс
const Usts ust_TsAz_TsPg1_m75 = { p153az, DISP_UST153AZ + 52, tdT, smdFor_dT, faz_Ts_az__Ts_pg1_more75_C_A }; //(Ts а.з. - Ts п.пг. 1) > 75 C
const Usts ust_TsAz_TsPg2_m75 = { p153az, DISP_UST153AZ + 53, tdT, smdFor_dT, faz_Ts_az__Ts_pg2_more75_C_A }; //(Ts а.з. - Ts п.пг. 2) > 75 C
const Usts ust_TsAz_TsPg3_m75 = { p153az, DISP_UST153AZ + 54, tdT, smdFor_dT, faz_Ts_az__Ts_pg3_more75_C_A }; //(Ts а.з. - Ts п.пг. 3) > 75 C
const Usts ust_TsAz_TsPg4_m75 = { p153az, DISP_UST153AZ + 55, tdT, smdFor_dT, faz_Ts_az__Ts_pg4_more75_C_A }; //(Ts а.з. - Ts п.пг. 4) > 75 C
const Usts ust_TsAz_Tgn1_l10 = { p153az, DISP_UST153AZ + 56, tdT, smdFor_dT, faz_Ts_az__Tgn1_less_10_C_A }; //(Ts а.з. -Tgn1) < 10 C
const Usts ust_TsAz_Tgn2_l10 = { p153az, DISP_UST153AZ + 57, tdT, smdFor_dT, faz_Ts_az__Tgn2_less_10_C_A }; //(Ts а.з. -Tgn2) < 10 C
const Usts ust_TsAz_Tgn3_l10 = { p153az, DISP_UST153AZ + 58, tdT, smdFor_dT, faz_Ts_az__Tgn3_less_10_C_A }; //(Ts а.з. -Tgn3) < 10 C
const Usts ust_TsAz_Tgn4_l10 = { p153az, DISP_UST153AZ + 59, tdT, smdFor_dT, faz_Ts_az__Tgn4_less_10_C_A }; //(Ts а.з. -Tgn4) < 10 C

const Usts ust_Tgn1_m325 = { p153pz, DISP_UST153PZ + 0 , tT, {0,250,1}, fpz_Tgn1_more325_C_A }; // текущее значение уставкиTgn1 > 325 C (ПЗ)
const Usts ust_Tgn2_m325 = { p153pz, DISP_UST153PZ + 1, tT, {0,250,1}, fpz_Tgn2_more325_C_A }; // текущее значение уставкиTgn2 > 325 C
const Usts ust_Tgn3_m325 = { p153pz, DISP_UST153PZ + 2, tT, {0,250,1}, fpz_Tgn3_more325_C_A }; // текущее значение уставкиTgn3 > 325 C
const Usts ust_Tgn4_m325 = { p153pz, DISP_UST153PZ + 3, tT, {0,250,1}, fpz_Tgn4_more325_C_A }; // текущее значение уставкиTgn4 > 325 C
const Usts ust_PazPz_m170_3 = { p153pz, DISP_UST153PZ + 4, tP, {0,250,1}, fpz_Ppz_more_170_3_A }; // текущее значение уставки Р а.з. >170,3 кгс
const Usts ust_PazPz_m165_2 = { p153pz, DISP_UST153PZ + 5, tP, {0,250,1}, fpz_Ppz_more_165_2_A }; // текущее значение уставки Р а.з. >165,2 кгс
const Usts ust_Pgpk_m70 = { p153pz, DISP_UST153PZ + 6, tP,  {0,100,1}, fpz_P_gpk_more_70_A  }; // текущее значение уставки Р гпк >70 кгс
const Usts ust_Ngcn1Pz_l3000 = {p153pz, DISP_UST153PZ + 7, tP, {0,16368,0}, fpz_Ngcn1_less_3000_A }; //N гцн 1 < 3000 кВт
const Usts ust_Ngcn2Pz_l3000 = {p153pz, DISP_UST153PZ + 8, tP, {0,16368,0}, fpz_Ngcn2_less_3000_A }; //N гцн 2 < 3000 кВт
const Usts ust_Ngcn3Pz_l3000 = {p153pz, DISP_UST153PZ + 9, tP, {0,16368,0}, fpz_Ngcn3_less_3000_A }; //N гцн 2 < 3000 кВт
const Usts ust_Ngcn4Pz_l3000 = {p153pz, DISP_UST153PZ + 10, tP, {0,16368,0}, fpz_Ngcn4_less_3000_A }; //N гцн 4 < 3000 кВт
const Usts ust_Ptpn1Pz_l0_408 = { p153pz, DISP_UST153PZ + 11, tP, {0,10,3}, faz_Ptpn1_less_0_408_A }; //P тпн 1 < 0,408 кгс
const Usts ust_Ptpn2Pz_l0_408 = { p153pz, DISP_UST153PZ + 12, tP, {0,10,3}, faz_Ptpn2_less_0_408_A }; //P тпн 2 < 0,408 кгс


Ss ss[AlgSlidesN][16] = { // макс.16 сигналов на одном слайде
    {  // слайд 1
        { SigInA2, {"Р РЕАК, кгс/см2", "Р РЕАК ≤ 150.0 кгс/см2", "Р РЕАК ≥ 178.5 кгс/см2" }, a_Paz, {ib_Paz_less150, ib_Paz_more178_5}, {{faz_Paz_less150_A, faz_Paz_less150_B, faz_Paz_less150_C},{faz_Paz_more178_5_A, faz_Paz_more178_5_B, faz_Paz_more178_5_C}}, {meq2_Paz_less150, meq2_Paz_more178_5 }, {ust_Paz_l150, ust_Paz_m178_5}, 1, ppn_Preak, pkcAi_Preak },
        { SigInD, {"N ≥ 75% , кгс/см2"}, 3, {ib_N_more75_az}, {{faz_N_more75_A, faz_N_more75_B, faz_N_more75_C}}, {meq2_N_more75} },
        { SigInD, {"N ≥ 5%, кгс/см2"}, 1, {ib_N_more5}, {{faz_N_more5_A, faz_N_more5_B, faz_N_more5_C}}, {meq2_N_more5} },
        { SigInA1, {"МЭД ГЦН1", "МЭД ГЦН1 ≤ 3000 КВт" }, a_Ngcn1, {ib_Ngcn1_less3000_az}, {{faz_Ngcn1_less_3000_A, faz_Ngcn1_less_3000_B, faz_Ngcn1_less_3000_C}}, {meq2_Ngcn1_less3000}, {ust_Ngcn1_l3000},1, ppn_MedGcn1, pkcAi_MedGcn1 },
        { SigInA1, {"МЭД ГЦН2", "МЭД ГЦН2 ≤ 3000 КВт" }, a_Ngcn2, {ib_Ngcn2_less3000_az}, {{faz_Ngcn2_less_3000_A, faz_Ngcn2_less_3000_B, faz_Ngcn2_less_3000_C}}, {meq2_Ngcn2_less3000}, {ust_Ngcn2_l3000},1, ppn_MedGcn2, pkcAi_MedGcn2 },
        { SigInA1, {"МЭД ГЦН3", "МЭД ГЦН3 ≤ 3000 КВт" }, a_Ngcn3, {ib_Ngcn3_less3000_az}, {{faz_Ngcn3_less_3000_A, faz_Ngcn3_less_3000_B, faz_Ngcn3_less_3000_C}}, {meq2_Ngcn3_less3000}, {ust_Ngcn3_l3000},1, ppn_MedGcn3, pkcAi_MedGcn3 },
        { SigInA1, {"МЭД ГЦН4", "МЭД ГЦН4 ≤ 3000 КВт" }, a_Ngcn4, {ib_Ngcn4_less3000_az}, {{faz_Ngcn4_less_3000_A, faz_Ngcn4_less_3000_B, faz_Ngcn4_less_3000_C}}, {meq2_Ngcn4_less3000}, {ust_Ngcn4_l3000},1, ppn_MedGcn4, pkcAi_MedGcn4 },
        { SigInA1, {"P ГАЗ/06", "P ГАЗ/06 > 0.306 кгс/см2" }, a_Pgaz, {ib_Ppod_more0_306}, {{faz_Pgaz06_more0_306_A, faz_Pgaz06_more0_306_B, faz_Pgaz06_more0_306_C}}, {meq2_P_pod_more_0_306}, {ust_Ppod_m0_306}, 1, ppn_Pgaz06, nothing },
    },
    {  // слайд 2
        { SigInA2, { "ΔР ГЦН1, кгс/см2", "dР ГЦН1≤2.50 кгс/см2","dР ГЦН1≤4.00 кгс/см2" }, a_dPgcn1, {ib_dPgcn1_less2_5, ib_dPgcn1_less4}, {{faz_dPgcn1_less_2_5_A,faz_dPgcn1_less_2_5_B,faz_dPgcn1_less_2_5_C}, {faz_dPgcn1_less_4_0_A,faz_dPgcn1_less_4_0_B,faz_dPgcn1_less_4_0_C}}, {meq2_dPgcn1_less2_5, meq2_dPgcn1_less4_0}, {ust_dPgcn1_l2_5,ust_dPgcn1_l4_0}, 1, ppn_dPgcn1, pkcAi_dPgcn1 },
        { SigInA2, { "ΔР ГЦН2, кгс/см2", "dР ГЦН2≤2.50 кгс/см2","dР ГЦН2≤4.00 кгс/см2" }, a_dPgcn2, {ib_dPgcn1_less2_5, ib_dPgcn1_less4}, {{faz_dPgcn2_less_2_5_A,faz_dPgcn2_less_2_5_B,faz_dPgcn2_less_2_5_C}, {faz_dPgcn2_less_4_0_A,faz_dPgcn2_less_4_0_B,faz_dPgcn2_less_4_0_C}}, {meq2_dPgcn2_less2_5, meq2_dPgcn2_less4_0}, {ust_dPgcn2_l2_5,ust_dPgcn2_l4_0}, 1, ppn_dPgcn2, pkcAi_dPgcn2 },
        { SigInA2, { "ΔР ГЦН3, кгс/см2", "dР ГЦН3≤2.50 кгс/см2","dР ГЦН3≤4.00 кгс/см2" }, a_dPgcn3, {ib_dPgcn1_less2_5, ib_dPgcn1_less4}, {{faz_dPgcn3_less_2_5_A,faz_dPgcn3_less_2_5_B,faz_dPgcn3_less_2_5_C}, {faz_dPgcn3_less_4_0_A,faz_dPgcn3_less_4_0_B,faz_dPgcn3_less_4_0_C}}, {meq2_dPgcn3_less2_5, meq2_dPgcn3_less4_0}, {ust_dPgcn3_l2_5,ust_dPgcn3_l4_0}, 1, ppn_dPgcn3, pkcAi_dPgcn3 },
        { SigInA2, { "ΔР ГЦН4, кгс/см2", "dР ГЦН4≤2.50 кгс/см2","dР ГЦН4≤4.00 кгс/см2" }, a_dPgcn4, {ib_dPgcn1_less2_5, ib_dPgcn1_less4}, {{faz_dPgcn4_less_2_5_A,faz_dPgcn4_less_2_5_B,faz_dPgcn4_less_2_5_C}, {faz_dPgcn4_less_4_0_A,faz_dPgcn4_less_4_0_B,faz_dPgcn4_less_4_0_C}}, {meq2_dPgcn4_less2_5, meq2_dPgcn4_less4_0}, {ust_dPgcn4_l2_5,ust_dPgcn4_l4_0}, 1, ppn_dPgcn4, pkcAi_dPgcn4 },
        { SigInD, {"N ≥ N уст АЗ"}, 4, {ib_N_moreUstAz}, {{faz_Npdrd_A, faz_Npdrd_B, faz_Npdrd_C }}, {meq2_Npdrd}}, // АЗ
        { SigInD, {"СЕЙС.ВОЗД ≥ 6 баллов"}, PVK151_2+2, {ib_Seism}, {{faz_Seism_A, faz_Seism_B, faz_Seism_C}}, {meq2_seism}},
    },
    { // #3
        { SigInA1, {"P ТРОП ПГ1, кгс/см2", "P ТРОП ПГ1 ≤ 50 кгс/см2" }, a_Ppg1, {ib_Ppg1_less50}, {{faz_Ppg1_less50_A, faz_Ppg1_less50_B, faz_Ppg1_less50_C}}, {meq2_Ppg1_less50}, {ust_Ppg1_l50}, 1, ppn_Ptrop1, pkcAi_Ptrop1 },
        { SigInA1, {"Т НАС 1К - Т НАС ПП1 °С,", "Т НАС 1К - Т НАС ПП1 ≥ 75.0 °С," }, PZ153 + 34, {ib_dTspg1_more75}, {{faz_Ts_az__Ts_pg1_more75_C_A,faz_Ts_az__Ts_pg1_more75_C_B,faz_Ts_az__Ts_pg1_more75_C_C}}, {meq2_Taz_pg1_more75}, {ust_TsAz_TsPg1_m75}, 2, {ofl_Preak, ofl_Ptrop1}, {{blank},{blank}}    }, // todo!
        { SigInA1, {"Т ГРЧН1,°С", "Т ГРЧН1 ≥ 200,°С" }, a_Tgn1, {ib_Tgn1_more200}, {{faz_Tgn1_more200_C_A,faz_Tgn1_more200_C_B,faz_Tgn1_more200_C_C }}, {meq2_Tgn1_more200}, {ust_Tgn1_m200}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn1,imPkc_Tpgn1}},
        { SigInA1, {"Т ГРЧН2,°С", "Т ГРЧН2 ≥ 200,°С" }, a_Tgn2, {ib_Tgn2_more200}, {{faz_Tgn2_more200_C_A,faz_Tgn2_more200_C_B,faz_Tgn2_more200_C_C }}, {meq2_Tgn2_more200}, {ust_Tgn2_m200}, 2, {ofl_Tgr2_tep, ofl_Tgr2_tsp}, {im157_Tpgn2,imPkc_Tpgn2}},
        { SigInA1, {"Т ГРЧН3,°С", "Т ГРЧН3 ≥ 200,°С" }, a_Tgn3, {ib_Tgn3_more200}, {{faz_Tgn3_more200_C_A,faz_Tgn3_more200_C_B,faz_Tgn3_more200_C_C }}, {meq2_Tgn3_more200}, {ust_Tgn3_m200}, 2, {ofl_Tgr3_tep, ofl_Tgr3_tsp}, {im157_Tpgn3,imPkc_Tpgn3}},
        { SigInA1, {"Т ГРЧН4,°С", "Т ГРЧН4 ≥ 200,°С" }, a_Tgn4, {ib_Tgn4_more200}, {{faz_Tgn4_more200_C_A,faz_Tgn4_more200_C_B,faz_Tgn4_more200_C_C }}, {meq2_Tgn4_more200}, {ust_Tgn4_m200}, 2, {ofl_Tgr4_tep, ofl_Tgr4_tsp}, {im157_Tpgn4,imPkc_Tpgn4}}
    },
    { // #4
        { SigInA1, {"P ТРОП ПГ2, кгс/см2", "P ТРОП ПГ2 ≤ 50 кгс/см2" }, a_Ppg2, {ib_Ppg2_less50}, {{faz_Ppg2_less50_A, faz_Ppg2_less50_B, faz_Ppg2_less50_C}}, {meq2_Ppg2_less50}, { ust_Ppg2_l50 }, 1, {ofl_Ptrop2}, {im157_Ptrop2} },
        { SigInA1, {"Т НАС 1К - Т НАС ПП2 °С,", "Т НАС 1К - Т НАС ПП2 ≥ 75.0 °С," }, PZ153 + 35,  {ib_dTspg2_more75}, {{faz_Ts_az__Ts_pg2_more75_C_A,faz_Ts_az__Ts_pg2_more75_C_B,faz_Ts_az__Ts_pg2_more75_C_C}}, {meq2_Taz_pg2_more75}, {ust_TsAz_TsPg2_m75}, 2, {ofl_Preak, ofl_Ptrop2}, {{blank},{blank}} },
        { SigInA1, {"Т ГРЧН1,°С", "Т ГРЧН1 ≥ 200,°С" }, a_Tgn1, {ib_Tgn1_more200}, {{faz_Tgn1_more200_C_A,faz_Tgn1_more200_C_B,faz_Tgn1_more200_C_C }}, {meq2_Tgn1_more200}, {ust_Tgn1_m200}, 2, {ofl_Tgr1_tep,ofl_Tgr1_tsp}, {im157_Tpgn1,imPkc_Tpgn1}},
        { SigInA1, {"Т ГРЧН2,°С", "Т ГРЧН2 ≥ 200,°С" }, a_Tgn2, {ib_Tgn2_more200}, {{faz_Tgn2_more200_C_A,faz_Tgn2_more200_C_B,faz_Tgn2_more200_C_C }}, {meq2_Tgn2_more200}, {ust_Tgn2_m200}, 2, {ofl_Tgr2_tep,ofl_Tgr2_tsp}, {im157_Tpgn2,imPkc_Tpgn2}},
        { SigInA1, {"Т ГРЧН3,°С", "Т ГРЧН3 ≥ 200,°С" }, a_Tgn3, {ib_Tgn3_more200}, {{faz_Tgn3_more200_C_A,faz_Tgn3_more200_C_B,faz_Tgn3_more200_C_C }}, {meq2_Tgn3_more200}, {ust_Tgn3_m200}, 2, {ofl_Tgr3_tep,ofl_Tgr3_tsp}, {im157_Tpgn3,imPkc_Tpgn3}},
        { SigInA1, {"Т ГРЧН4,°С", "Т ГРЧН4 ≥ 200,°С" }, a_Tgn4, {ib_Tgn4_more200}, {{faz_Tgn4_more200_C_A,faz_Tgn4_more200_C_B,faz_Tgn4_more200_C_C }}, {meq2_Tgn4_more200}, {ust_Tgn4_m200}, 2, {ofl_Tgr4_tep,ofl_Tgr4_tsp}, {im157_Tpgn4,imPkc_Tpgn4}},
    },
    { // #5
        { SigInA1, {"P ТРОП ПГ3, кгс/см2", "P ТРОП ПГ3 ≤ 50 кгс/см2" }, a_Ppg3, {ib_Ppg3_less50}, {{faz_Ppg3_less50_A, faz_Ppg3_less50_B, faz_Ppg3_less50_C}}, {meq2_Ppg3_less50}, { ust_Ppg3_l50 }, 1, {ofl_Ptrop3}, {im157_Ptrop3} },
        { SigInA1, {"Т НАС 1К - Т НАС ПП3 °С,", "Т НАС 1К - Т НАС ПП3 ≥ 75.0 °С," }, PZ153 + 36,  {ib_dTspg3_more75}, {{faz_Ts_az__Ts_pg3_more75_C_A,faz_Ts_az__Ts_pg3_more75_C_B,faz_Ts_az__Ts_pg3_more75_C_C}}, {meq2_Taz_pg3_more75}, {ust_TsAz_TsPg3_m75}, 2, {ofl_Preak, ofl_Ptrop3}, {{blank},{blank}} },
        { SigInA1, {"Т ГРЧН1,°С", "Т ГРЧН1 ≥ 200,°С" }, a_Tgn1, {ib_Tgn1_more200}, {{faz_Tgn1_more200_C_A,faz_Tgn1_more200_C_B,faz_Tgn1_more200_C_C }}, {meq2_Tgn1_more200}, {ust_Tgn1_m200}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn1,imPkc_Tpgn1}},
        { SigInA1, {"Т ГРЧН2,°С", "Т ГРЧН2 ≥ 200,°С" }, a_Tgn2, {ib_Tgn2_more200}, {{faz_Tgn2_more200_C_A,faz_Tgn2_more200_C_B,faz_Tgn2_more200_C_C }}, {meq2_Tgn2_more200}, {ust_Tgn2_m200}, 2, {ofl_Tgr2_tep, ofl_Tgr2_tsp}, {im157_Tpgn2,imPkc_Tpgn2}},
        { SigInA1, {"Т ГРЧН3,°С", "Т ГРЧН3 ≥ 200,°С" }, a_Tgn3, {ib_Tgn3_more200}, {{faz_Tgn3_more200_C_A,faz_Tgn3_more200_C_B,faz_Tgn3_more200_C_C }}, {meq2_Tgn3_more200}, {ust_Tgn3_m200}, 2, {ofl_Tgr3_tep, ofl_Tgr3_tsp}, {im157_Tpgn3,imPkc_Tpgn3}},
        { SigInA1, {"Т ГРЧН4,°С", "Т ГРЧН4 ≥ 200,°С" }, a_Tgn4, {ib_Tgn4_more200}, {{faz_Tgn4_more200_C_A,faz_Tgn4_more200_C_B,faz_Tgn4_more200_C_C }}, {meq2_Tgn4_more200}, {ust_Tgn4_m200}, 2, {ofl_Tgr4_tep, ofl_Tgr4_tsp}, {im157_Tpgn4,imPkc_Tpgn4}},
    },
    { // #6
        { SigInA1, {"P ТРОП ПГ4, кгс/см2", "P ТРОП ПГ4 ≤ 50 кгс/см2" }, a_Ppg4, {ib_Ppg4_less50}, {{faz_Ppg4_less50_A, faz_Ppg4_less50_B, faz_Ppg4_less50_C}}, {meq2_Ppg4_less50}, { ust_Ppg4_l50 }, 1, {ofl_Ptrop4}, {im157_Ptrop4}   },
        { SigInA1, {"Т НАС 1К - Т НАС ПП4 °С,", "Т НАС 1К - Т НАС ПП4 ≥ 75.0 °С," }, PZ153 + 37, {ib_dTspg4_more75}, {{faz_Ts_az__Ts_pg4_more75_C_A,faz_Ts_az__Ts_pg4_more75_C_B,faz_Ts_az__Ts_pg4_more75_C_C}}, {meq2_Taz_pg4_more75}, {ust_TsAz_TsPg4_m75}, 2, {ofl_Preak, ofl_Ptrop4}, {{blank},{blank}} },
        { SigInA1, {"Т ГРЧН1,°С", "Т ГРЧН1 ≥ 200,°С" }, a_Tgn1, {ib_Tgn1_more200}, {{faz_Tgn1_more200_C_A,faz_Tgn1_more200_C_B,faz_Tgn1_more200_C_C }}, {meq2_Tgn1_more200}, {ust_Tgn1_m200}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn1,imPkc_Tpgn1}},
        { SigInA1, {"Т ГРЧН2,°С", "Т ГРЧН2 ≥ 200,°С" }, a_Tgn2, {ib_Tgn2_more200}, {{faz_Tgn2_more200_C_A,faz_Tgn2_more200_C_B,faz_Tgn2_more200_C_C }}, {meq2_Tgn2_more200}, {ust_Tgn2_m200}, 2, {ofl_Tgr2_tep, ofl_Tgr2_tsp}, {im157_Tpgn2,imPkc_Tpgn2}},
        { SigInA1, {"Т ГРЧН3,°С", "Т ГРЧН3 ≥ 200,°С" }, a_Tgn3, {ib_Tgn3_more200}, {{faz_Tgn3_more200_C_A,faz_Tgn3_more200_C_B,faz_Tgn3_more200_C_C }}, {meq2_Tgn3_more200}, {ust_Tgn3_m200}, 2, {ofl_Tgr3_tep, ofl_Tgr3_tsp}, {im157_Tpgn3,imPkc_Tpgn3}},
        { SigInA1, {"Т ГРЧН4,°С", "Т ГРЧН4 ≥ 200,°С" }, a_Tgn4, {ib_Tgn4_more200}, {{faz_Tgn4_more200_C_A,faz_Tgn4_more200_C_B,faz_Tgn4_more200_C_C }}, {meq2_Tgn4_more200}, {ust_Tgn4_m200}, 2, {ofl_Tgr4_tep, ofl_Tgr4_tsp}, {im157_Tpgn4,imPkc_Tpgn4}},
    },
    { // #7
        { SigInA1, {"P ТРОП ПГ1, кгс/см2", "P ТРОП ПГ1 ≥ 80 кгс/см2" }, a_Ppg1, {ib_Ppg2_more80}, {{faz_Ppg1_more80_A, faz_Ppg1_more80_B, faz_Ppg1_more80_C}}, {meq2_Ppg1_more80}, { ust_Ppg1_m80}, 1, {ofl_Ptrop1}, {im157_Ptrop1,imPkc_Ptrop1} },
        { SigInA1, {"P ТРОП ПГ2, кгс/см2", "P ТРОП ПГ2 ≥ 80 кгс/см2" }, a_Ppg2, {ib_Ppg2_more80}, {{faz_Ppg2_more80_A, faz_Ppg2_more80_B, faz_Ppg2_more80_C}}, {meq2_Ppg2_more80}, { ust_Ppg2_m80 }, 1, {ofl_Ptrop2}, {im157_Ptrop2,imPkc_Ptrop2} },
        { SigInA1, {"P ТРОП ПГ3, кгс/см2", "P ТРОП ПГ3 ≥ 80 кгс/см2" }, a_Ppg3, {ib_Ppg3_more80}, {{faz_Ppg3_more80_A, faz_Ppg3_more80_B, faz_Ppg3_more80_C}}, {meq2_Ppg3_more80}, { ust_Ppg3_m80 }, 1, {ofl_Ptrop3}, {im157_Ptrop3,imPkc_Ptrop3} },
        { SigInA1, {"P ТРОП ПГ4, кгс/см2", "P ТРОП ПГ4 ≥ 80 кгс/см2" }, a_Ppg4, {ib_Ppg4_more80}, {{faz_Ppg4_more80_A, faz_Ppg4_more80_B, faz_Ppg4_more80_C}}, {meq2_Ppg4_more80}, { ust_Ppg4_m80 }, 1, {ofl_Ptrop4}, {im157_Ptrop4,imPkc_Ptrop4} },
        { SigInD, {"N ≥ 40%"}, 2, {ib_N_more40}, {{faz_N_more40_A, faz_N_more40_B, faz_N_more40_C}}, {meq2_N_more40} },
        { SigInA1, {"P ЗКП ТПН1, кгс/см2", "P ЗКП ТПН1 ≤ 0.408 кгс/см2" }, a_Ptpn1, {ib_dPtpn1_less0_408_az}, {{faz_Ptpn1_less_0_408_A, faz_Ptpn1_less_0_408_B, faz_Ptpn1_less_0_408_C}}, {meq2_Ptpn1_less0_408}, { ust_Ptpn1_l0_408 }, 1, {ofl_Pzkp1}, {im157_Ptpn1,imPkc_Ptpn1} },
        { SigInA1, {"P ЗКП ТПН2, кгс/см2", "P ЗКП ТПН2 ≤ 0.408 кгс/см2" }, a_Ptpn2, {ib_dPtpn2_less0_408_az}, {{faz_Ptpn2_less_0_408_A, faz_Ptpn2_less_0_408_B, faz_Ptpn2_less_0_408_C}}, {meq2_Ptpn2_less0_408}, { ust_Ptpn2_l0_408 }, 1, {ofl_Pzkp2}, {im157_Ptpn2,imPkc_Ptpn2} },
    },
    { // #8
        { SigInA1, {"Т ГРЧН1,°С", "Т ГРЧН1 ≥ 330 °С" }, a_Tgn1, {ib_Tgn1_more330}, {{faz_Tgn1_more330_C_A, faz_Tgn1_more330_C_B, faz_Tgn1_more330_C_C}}, {meq2_Tgn1_more330}, {ust_Tgn1_m330}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn1,imPkc_Tpgn1} },
        { SigInA1, {"Т ГРЧН2,°С", "Т ГРЧН2 ≥ 330 °С" }, a_Tgn2, {ib_Tgn2_more330}, {{faz_Tgn2_more330_C_A, faz_Tgn2_more330_C_B, faz_Tgn2_more330_C_C}}, {meq2_Tgn2_more330}, {ust_Tgn2_m330}, 2, {ofl_Tgr2_tep, ofl_Tgr2_tsp}, {im157_Tpgn2,imPkc_Tpgn2} },
        { SigInA1, {"Т ГРЧН3,°С", "Т ГРЧН3 ≥ 330 °С" }, a_Tgn3, {ib_Tgn3_more330}, {{faz_Tgn3_more330_C_A, faz_Tgn3_more330_C_B, faz_Tgn3_more330_C_C}}, {meq2_Tgn3_more330}, {ust_Tgn3_m330}, 2, {ofl_Tgr3_tep, ofl_Tgr3_tsp}, {im157_Tpgn3,imPkc_Tpgn3} },
        { SigInA1, {"Т ГРЧН4,°С", "Т ГРЧН4 ≥ 330 °С" }, a_Tgn4, {ib_Tgn4_more330}, {{faz_Tgn4_more330_C_A, faz_Tgn4_more330_C_B, faz_Tgn4_more330_C_C}}, {meq2_Tgn4_more330}, {ust_Tgn4_m330}, 2, {ofl_Tgr4_tep, ofl_Tgr4_tsp}, {im157_Tpgn4,imPkc_Tpgn4} },
        { SigInD, {"N ≥ 5%, кгс/см2"}, 1, {ib_N_more5}, {{faz_N_more5_A, faz_N_more5_B, faz_N_more5_C}}, {meq2_N_more5} },
        { SigInA2, { "L КД, см", "L КД ≤ 460 см", "НЕИСПР. L КД"}, a_Lkd, {ib_Lkd_less460, ib_Lkd_neispr}, {{faz_L_kd_less_460_A, faz_L_kd_less_460_B, faz_L_kd_less_460_C}, {faz_fail_Lkd_A,faz_fail_Lkd_B,faz_fail_Lkd_C}}, {meq2_Lkd_less460, meq2_Lkd_neispr}, {ust_Lkd_l460,{blank}}, 1, {ofl_Lkd}, {im157_Lkd,imPkc_Lkd} },
    },
    { // #9
        { SigInA1, {"F ПИТ ГЦН1, Гц", "F ПИТ ГЦН1 ≤ 46.000 Гц" }, a_Fgcn1, {ib_Fgcn1_less46}, {{faz_Fgcn1_less_46_A, faz_Fgcn1_less_46_B, faz_Fgcn1_less_46_C}}, {meq2_Fgcn1_less46}, {ust_Fgcn1_l46}, 1, {ofl_Fpit1}, {im157_Fgcn1,imPkc_Fgcn1} },
        { SigInA1, {"F ПИТ ГЦН2, Гц", "F ПИТ ГЦН2 ≤ 46.000 Гц" }, a_Fgcn2, {ib_Fgcn2_less46}, {{faz_Fgcn2_less_46_A, faz_Fgcn2_less_46_B, faz_Fgcn2_less_46_C}}, {meq2_Fgcn2_less46}, {ust_Fgcn2_l46}, 1, {ofl_Fpit2}, {im157_Fgcn2,imPkc_Fgcn2} },
        { SigInA1, {"F ПИТ ГЦН3, Гц", "F ПИТ ГЦН3 ≤ 46.000 Гц" }, a_Fgcn3, {ib_Fgcn3_less46}, {{faz_Fgcn3_less_46_A, faz_Fgcn3_less_46_B, faz_Fgcn3_less_46_C}}, {meq2_Fgcn3_less46}, {ust_Fgcn3_l46}, 1, {ofl_Fpit3}, {im157_Fgcn3,imPkc_Fgcn3} },
        { SigInA1, {"F ПИТ ГЦН4, Гц", "F ПИТ ГЦН4 ≤ 46.000 Гц" }, a_Fgcn4, {ib_Fgcn4_less46}, {{faz_Fgcn4_less_46_A, faz_Fgcn4_less_46_B, faz_Fgcn4_less_46_C}}, {meq2_Fgcn4_less46}, {ust_Fgcn4_l46}, 1, {ofl_Fpit4}, {im157_Fgcn4,imPkc_Fgcn4} },
    },
    { //10
        { SigInA1, { "Т НАС 1К - Т МАХ ГРЧН 1", "Т НАС 1К - Т МАХ ГРЧН 1 ≤ 10.0°С" }, a_Tsaz_Tsgn1, {ib_dTsgn1_less10}, {{faz_Ts_az__Tgn1_less_10_C_A,faz_Ts_az__Tgn1_less_10_C_B,faz_Ts_az__Tgn1_less_10_C_C}}, {meq2_Taz_Tgn1_less10}, {ust_TsAz_Tgn1_l10}, 3, {ofl_Preak, ofl_Tgr1_tep, ofl_Tgr1_tsp}, {{blank},{blank}} },
        { SigInA1, { "Т НАС 1К - Т МАХ ГРЧН 2", "Т НАС 1К - Т МАХ ГРЧН 2 ≤ 10.0°С" }, a_Tsaz_Tsgn2, {ib_dTsgn2_less10}, {{faz_Ts_az__Tgn2_less_10_C_A,faz_Ts_az__Tgn2_less_10_C_B,faz_Ts_az__Tgn2_less_10_C_C}}, {meq2_Taz_Tgn2_less10}, {ust_TsAz_Tgn2_l10}, 3, {ofl_Preak, ofl_Tgr2_tep, ofl_Tgr2_tsp}, {{blank},{blank}} },
        { SigInA1, { "Т НАС 1К - Т МАХ ГРЧН 3", "Т НАС 1К - Т МАХ ГРЧН 3 ≤ 10.0°С" }, a_Tsaz_Tsgn3, {ib_dTsgn3_less10}, {{faz_Ts_az__Tgn3_less_10_C_A,faz_Ts_az__Tgn3_less_10_C_B,faz_Ts_az__Tgn3_less_10_C_C}}, {meq2_Taz_Tgn3_less10}, {ust_TsAz_Tgn3_l10}, 3, {ofl_Preak, ofl_Tgr3_tep, ofl_Tgr3_tsp}, {{blank},{blank}} },
        { SigInA1, { "Т НАС 1К - Т МАХ ГРЧН 4", "Т НАС 1К - Т МАХ ГРЧН 4 ≤ 10.0°С" }, a_Tsaz_Tsgn4, {ib_dTsgn4_less10}, {{faz_Ts_az__Tgn4_less_10_C_A,faz_Ts_az__Tgn4_less_10_C_B,faz_Ts_az__Tgn4_less_10_C_C}}, {meq2_Taz_Tgn4_less10}, {ust_TsAz_Tgn4_l10}, 3, {ofl_Preak, ofl_Tgr4_tep, ofl_Tgr4_tsp}, {{blank},{blank}} },
    },
    { //11
      { SigInA2, { "P РЕАК, кгс/см2", "P РЕАК ≤ 140.0 кгс/см2","P РЕАК ≥ 150.0 кгс/см2"}, a_Paz, {ib_Paz_less140, ib_Paz_more150}, {{faz_Paz_less140_A, faz_Paz_less140_B, faz_Paz_less140_C},{faz_Paz_more150_A, faz_Paz_more150_B, faz_Paz_more150_C}}, {meq2_Paz_less140, meq2_Paz_more150}, {ust_Paz_l140, ust_Paz_m150}, 1, {ofl_Preak} },
      { SigInA2, { "Т ГРЧН 1, °С", "Т ГРЧН 1 ≥ 260.0°С", "Т РГЧН 1 ≤ 268.0°С"}, a_Tgn1, {ib_Tgn1_more260, ib_Tgn1_less268}, {{faz_Tgn1_more260_C_A,faz_Tgn1_more260_C_B,faz_Tgn1_more260_C_C}, {faz_Tgn1_less_268_C_A,faz_Tgn1_less_268_C_B,faz_Tgn1_less_268_C_C}}, {meq2_Tgn1_more260, meq2_Tgn1_less268}, {ust_Tgn1_m260,ust_Tgn1_l268}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn1,imPkc_Tpgn1} },
      { SigInA2, { "Т ГРЧН 2, °С", "Т РГЧН 2 ≥ 260.0°С", "Т ГРЧН 2 ≤ 260.0°С"}, a_Tgn2, {ib_Tgn2_more260, ib_Tgn2_less268}, {{faz_Tgn2_more260_C_A,faz_Tgn2_more260_C_B,faz_Tgn2_more260_C_C}, {faz_Tgn2_less_268_C_A,faz_Tgn2_less_268_C_B,faz_Tgn2_less_268_C_C}}, {meq2_Tgn2_more260, meq2_Tgn2_less268}, {ust_Tgn2_m260,ust_Tgn2_l268}, 2, {ofl_Tgr2_tep, ofl_Tgr2_tsp}, {im157_Tpgn2,imPkc_Tpgn2} },
      { SigInA2, { "Т ГРЧН 3, °С", "Т РГЧН 3 ≥ 260.0°С", "Т ГРЧН 3 ≤ 260.0°С"}, a_Tgn3, {ib_Tgn3_more260, ib_Tgn3_less268}, {{faz_Tgn3_more260_C_A,faz_Tgn3_more260_C_B,faz_Tgn3_more260_C_C}, {faz_Tgn3_less_268_C_A,faz_Tgn3_less_268_C_B,faz_Tgn3_less_268_C_C}}, {meq2_Tgn3_more260, meq2_Tgn3_less268}, {ust_Tgn3_m260,ust_Tgn3_l268}, 2, {ofl_Tgr3_tep, ofl_Tgr3_tsp}, {im157_Tpgn3,imPkc_Tpgn3} },
      { SigInA2, { "Т ГРЧН 4, °С", "Т РГЧН 4 ≥ 260.0°С", "Т ГРЧН 4 ≤ 260.0°С"}, a_Tgn4, {ib_Tgn4_more260, ib_Tgn4_less268}, {{faz_Tgn4_more260_C_A,faz_Tgn4_more260_C_B,faz_Tgn4_more260_C_C}, {faz_Tgn4_less_268_C_A,faz_Tgn4_less_268_C_B,faz_Tgn4_less_268_C_C}}, {meq2_Tgn4_more260, meq2_Tgn4_less268}, {ust_Tgn4_m260,ust_Tgn4_l268}, 2, {ofl_Tgr4_tep, ofl_Tgr4_tsp}, {im157_Tpgn4,imPkc_Tpgn4} },
    },
    { //12
      { SigInA1, {"Т ГРЧН 1, °С", "Т ГРЧН 1 ≥ 325°С" }, p_Tgn1, {ib_Tgn1_more260, ib_Tgn1_less268}, {{fpz_Tgn1_more325_C_A,fpz_Tgn1_more325_C_B,fpz_Tgn1_more325_C_C}}, {meq2_Tgn1_more325_pz}, {ust_Tgn1_m325}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn1,imPkc_Tpgn1} }, // Тном +3 = 322+3 = 325
      { SigInA1, {"Т ГРЧН 2, °С", "Т ГРЧН 2 ≥ 325°С" }, p_Tgn2, {ib_Tgn2_more260, ib_Tgn2_less268}, {{fpz_Tgn2_more325_C_A,fpz_Tgn2_more325_C_B,fpz_Tgn2_more325_C_C}}, {meq2_Tgn2_more325_pz}, {ust_Tgn2_m325}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn2,imPkc_Tpgn2} },
      { SigInA1, { "Т ГРЧН 3, °С", "Т ГРЧН 3 ≥ 325°С"}, p_Tgn3, {ib_Tgn3_more260, ib_Tgn3_less268}, {{fpz_Tgn3_more325_C_A,fpz_Tgn3_more325_C_B,fpz_Tgn3_more325_C_C}}, {meq2_Tgn3_more325_pz}, {ust_Tgn3_m325}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn3,imPkc_Tpgn3} },
      { SigInA1, { "Т ГРЧН 4, °С", "Т ГРЧН 4 ≥ 325°С"}, p_Tgn4, {ib_Tgn4_more260, ib_Tgn4_less268}, {{fpz_Tgn4_more325_C_A,fpz_Tgn4_more325_C_B,fpz_Tgn4_more325_C_C}}, {meq2_Tgn4_more325_pz}, {ust_Tgn4_m325}, 2, {ofl_Tgr1_tep, ofl_Tgr1_tsp}, {im157_Tpgn4,imPkc_Tpgn4} },
      { SigInA1, { "P РЕАК, кгс/см2", "P РЕАК ≥ 170.3 кгс/см2"}, p_Paz, {ib_Paz_more170_3}, {{fpz_Ppz_more_170_3_A,fpz_Ppz_more_170_3_B,fpz_Ppz_more_170_3_C}}, {meq2_Preak_more170_3_pz }, {ust_PazPz_m170_3}, 1, {ofl_Preak}, {im157_Preak,imPkc_Preak} },
      { SigInA1, { "Р ГПК, кг/см2", "Р ГПК ≥ 65.00 кгс/см2"}, p_Pgpk, {ib_Pgpk_more70}, {{fpz_P_gpk_more_70_A,fpz_P_gpk_more_70_B,fpz_P_gpk_more_70_C}}, {meq2_Pgpk_more70_pz}, {ust_Pgpk_m70}, 1, {ofl_Pgpk}, {im157_Pgpk,imPkc_Pgpk} },
      { SigInD, { "Т ≤ 20с ПЗ-1"}, 8, {ib_period_less20}, {{fpz_Tpdrd_less20_A,fpz_Tpdrd_less20_B,fpz_Tpdrd_less20_C}}, {meq2_Tpdrd_less20_pz} },
      { SigInD, { "N ≥ Nуст.ПЗ-1"}, 5, {ib_N_moreUstPz1},  {{fpz_Npdrd_moreX_A,fpz_Npdrd_moreX_B,fpz_Npdrd_moreX_C}}, {meq2_Npdrd_pz} },
      { SigInD, {"РОМ"}, PVK151_2 + 5, {ib_razgruz}, {{fpz_Razgr_A,fpz_Razgr_B,fpz_Razgr_C}}, {meq2_Razgruzka_pz} },
    },
    { //13
      { SigInD, { "N ≥ N ПЗ-2"}, 6, {ib_N_moreUstPz2}, {{fpz_Npdrd_more10_A,fpz_Npdrd_more10_B,fpz_Npdrd_more10_C}}, {meq2_Npdrd_more10_15az_pz}},
      { SigInA1, { "P РЕАК, кгс/см2", "P РЕАК ≥ 165.2 кгс/см2"}, p_Paz, {ib_Paz_more165_2}, {{fpz_Ppz_more_165_2_A,fpz_Ppz_more_165_2_B,fpz_Ppz_more_165_2_C}}, {meq2_Paz_more165_2_pz}, {ust_PazPz_m165_2}, 1, {ofl_Preak}, {im157_Preak,imPkc_Preak} },
      { SigInD, { "ПАДЕНИЕ ОР"}, 18, {ib_padenieOr}, {{fpz_Paden_A,fpz_Paden_B,fpz_Paden_C}}, {meq2_padenie1OR_pz}},
      { SigInD, { "QL ≥ 350 ПЗ-2(СВРК)"}, PVK151_2+16, {ib_QL_Pz2}, {{fpz_QL_more350_A, fpz_QL_more350_B, fpz_QL_more350_C}}, {meq2_QL_more350_pz}},
      { SigInD, { "DNBR ≤ 1.5(СВРК)"}, PVK151_2+17, {ib_DNBR_Pz2}, {{fpz_DNBR_less1_45_A, fpz_DNBR_less1_45_B, fpz_DNBR_less1_45_C}}, {meq2_DNBR_less1_45_pz}},
    },
    { //14
      { SigInD, { "N ≥ 75%"}, 3, {ib_N_more75_pz}, {{fpz_N_more75_A,fpz_N_more75_B,fpz_N_more75_C}}, {meq2_N_more75_pz} },
      { SigInA1, {"МЭД ГЦН 1", "МЭД ГЦН1 ≤ 3000 кВт"}, p_Ngcn1, {ib_Ngcn1_less3000_pz}, {{fpz_Ngcn1_less_3000_A,fpz_Ngcn1_less_3000_B,fpz_Ngcn1_less_3000_C}}, {meq2_Ngcn1_less3000_pz}, {ust_Ngcn1Pz_l3000}, 1, {ofl_Med1}, {im157_MedGcn1,imPkc_MedGcn1} },
      { SigInA1, {"МЭД ГЦН 2", "МЭД ГЦН2 ≤ 3000 кВт"}, p_Ngcn2, {ib_Ngcn2_less3000_pz}, {{fpz_Ngcn2_less_3000_A,fpz_Ngcn2_less_3000_B,fpz_Ngcn2_less_3000_C}}, {meq2_Ngcn2_less3000_pz}, {ust_Ngcn2Pz_l3000}, 1, {ofl_Med2}, {im157_MedGcn2,imPkc_MedGcn2} },
      { SigInA1, {"МЭД ГЦН 3", "МЭД ГЦН3 ≤ 3000 кВт"}, p_Ngcn3, {ib_Ngcn3_less3000_pz}, {{fpz_Ngcn3_less_3000_A,fpz_Ngcn3_less_3000_B,fpz_Ngcn3_less_3000_C}}, {meq2_Ngcn3_less3000_pz}, {ust_Ngcn3Pz_l3000}, 1, {ofl_Med3}, {im157_MedGcn3,imPkc_MedGcn3} },
      { SigInA1, {"МЭД ГЦН 4", "МЭД ГЦН4 ≤ 3000 кВт"}, p_Ngcn4, {ib_Ngcn4_less3000_pz}, {{fpz_Ngcn4_less_3000_A,fpz_Ngcn4_less_3000_B,fpz_Ngcn4_less_3000_C}}, {meq2_Ngcn4_less3000_pz}, {ust_Ngcn4Pz_l3000}, 1, {ofl_Med4}, {im157_MedGcn4,imPkc_MedGcn4} },
      { SigInD, { "ОТКЛ. ЭБ"}, PVK151_2 +6, {ib_otklEb}, {{fpz_Otkl_EB_A,fpz_Otkl_EB_B,fpz_Otkl_EB_C}},{meq2_otklEB_pz} },
      { SigInD, { "ВЫКЛ. ГЕНЕР."}, PVK151_2+7, {ib_otklGen}, {{fpz_Otkl_Gen_A,fpz_Otkl_Gen_B,fpz_Otkl_Gen_C}}, {meq2_otlGen_pz}},
    },
    { //15
      { SigInD, { "N ≥ 5%"}, 1, {ib_N_more5}, {{faz_N_more5_A,faz_N_more5_B,faz_N_more5_C}}, {meq2_N_more5}},
      { SigInA2, { "L ПВ ПГ 1", "L ПВ ПГ 1 ≤ 175 см", "НЕИСПР. L ПВ ПГ 1"}, a_Lpg1, {ib_Ppg1_less175}, {{faz_Lpg1_less_175_A,faz_Lpg1_less_175_B,faz_Lpg1_less_175_C}, {faz_fail_Lpg1_A,faz_fail_Lpg1_B,faz_fail_Lpg1_C}}, {meq2_Lpg1_less175, meq2_Lpg1_neispr}, {ust_Lpg1_l175, {blank}}, 1, {ofl_Lpg1}, {im157_Lpg1,imPkc_Lpg1} },
      { SigInA2, { "L ПВ ПГ 2", "L ПВ ПГ 2 ≤ 175 см", "НЕИСПР. L ПВ ПГ 2"}, a_Lpg2, {ib_Ppg2_less175}, {{faz_Lpg2_less_175_A,faz_Lpg2_less_175_B,faz_Lpg2_less_175_C}, {faz_fail_Lpg2_A,faz_fail_Lpg2_B,faz_fail_Lpg2_C}}, {meq2_Lpg2_less175, meq2_Lpg2_neispr}, {ust_Lpg2_l175, {blank}}, 1, {ofl_Lpg2}, {im157_Lpg1,imPkc_Lpg2} },
      { SigInA2, { "L ПВ ПГ 3", "L ПВ ПГ 3 ≤ 175 см", "НЕИСПР. L ПВ ПГ 3"}, a_Lpg3, {ib_Ppg3_less175}, {{faz_Lpg3_less_175_A,faz_Lpg3_less_175_B,faz_Lpg3_less_175_C}, {faz_fail_Lpg3_A,faz_fail_Lpg3_B,faz_fail_Lpg3_C}}, {meq2_Lpg3_less175, meq2_Lpg3_neispr}, {ust_Lpg3_l175, {blank}}, 1, {ofl_Lpg3}, {im157_Lpg1,imPkc_Lpg3} },
      { SigInA2, { "L ПВ ПГ 4", "L ПВ ПГ 4 ≤ 175 см", "НЕИСПР. L ПВ ПГ 4"}, a_Lpg4, {ib_Ppg4_less175}, {{faz_Lpg4_less_175_A,faz_Lpg4_less_175_B,faz_Lpg4_less_175_C}, {faz_fail_Lpg4_A,faz_fail_Lpg4_B,faz_fail_Lpg4_C} }, {meq2_Lpg4_less175, meq2_Lpg4_neispr}, {ust_Lpg4_l175, {blank}}, 1, {ofl_Lpg4}, {im157_Lpg1,imPkc_Lpg4} },
      { SigInD, { "ПОТЕРЯ ПИТ.СУЗ"}, 17, {ib_potPitSuz}, {{faz_PotPitSuz_A,faz_PotPitSuz_B,faz_PotPitSuz_C}}, {meq2_PotPitSuz}},
      { SigInD, { "Т ≤ 10с АЗ"}, 7, {ib_period_less10}, {{faz_Tpdrd_less10_A, faz_Tpdrd_less10_B, faz_Tpdrd_less10_C}}, {meq2_Tpdrd_less10}},
    },
    { //16
      { SigInD, { "N ≥ 75%"}, 3, {ib_N_more75_pz}, {{fpz_N_more75_A,fpz_N_more75_B,fpz_N_more75_C}},{meq2_N_more75_pz}},
      { SigInA1, { "Р ЗКП ТПН 1, кгс/см2", "Р ЗКП ТПН 1 ≤ 0.408 кгс/см2"}, a_Ptpn1, {ib_dPtpn1_less0_408_pz}, {{fpz_P_tpn1_less_0_408_A,fpz_P_tpn1_less_0_408_B,fpz_P_tpn1_less_0_408_C}}, {meq2_Ptpn1_less0_408}, {ust_Ptpn1Pz_l0_408}, 1, {ofl_Pzkp1}, {im157_Ptpn1,imPkc_Ptpn1} },
      { SigInA1, { "Р ЗКП ТПН 2, кгс/см2", "Р ЗКП ТПН 2 ≤ 0.408 кгс/см2"}, a_Ptpn2, {ib_dPtpn2_less0_408_pz}, {{fpz_P_tpn2_less_0_408_A,fpz_P_tpn2_less_0_408_B,fpz_P_tpn2_less_0_408_C}}, {meq2_Ptpn2_less0_408}, {ust_Ptpn2Pz_l0_408}, 1, {ofl_Pzkp2}, {im157_Ptpn2,imPkc_Ptpn2} },
      { SigInD, { "ЗАКР. 2/4 СК."}, PVK151_2+8, {ib_zakrSk}, {{fpz_Zakr_A,fpz_Zakr_B,fpz_Zakr_C}}, {meq2_ZakrBolee1sk_pz} },
    },
    { //17
      { SigInD, { "QL ≥ 400 ПЗ-1(СВРК)"}, PVK151_2+14, {ib_QL_Pz1},  {{fpz_QL_more400_A, fpz_QL_more400_B, fpz_QL_more400_C}}, {meq2_QL_more400_pz}},
      { SigInD, { "DNBR ≤ 1.4(СВРК)"}, PVK151_2+15, {ib_DNBR_Pz1}, {{fpz_DNBR_less1_40_A, fpz_DNBR_less1_40_B, fpz_DNBR_less1_40_C}}, {meq2_DNBR_less1_40_pz}},
    }
};


// количество выходов на каждом из алг.слайдов
int algSlideOutputsN[AlgSlidesN] = {5,6,1,1,1,1,2,2,1,4,1,6,5,3,6,2,2};


// x,y, модуль, #номер выхода (нумерация с 1 !!!), наименование
const int px1=610;

OutSignals outSigs[AlgSlidesN][6] = {
    { // слайд алг#1
        { px1, 40, p155az1, 4+1, "АЗ №1",alg153az_1, isAz1 }, // АЗ №1, пвк155_1 и т.д.
        { px1, 115, p155az1, 4+9, "АЗ №9",alg153az_9, isAz9 }, // АЗ №9
        { px1, 230, p155az1, 4+2, "АЗ №2",alg153az_2, isAz2 },
        { px1, 310, p155az1, 4+3, "АЗ №3",alg153az_3, isAz3 },
        { px1, 540, p155az1, 4+10, "АЗ №10",alg153az_10, isAz10 }
    },
    { // слайд алг#2
        { px1, 50, p155az1, 4+12, "АЗ №12",alg153az_12, isAz12 },
        { px1, 160, p155az1, 4+13, "АЗ №13",alg153az_13, isAz13 },
        { px1, 280, p155az1, 4+14, "АЗ №14",alg153az_14, isAz14 },
        { px1, 405, p155az1, 4+15, "АЗ №15",alg153az_15, isAz15 },
        { px1, 470, p155az2, 10, "АЗ №29",alg153az_29, isAz29 }, // АЗ №29, пвк 155_2
        { px1, 535, p155az2, 11, "АЗ №30",alg153az_30, isAz30 } // АЗ №30
    },
    { // #3
        { px1, 50, p155az1, 4+16, "АЗ №16",alg153az_16, isAz16 } // пвк 155_1
    },
    { // #4
        { px1, 50, p155az1, 4+17, "АЗ №17",alg153az_17, isAz17 }
    },
    { // #5
        { px1, 50, p155az1, 4+18, "АЗ №18",alg153az_18, isAz18 }
    },
    { // #6
        { px1, 50, p155az1, 4+19, "АЗ №19",alg153az_19, isAz19 }
    },
    { // #7
        { px1, 60, p155az2, 4+20, "АЗ №20",alg153az_20, isAz20 },
        { px1, 330, p155az2, 8, "АЗ №27",alg153az_27, isAz27 } // АЗ№27 пвк155_2
    },
    { // #8
        { px1, 60, p155az1, 24,  "АЗ №21",alg153az_21, isAz21 }, // АЗ№21
        { px1, 400, p155az1, 24,  "АЗ №8",alg153az_8, isAz8 } // АЗ№8
    },
    { // #9
        { px1, 60, p155az2, 3, "АЗ №22",alg153az_22, isAz22 } //
    },
    { // #10
      { px1, 30, p155az2, 4, "АЗ №23",alg153az_23, isAz23},
      { px1, 110, p155az2, 5, "АЗ №24",alg153az_24, isAz24 },
      { px1, 190, p155az2, 6, "АЗ №25",alg153az_25, isAz25 },
      { px1, 270, p155az2, 7, "АЗ №26",alg153az_26, isAz26 }
    },
    { // #11
      { px1, 50, p155az2, 12, "АЗ №31",alg153az_31, isAz31 }
    },
    { // слайд алг#12
//      { px1, 150, p155pz1, 19, "ПЗ-1 №1", alg153pz1_1, isPz1_1 },
//      { px1, 230, p155pz1, 20, "ПЗ-1 №2", alg153pz1_2, isPz1_2 },
//      { px1, 300, p155pz1, 21, "ПЗ-1 №3", alg153pz1_3, isPz1_3 },
//      { px1, 380, p155pz1, 22, "ПЗ-1 №4", alg153pz1_4, isPz1_4 },
//      { px1, 460, p155pz1, 23, "ПЗ-1 №5", alg153pz1_5, isPz1_5 },
      { px1, 150, p155pz1, 20, "ПЗ-1 №1", alg153pz1_1, isPz1_1 },
      { px1, 230, p155pz1, 21, "ПЗ-1 №2", alg153pz1_2, isPz1_2 },
      { px1, 300, p155pz1, 22, "ПЗ-1 №3", alg153pz1_3, isPz1_3 },
      { px1, 380, p155pz1, 23, "ПЗ-1 №4", alg153pz1_4, isPz1_4 },
      { px1, 460, p155pz1, 24, "ПЗ-1 №5", alg153pz1_5, isPz1_5 },

      { px1, 540, p155pz2, 7, "ПЗ-1 №6", alg153pz1_6, isPz1_6 }
    },
    { // #13
      { px1, 0, p155pz2, 10, "ПЗ-2 №1", alg153pz2_1, isPz2_1 },
      { px1, 70, p155pz2, 11, "ПЗ-2 №2", alg153pz2_2, isPz2_2 },
      { px1, 160, p155pz2, 12, "ПЗ-2 №3", alg153pz2_3, isPz2_3 },
      { px1, 240, p155pz2, 13, "ПЗ-2 №4", alg153pz2_4, isPz2_4 },
      { px1, 320, p155pz2, 14, "ПЗ-2 №5", alg153pz2_5, isPz2_5 }
    },
    { // #14
      { px1, 5, p155pz2, 15, "УПЗ №1", alg153upz_1, isUpz1 },
      { px1, 375, p155pz2, 16, "УПЗ №2", alg153upz_2, isUpz2 },
      { px1, 445, p155pz2, 17, "УПЗ №3", alg153upz_3, isUpz3 },
    },
    { // #15
      { px1, 50, p155az1, 4+4, "АЗ №4", alg153az_4, isAz4 },
      { px1, 150, p155az1, 4+5, "АЗ №5", alg153az_5, isAz5 },
      { px1, 250, p155az1, 4+6, "АЗ №6", alg153az_6, isAz6 },
      { px1, 350, p155az1, 4+7, "АЗ №7", alg153az_7, isAz7  },
      { px1, 440, p155az1, 4+11, "АЗ №11", alg153az_11, isAz11 },
      { px1, 530, p155az2, 9, "АЗ №28", alg153az_28, isAz28 }
    },
    { // #16
      { px1, 0, p155pz2, 18, "УПЗ №4", alg153upz_4, isUpz4 },
      { px1, 180, p155pz2, 19, "УПЗ №5", alg153upz_5, isUpz5 },
    },
    { // #17
      { px1, 50, p155pz2, 8, "ПЗ-1 №7", alg153pz1_7, isPz1_7 },
      { px1, 150, p155pz2, 9, "ПЗ-1 №8", alg153pz1_8, isPz1_8 }
    }
};

// вых. ШАК
const int shx=712;

OutSignals shakSigs[AlgSlidesN][6] = {
    { // слайд алг#1
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#2
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#3
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#4
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#5
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#6
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#7
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#8
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#9
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#10
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#11
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#12 ПЗ-1
        { shx, 150, p155pz1, 1, "ШАК 1.1", {p153pz,80,2} },
        { shx, 250, p155pz2, 1, "ШАК 1.2", {p153pz,80,2} },
        { shx, 350, p155pz1, 2, "ШАК 2.1", {p153pz,80,2} },
        { shx, 450, p155pz2, 2, "ШАК 2.2", {p153pz,80,2} }
    },
    { // слайд алг#13, ПЗ-2
        { shx, 150, p155pz1, 3, "ШАК 1.1", {p153pz,80,3} },
        { shx, 250, p155pz2, 3, "ШАК 1.2", {p153pz,80,3} },
        { shx, 350, p155pz1, 4, "ШАК 2.1", {p153pz,80,3} },
        { shx, 450, p155pz2, 4, "ШАК 2.2", {p153pz,80,3} }
    },
    { // слайд алг#14, УПЗ
        { shx, 150, p155pz1, 5, "ШАК 1.1", {p153pz,80,4} },
        { shx, 250, p155pz2, 5, "ШАК 1.2", {p153pz,80,4} },
        { shx, 350, p155pz1, 6, "ШАК 2.1", {p153pz,80,4} },
        { shx, 450, p155pz2, 6, "ШАК 2.2", {p153pz,80,4} }
    },
    { // слайд алг#15, АЗ
        { shx, 150, p155az1, 1, "ШАК 1.1", {p153az,282,7} },
        { shx, 250, p155az2, 1, "ШАК 1.2", {p153az,282,7} },
        { shx, 350, p155az1, 2, "ШАК 2.1", {p153az,282,7} },
        { shx, 450, p155az2, 2, "ШАК 2.2", {p153az,282,7} }
    },
    { // слайд алг#16, УПЗ
        { shx, 150, p155pz1, 5, "ШАК 1.1", {p153pz,80,4} },
        { shx, 250, p155pz2, 5, "ШАК 1.2", {p153pz,80,4} },
        { shx, 350, p155pz1, 6, "ШАК 2.1", {p153pz,80,4} },
        { shx, 450, p155pz2, 6, "ШАК 2.2", {p153pz,80,4} }
    },
    { // слайд алг#17 ПЗ-1
        { shx, 150, p155pz1, 1, "ШАК 1.1", {p153pz,80,2} },
        { shx, 250, p155pz2, 1, "ШАК 1.2", {p153pz,80,2} },
        { shx, 350, p155pz1, 2, "ШАК 2.1", {p153pz,80,2} },
        { shx, 450, p155pz2, 2, "ШАК 2.2", {p153pz,80,2} }
    }
};




//QString get_analog(int sig) {

//    s = vi(tP, G3 + 20, 2) // P реак

