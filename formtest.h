#ifndef FORMTEST_H
#define FORMTEST_H

#include <QWidget>
#include <QButtonGroup>

namespace Ui {
class FormTest;
}

class FormTest : public QWidget
{
    Q_OBJECT

public:
    explicit FormTest(QWidget *parent = 0);
    ~FormTest();

    void set();
    void reset();
    int getBits();

    void refreshV();
    void setReset(bool setreset);


    QButtonGroup *btGrp;


private slots:
    void on_pb_set_pressed();

//private:
    void on_pb_reset_pressed();

    void on_ck_mask_stateChanged(int arg1);

public:
    Ui::FormTest *ui;
};

#endif // FORMTEST_H
