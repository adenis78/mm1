#ifndef PKC_H
#define PKC_H

#define pkcNeiprStart {pkc,238,0}
enum PkcNeisprAlg {
pkc_na_az1, //неисправность алгоритма АЗ 1
pkc_na_az2, //неисправность алгоритма АЗ 2
pkc_na_az3, //неисправность алгоритма АЗ 3
pkc_na_az4, //неисправность алгоритма АЗ 4
pkc_na_az5, //неисправность алгоритма АЗ 5
pkc_na_az6, //неисправность алгоритма АЗ 6
pkc_na_az7, //неисправность алгоритма АЗ 7
pkc_na_az8, //неисправность алгоритма АЗ 8
pkc_na_az9, //неисправность алгоритма АЗ 9
pkc_na_az10, //неисправность алгоритма АЗ 10
pkc_na_az11, //неисправность алгоритма АЗ 11
pkc_na_az12, //неисправность алгоритма АЗ 12
pkc_na_az13, //неисправность алгоритма АЗ 13
pkc_na_az14, //неисправность алгоритма АЗ 14
pkc_na_az15, //неисправность алгоритма АЗ 15
pkc_na_az16, //неисправность алгоритма АЗ 16
pkc_na_az17, //неисправность алгоритма АЗ 17
pkc_na_az18, //неисправность алгоритма АЗ 18
pkc_na_az19, //неисправность алгоритма АЗ 19
pkc_na_az20, //неисправность алгоритма АЗ 20
pkc_na_az21, //неисправность алгоритма АЗ 21
pkc_na_az22, //неисправность алгоритма АЗ 22
pkc_na_az23, //неисправность алгоритма АЗ 23
pkc_na_az24, //неисправность алгоритма АЗ 24
pkc_na_az25, //неисправность алгоритма АЗ 25
pkc_na_az26, //неисправность алгоритма АЗ 26
pkc_na_az27, //неисправность алгоритма АЗ 27
pkc_na_az28, //неисправность алгоритма АЗ 28
pkc_na_az29, //неисправность алгоритма АЗ 29
pkc_na_az30, //неисправность алгоритма АЗ 30
pkc_na_az31, //неисправность алгоритма АЗ резерв
pkc_na_az32, //неисправность алгоритма АЗ резерв
pkc_na_az33, //неисправность алгоритма АЗ резерв
pkc_na_az34, //неисправность алгоритма АЗ резерв
pkc_na_az35, //неисправность алгоритма АЗ резерв
pkc_na_az36, //неисправность алгоритма АЗ резерв
pkc_na_az37, //неисправность алгоритма АЗ резерв
pkc_na_az38, //неисправность алгоритма АЗ резерв
pkc_na_az39, //неисправность алгоритма АЗ резерв
pkc_na_az40, //неисправность алгоритма АЗ резерв
pkc_na_pz1, //неисправность алгоритма ПЗ 1
pkc_na_pz2, //неисправность алгоритма ПЗ 2
pkc_na_pz3, //неисправность алгоритма ПЗ 3
pkc_na_pz4, //неисправность алгоритма ПЗ 4
pkc_na_pz5, //неисправность алгоритма ПЗ 5
pkc_na_pz6, //неисправность алгоритма ПЗ 6
pkc_na_pz7, //неисправность алгоритма ПЗ 7
pkc_na_pz8, //неисправность алгоритма ПЗ 8
pkc_na_pz9, //неисправность алгоритма ПЗ 9
pkc_na_pz10, //неисправность алгоритма ПЗ 10
pkc_na_pz11, //неисправность алгоритма ПЗ 11
pkc_na_pz12, //неисправность алгоритма ПЗ 12
pkc_na_pz13, //неисправность алгоритма ПЗ 13
pkc_na_pz14, //неисправность алгоритма ПЗ 14
pkc_na_pz15, //неисправность алгоритма ПЗ 15
pkc_na_pz16, //неисправность алгоритма ПЗ 16
pkc_na_pz17, //неисправность алгоритма ПЗ 17
pkc_na_pz18, //неисправность алгоритма ПЗ 18
pkc_na_pz19, //неисправность алгоритма ПЗ резерв
pkc_na_pz20, //неисправность алгоритма ПЗ резерв
pkc_na_pz21, //неисправность алгоритма ПЗ резерв
pkc_na_pz22, //неисправность алгоритма ПЗ резерв
pkc_na_pz23, //неисправность алгоритма ПЗ резерв
pkc_na_pz24 //неисправность алгоритма ПЗ резерв
};

#define pkcImit1AzStart {pkc,92,0}
#define pkcImit2AzStart {pkc,104,0}
#define pkcImit3AzStart {pkc,116,0}
#define pkcBlk1AzStart {pkc,128,0}
#define pkcBlk2AzStart {pkc,140,0}
#define pkcBlk3AzStart {pkc,152,0}
enum PkcImitAz {
pkcImAz_Aknp_T_less10_Az1, //    АКНП:T ≤ 10 с АЗ-1
pkcImAz_Aknp_N_moreN_Az1, //    АКНП: N ≥ N УСТ. АЗ-1
pkcImAz_dPreakDt_more0_500,//    dP РЕАК/dt ≥ 0.500 кгс/см²*с
pkcImAz_Preak_less153_0,//    P РЕАК ≤ 153.0 кгс/см²
pkcImAz_dPreak_more4_500,//    ΔP РЕАК ≥ 4.500 кгс/см²
pkcImAz_Preak_more150_0,//    P РЕАК ≥ 150.0 кгс/см²
pkcImAz_Preak_less140_0,//    P РЕАК ≤ 140.0 кгс/см²
pkcImAz_Tgn1_less268_0,//    T ГРЧН 1 ≤ 268.0 °C
pkcImAz_Tgn1_more260_0, //    T ГРЧН 1 ≥ 260.0 °C
pkcImAz_Tgn2_less268_0, //    T ГРЧН 2 ≤ 268.0 °C
pkcImAz_Tgn2_more260_0, //    T ГРЧН 2 ≥ 260.0 °C
pkcImAz_Tgn3_less268_0, //    T ГРЧН 3 ≤ 268.0 °C
pkcImAz_Tgn3_more260_0, //    T ГРЧН 3 ≥ 260.0 °C
pkcImAz_Tgn4_less268_0, //    T ГРЧН 4 ≤ 268.0 °C
pkcImAz_Tgn4_more260_0, //    T ГРЧН 4 ≥ 260.0 °C
pkcImAz_Preak_less150_0, //    P РЕАК ≤ 150.0 кгс/см²
pkcImAz_Aknp_N_more75, //    АКНП: N ≥ 75 %
pkcImAz_dPgcn1_more8_00, //    ΔP ГЦН 1  ≥ 8.00 кгс/см²
pkcImAz_dPgcn2_more8_00, //    ΔP ГЦН 2  ≥ 8.00 кгс/см²
pkcImAz_dPgcn3_more8_00, //    ΔP ГЦН 3  ≥ 8.00 кгс/см²
pkcImAz_dPgcn4_more8_00, //    ΔP ГЦН 4  ≥ 8.00 кгс/см²
pkcImAz_dPgcn1_less4_00, //    ΔP ГЦН 1  ≤ 4.00 кгс/см²
pkcImAz_dPgcn1_less2_50, //    ΔP ГЦН 1  ≤ 2.50 кгс/см²
pkcImAz_dPgcn2_less4_00, //    ΔP ГЦН 2  ≤ 4.00 кгс/см²
pkcImAz_dPgcn2_less2_50, //    ΔP ГЦН 2  ≤ 2.50 кгс/см²
pkcImAz_dPgcn3_less4_00, //    ΔP ГЦН 3  ≤ 4.00 кгс/см²
pkcImAz_dPgcn3_less2_50, //    ΔP ГЦН 3  ≤ 2.50 кгс/см²
pkcImAz_dPgcn4_less4_00, //    ΔP ГЦН 4  ≤ 4.00 кгс/см²
pkcImAz_dPgcn4_less2_50, //    ΔP ГЦН 4  ≤ 2.50 кгс/см²
pkcImAz_Lpg1_less1600, //    L ПГ 1 ≤ 1600 мм
pkcImAz_Afsz_neisprLpg1, //    АФСЗ: неиспр (↑) L ПГ 1
pkcImAz_MedGcn1_more4600, //    РЕЗЕРВ (МЭД ГЦН 1 ≥ 4600 кВт)
pkcImAz_Aknp_N_more5, //    АКНП: N ≥ 5 %
pkcImAz_Lpg2_less1600, //    L ПГ 2 ≤ 1600 мм
pkcImAz_Afsz_neisprLpg2, //    АФСЗ: неиспр (↑) L ПГ 2
pkcImAz_MedGcn2_more4600, //    РЕЗЕРВ (МЭД ГЦН 2 ≥ 4600 кВт)
pkcImAz_Lpg3_less1600, //    L ПГ 3 ≤ 1600 мм
pkcImAz_Afsz_neisprLpg3, //    АФСЗ: неиспр (↑) L ПГ 3
pkcImAz_MedGcn3_more4600, //    РЕЗЕРВ (МЭД ГЦН 3 ≥ 4600 кВт)
pkcImAz_Lpg4_less1600, //    L ПГ 4 ≤ 1600 мм
pkcImAz_Afsz_neisprLpg4, //    АФСЗ: неиспр (↑) L ПГ 4
pkcImAz_MedGcn4_more4600, //    РЕЗЕРВ (МЭД ГЦН 4 ≥ 4600 кВт)
pkcImAz_ShP6_PotPitSuz, //    ШП6: ПОТ. ПИТ. СУЗ.
pkcImAz_MedGcn1_less3000, //    МЭД ГЦН 1 ≤ 3000 кВт
pkcImAz_MedGcn2_less3000, //    МЭД ГЦН 2 ≤ 3000 кВт
pkcImAz_MedGcn3_less3000, //    МЭД ГЦН 3 ≤ 3000 кВт
pkcImAz_MedGcn4_less3000, //    МЭД ГЦН 4 ≤ 3000 кВт
pkcImAz_Pobol_more0_7000, //    P ОБОЛ. ≥ 0.7000 кгс/см²
pkcImAz_Tobol_more90_0, //    T ОБОЛ. ≥ 90.0 °C
pkcImAz_Preak_more180_0, //    P РЕАК ≥ 180.0 кгс/см²
pkcImAz_dPtropPg1Dt_more0_500, //    dP ТРОП ПГ 1 / dt ≥ 0.500 кгс/см²*с
pkcImAz_PtropPg1_less55_00, //    P ТРОП ПГ 1 ≤ 55.00 кгс/см²
pkcImAz_dPtropPg2Dt_more0_500, //    dP ТРОП ПГ 2 / dt ≥ 0.500 кгс/см²*с
pkcImAz_PtropPg2_less55_00, //    P ТРОП ПГ 2 ≤ 55.00 кгс/см²
pkcImAz_dPtropPg3Dt_more0_500, //    dP ТРОП ПГ 3 / dt ≥ 0.500 кгс/см²*с
pkcImAz_PtropPg3_less55_00, //    P ТРОП ПГ 3 ≤ 55.00 кгс/см²
pkcImAz_dPtropPg4Dt_more0_500, //    dP ТРОП ПГ 4 / dt ≥ 0.500 кгс/см²*с
pkcImAz_PtropPg4_less55_00, //    P ТРОП ПГ 4 ≤ 55.00 кгс/см²
pkcImAz_Hkd_less200, //    H КД ≤ 200 мм
pkcImAz_Afsz_neisp_Hkd, //    АФСЗ: неиспр (↑) H КД
pkcImAz_SeismVozd_more4, //    СЕЙСМИЧ. ВОЗД ≥ 4 БАЛЛ.
pkcImAz_Ppg1_more80_00, //    P ПГ 1 ≥ 80.00 кгс/см²
pkcImAz_Ppg2_more80_00, //    P ПГ 2 ≥ 80.00 кгс/см²
pkcImAz_Ppg3_more80_00, //    P ПГ 3 ≥ 80.00 кгс/см²
pkcImAz_Ppg4_more80_00, //    P ПГ 4 ≥ 80.00 кгс/см²
pkcImAz_Aknp_N_more30, //    АКНП: N ≥ 30 %
pkcImAz_PzkpTpn1_less0_500, //    P ЗКП ТПН 1 ≤ 0.500 кгс/см²
pkcImAz_PzkpTpn2_less0_500, //    P ЗКП ТПН 2 ≤ 0.500 кгс/см²
pkcImAz_ShP30_PotPit1, //    ШП30: ПОТ. ПИТ. 1
pkcImAz_ShP30_PotPit2, //    ШП30: ПОТ. ПИТ. 2
pkcImAz_PotPitSborokPT30, //    ПОТ. ПИТ. СБОРОК РТЗО
pkcImAz_Aknp_Neisp, //    АКНП: НЕИСПРАВНОСТЬ
pkcImAz_Tgn1_more330_0, //    T ГРЧН 1 ≥ 330.0 °C
pkcImAz_Tgn2_more330_0, //    T ГРЧН 2 ≥ 330.0 °C
pkcImAz_Tgn3_more330_0, //    T ГРЧН 3 ≥ 330.0 °C
pkcImAz_Tgn4_more330_0, //    T ГРЧН 4 ≥ 330.0 °C
pkcImAz_Svrk_QL_more448, //    РЕЗЕРВ (СВРК: QL ≥ 448 Вт*см¯¹)
pkcImAz_Svrk_Dnbr_less1_35, //    РЕЗЕРВ (СВРК: DNBR ≤ 1,35)
pkcImAz_Tnas1k_TmaxGn1_less10_0, //    РЕЗЕРВ (T НАС 1К - T МАХ ГРЧН 1 ≤ 10.0 °C)
pkcImAz_Tnas1k_TmaxGn2_less10_0, //    РЕЗЕРВ (T НАС 1К - T МАХ ГРЧН 2 ≤ 10.0 °C)
pkcImAz_Tnas1k_TmaxGn3_less10_0, //    РЕЗЕРВ (T НАС 1К - T МАХ ГРЧН 3 ≤ 10.0 °C)
pkcImAz_Tnas1k_TmaxGn4_less10_0, //    РЕЗЕРВ (T НАС 1К - T МАХ ГРЧН 4 ≤ 10.0 °C)
pkcImAz_Tnas1k_TnasPp1_more75, //    РЕЗЕРВ (T НАС 1К - T НАС ПП 1 ≥ 75.0 °C)
pkcImAz_Tnas1k_TnasPp2_more75, //    РЕЗЕРВ (T НАС 1К - T НАС ПП 2 ≥ 75.0 °C)
pkcImAz_Tnas1k_TnasPp3_more75, //    РЕЗЕРВ (T НАС 1К - T НАС ПП 3 ≥ 75.0 °C)
pkcImAz_Tnas1k_TnasPp4_more75, //    РЕЗЕРВ (T НАС 1К - T НАС ПП 4 ≥ 75.0 °C)
pkcImAz_Tgn1_more200_0, //    РЕЗЕРВ2(T ГРЧН 1 ≥ 200.0 °C)
pkcImAz_Tgn2_more200_0, //    РЕЗЕРВ (T ГРЧН 2 ≥ 200.0 °C)
pkcImAz_Tgn3_more200_0, //    РЕЗЕРВ (T ГРЧН 3 ≥ 200.0 °C)
pkcImAz_Tgn4_more200_0, //    РЕЗЕРВ (T ГРЧН 4 ≥ 200.0 °C)
pkcImAz_res0,
pkcImAz_res1,
pkcImAz_res2,
pkcImAz_res3,
pkcImAz_res4
};

#define pkcImit1PzStart {pkc,164,0}
#define pkcImit2PzStart {pkc,176,0}
#define pkcImit3PzStart {pkc,188,0}
#define pkcBlk1PzStart {pkc,200,0}
#define pkcBlk2PzStart {pkc,212,0}
#define pkcBlk3PzStart {pkc,224,0}
#define PkcImPz 1000
enum PkcImitPz {
pkcImPz_Aknp_T_less20_Az3 = PkcImPz, //    АКНП: T ≤ 20 с АЗ-3
pkcImPz_Aknp_Nrd_moreNust_Az3, //    АКНП: N РД  ≥ N УСТ. АЗ-3
pkcImPz_Preak_more175_0, //    P РЕАК ≥ 175.0 кгс/см²
pkcImPz_Tgn1_more327_0, //    T ГРЧН 1 ≥ 327.0 °C
pkcImPz_Tgn2_more327_0, //    T ГРЧН 2 ≥ 327.0 °C
pkcImPz_Tgn3_more327_0, //    T ГРЧН 3 ≥ 327.0 °C
pkcImPz_Tgn4_more327_0, //    T ГРЧН 4 ≥ 327.0 °C
pkcImPz_Pgpk_more70_00, //    P ГПК ≥ 70.00 кгс/см²
pkcImPz_Arom_ObobRazgr, //    АРОМ: ОБОБЩ. РАЗГРУЗКА
pkcImPz_Svrk_QL_more400, //    РЕЗЕРВ (СВРК: QL ≥ 400 Вт*см¯¹)
pkcImPz_Svrk_Dnbr_less1_40, //    РЕЗЕРВ (СВРК: DNBR ≤ 1,40)
pkcImPz_T_less20_Az4, //    АКНП: T ≤ 20 с АЗ-4
pkcImPz_Preak_more165_0, //    P РЕАК ≥ 165.0 кгс/см²
pkcImPz_PadenieOr, //    ПАДЕНИЕ ОР
pkcImPz_Tgn1_more324_0, //    T ГРЧН 1 ≥ 324.0 °C
pkcImPz_Tgn2_more324_0, //    T ГРЧН 2 ≥ 324.0 °C
pkcImPz_Tgn3_more324_0, //    T ГРЧН 3 ≥ 324.0 °C
pkcImPz_Tgn4_more324_0, //    T ГРЧН 4 ≥ 324.0 °C
pkcImPz_Svrk_QL_more350, //    РЕЗЕРВ (СВРК: QL ≥ 350 Вт*см¯¹)
pkcImPz_Svrk_Dnbr_less1_45, //    РЕЗЕРВ (СВРК: DNBR ≤ 1,45)
pkcImPz_Aknp_N_more75, //    АКНП: N ≥ 75 %
pkcImPz_MedGcn1_less3000, //    МЭД ГЦН 1 ≤ 3000 кВт
pkcImPz_MedGcn2_less3000, //    МЭД ГЦН 2 ≤ 3000 кВт
pkcImPz_MedGcn3_less3000, //    МЭД ГЦН 3 ≤ 3000 кВт
pkcImPz_MedGcn4_less3000, //    МЭД ГЦН 4 ≤ 3000 кВт
pkcImPz_PzkpTpn1_less0_400, //    P ЗКП ТПН 1 ≤ 0,400 кгс/см²
pkcImPz_PzkpTpn2_less0_400, //    P ЗКП ТПН 2 ≤ 0,400 кгс/см²
pkcImPz_ZakrSk1, //    ЗАКР. СК 1
pkcImPz_ZakrSk2, //    ЗАКР. СК 2
pkcImPz_ZakrSk3, //    ЗАКР. СК 3
pkcImPz_ZakrSk4, //    ЗАКР. СК 4
pkcImPz_OtklEb //    ОТКЛ. ЭБ
};


// признаки имитации аналоговых сигналов
enum  PkcAnIm  {
pkcAi_Tgn1=0,       //ТП1 (для Тгн1)
pkcAi_Tgn4,         //ТП2 (для Тгн4)
pkcAi_Tgn1_4_tsp,   //ТСП (ТП1) (для Тгн1,4)
pkcAi_Treserv_tsp,  //ТСП (ТП2)
pkcAi_Tgn2,         //ТП3 (для Тгн2)
pkcAi_Tgn3,         //ТП4 (для Тгн3)
pkcAi_Tgn2_3_tsp3,  //ТСП (ТП3) (для Тгн2,3)
pkcAi_Treserv_tsp4, //ТСП (ТП4)(неиспольз)
pkcAi_Thn1,         //ТП ХН1
pkcAi_Thn2,         //ТП ХН2 (неиспольз)
pkcAi_Thn1_tsp,     //ТСП ХН1
pkcAi_Thn2_tsp,     //ТСП ХН2(неиспольз)
pkcAi_Preak,        //Р РЕАК, кгс/см²
pkcAi_Pgpk,         //Ргпк, кгс/см²
pkcAi_P_Preak_res,  //резерв (Р-Р РЕАК, кгс/см²)[Ts реак]
pkcAi_dPreak_res,   //резерв (dР РЕАК, кгс/см²)
pkcAi_Fpit1,        //F ПИТ ГЦН1, Гц
pkcAi_Fpit2,        //F ПИТ ГЦН2, Гц
pkcAi_Fpit3,        //F ПИТ ГЦН3, Гц
pkcAi_Fpit4,        //F ПИТ ГЦН4, Гц
pkcAi_PtropPg1,           //Р ТРОП ПГ1, кгс/см²
pkcAi_PtropPg2,           //Р ТРОП ПГ2, кгс/см²
pkcAi_PtropPg3,           //Р ТРОП ПГ3, кгс/см²
pkcAi_PtropPg4,           //Р ТРОП ПГ4, кгс/см²
pkcAi_PzkpTpn1,           //Р ЗКП ТПН1, кгс/см²
pkcAi_PzkpTpn2,           //Р ЗКП ТПН2, кгс/см²
pkcAi_PzkpTpn3,           //Р ГАЗ06/1, кгс/см²
pkcAi_PzkpTpn4,           //L КД, см
pkcAi_dPgcn1,    //Р-Р ГЦН1, кгс/см²
pkcAi_dPgcn2,    //Р-Р ГЦН2, кгс/см²
pkcAi_dPgcn3,    //Р-Р ГЦН3, кгс/см²
pkcAi_dPgcn4,    //Р-Р ГЦН4, кгс/см²
pkcAi_MedGcn1,    //МЭД ГЦН1, кВт
pkcAi_MedGcn2,    //МЭД ГЦН2, кВт
pkcAi_MedGcn3,    //МЭД ГЦН3, кВт
pkcAi_MedGcn4,    //МЭД ГЦН4, кВт
pkcAi_Lpg1,    //L ПГ1, см
pkcAi_Lpg2,    //L ПГ2, см
pkcAi_Lpg3,    //L ПГ3, см
pkcAi_Lpg4     //L ПГ4, см
//резерв (dР ТРОП ПГ1, кгс/см²)[Ts ПГ1]
//резерв (dР ТРОП ПГ2, кгс/см²)[Ts ПГ2]
//резерв (dР ТРОП ПГ3, кгс/см²)[Ts ПГ3]
//резерв (dР ТРОП ПГ4, кгс/см²)[Ts ПГ4]
};


enum PkcBytes {
pkcMode =0,//     режим ПКЦ
pkcChanAlg,//    номер канала/алгоритма
pkcCodeStage,//    код/этап
//pkcCodeStage_1,
pkcExpo_0,//    экспозиция мл.байт
pkcExpo_1,//    экспозиция ст.байт
pkcB6
};

// в след., 5-м (6м) байте, биты:
enum PkcB6 {
pkcB6_azpz,//    АЗ/ПЗ
pkcB6_manAuto,//    ручной/автоматизированный
pkcB6_up,//    вверх
pkcB6_down,//    вниз
pkcB6_run,//    пуск
pkcB6_stop,//    стоп
pkcB6_rst//    сброс
};

#endif // PKC_H

