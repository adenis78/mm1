#include "form3.h"
#include <QRect>
#include <QPainter>
#include "formula.h"
#include "alg_fact_meq2.h"
enum DevType { in151, out155az, out155pz, pca };
extern unsigned char valData[ETH_SRC_NUM][1600];
extern QString strDiscr_pvk151_1[24];
extern QString strDiscr_pvk151_2[24];
extern QString strDiscr_columns[7];
// перевод порядкового номера 0...6 в порядок следования групп сигналов в таблице сигналов 151-152
//extern int cnv_in[7];

// перевод порядкового номера 0...6 в порядок следования групп сигналов в таблице сигналов 151-152
static int cnv_in[7] {
    0, //"0.вход.",
    2, //"1.испр",
    1, //"2.лин.св",
    4, //3."имит.",
    5,//4."блок",
    3,//"5.результ",
    0, //6."вх.лог" (см 153-154 узел)

};

static int cnv_out[5] {
    //..."вых.153/154
    4, //"1.инверсия.",
    2, //"2.имит",
    3, //3."блок.",
    1,//4."испр",
    0, //5."результ."
};

extern QString strOutDiscr_columns[6];
extern QString strOutDiscr_pvk155_1az[];
extern QString strOutDiscr_pvk155_2az[];
extern QString strOutDiscr_pvk155_1pz[];
extern QString strOutDiscr_pvk155_2pz[];
extern QString str_pca_az[];
extern QString str_pca_pz[];

const PvkByteBit pcaImAz_first = {p08az, 128,0 };
const PvkByteBit pcaImPz_first = {p08pz, 128,0 };

static QColor green() {
 return QColor(Qt::green).darker(125);
 //return QColor(0x01DF01);
}
static QColor gray() {
 return QColor(Qt::lightGray).lighter(100);//.darker(130);
 //return QColor(0xF2F2F2);
}
static QColor red() {
 return QColor(Qt::red).lighter(135); //#01DF01
 //return QColor(0xDF0101);
}
static QColor getCl(int v) {
    if(v==1)
        return Qt::red;
    if(v==2)
        return Qt::gray;
    if(v==0)
        return Qt::green;
}

// для отрисовки вертикального текста
void Form3::drawVTxt(QPainter *painter, const QPoint& p, const QString& str) {

    QTransform old = painter->transform();
    painter->translate(p);
    painter->rotate(270);
    painter->drawText(0, 0, str);
    //painter.rotate(-270);
    painter->setTransform(old);
}

double cnvPcaToFreq (short code) {
    double res = ((double)code) * 62.5 / 4095;
    return res;
}

int b(PvkByteBit pbb) {
    pbb.byte--;
    int res;
    if(pbb.pvk == blank)
        res = 2;
    else
        res = (valData[pbb.pvk][pbb.byte]>>pbb.bit) & 1;
    return res;
}

int b(PvkByteBitSign pbb) {
    pbb.byte--;
    int res;
    if(pbb.pvk == blank)
        res = 2;
    else
        res = (valData[pbb.pvk][pbb.byte]>>pbb.bit) & 1;
    return res;
}

// вх: номер канала
// возвращает значение бита в  заданном ПВК, соотв. каналу
static int bit(Pvk pvk, int ix) {
//    if(ix>=G3)
//        ix-=G3; // на всякий случай

    int byte = 0+ix/8;
    int bit = ix % 8;
    //byte--;
    int res= (valData[pvk][byte]>>bit) & 1;
    return res;
}

static int bitD(PvkByteBit pbb, int disp) {
    int byte = pbb.byte + disp/8;
    int bit =  pbb.bit  + disp%8;
    if(bit>=8)
        bit-=7;
    int res= (valData[pbb.pvk][byte]>>bit) & 1;
    return res;
}

// отрисовка ДИСКРЕТНЫХ СИГНАЛОВ в таблице
void Form3::paintEvent1(DevType type) {
    int i,j;
    int x=0;
    QPainter p(this);
    QFont oldfont, font = p.font();
    oldfont = font;
    font.setPixelSize(28);
    p.setFont(font);
    if(type == in151)
        p.drawText(QPoint(175, 28), "Входные дискретные сигналы");
    else if(type==pca) {
        p.drawText(QPoint(175, 28), "Выходные частотные сигналы");
        QFont oldfont, font = p.font();
        oldfont = font;
        font.setPixelSize(13);
        p.setFont(font);
        p.drawLine(0, 100, 799,100);
        p.drawText(QPoint(15, 115), "Наименование, кГц");
        p.drawText(QPoint(15+400, 115), "Наименование, кГц");
    }
    else
        p.drawText(QPoint(175, 28), "Выходные дискретные сигналы");
    p.setFont(oldfont);

    font = p.font();
    oldfont = font;
    font.setPixelSize(14);
    p.setFont(font);

    if(type==in151) {
        p.drawText(QPoint(145, 70), "ПВК-151Р 1");
        p.drawText(QPoint(545, 70), "ПВК-151Р 2");
    }
    else if(type==out155az) {
        p.drawText(QPoint(145, 70), "ПВК-155_1 АЗ");
        p.drawText(QPoint(545, 70), "ПВК-155_2 АЗ");
    }
    else if(type==out155pz) {
        p.drawText(QPoint(145, 70), "ПВК-155_1 ПЗ");
        p.drawText(QPoint(545, 70), "ПВК-155_2 ПЗ");
    }
    else if(type==pca) {
        p.drawText(QPoint(145, 70), "ПЧА-08Р АЗ");
        p.drawText(QPoint(545, 70), "ПЧА-08Р ПЗ");
    }
    p.setFont(oldfont);

    // гориз.линия сверху
    p.drawLine(0, 35, 799, 35);

    // вертикальные линии
    p.drawLine(400, 35, 400, 599);
    p.drawLine(799, 120, 799, 120+20*24);

    // табл.СЛЕВА: горизонтальные линии, текст и прямоугольники (по 6 шт в строке)
    QRect rect(300, 120+2, 10, 15);
    int flag;
    int tmp=0;
    for(i=0;i<24;i++) {
        p.drawLine(0, 120+20*i, 400, 120+20*i);
        if(type == in151) {
            p.drawText(QPoint(5, 120+20*i + 15), QString::number(i+1) + ".");
            p.drawText(QPoint(25, 120+20*i + 15), strDiscr_pvk151_1[i]);
        }
        else if(type == out155az) {
            p.drawText(QPoint(5, 120+20*i + 15), QString::number(i+1) + ".");
            p.drawText(QPoint(25, 120+20*i + 15), strOutDiscr_pvk155_1az[i]);
        }
        else if(type == out155pz) {
            p.drawText(QPoint(5, 120+20*i + 15), QString::number(i+1) + ".");
            p.drawText(QPoint(25, 120+20*i + 15), strOutDiscr_pvk155_1pz[i]);
        }
        else if(type == pca && i<9 ) {
            p.drawText(QPoint(5, 120+20*i + 15), QString::number(i+1) + ".");
            p.drawText(QPoint(25, 120+20*i + 15), str_pca_az[i] + ", КГц");
        }
        rect.moveTop(120+2 + i*20);
        x = rect.x();
        if(type == in151) {
            for(j=0;j<6;j++) {
                tmp = ::bit(p151_1, cnv_in[j]*24 + i);
                p.fillRect(rect, tmp? Qt::red : Qt::green);
                rect.moveLeft(x + (j+1)*15);
            }
            //p.fillRect(rect, ::b(in153_1az[i])? Qt::red : Qt::green);
            p.fillRect(rect, getCl(::b(in153_1az[i])));


        }
        else if(type == out155az) {
            //p.fillRect(rect, ::b(out153_1az[i])? Qt::red : Qt::green);
            p.fillRect(rect, getCl(::b(out153_1az[i])));
            for(j=0;j<5;j++) {
                rect.moveLeft(x + (j+1)*15);
                tmp = ::bit(p155az1, cnv_out[j]*24 + i);
                p.fillRect(rect, tmp? Qt::red : Qt::green);
            }
        }
        else if(type == out155pz) {
            //из 153
            //p.fillRect(rect, ::b(out153_1pz[i])? Qt::red : Qt::green);
            p.fillRect(rect, getCl(::b(out153_1pz[i])));
            for(j=0;j<5;j++) {
                rect.moveLeft(x + (j+1)*15);
                tmp = ::bit(p155pz1, cnv_out[j]*24 + i);
                p.fillRect(rect, tmp? Qt::red : Qt::green);

            }
        }
        rect.moveLeft(x);
    }
    double v;
    if(type == pca) {
        for(i=0;i<9;i++) {
            // взять из пакета из ПЧА данные  // 1. Рреак   // 2..5 Tхн1..4     // 6...9 Fпит1..4
            // преобразовав по формуле: = * 4095 / 62.5, получить значение в КГц
            v = cnvPcaToFreq(w(p08az,i));
            p.drawText(QPoint(285, 120+20*i + 15), QString::number(v,'f',2) + " /");

            v = cnvPcaToFreq(w(p08az,i+32));

            QPen oldPen=p.pen();
            int imBit = bitD(pcaImAz_first, i);
            if( !imBit ) p.setPen(::gray());
            else p.setPen(::red());
            p.drawText(QPoint(330, 120+20*i + 15), QString::number(v,'f',2));
            p.setPen(oldPen);
        }
    }
    // =================================================================================================
    // табл.СПРАВА: горизонтальные линии, текст и прямоугольники (дискр.значения) (по 7|6 шт в строке)
    rect.moveLeft(698);
    for(i=0;i<24;i++) {
        //i1(x,y+ i*h + 1,w,h);
        p.drawLine(400, 120+20*i, 799, 120+20*i);

        if(type == in151)
            p.drawText(QPoint(5 + 400, 120+20*i + 15), strDiscr_pvk151_2[i]);
        else if(type == out155az)
            p.drawText(QPoint(5+400, 120+20*i + 15), strOutDiscr_pvk155_2az[i]);
        else if(type == out155pz)
            p.drawText(QPoint(5+400, 120+20*i + 15), strOutDiscr_pvk155_2pz[i]);
        else if(type == pca && i<4)
            p.drawText(QPoint(5+400, 120+20*i + 15), str_pca_pz[i] + ", КГц");


        rect.moveTop(120+2 + i*20);
        x = rect.x();

        if(type == in151) {
            for(j=0;j<6;j++) {
                tmp = ::bit(p151_2, cnv_in[j]*24 + i);
                p.fillRect(rect, tmp? Qt::red : Qt::green);
                rect.moveLeft(x + (j+1)*15);
            }
            //p.fillRect(rect, ::b(in153_2az[i])? Qt::red : Qt::green);
            p.fillRect(rect, getCl(::b(in153_2az[i])));

        }
        else if(type == out155az) {
            // 153 (1 столбец)
            //p.fillRect(rect, ::b(out153_2az[i])? Qt::red : Qt::green);
            p.fillRect(rect, getCl(::b(out153_2az[i])));
            // 155 (остальные 5 столбцов)
            for(j=0;j<5;j++) {
                rect.moveLeft(x + (j+1)*15);
                tmp = ::bit(p155az2, cnv_out[j]*24 + i);
                p.fillRect(rect, tmp? Qt::red : Qt::green);

            }
        }
        else if(type == out155pz) {
            //из 153
            //p.fillRect(rect, ::b(out153_2pz[i])? Qt::red : Qt::green);
            p.fillRect(rect, getCl(::b(out153_2pz[i])));
            // 155 (остальные 5 столбцов)
            for(j=0;j<5;j++) {
                rect.moveLeft(x + (j+1)*15);
                tmp = ::bit(p155pz2, cnv_out[j]*24 + i);
                p.fillRect(rect, tmp? Qt::red : Qt::green);

            }

        }
        rect.moveLeft(x);
    }

    if(type == pca) {
        for(i=0;i<4;i++) {
            // взять из пакета из ПЧА данные  // 1. Рреак   // 2..5 Tхн1..4     // 6...9 Fпит1..4
            // преобразовав по формуле: = * 4095 / 62.5, получить значение в КГц
            v = cnvPcaToFreq(w(p08pz,i));
            p.drawText(QPoint(685, 120+20*i + 15), QString::number(v,'f',2) + " /");

            v = cnvPcaToFreq(w(p08pz,i+32));

            QPen oldPen=p.pen();
            int imBit = bitD(pcaImPz_first, i);
            if( !imBit ) p.setPen(::gray());
            else p.setPen(::red());
            p.drawText(QPoint(730, 120+20*i + 15), QString::number(v,'f',2));
            p.setPen(oldPen);

        }
    }

    // подписи к колонкам с прямоугольниками
    if(type==in151)
        for(i=0; i<7; i++) {
            drawVTxt(&p, QPoint(310 + 15*i, 118), strDiscr_columns[i]);
            drawVTxt(&p, QPoint(710 + 15*i, 118), strDiscr_columns[i]);
        }
    else if(type==out155az)
        for(i=0; i<6; i++) {
            drawVTxt(&p, QPoint(307 + 15*i, 118), strOutDiscr_columns[i]);
            drawVTxt(&p, QPoint(707 + 15*i, 118), strOutDiscr_columns[i]);
        }
    else if(type==out155pz)
        for(i=0; i<6; i++) {
            drawVTxt(&p, QPoint(307 + 15*i, 118), strOutDiscr_columns[i]);
            drawVTxt(&p, QPoint(707 + 15*i, 118), strOutDiscr_columns[i]);
        }
}

