/* функции для отрисовки слайдов алгоритмов */
#include <QDebug>
#include "form3.h"
#include "ui_form3.h"
#include <QRect>
#include <QPainter>
#include "formula.h"
#include "./algSch/f1.h"
#include "ui_f1.h"
#include "./algSch/f2.h"
#include "ui_f2.h"
#include "./algSch/f3.h"
#include "ui_f3.h"
#include "./algSch/f4.h"
#include "ui_f4.h"
#include "./algSch/f5.h"
#include "ui_f5.h"
#include "./algSch/f6.h"
#include "ui_f6.h"
#include "./algSch/f7.h"
#include "ui_f7.h"
#include "./algSch/f8.h"
#include "ui_f8.h"
#include "./algSch/f9.h"
#include "ui_f9.h"
#include "./algSch/f10.h"
#include "ui_f10.h"
#include "./algSch/f11.h"
#include "ui_f11.h"
#include "./algSch/f12.h"
#include "ui_f12.h"
#include "./algSch/f13.h"
#include "ui_f13.h"
#include "./algSch/f14.h"
#include "ui_f14.h"
#include "./algSch/f15.h"
#include "ui_f15.h"
#include "./algSch/f16.h"
#include "ui_f16.h"
#include "alg_fact_meq2.h"
#include "usts.h"
#include "parType.h"
#include "srab_ispr_alg_153.h"
#include "constants.h"
#include "im_blk_pkc.h"

extern int rackN; // номер стойки (фактически, № канала: 0, 1 или 2 )
extern unsigned char valData[ETH_SRC_NUM][1600];
const int MidRectLen=195;
const int RightRectLen=84;

static QColor green() {
 return QColor(Qt::green).darker(125);
 //return QColor(0x01DF01);
}
static QColor gray() {
 return QColor(Qt::lightGray).lighter(100);//.darker(130);
 //return QColor(0xF2F2F2);
}
static QColor red() {
 return QColor(Qt::red).lighter(135); //#01DF01
 //return QColor(0xDF0101);
}
static QColor yellow() {
 return QColor(Qt::yellow).darker(120);
}
static QColor black() {
 return QColor(Qt::black);
}
static QColor blue() {
 return QColor(Qt::blue);
}


// вх: номер блока, номер канала (нумерация с 1!)
// возвращает значение бита в  заданном ПВК, соотв. каналу
static int bit(Pvk pvk, int chan) {
    chan--; // каналы в 151 нумеруются с __единицы__ по таблице excel, с №1
    int byte = chan/8;
    int bit = chan % 8;
    int res= (valData[pvk][byte]>>bit) & 1;
    return res;
}

// возвращает точный адрес (тройку) ПВК-байт-байт, для идентификации дискр.сигнала (напр., имит-блк входа мажоритара) с точностью до бита.
// pvk = тройка-адрес для первого из битов в группе сигналов, которые идут подряд. disp = смещение (от 0!) в битах в этой группе сигналов, начиная от pvk
static PvkByteBit getPbb(PvkByteBit pvk, int disp) {
    PvkByteBit res;
    res.pvk = pvk.pvk;
    res.byte = pvk.pvk + disp/8;
    res.bit = pvk.bit + disp % 8;
        return res;
}


// отрисовка выхода за пределы измерений (в виде 1..3 пары треугольников)
void Form3::triangle(QPainter *painter, int up, int x, int y, PvkByteBit pbb ) {
    //QPainter painter(this);
    int color = b(pbb.pvk, pbb.byte, pbb.bit);

    QBrush oldBrush=painter->brush();

    if(color==0) painter->setBrush(::green());
    else painter->setBrush(::red());
    QPolygon polygon;
    if(up==1)
        polygon << QPoint(x,y+8) << QPoint(x+4, y)<< QPoint(x+8, y+8); // ▲, острием вверх
    else
        polygon << QPoint(x,y) << QPoint(x+4, y+8)<< QPoint(x+8, y); // острием вниз
    painter->drawPolygon(polygon);
    painter->setBrush(oldBrush);
}

void Form3::drawMoreOrEqTwo(QPainter &painter, int x, int y, int h, Ss &ss, int i) { //i={0,1}
    // отрисовка трехвходового мажоритарного элемента
    // три линии - к прямоуг. "1","2","3"
    int sm = 12;
    QFont oldfont,font;
    x += RightRectLen;
    painter.drawLine(x, y + h/2 - sm, x+10, y+ h/2 - sm );
    painter.drawLine(x, y + h/2, x+10, y+ h/2);
    painter.drawLine(x, y + h/2 + sm, x+10, y+ h/2 + sm);
    // 1,2,3 >=2
    x+=10;
    int width=15; int w1=20;
    // подкрашивание активного входа (1|2|3)
    QColor cl;
    int *d; int d0[3]={0,1,2}, d1[3] = {2,0,1}, d2[3] = {1,2,0};
    if(rackN==0)
        d = d0;
    else if(rackN==1)
        d = d1;
    else if(rackN==2)
        d = d2;
    // 1й вход
    cl = b(ss.fact[i][d[0]].pvk, ss.fact[i][d[0]].byte,ss.fact[i][d[0]].bit)? ::red(): ::gray(); // напр., для 2й стойки вход A на 3месте, вход B на 1м, C на 2м
    painter.fillRect(x+1, y+1, width, h/3, cl);
    painter.drawRect(x, y, width, h/3);
    // линии по краям: имитация-блокировка входов мажорит.эл-тов 1-2-3
    cl = b(ss.imit.im157.pvk, ss.imit.im157.byte,ss.imit.im157.bit)? ::yellow(): cl;
    painter.fillRect(x+1, y+1, width/5, h/3-1, cl);
    painter.fillRect(x+width-width/5, y+1, width/5, h/3-1, cl);
    // 2й вход
    cl = b(ss.fact[i][d[1]].pvk, ss.fact[i][d[1]].byte,ss.fact[i][d[1]].bit)? ::red(): ::gray();
    painter.fillRect(x+1, y+h/3+1, width, h/3, cl );
    painter.drawRect(x, y+h/3, width, h/3);
    // линии по краям: имитация-блокировка входов мажорит.эл-тов 1-2-3
    cl = b(ss.fact[i][d[1]].pvk, ss.fact[i][d[1]].byte,ss.fact[i][d[1]].bit)? ::yellow(): cl;
    painter.fillRect(x+1, y+1, width/5, h/3-1, cl);
    painter.fillRect(x+width-width/5, y+1, width/5, h/3-1, cl);
    // 3й вход
    cl = b(ss.fact[i][d[2]].pvk, ss.fact[i][d[2]].byte,ss.fact[i][d[2]].bit)? ::red(): ::gray();
    painter.fillRect(x+1, y+h*2/3+1, width, h/3, cl );
    painter.drawRect(x, y+h*2/3, width, h/3+1);
    // линии по краям: имитация-блокировка входов мажорит.эл-тов 1-2-3
    cl = b(ss.fact[i][d[2]].pvk, ss.fact[i][d[2]].byte,ss.fact[i][d[2]].bit)? ::yellow(): cl;
    painter.fillRect(x+1, y+1, width/5, h/3-1, cl);
    painter.fillRect(x+width-width/5, y+1, width/5, h/3-1, cl);
    // цифры 1,2,3
    font = painter.font();
    oldfont = font;
    font.setPixelSize(11);
    painter.setFont(font);
    painter.drawText(x+width*2/5-1,y+10, "1");
    painter.drawText(x+width*2/5-1,y+h/3+10, "2");
    painter.drawText(x+width*2/5-1,y+h*2/3+10, "3");
    painter.setFont(oldfont);
    // подсветка и рамка "≥2"
    x+=width;
    cl = b(ss.meq2[i].pvk,ss.meq2[i].byte,ss.meq2[i].bit)? ::red(): ::gray();
    painter.fillRect(x+1, y, w1, h, cl);
    painter.drawRect(x, y, w1, h);
    // надпись "≥2"
    font = painter.font();
    oldfont = font;
    font.setPixelSize(13);
    painter.setFont(font);
    painter.drawText(x+1,y+h/3+13, "≥2");
    painter.setFont(oldfont);
}

static double get_U (short code3) {
    double res = ((double)code3 - 655) * 31.492 / 2621 ;
    return res;
}

static double get_P (short code3, double sm, double diap ) {
    double res = ((double)code3 - 655) * diap/2621 + sm ;
    return res;
}

static double get_T (short code1) {
    double res = ((double)code1 - 655) * 400 / 2621;
    return res;
}

static double get_dT (short code1) {
    double res = ((double)code1) * 400 / 2621 - 400;
    return res;
}

static double get_F (short code1) {
    double res = ((double)code1 - 655) * 10 / 2621 + 45;
    return res;
}

static double get_R (short code1) {
    double res = ((double)code1 - 655) * 39.1 / 2621 + 100;
    return res;
}

static double get_I (short code1) {
    double res = ((double)code1 - 655) * 16 / 2621 + 4;
    return res;
}


// выдать 'текущее значение параметра'из 153 (аз|пз) c правильной размерностью и точностью
static QString getW153(Ss &ss) {
    int ix = ss.sig;
    Usts ust = ss.ust[0];
    Pvk pvk;
    double d;
    if (ix>=PZ153) {
        ix-=PZ153;
        pvk = p153pz;
    }
    else {
        pvk = p153az;
    }
    unsigned short *w = (unsigned short*)valData[pvk];
    unsigned short r =  w[ix];

    switch (ust.type) {
    case tT:
        d = get_T(r);
        break;
    case tP:
        //d = get_P(r, smd[ix].sm,smd[ix].diap );
        d = get_P(r, ust.smd.sm , ust.smd.diap );
        break;
    case tU:
        d = get_U(r);
        break;
    case tI:
        d = get_I(r);
        break;
    case tR:
        d = get_R(r);
        break;
    case tF:
        d = get_F(r);
        break;
    case tdT:
        d = get_dT(r);
        break;
    default:
        d = 0;
    }
    QString s= QString::number(d,'f', ust.smd.prec);  //000.0
    return s;
}

static QString getUst153(Usts &ust) {
    double d;
    unsigned short *w = (unsigned short*)valData[ust.pvk];
    unsigned short r =  w[ust.ix];

    switch (ust.type) {
    case tT:
        d = get_T(r);
        break;
    case tP:
        d = get_P(r, ust.smd.sm, ust.smd.diap );
        break;
    case tU:
        d = get_U(r);
        break;
    case tI:
        d = get_I(r);
        break;
    case tR:
        d = get_R(r);
        break;
    case tF:
        d = get_F(r);
        break;
    case tdT:
        d = get_dT(r);
        break;
    }

    QString s= QString::number(d,'f', ust.smd.prec);  //000.0
    return s;
}

QColor col(Ss &ss, SigGrp grp) {
    if(ss.type == SigInD) {
        Pvk pvk = p151_1;
        int sig = ss.sig;
        if(sig >= PVK151_2) {
            sig -= PVK151_2;
            pvk = p151_2;
        }
        switch(grp) {
        case in: // вх. сигнал: красный - серый
            return bit(pvk, sig)? red():gray();
        case ispr: // исправность: зеленый - желтый - серый
            if( bit(pvk, sig+ispr)==0 && bit(pvk, sig+sost)==0 )
                return green();
            else if( bit(pvk, sig+ispr)==0 && bit(pvk, sig+sost)==1 )
                    return yellow();
            else if( bit(pvk, sig+ispr)==1)
                    return gray();
            break;
        case res: // результирующий: красный - серый
            return bit(pvk, sig+res)? red():gray();
        case im: // вх. сигнал, красный - серый
            return ( bit(pvk, sig+im)==0 && bit(pvk, sig+blk)==0 ) ? gray():yellow();
        case sost: // состояние линии
            break;
        case blk: // состояние линии
            break;
//        case in153: // вх. сигнал, красный - серый
//            return ( bit(p153az, cnv151_153[sig])==0  ) ? gray():green();
        }
    }
    else return blue(); // чтобы ошибку заметить
    return blue();
};
static PvkByteBitSign faz_blank = {blank,-1,-1};

PvkByteBitSign from151_1_to153[] {
    faz_N_more5_A, //1 "АКНП: N ≥ 5%",
    faz_N_more40_A, //2 "АКНП: N ≥ 40%",
    faz_N_more75_A, //3"АКНП: N ≥ 75%",
    faz_Npdrd_A, //4"АКНП: N ≥ N АЗ",
    fpz_Npdrd_moreX_A, //5 "АКНП: N ≥ N ПЗ-1",
    fpz_Npdrd_more10_A, //6"АКНП: N ≥ N ПЗ-2",
    faz_Tpdrd_less10_A, // 7    //faz_blank, //7 "АКНП:",
    fpz_Tpdrd_less20_A, //8 "АКНП: T < 20c ПЗ-1",
    faz_blank, //9 "",
    faz_blank ,//10 "АКНП: НЕИСПРАВНОСТЬ",
    faz_blank, //11
    faz_blank, //12
    faz_blank, //13
    faz_blank, //14
    faz_blank, //15
    faz_blank, //16
    faz_PotPitSuz_A, //17 "КЭ СУЗ: П ПИТ.СУЗ",
    fpz_Paden_A, //18 "КЭ СУЗ: ПАДЕНИЕ ОР",
    faz_blank, //19 "КЭ СУЗ: ОПРОБЫВАНИЕ",
    faz_blank,//20
    faz_blank,//21
    faz_blank,//22
    faz_blank,//23
    faz_blank,//24 "АФСЗ: ОТКР.ДВЕРИ"
};
PvkByteBitSign from151_2_to153[] {
faz_blank, //РЕЗЕРВ
faz_Seism_A,  //CД: СЕЙСМИЧ.ВОЗД ≥ 6 БАЛЛ.
faz_blank, //РЕЗЕРВ
faz_blank, //РЕЗЕРВ
fpz_Razgr_A,//АРОМ: ОБОБЩ.РАЗГРУЗКА
fpz_Otkl_EB_A,//РЩГ: ОТКЛ. ЭБ
fpz_Otkl_Gen_A, //ВЫКЛ. ГЕНЕР.
fpz_Zakr_A, //ШК2: ЗАКР. 2/4 СК.
faz_blank, //РЕЗЕРВ
faz_blank, //РЕЗЕРВ
faz_blank, //РЕЗЕРВ
faz_blank, //РЕЗЕРВ
faz_blank, //РЕЗЕРВ
fpz_QL_more400_A,//СВРК: QL ≥ 400 Вт*см¯¹ АЗ-3
fpz_DNBR_less1_40_A,//СВРК: DNBR ≤ 1,40 АЗ-3
fpz_QL_more350_A,//СВРК: QL ≥ 350 Вт*см¯¹ АЗ-4
fpz_DNBR_less1_45_A,//СВРК: DNBR ≤ 1,45 АЗ-4
faz_blank,//ОТКЛ КЭН
faz_blank,//ОТКЛ ЦН
faz_blank,//РЕЗЕРВ
faz_blank,//РЕЗЕРВ
faz_blank,//РЕЗЕРВ
faz_blank,//РЕЗЕРВ
faz_blank//РЕЗЕРВ
};

#include <QList>
QList<QLabel *> av;

// создать QLabel'ы для отображ.аналоговых значений
void Form3::createAnalogLabels() {
    for (int i = 0; i < 10; ++i)
       av << new QLabel(this);

    for (int i = 0; i < 10; ++i)
       av.at(i)->setText(QString::number(i));
}


// вх.дискр|аналоговый сигнал, для схем алгоритмов
void Form3::i1(Ss &ss, int x, int y, int width=30, int h=40) {
    int x1=x;
    int lastY;
    QPainter painter(this);
    QFont oldfont,font;
    QString tekStr;
    //Pvk pvk;
    // только для дискр.сигналов: входной 151|152, исправность 151|152
    if(ss.type==SigInD) { // левый прямоуг. с рамкой
        h=40;
        QRect r1_outborder(x,y,width,h);
        painter.drawRect(r1_outborder);
        QRect r1_outter(r1_outborder.adjusted(1,1,0,0)); // внеш.рамка, исправность 151
        painter.fillRect(r1_outter, col(ss, ispr)); // зеленый (неиспр=0 и лин.св=0), желтый(0 и 1) серый (1 неиспр)
        QRect r1_inborder(r1_outter.adjusted(4,4,-5,-5));
        painter.drawRect(r1_inborder);
        QRect r1_inner(r1_inborder.adjusted(1,1,-1,-1)); // внутр. прямоуг., входной 151-152
        painter.fillRect(r1_inner, col(ss, in) );
        x1=r1_outborder.x()+r1_outborder.width();
    }
    else if(ss.type>=SigInA1) {
        //h=42;
    }
    // для дискр и аналог.сигналов: центральный прямоугольник (для аналог. - левый) прямоуг с рамкой
    QRect r2_outborder(x1,y,ss.type>=SigInA1?MidRectLen+width:MidRectLen ,h);
    painter.drawRect(r2_outborder);
    QRect r2_outter(r2_outborder.adjusted(1,1,0,0));
    if(ss.type==SigInD)
        painter.fillRect(r2_outter, col(ss, im )); // дискр: внешний прям-к: факт имит-блк 151
    else
        painter.fillRect(r2_outter, gray()); // аналог: внеш.прям-к, факт имитации (157)
    QRect r2_inborder(r2_outter.adjusted(4,4,-5,-5));
    painter.drawRect(r2_inborder);
    QRect r2_inner(r2_inborder.adjusted(1,1,0,0));
    if(ss.type==SigInD)
        painter.fillRect(r2_inner, col(ss, res)); // для дискр: внутр.прям-к, результирующий (151_1, 151_2)
    else // аналог
        painter.fillRect(r2_inner, gray()); // для аналог: внутр.прям-к, не подсвечивается
    oldfont = font;
    font.setPixelSize(12);
    painter.setFont(font);
    painter.drawText(QPoint(x1+6,y+h/2+5), ss.name[0] ); // назв.сигнала
    painter.setFont(oldfont);

    // правый прямоугольник без рамки (для дискр и аналог)
    x = r2_outborder.x()+r2_outborder.width();
    QRect r3_outborder(x,y,RightRectLen,h);
    painter.drawRect(r3_outborder);
    QRect r3_outter(r3_outborder.adjusted(1,1,0,0));
    int sig=ss.sig;
    if(ss.type==SigInD) { // для дискр:
        if(sig>=PVK151_2) {
            //pvk=p153pz;
            sig-=PVK151_2;
            painter.fillRect(r3_outter, b(from151_2_to153[sig-1] )?::red(): ::gray());
        }
        else
            painter.fillRect(r3_outter, b(from151_1_to153[sig-1] )?::red(): ::gray());
        // дискр., нарисовать мажорит.эл-т
        drawMoreOrEqTwo(painter, x, y, h, ss);
    }
    else if(ss.type>=SigInA1) { // для аналог:
        painter.fillRect(r3_outter, ::gray()); // без подсветки
        // тек. значение
        oldfont = font;
        font.setPixelSize(12);
        painter.setFont(font);

        int im=0;
        //im = b(ss.imit.im157) || b(ss.imit.imPkc);
        // im = b(ss.imit.im157);

        QPen oldPen = painter.pen();
        painter.setPen( im?::red(): ::black() ); // красный текст при имитации (153)
        painter.drawText(QPoint(x+5,y+h/2+5), getW153(ss)); // == тек.значение параметра в соотв.ед.изм. (153)
        painter.setPen(oldPen);
        painter.setFont(oldfont);

        // далее
        lastY=y;
        int i;
        painter.drawLine(x-45, y+5,  x-45, y+h-5); // разделительная.вертик.линия перед треугольниками
        switch(ss.outN) {
        case 3:
            for(i=0, y+=7; i<3; y+=10,i++) {
                triangle(&painter, 0, x-40,y, ss.out[i].up);
                triangle(&painter, 1, x-20,y, ss.out[i].down);
            }
            break;
        case 2:
            for(i=0, y+=9; i<2; y+=17,i++) {
                triangle(&painter, 0, x-40,y, ss.out[i].up);
                triangle(&painter, 1, x-20,y, ss.out[i].down);
            }
            break;
        case 1:
            y+=18;
            triangle(&painter, 0, x-40,y, ss.out[0].up);
            triangle(&painter, 1, x-20,y, ss.out[0].down);
            break;
        }
        y=lastY+h;
        h=40;
        oldfont = font;
        font.setPixelSize(12);
        painter.setFont(font);
        for( i=0; i<=ss.type-1; i++, y+=h) { // уставки аналогового сигнала, от количества уставок
            QRect r2_outborder(x1,y,MidRectLen+width,h);
            painter.drawRect(r2_outborder);
            painter.drawText(QPoint(x1+5,y+h/2+5), ss.name[i+1]);
            QRect r3_outborder(x=r2_outborder.x()+r2_outborder.width(),y,RightRectLen,h);
            painter.drawRect(r3_outborder);
            QRect r3_outter(r3_outborder.adjusted(1,1,0,0));
            painter.fillRect(r3_outter, ::gray());

            // тек.знач.уставки
            QPen oldPen = painter.pen();
            painter.setPen(b(ss.fact[i][0])?::red(): ::black());
            int moreless = bs(ss.ust[i].srab); // знак ≤ или ≥
            if(ss.ust[i].pvk != blank)
                painter.drawText(QPoint(x+5,y+h/2+5), z(moreless) + getUst153(ss.ust[i])); // == тек.значение уставки в соотв.ед.изм. (153)
            painter.setPen(oldPen);

            drawMoreOrEqTwo(painter, x,y,h,ss,i);// отрисовка трехвходового мажоритарного элемента
        }
        painter.setFont(oldfont);
    }
}

void Form3::o1(OutSignals &os, bool pinsLeft) {
    const int MidRectLen = 60;
    int x = os.x, y = os.y;
    int width=20, h=40;
    // int leftlineWidth=20;
    int x1=x;
    QPainter painter(this);
    QFont oldfont,font;

    h=40;
    QRect r1_outborder(x,y,width,h);
    painter.drawRect(r1_outborder);
    QRect r1_outter(r1_outborder.adjusted(1,1,0,0));
    painter.fillRect(r1_outter, ::gray()); //резерв, в будущем будет имит.блк из ПКЦ -
    QRect r1_inborder(r1_outter.adjusted(4,4,-5,-5));
    painter.drawRect(r1_inborder);

    QRect r1_inner(r1_inborder.adjusted(1,1,0,0)); //
    painter.fillRect(r1_inner, b(os.srab153)?::red(): ::gray() ); // сраб. алг в 153 (для выходов алгоритмов) или сраб.защиты (обобщ АЗ/ПЗ) (для выход ШАК)

    x1=r1_outborder.x()+r1_outborder.width();

    QRect r2_outborder(x1,y,MidRectLen ,h);
    painter.drawRect(r2_outborder);
    QRect r2_outter(r2_outborder.adjusted(1,1,0,0));
    painter.fillRect(r2_outter, bit(os.pvk, os.chan + 24*2) || bit(os.pvk, os.chan + 24*3)?::yellow(): ::gray() ); // факт имит./блк в 155
    QRect r2_inborder(r2_outter.adjusted(4,4,-5,-5));
    painter.drawRect(r2_inborder);
    QRect r2_inner(r2_inborder.adjusted(1,1,0,0));
    painter.fillRect(r2_inner, bit(os.pvk, os.chan)?::red(): ::gray() ); // результир.в 155
    oldfont = font;
    font.setPixelSize(11);
    painter.setFont(font);
    painter.drawText(QPoint(x1+7,y+h/2+5), os.text); // назв.сигнала
    painter.setFont(oldfont);

    y+=h;  h=20;
    QRect outborder(x,y,MidRectLen+width,h);
    //painter.fillRect(outborder, bit(os.pvk, os.chan + 24*1)?::green(): ::gray() ); // неиспр. выходного оптрона в 155
    painter.fillRect(outborder, b(os.isprPkc)?::gray(): ::green() ); // неиспр. выходного оптрона в 155
    painter.drawRect(outborder);
    painter.drawText(QPoint(x+7,y+h/2+7), "ИСПРАВНО");

    if(pinsLeft) // гориз.линия слева
        painter.drawLine(x-10,y-10,x,y-10);
    else { // справа
        painter.drawLine(x1+MidRectLen,y-10,x1+MidRectLen+10,y-10);
        //painter.drawLine(x1-leftlineWidth-width,y-10,x1-width,y-10);
    }
}




void Form3::paintEvent(QPaintEvent* e) {
    int slideNtoN1[AlgSlidesN] = {0,14,1,2,3,4,5,6,7,8,9,10,11,12,16,13,15}; // внезапно пришлось переставлять страницы, из-за несовпадения с аналогичными слайдами, выданными М. для рисования АОП'а. Почему так вышло, ноу комментс.
    int ix = ui->stackedWidget->currentIndex();
    extern int algSlideInputsN[AlgSlidesN];
    extern int algSlideOutputsN[AlgSlidesN];
    extern Ss ss[][16];
    extern OutSignals outSigs[][6];
    extern OutSignals shakSigs[][6];

    if( ix==3 ) { // дискр.входы-выходы (151/152, 155, ПЧА)
        switch(discrSlideCurr) {
        case 0: // вх 151,152
            paintEvent1(in151);
            break;
        case 1:
            paintEvent1(out155az);
            break;
        case 2:
            paintEvent1(out155pz);
            break;
        case 3:
            paintEvent1(pca);
            break;
        }
    }
    else if(ix==1) { // слайды схем алгоритмов
        int n0 = algs->currentIndex();
        int n = slideNtoN1[n0];
        if( n>=0 && n<AlgSlidesN ) {
            int x=0,y=0;//,width=80,h=49;
            int i;
            for(i=0; i < algSlideInputsN[n]; i++) {
                i1(ss[n][i],x,y);
                if(ss[n][i].type==SigInD)
                    y+=(40-1);
                else if(ss[n][i].type != SigInD) {
                    y+=42-1;
                    y+= (40-1) * ss[n][i].type; // поле "тип" содержит в себе еще и количество аналог.уставок, см. структуру Ss
                }
            }
            // выходные
            for(i=0; i < algSlideOutputsN[n]; i++) {
                o1(outSigs[n][i]);
            }
            // жирная вертикальная черта
            QPainter painter(this);
            painter.fillRect(700,0,3,703,Qt::black);
            painter.drawText(780, 15, QString::number(n));
            painter.drawText(750, 15, QString(algs->currentWidget()->metaObject()->className()));


            // вых. ШАК
            for(i=0; i < 4; i++) {
                o1(shakSigs[n][i],true);
            }
            switch(n+1) {
            case 1: { // первый слайд, Pреак <150, <178,5
                // таймеры и логика, слайд 1
                Ui_f1* p = ((f1*)(fa[n0]))->ui;
                p->f1t1->setText(wst16(p153az, 204));
                p->f1t2->setText(wst16(p153az, 212));
                p->f1t3->setText(wst16(p153az, 206));
                p->f1t4->setText(wst16(p153az, 214));
                p->f1t5->setText(wst16(p153az, 208));
                p->f1t6->setText(wst16(p153az, 216));
                p->f1t7->setText(wst16(p153az, 210));
                p->f1t8->setText(wst16(p153az, 218));
                lbx(p->f1and1, b(p153az,270,0));
                lbx(p->f1and2, b(p153az,270,1));
                lbx(p->f1and3, b(p153az,270,2));
                lbx(p->f1t1, b(p153az,290,7));
                lbx(p->f1t3, b(p153az,291,0));
                lbx(p->f1t5, b(p153az,291,1));
                lbx(p->f1t7, b(p153az,291,2));
                lbx(p->f1or1, b(p153az,291,3));
                lbx(p->f1t2, b(p153az,291,4));
                lbx(p->f1t4, b(p153az,291,5));
                lbx(p->f1t6, b(p153az,291,6));
                lbx(p->f1t8, b(p153az,291,7));
                lbx(p->f1_threefour, b(p153az,292,0));
            } break;
            case 2: // 2 слайд, (зачеркнуто Pгцн1..4 <2.50)
            {
                Ui_f2* p = ((f2*)(fa[n0]))->ui;
                // таймеры и логика, слайд 1
                p->f2t1->setText(wst16(p153az, 222));
                p->f2t2->setText(wst16(p153az, 224));
                p->f2t3->setText(wst16(p153az, 226));
                p->f2t4->setText(wst16(p153az, 228));
                lbx(p->f2and1, b(p153az,271,3));
                lbx(p->f2and2, b(p153az,271,4));
                lbx(p->f2and3, b(p153az,271,5));
                lbx(p->f2and4, b(p153az,271,6));
                lbx(p->f2t1, b(p153az,292,6)); // срабатывание таймера ΔР гцн 1
                lbx(p->f2t2, b(p153az,292,7)); // срабатывание таймера ΔР гцн 2
                lbx(p->f2t3, b(p153az,293,0)); // срабатывание таймера ΔР гцн 3
                lbx(p->f2t4, b(p153az,293,1)); // срабатывание таймера ΔР гцн 4
            } break;
            case 3: { // 3 слайд, Pгцн1..4 <2.50
                Ui_f3* p = ((f3*)(fa[n0]))->ui;
                lbx(p->f3and1, b(p153az,271,7));
                lbx(p->f3or1, b(p153az,293,2)); //лог.ИЛИ
            } break;
            case 4: { // 4 слайд, Pгцн1..4 <2.50
                Ui_f4* p = ((f4*)(fa[n0]))->ui;
                lbx(p->f4and1, b(p153az,272,0));
                lbx(p->f4or1, b(p153az,293,2)); //лог.ИЛИ
            } break;
            case 5: { // 5 слайд, Pгцн1..4 <2.50
                Ui_f5* p = ((f5*)(fa[n0]))->ui;
                lbx(p->f5and1, b(p153az,272,1));
                lbx(p->f5or1, b(p153az,293,2)); //лог.ИЛИ
            } break;
            case 6: { // 6 слайд, Pгцн1..4 <2.50
                Ui_f6* p = ((f6*)(fa[n0]))->ui;
                lbx(p->f6and1, b(p153az,272,2));
                lbx(p->f6or1, b(p153az,293,2)); // лог.ИЛИ
            }    break;
            case 7: { // 7 слайд,
                Ui_f7* p = ((f7*)(fa[n0]))->ui;
                lbx(p->f7or1, b(alg153az_20)); // выход схемы "≥1" Ppg> 80(выход алг.АЗ №20)
                lbx(p->f7and1, b(p153az,273,2)); //выход схемы "&" P тпн < 0,408 (выход алг. 27)
                p->f7t1->setText(wst16(p153az, 230));
                p->f7t2->setText(wst16(p153az, 232));
                lbx(p->f7t1, b(p153az,293,3)); // сраб таймера Pтпн1 < 0,408 АЗ
                lbx(p->f7t2, b(p153az,293,4)); // сраб таймера Pтпн2 < 0,408 АЗ
            } break;
            case 8: { // 8 слайд,
                Ui_f8* p = ((f8*)(fa[n0]))->ui;
                lbx(p->f8or1, b(alg153az_21)); // ИЛИ = выход алг АЗ №21
                lbx(p->f8or2, b(alg153az_8)); // ИЛИ = выход алг АЗ №8
                lbx(p->f8and1, b(p153az,292,5)); // выход схемы "&" N>5&неиспр.Lкд
            } break;
            case 9: { // 9 слайд,
                Ui_f9* p = ((f9*)(fa[n0]))->ui;
                lbx(p->f9threefour, b(p153az,272,5));
            } break;
//            case 10: // 10 слайд,
//                Ui_f10* p = ((f10*)(fa[n0]))->ui;
//            break;
            case 11: {// 11 слайд,
                Ui_f11* p = ((f11*)(fa[n0]))->ui;

                lbx(p->f11and1, b(p153az,293,5)); // выход схемы "&" Т г.н.1>260&Т г.н.1<268
                lbx(p->f11and3, b(p153az,293,6)); // выход схемы "&" Т г.н.2>260&Т г.н.1<268
                lbx(p->f11and5, b(p153az,293,7)); // выход схемы "&" Т г.н.3>260&Т г.н.1<268
                lbx(p->f11and4, b(p153az,294,0)); // выход схемы "&" Т г.н.4>260&Т г.н.1<268

                lbx(p->f11r1, b(p153az,293,5)); // вход R триггера 1, == выход схемы "&" Т г.н.1>260&Т г.н.1<268
                lbx(p->f11r2, b(p153az,293,6));
                lbx(p->f11r3, b(p153az,293,7));
                lbx(p->f11r4, b(p153az,294,0));

                lbx(p->f11and2, b(p153az,294,1)); // выход схемы "&" Т г.н.1>260&Р а.з. >150
                lbx(p->f11and4, b(p153az,294,2));
                lbx(p->f11and6, b(p153az,294,3));
                lbx(p->f11and8, b(p153az,294,4));

                lbx(p->f11s1, b(p153az,294,1)); // вход S триггера 1, == выход схемы "&" Т г.н.1>260&Р а.з. >150
                lbx(p->f11s2, b(p153az,294,2));
                lbx(p->f11s3, b(p153az,294,3));
                lbx(p->f11s4, b(p153az,294,4));

                lbx(p->f11rs1, b(p153az,294,5)); //выход триггера Т г.н.1
                lbx(p->f11rs2, b(p153az,294,6));
                lbx(p->f11rs3, b(p153az,294,7));
                lbx(p->f11rs4, b(p153az,295,0));

                lbx(p->f11t1, b(p153az,295,1));// срабатывание таймера триггера Т г.н.1
                lbx(p->f11t2, b(p153az,295,2));// срабатывание таймера триггера Т г.н.2
                lbx(p->f11t3, b(p153az,295,3));// срабатывание таймера триггера Т г.н.3
                lbx(p->f11t4, b(p153az,295,4));// срабатывание таймера триггера Т г.н.4

                p->f11t1->setText(wst16(p153az, 234)); // значение таймера триггер Т г.н.1
                p->f11t2->setText(wst16(p153az, 236));
                p->f11t3->setText(wst16(p153az, 238));
                p->f11t4->setText(wst16(p153az, 240));

                lbx(p->f11and9, b(p153az,295,5)); // выход схемы "&" Т г.н.1&Р а.з.<140
                lbx(p->f11and10, b(p153az,295,6));
                lbx(p->f11and11, b(p153az,295,7));
                lbx(p->f11and12, b(p153az,296,0));


                lbx(p->f11or1, b(p153az,273,6));

            } break;
            case 12: { // 12 слайд,
                Ui_f12* p = ((f12*)(fa[n0]))->ui;
                lbx(p->f12or1, b(p153pz,72,0));
            }    break;
//          case 13: // 13 слайд,
//                Ui_f13* p = ((f13*)(fa[n0]))->ui;
//              break;
            case 14: { // 14 слайд,
                Ui_f14* p = ((f14*)(fa[n0]))->ui;
                p->f14t1->setText(wst16(p153pz, 50)); //текущее значение таймера N>75&Откл. Гцн 1 (ПЗ)
                p->f14t2->setText(wst16(p153pz, 52));
                p->f14t3->setText(wst16(p153pz, 54));
                p->f14t4->setText(wst16(p153pz, 56));
                lbx(p->f14t1, b(p153pz,76,4));
                lbx(p->f14t2, b(p153pz,76,5));
                lbx(p->f14t3, b(p153pz,76,6));
                lbx(p->f14t4, b(p153pz,76,7));
                lbx(p->f14or1, b(p153pz,78,2)); //выход схемы "≥1" N>75&Откл. Гцн
                lbx(p->f14and1, b(p153pz,73,3)); // алг12, он же УПЗ1
                lbx(p->f14and2, b(p153pz,73,4)); // алг13, он же УПЗ2
                lbx(p->f14and3, b(p153pz,73,5)); // алг14, он же УПЗ3
            } break;
            case 15: { // 15 слайд,
                Ui_f15* p = ((f15*)(fa[n0]))->ui;
                p->f15t1->setText(wst16(p153az, 220));
                lbx(p->f15t1, b(p153az,271,2)); // срабатывание таймера Пот. Пит. СУЗ(выход алг. 11)
                lbx(p->f15and1, b(p153az,292,1));
                lbx(p->f15and2, b(p153az,292,2));
                lbx(p->f15and3, b(p153az,292,3));
                lbx(p->f15and4, b(p153az,292,4));
                lbx(p->f15or1, b(p153az,270,3));
                lbx(p->f15or2, b(p153az,270,4));
                lbx(p->f15or3, b(p153az,270,5));
                lbx(p->f15or4, b(p153az,270,6));
            }    break;
            case 16: { // 16 слайд,
                Ui_f16* p = ((f16*)(fa[n0]))->ui;
                p->f16t1->setText(wst16(p153pz, 58)); // значение таймера Откл. ТПН 1
                p->f16t2->setText(wst16(p153pz, 60));
                lbx(p->f16t1, b(p153pz,78,3)); // срабатывание таймера P тпн 1 < 0,408
                lbx(p->f16t2, b(p153pz,78,4)); // срабатывание таймера P тпн 2 < 0,408
                lbx(p->f16and1, b(p153pz,73,6));
                lbx(p->f16and2, b(p153pz,73,7));
                lbx(p->f16or1, b(p153pz,78,5));
            } break;
            case 17: { // 17 слайд,
            }    break;
        }
      }
   }
}
