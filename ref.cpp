// оглавление для слайдов с алгоритмами

#include "ref.h"
#include "ui_ref.h"
#include "form3.h"
extern Form3 *mf;

Ref::Ref(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Ref)
{
    ui->setupUi(this);

    setWindowFlags(Qt::FramelessWindowHint);
    //setModal(true);
    setWindowModality(Qt::WindowModal);
    digCnt=0;
}

Ref::~Ref()
{
    delete ui;
}

void Ref::keyPressEvent(QKeyEvent * ev) {

    int d = ev->key();
    d-= Qt::Key_0;
    if(d==0) {
        mf->setPage(9);
        hide();
        mf->show();
        mf->setFocus();
    }
    if(d >= 1 && d <= 9) { // номер группы слайдов
        //mf->setPage(m[d]);
        mf->setPage(d-1);
        hide();
        mf->show();
        mf->setFocus();
    }


    else {
        hide();
        mf->show();
        mf->setFocus();
    }
}
