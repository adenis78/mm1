#include <QLabel>
#include <QMessageBox>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QInputDialog>
#include <QtGlobal>
#include "form3.h"
#include "ui_form3.h"
#include "constants.h"
#include "eth.h"
//#include "dlgppn.h"
#include "referenceppn.h"
#include <QDesktopWidget>
#include "formtest.h"

#define SLIDE_EVENTS 0
#define SLIDE_ALGS 1
#define SLIDE_PPN 2
#define SLIDE_DISCR 3
#define SLIDE_STATE 5
#define SLIDE_DATA 6

extern int ethPackSend();
extern int ethPackBuild();

extern FormTest *ft;

Form3::Form3(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form3)
{
    ethPackBuild();

    ix= discrSlideCurr=0;
    hideFrameNumbersTimerEnabled =hide2DigitTimerEnabled = false;
    lastMousePosX=lastMousePosY=0;
    dig[0] = dig[1] = 0; digCnt = 0;
    ui->setupUi(this);
    move(100,100);
    lbFrameNum = new QLabel(this);
    lbFrameNum->setGeometry(240,130,291,311);
    QFont font = lbFrameNum->font(); //создание объекта класса QFont копированием свойсв QFont у QLabel
    font.setPointSize(200); //установка высоты шрифта 200
    lbFrameNum->setFont(font);
    lbFrameNum->hide();

    setWindowFlags(Qt::FramelessWindowHint);

    mdlDataBus = new DataTableModel();
    ethTestTV = new EthTestTV();

    ui->tableView->installEventFilter(this);
    ui->dial->installEventFilter(this);
    ui->tv1->installEventFilter(this);
//    ui->listInDiscr->installEventFilter(this);
//    ui->listPrevPorog->installEventFilter(this);
//    ui->listSrabAlg->installEventFilter(this);

    // создаем и настраиваем 3 списка для отрисовки сраб.алгоритма, сраб.уставки и вх.дискр.сигнала
    lstAlg = new QListWidget(ui->stackedWidget->widget(0));
    lstPorog = new QListWidget(ui->stackedWidget->widget(0));
    lstSig = new QListWidget(ui->stackedWidget->widget(0));
//    ui->stackedWidget->addWidget(lstAlg);
    lstAlg->setGeometry(0,80,270,520);
    lstPorog->setGeometry(269,80,262,520);
    lstSig->setGeometry(530,80,271,520);


    ui->tableView->setModel(mdlDataBus);
    ui->tableView->show();

    ui->tv1->setModel(ethTestTV);
    ui->tv1->show();

    timerId = startTimer(300);

    // заполняем таблицу вх. сигналов на слайде 2
        QFont f;
    f.setBold(true);
    f.setFamily("Tahoma");
    f.setItalic(true);
    f.setPointSize(15);


    ref = new Ref(this);
    ref->hide();
//    QDesktopWidget desktop;
//    QRect rect = desktop.availableGeometry(desktop.primaryScreen()); // прямоугольник с размерами экрана
//    QPoint topLeft = rect.topLeft();
//    ref->move(topLeft);

    ppnRef = new referencePpn(this);
    ppnRef->hide();
//    ppnRef->move(topLeft);

    refAlg = new RefAlg(this);
    refAlg->hide();

    // подключаем формы со схемами алгоритмов
    algs = new QStackedWidget(this);

    int i=0;
    fa[0] = new f1(this);
    fa[1] = new f15(this);
    fa[2] = new f2(this);
    fa[3] = new f3(this);
    fa[4] = new f4(this);
    fa[5] = new f5(this);
    fa[6] = new f6(this);
    fa[7] = new f7(this);
    fa[8] = new f8(this);
    fa[9] = new f9(this);
    fa[10] = new f10(this);
    fa[11] = new f11(this);
    fa[12] = new f12(this);
    fa[13] = new f13(this);
    fa[14] = new f17(this);
    fa[15] = new f14(this);
    fa[16] = new f16(this);

    for(i=0; i<AlgSlidesN; i++)
        algs->addWidget(fa[i]);


    ui->stackedWidget->insertWidget(1,algs);
    //ui->stackedWidget->setCurrentIndex( SLIDE_DISCR );
    //ui->stackedWidget->setCurrentIndex( SLIDE_ALGS );
}

Form3::~Form3()
{
    killTimer(timerId);
    for(int i=0; i<lbTblMAX; i++)
        delete lbTbl[i];
    delete ui;
}

bool Form3::eventFilter(QObject *target, QEvent *event) {
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = (QKeyEvent *)event;
        int key = keyEvent->key();
        if (key == Qt::Key_Left || key == Qt::Key_Right ) {
            //focusNextPrevChild(true);
            keyPressEvent((QKeyEvent*)event);
            return true;
        }
        else {
            int ix = ui->stackedWidget->currentIndex();
            if(ix==SLIDE_DATA) {
                if(key == Qt::Key_Down)
                    ui->dial->setValue( ui->dial->value()>0?ui->dial->value() - 1:0);
                if(key == Qt::Key_Up)
                    ui->dial->setValue( ui->dial->value() < 12? ui->dial->value() + 1 : 12);
                return true;
            }
        }

    }
    return QWidget::eventFilter(target, event);
}

bool testMode; // режим для проверки отсветки слайдов без аппаратуры: буфер данных не обновляется, просто
              // по адресу <adr> записывает слово <val>  в массив array соотв. класса наследника - только для проверки с


// соглашения по названиям переменных: fr1 = фрейм 1, in1 = вход 1, val3 = значение входа 3
void Form3::inputs() {
}


// TODO
// управление переходами со слайда на слайд с пом. шара (мыши).
//void Form3::mouseMoveEvent(QMouseEvent * event) {
//    int d = event->x() - lastMousePosX;
//    QStackedWidget *sw = ui->stackedWidget;
//    if(d > 10) {
//        sw->setCurrentIndex(( sw->currentIndex() <sw->count()-1 )? sw->currentIndex() + 1 : 0);
//         lastMousePosX = event->x();
//    }
//    else if(d < -10) {
//        sw->setCurrentIndex((sw->currentIndex() > 0 )? sw->currentIndex()-1 : sw->count()-1);
//         lastMousePosX = event->x();
//    }
//    //lastMousePosX = event->x();
//    //lastMousePosY = event->y();
//}

void Form3::onHideFrameNumbers () {
    if(hideFrameNumbersTimerEnabled) {
        ui->stackedWidget->setCurrentIndex( dig[0]-Qt::Key_0 );
        lbFrameNum->hide();
        digCnt = 0;
        hideFrameNumbersTimerEnabled=false;
        hide2DigitTimerEnabled = false;
    }
}
void Form3::onHide2Digit() {
    if(hide2DigitTimerEnabled) {
        lbFrameNum->hide();
    }

}

void Form3::keyPressEvent(QKeyEvent * ev) {
    QStackedWidget *sw = ui->stackedWidget;
    //if(!testEnabled) return;
    bool ok;
    ix = sw->currentIndex();
    int key = ev->key();
    if(key == Qt::Key_F12) {
        if(testMode == true) {
            QMessageBox mb;
            //mb.setText(tr("Ручная правка данных отключена"));
            //mb.setInformativeText( tr("Чтение из аппаратуры восстановлено") );
            mb.exec();
            testMode=false;
        }
    }
    else if(key == Qt::Key_F11) {
        QString text = QInputDialog::getText(this,
            "",//tr("Ручная правка данных"),
            "",//tr("Чтение из аппаратуры выключено. Введите через запятую: адрес, значение"),
            QLineEdit::Normal, 0, &ok);
        if (ok && !text.isEmpty()) {
            QStringList slist = text.split(",");
            int dev=slist[0].toInt(&ok);  if(!ok) return;
            int adr=slist[1].toInt(&ok);  if(!ok) return;
            if(adr>0) adr--;
            int val;
            QString c  = slist[2].at(0);
            if(c =="s" || c=="r") {
                slist[2].remove(0, 1);
                val=slist[2].toInt(&ok);  if(!ok) return;
                unsigned char v = valData[dev][adr];
                (c=="s")?  v |= 1<<val : v &= ~(1<<val);
                valData[dev][adr] = v;
            }
            else {
                val=slist[2].toInt(&ok);  if(!ok) return;
                valData[dev][adr] = val;
            }
        }
        testMode = true;
    }
    else if(key == Qt::Key_Backspace) {
        ref->show();
        ref->setFocus();
    }

    else if(key == Qt::Key_Slash) { // меню ППН
        if(ix== SLIDE_PPN ) { // оглавление для слайда ППН
            ppnRef->show();
            ppnRef->setFocus();
        }
        else if(ix== SLIDE_ALGS ) { // оглавление слайд c алгоритмами
            refAlg->show();
            refAlg->setFocus();
        }
    }

    else
    if(key == Qt::Key_Right) { // след.страница
        sw->setCurrentIndex( (ix<(sw->count()-1))? ix+1 : 0);
    }
    else if(key == Qt::Key_Left) {// предыд.страница
        sw->setCurrentIndex(ix > 0? ix-1 : sw->count()-1);
    }
    else if(key == Qt::Key_Down) { // след.слайд в группе слайдов
        if(ix==SLIDE_ALGS) { // если на алгоритмах защит
            algs->setCurrentIndex( (algs->currentIndex()< AlgSlidesN-1)? algs->currentIndex()+1 : 0);
        }
        else if(ix==SLIDE_PPN) { // если мы на таблице ППН (2й, считая от 0)
            ui->sw2->setCurrentIndex(( ui->sw2->currentIndex() <ui->sw2->count()-1 )? ui->sw2->currentIndex() + 1 : 0);
        }
        else if(ix==SLIDE_DISCR) { // слайд дискр сигналов
            discrSlideCurr<3?discrSlideCurr++:discrSlideCurr=0;
            update();
        }
    }
    else if(key == Qt::Key_Up) { // предыд.страница
        if(ix==SLIDE_ALGS) { // если на алгоритмах защит
            algs->setCurrentIndex( (algs->currentIndex()>0)? algs->currentIndex()-1 : AlgSlidesN-1);
        }
        else if(ix==SLIDE_PPN) { // если мы на таблице ППН
            ui->sw2->setCurrentIndex((ui->sw2->currentIndex() > 0 )? ui->sw2->currentIndex()-1 : ui->sw2->count()-1);
        }
        else if(ix==SLIDE_DISCR) { // слайд дискр сигналов
            discrSlideCurr>0?discrSlideCurr--:discrSlideCurr=3;
            update();
        }

    }
    else if(key >= Qt::Key_1 && ev->key() <= Qt::Key_9) { //  цифра  = номер одного из типов слайдов (1..4)
        int k = key - Qt::Key_0 - 1;
         sw->setCurrentIndex( k );

//        switch(k) {
//            case SLIDE_EVENTS:
//                sw->setCurrentIndex( 0 );
//            break;
//            case SLIDE_ALGS:
//                sw->setCurrentIndex( 1 );
//            break;
//            case SLIDE_PPN:
//                sw->setCurrentIndex( 2 );
//            break;
//            case SLIDE_DISCR:
//                sw->setCurrentIndex( 3 );
//            break;
//            case SLIDE_STATE:
//                  sw->setCurrentIndex( 0 );
//            break;
//            case SLIDE_DATA:
//            break;
//        }
    }

#if 0
    else if(key >= Qt::Key_0 && ev->key() <= Qt::Key_9) { // двузначный номер слайда
        dig[digCnt] = ev->key();
        if (digCnt==0) {
            QTimer::singleShot(2000,Qt::CoarseTimer , this, SLOT(onHideFrameNumbers()));
            hideFrameNumbersTimerEnabled = true;
            hide2DigitTimerEnabled = false;
            digCnt++;
            lbFrameNum->setText(QString::number(dig[0]-Qt::Key_0));
            lbFrameNum->setVisible(true);
        }
        else {
            hideFrameNumbersTimerEnabled = false;
            hide2DigitTimerEnabled = true;
            sw->setCurrentIndex( ((dig[0]-Qt::Key_0) * 10) + (dig[1]-Qt::Key_0) );
            lbFrameNum->setText( QString::number((dig[0]-Qt::Key_0) *10)
                               + QString::number( dig[1]-Qt::Key_0) );
            QTimer::singleShot(1000,Qt::CoarseTimer , this, SLOT(onHide2Digit()));
            digCnt=0;
        }

    }
#endif
}

const int RegimyAFSZ_N=11;
extern unsigned char lastCnt[ETH_SRC_NUM_MAX];
extern unsigned char ethErrFlag[ETH_SRC_NUM_MAX];
unsigned char timedCnt[ETH_SRC_NUM_MAX];
void Form3::timerEvent(QTimerEvent *event) {
     repaint();

    int ix = ui->stackedWidget->currentIndex();

//    if(ix==SLIDE_EVENTS) { // TODO
        srabAlg();
        prevPorog();
        discrSig();
//    }
//    else
    if(ix==SLIDE_PPN) {
        ppn();
        ppn1();
        ustav();
    }
    else if(ix==SLIDE_STATE)
        fail_E();
    // технологич.слайд: отобразить кол-во ошибок из блоков 151-2 и 152-1
//    extern int errCnt[ETH_SRC_NUM];
//    ui->lcd151_2->display(errCnt[1]);pai

    // слайд2 таблицы входных сигналов
    // режим АФСЗ
    QString RegimyAFSZ[RegimyAFSZ_N] {"ВЫКЛ.","ПРОВ.ЗАЩИТ","ПОВЕРКА ИК","ПОВЕРКА ПС","УСТ.ВХ.ПАРАМ"};
    //QString smthstr[] {  "АЗ/ПЗ", "ручной/автоматизированный", "вверх", "вниз", "пуск", "стоп","сброс"};


    // 2й слайд: режим АФСЗ
    int mode = valData[pkc][0];
    if (mode<RegimyAFSZ_N)
        ui->lbRegimAfsz->setText(RegimyAFSZ[mode]);

    ethPackSend();

    //setEthFailSlide();
    extern unsigned char oneOrTwo[ETH_SRC_NUM];

    if(oneOrTwo[p153az])
        ui->pvk153az->setText("ПВК-154Р(1)");
    else
        ui->pvk153az->setText("ПВК-153Р(1)");

    if(oneOrTwo[p153pz])
        ui->pvk153pz->setText("ПВК-154Р(2)");
    else
        ui->pvk153pz->setText("ПВК-153Р(2)");

    if(oneOrTwo[p151_1])
        ui->pvk151->setText("ПВК-152Р(1)");
    else
        ui->pvk151->setText("ПВК-151Р(1)");

    if(oneOrTwo[p151_2])
        ui->pvk152->setText("ПВК-152Р(2)");
    else
        ui->pvk152->setText("ПВК-151Р(2)");

    if(oneOrTwo[p157_1])
        ui->pvk157->setText("ПВК-158Р(1)");
    else
        ui->pvk157->setText("ПВК-157Р(1)");

    if(oneOrTwo[p157_2])
        ui->pvk158->setText("ПВК-158Р(2)");
    else
        ui->pvk158->setText("ПВК-157Р(2)");

    if(oneOrTwo[p155az1])
        ui->pvk155r1->setText("ПВК-156Р1(1)");
    else
        ui->pvk155r1->setText("ПВК-155Р1(1)");
    if(oneOrTwo[p155az2])
        ui->pvk155r2->setText("ПВК-156Р1(2)");
    else
        ui->pvk155r2->setText("ПВК-155Р1(2)");

    if(oneOrTwo[p155pz1])
        ui->pvk156r1->setText("ПВК-156Р1(1)");
    else
        ui->pvk156r1->setText("ПВК-155Р1(1)");
    if(oneOrTwo[p155pz2])
        ui->pvk156r2->setText("ПВК-156Р1(2)");
    else
        ui->pvk156r2->setText("ПВК-155Р1(2)");


    //commonSlide();

    // проверка ошибок ethernet
//    for(int i =0;i<ETH_SRC_NUM;i++) {
//        if(timedCnt[i] == lastCnt[i])
//            ethErrFlag[i]=1;
//        timedCnt[i] = lastCnt[i];
//    }

#ifdef FORMTEST
    ft->refreshV();
#endif
}

char ethName   [ETH_SRC_NUM_MAX][20] {
    "153/154 АЗ", "153/154 ПЗ",
    "151/152№1", "151/152№2",
    "157№1/2", "158№1/2",
    "155/6 АЗ №1", "155/6 АЗ №2", "155/6 ПЗ №1", "155/6 ПЗ №2",
    "ПЧА-08/9 АЗ", "ПЧА-08/9 ПЗ",
    "ПКЦ-184/5/6"};

//extern QString getDevNam(int n);
void Form3::on_dial_valueChanged(int value) {
    mdlDataBus->pkN = ui->dial->value();
    ui->lcdNumber->display(mdlDataBus->pkN);
    ui->lbCurDevName->setText(ethName[mdlDataBus->pkN]);

}

void Form3::setPage(int n) {
    ui->stackedWidget->setCurrentIndex(n);
}
void Form3::setPpnPage(int n) {
//    ui->stackedWidget->setCurrentIndex(12);
    ui->sw2->setCurrentIndex(n);
}


#include <map>
using namespace std;

void Form3::setAlgPage(int n) {
    int slide=0;
    if(n==1 || n==9 || n==2 || n==3 || n==10)
        slide = 0;
    else if(n==4 || n==5 || n==6 || n==7 || n==11 || n==28)
        slide = 1;
    else if (n==12 || n==13 || n==14 || n==15 || n==29 || n==30)
        slide = 2;
    else if (n==16)
        slide = 3;
    else if (n==17)
        slide = 4;
    else if (n==18)
        slide = 5;
    else if (n==19)
        slide = 6;
    else if (n==20 || n==27)
        slide = 7;
    else if (n==21)
        slide = 8;
    else if (n==22)
        slide = 9;
    else if (n==23 || n==24 || n==25 || n==26)
        slide = 10;
    else if (n==31)
        slide = 11;

    // алгоритмы ПЗ
    else if(n>31) {
        n-=31;
        if(n >= 1 && n<=6) // алг ПЗ1 1..6
            slide = 12;
        else if(n >= 7 && n<=11) // алг ПЗ2 1..6
            slide = 13;
        else if(n >= 12 && n<=14) // алг УПЗ 1..3
            slide = 14;
        else if(n >= 15 && n<=16) // алг УПЗ 4..5
            slide = 15;
        else if(n >= 17 && n<=18) // алг ПЗ1 7..8
            slide = 16;
    }
    else return;

    algs->setCurrentIndex(slide);
}

// номер слайда (0..3) для дискр сигналов
int dSlideN=0;

