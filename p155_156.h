#ifndef P155_156_H
#define P155_156_H

enum P155_Grp {
    p155_res=0,
    p155_ispr=24,
    p155_imit=24*2,
    p155_blk=24*3,
    p155_inv=24*4,
    p155_sost=24*5,
};


enum P155_1az {
p155_1az_Shak1_1Az, //"ШАК 1.1 АЗ",
p155_1az_Shak2_1Az, //"ШАК 2.1 АЗ",
p155_1az_Oprob_Aknp, //"ОПРОБЫВАНИЕ (АКНП)", // нет в 153
p155_1az_Oprob_Akp //"ОПРОБЫВАНИЕ (АКП)", // нет в 153
};

enum P155_2az {
p155_2az_Shak1_2Az, //   "ШАК 1.2 АЗ",
p155_2az_Shak2_2Az, //   "ШАК 2.2 АЗ",
p155_2az_res1, //   "РЕЗЕРВ",    //    "F 3/4 ГЦН ≤ 46 Гц",
p155_2az_res2, //   "РЕЗЕРВ",    //    "Т НАС 1К - Т MAX ГРЧН 1 ≤ 10°C",
p155_2az_res3, //   "РЕЗЕРВ",    //    "Т НАС 1К - Т MAX ГРЧН 2 ≤ 10°C",
p155_2az_res4, //   "РЕЗЕРВ",    //    "Т НАС 1К - Т MAX ГРЧН 3 ≤ 10°C",
p155_2az_res5, //   "РЕЗЕРВ",    //    "Т НАС 1К - Т MAX ГРЧН 4 ≤ 10°C",
p155_2az_res6, //   "РЕЗЕРВ",    //    "2 ТПН ВЫКЛ & N ≥ 40%",
p155_2az_res7, //   "РЕЗЕРВ",    //    "T ≤ 10c АЗ",
p155_2az_res8, //   "РЕЗЕРВ",    //    "N ≥ N УСТ.АЗ",
p155_2az_res9, //   "РЕЗЕРВ",    //    "СЕЙСМИЧ.ВОЗД ≥ 6 баллов",
p155_2az_res10, //   "РЕЗЕРВ",    //    "Р РЕАК ≤ 140 кгс/см2",
p155_2az_res11, //   "РЕЗЕРВ",    //    "Р ТРОП ПГ ≥ 80 кгс/см2",
p155_2az_OtklGcn1, //   "ОТКЛ. ГЦН 1 (АКНП)",
p155_2az_OtklGcn2, //   "ОТКЛ. ГЦН 2 (АКНП)",
p155_2az_OtklGcn3, //   "ОТКЛ. ГЦН 3 (АКНП)",
p155_2az_OtklGcn4, //   "ОТКЛ. ГЦН 4 (АКНП)",
p155_2az_res12, //   "РЕЗЕРВ",    //    "ОБОБЩ.ИСПРАВНОСТЬ",
p155_2az_res13, //   "РЕЗЕРВ",    //    "ОБОБЩ.ПРОВЕРКА",
p155_2az_res14, //   "РЕЗЕРВ",    //    "",
p155_2az_res15, //   "РЕЗЕРВ",    //    "",
p155_2az_PeregruzMashina, //   "ПЕРЕГРУЗ.МАШИНА",
p155_2az_res16, //   "РЕЗЕРВ",//"КРУГОВОЙ КРАН",
p155_2az_SrabAz //   "СРАБАТЫВАНИЕ АЗ"
};

enum P155_1pz {
p155_1pz_Shak1_1Az3, // "ШАК 1.1 АЗ-3",
p155_1pz_Shak2_1Az3,//    "ШАК 2.1 АЗ-3",
p155_1pz_Shak1_1Az4,//    "ШАК 1.1 АЗ-4",
p155_1pz_Shak2_1Az4,//    "ШАК 2.1 АЗ-4",
p155_1pz_Shak1_1Urb,//    "ШАК 1.1 УРБ",
p155_1pz_Shak2_1Urb//    "ШАК 2.1 УРБ",
//    "ОТКЛ ГЦН 1 (АРОМ)",
//    "ОТКЛ ГЦН 2 (АРОМ)",
//    "ОТКЛ ГЦН 3 (АРОМ)",
//    "ОТКЛ ГЦН 4 (АРОМ)",
//    "ОТКЛ ТПН 1 (АРОМ)",
//    "ОТКЛ ТПН 2 (АРОМ)",
//    "ОТКЛ ЭБ (АРОМ)",
//    "РЕЗЕРВ", //"ВЫКЛ ГЕНЕР (АРОМ)",
//    "ЗАКР 2/4 СК (АРОМ)",
//    "РЕЗЕРВ", //"ОТКЛ KЭН (АРОМ)",
//    "РЕЗЕРВ",
//    "ЗАКР.2 ОК ЦСД || КЭН-2 (АРОМ)", //"ОТКЛ ЦЭН (АРОМ)",
//    "РЕЗЕРВ",
//    "РЕЗЕРВ",    //    "Т ГРЧН ≥ +3",
//    "РЕЗЕРВ",    //    "Р РЕАК ≥ 170.3 кгс/см2",
//    "РЕЗЕРВ",    //    "Р ГПК ≥ 65 кгс/см2",
//    "РЕЗЕРВ",    //    "T ≤ 20c ПЗ-1",
//    "РЕЗЕРВ",     //    "N ≥ N УСТ.ПЗ-1"

};


enum P155_2pz {
p155_2pz_Shak1_2Az3, //    "ШАК 1.2 АЗ-3",
p155_2pz_Shak2_2Az3, //    "ШАК 2.2 АЗ-3",
p155_2pz_Shak1_2Az4, //    "ШАК 1.2 АЗ-4",
p155_2pz_Shak2_2Az4, //    "ШАК 2.2 АЗ-4",
p155_2pz_Shak1_2Urb, // "ШАК 1.2 УРБ",
p155_2pz_Shak2_2Urb  //  "ШАК 2.2 УРБ",
//    "РЕЗЕРВ",//    "ОБОБЩЕННАЯ РАЗГРУЗКА",
//    "РЕЗЕРВ",//    "СВРК: QL ≥ 400 Вт/см ПЗ-1",
//    "РЕЗЕРВ",//    "СВРК: DNBR ≤ 1.35 ПЗ-1",
//    "РЕЗЕРВ",//    "N ≥ N УСТ.ПЗ-2",
//    "РЕЗЕРВ",//    "P РЕАК ≥ 165.2 кгс/см2",
//    "РЕЗЕРВ",//    "ПАДЕНИЕ ОР",
//    "РЕЗЕРВ",//    "QL ≥ УСТ.ПЗ-2",
//    "РЕЗЕРВ",//    "DNBR ≤ УСТ ПЗ-2",
//    "РЕЗЕРВ",//    "ОТКЛ 1/4 ГЦН & N ≥ 75%",
//    "РЕЗЕРВ",//    "ОТКЛ ЭБ & N ≥ 75%",
//    "РЕЗЕРВ",//    "ВЫКЛ ГЕНЕР & N ≥ 75%",
//    "РЕЗЕРВ",//    "ОТКЛ 1/2 ТПН & N ≥ 75%",
//    "РЕЗЕРВ",//    "ЗАКР.2/4 СК. & N ≥ 75%",
//    "РЕЗЕРВ",//    "ОБОБЩ. ПЗ-1",
//    "РЕЗЕРВ",//    "ОБОБЩ. ПЗ-1",
//    "РЕЗЕРВ",//    "ОБОБЩ. УПЗ-1",
//    "РЕЗЕРВ",
//    "СРАБАТЫВАНИЕ ПЗ"
};

#endif // P155_156_H

