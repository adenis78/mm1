#ifndef USTS_H
#define USTS_H

#include "parType.h"
#include "pvk.h"


struct Usts {
    Pvk pvk;
    int ix; // индекс "текущее значение уставки" в 153
    ParType type; // тип уставки (давление, или температура и т.д.)
    SmD smd; // смещение, диапазон и точность
    PvkByteBitSign srab; // индекс "факт сраб.уставки" в 153 (номер байта-бита как в таблице Алексея, т.е. байты нумеруются с 1цы)
};



#endif // USTS_H

