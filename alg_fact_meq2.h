#ifndef ALG_FACT_MEQ2
#define ALG_FACT_MEQ2
#include "algsignals.h"
// индексы: выход за пределы измерений

//static const OutOfLimits ofl_Tgr1_tep =  { {p157_1,423,4}, {p157_1,430,0} }; // ↑↓ Т грчн1, ТЭП
//static const OutOfLimits ofl_Tgr1_tsp =  { {p157_1,423,4}, {p157_1,430,2} }; // ↑↓ Т грчн1, ТСП
//static const OutOfLimits ofl_Tgr2_tep =  { {p157_1,424,0}, {p157_1,430,4} }; // ↑↓ Т грчн2
//static const OutOfLimits ofl_Tgr2_tsp =  { {p157_1,424,2}, {p157_1,430,6} };
//static const OutOfLimits ofl_Tgr3_tep =  { {p157_1,424,1}, {p157_1,430,5} }; // ↑↓ Т грчн3
//static const OutOfLimits ofl_Tgr3_tsp =  { {p157_1,423,4}, {p157_1,430,6} };
//static const OutOfLimits ofl_Tgr4_tep =  { {p157_1,423,5}, {p157_1,430,1} }; // ↑↓ Т грчн4
//static const OutOfLimits ofl_Tgr4_tsp =  { {p157_1,423,6}, {p157_1,430,2} };
//static const OutOfLimits ofl_Thn_tep =  { {p157_1,424,4}, {p157_1,431,0} }; // ↑↓ Т хн
//static const OutOfLimits ofl_Thn_tsp =  { {p157_1,424,6}, {p157_1,431,2} };

//static const OutOfLimits ofl_Preak =  { {p157_1,425,0}, {p157_1,431,4} }; // ↑↓ P реак
//static const OutOfLimits ofl_Pgpk =  { {p157_1,425,1}, {p157_1,431,5} }; // ↑↓ P гпк
//static const OutOfLimits ofl_Fpit1 =  { {p157_1,425,4}, {p157_1,432,0} }; // ↑↓ Fпит1
//static const OutOfLimits ofl_Fpit2 =  { {p157_1,425,5}, {p157_1,432,1} }; // ↑↓ Fпит2
//static const OutOfLimits ofl_Fpit3 =  { {p157_1,425,6}, {p157_1,432,2} }; // ↑↓ Fпит3
//static const OutOfLimits ofl_Fpit4 =  { {p157_1,425,7}, {p157_1,432,3} }; // ↑↓ Fпит4
//static const OutOfLimits ofl_Ptrop1 =  { {p157_1,426,0}, {p157_1,432,4} }; // ↑↓ Pтроп1
//static const OutOfLimits ofl_Ptrop2 =  { {p157_1,426,1}, {p157_1,432,5} }; // ↑↓ Pтроп2
//static const OutOfLimits ofl_Ptrop3 =  { {p157_1,426,2}, {p157_1,432,6} }; // ↑↓ Pтроп3
//static const OutOfLimits ofl_Ptrop4 =  { {p157_1,426,3}, {p157_1,432,7} }; // ↑↓ Pтроп4

//static const OutOfLimits ofl_Pzkp1 =  { {p157_1,426,4}, {p157_1,433,0} }; // ↑↓ Pзкп1
//static const OutOfLimits ofl_Pzkp2 =  { {p157_1,426,5}, {p157_1,433,1} }; // ↑↓ Pзкп2
//static const OutOfLimits ofl_Pgaz =  { {p157_1,426,6}, {p157_1,433,2} }; // ↑↓ Pгаз
//static const OutOfLimits ofl_Lkd =  { {p157_1,426,7}, {p157_1,433,3} }; // ↑↓ L кд
//static const OutOfLimits ofl_dPgcn1 =  { {p157_1,427,0}, {p157_1,433,4} }; // ↑↓ разница P гцн1
//static const OutOfLimits ofl_dPgcn2 =  { {p157_1,427,1}, {p157_1,433,5} }; // ↑↓ разница P гцн2
//static const OutOfLimits ofl_dPgcn3 =  { {p157_1,427,2}, {p157_1,433,6} }; // ↑↓ разница P гцн3
//static const OutOfLimits ofl_dPgcn4 =  { {p157_1,427,3}, {p157_1,433,7} }; // ↑↓ разница P гцн4
//static const OutOfLimits ofl_Med1 =  { {p157_1,427,4}, {p157_1,434,0} }; // ↑↓ МЭД ГЦН1
//static const OutOfLimits ofl_Med2 =  { {p157_1,427,5}, {p157_1,434,1} }; // ↑↓ МЭД ГЦН2
//static const OutOfLimits ofl_Med3 =  { {p157_1,427,6}, {p157_1,434,2} }; // ↑↓ МЭД ГЦН3
//static const OutOfLimits ofl_Med4 =  { {p157_1,427,7}, {p157_1,434,3} }; // ↑↓ МЭД ГЦН4

//static const OutOfLimits ofl_Lpg1 =  { {p157_1,428,0}, {p157_1,434,4} }; // ↑↓ L пг1
//static const OutOfLimits ofl_Lpg2 =  { {p157_1,428,1}, {p157_1,434,5} }; // ↑↓ L пг2
//static const OutOfLimits ofl_Lpg3 =  { {p157_1,428,2}, {p157_1,434,6} }; // ↑↓ L пг3
//static const OutOfLimits ofl_Lpg4 =  { {p157_1,428,3}, {p157_1,434,7} }; // ↑↓ L пг4

// индексы: подсветка "≥2"
static const PvkByteBit meq2_Paz_more178_5 = {p153az, 271,0}; // факт срабатывания схемы "≥2" для Р а.з. >178,5 кгс(выход алг. 9)		271	0
static const PvkByteBit meq2_P_pod_more_0_306 = {p153az,271,1}; // факт срабатывания схемы "≥2" для Р под.об. > 0,306 кгс(выход алг. 10)		271	1
static const PvkByteBit meq2_Taz_Tgn1_less10 = {p153az,272,6};// факт срабатывания схемы "≥2" для (Ts а.з. -Tgn1) < 10 C(выход алг. 23)
static const PvkByteBit meq2_Taz_Tgn2_less10 = {p153az,272,7};// факт срабатывания схемы "≥2" для (Ts а.з. -Tgn2) < 10 C(выход алг. 24)
static const PvkByteBit meq2_Taz_Tgn3_less10 = {p153az,273,0};// факт срабатывания схемы "≥2" для (Ts а.з. -Tgn3) < 10 C(выход алг. 25)
static const PvkByteBit meq2_Taz_Tgn4_less10 = {p153az,273,1};   // факт срабатывания схемы "≥2" для (Ts а.з. -Tgn4) < 10 C(выход алг. 26)
static const PvkByteBit meq2_Tpdrd_less10 = {p153az,273,3};// факт срабатывания схемы "≥2" для T(пд.рд.) < 10 c(выход алг. 28)
static const PvkByteBit meq2_Npdrd = {p153az,273,4}; // факт срабатывания схемы "≥2" для N(пд.рд.) > плав.уст.(выход алг. 29)
static const PvkByteBit meq2_seism = {p153az,273,5}; // факт срабатывания схемы "≥2" для Сейсмика(выход алг. 30)
static const PvkByteBit meq2_Paz_less150 = {p153az,	283	,	0	};	//	факт срабатывания схемы "≥2" для Р а.з. <150 кгс
static const PvkByteBit meq2_Paz_less140 = {p153az,	283	,	1	};	//	факт срабатывания схемы "≥2" для Р а.з. <140 кгс
static const PvkByteBit meq2_Paz_more150 = {p153az,	283	,	2	};	//	факт срабатывания схемы "≥2" для Р а.з. >150 кгс
static const PvkByteBit meq2_Ngcn1_less3000 = {p153az,	283	,	3	};	//	факт срабатывания схемы "≥2" для N гцн 1 < 3000 кВт
static const PvkByteBit meq2_Ngcn2_less3000 = {p153az,	283	,	4	};	//	факт срабатывания схемы "≥2" для N гцн 2 < 3000 кВт
static const PvkByteBit meq2_Ngcn3_less3000 = {p153az,	283	,	5	};	//	факт срабатывания схемы "≥2" для N гцн 3 < 3000 кВт
static const PvkByteBit meq2_Ngcn4_less3000 = {p153az,	283	,	6	};	//	факт срабатывания схемы "≥2" для N гцн 4 < 3000 кВт
static const PvkByteBit meq2_Lpg1_less175 = {p153az,	283	,	7	};	//	факт срабатывания схемы "≥2" для L пг 1 < 175 см
static const PvkByteBit meq2_Lpg2_less175 = {p153az,	284	,	0	};	//	факт срабатывания схемы "≥2" для L пг 2 < 175 см
static const PvkByteBit meq2_Lpg3_less175 = {p153az,	284	,	1	};	//	факт срабатывания схемы "≥2" для L пг 3 < 175 см
static const PvkByteBit meq2_Lpg4_less175 = {p153az,	284	,	2	};	//	факт срабатывания схемы "≥2" для L пг 4 < 175 см
static const PvkByteBit meq2_Lpg1_neispr = {p153az,	284	,	3	};	//	факт срабатывания схемы "≥2" для неиспр. L пг 1
static const PvkByteBit meq2_Lpg2_neispr = {p153az,	284	,	4	};	//	факт срабатывания схемы "≥2" для неиспр. L пг 2
static const PvkByteBit meq2_Lpg3_neispr = {p153az,	284	,	5	};	//	факт срабатывания схемы "≥2" для неиспр. L пг 3
static const PvkByteBit meq2_Lpg4_neispr = {p153az,	284	,	6	};	//	факт срабатывания схемы "≥2" для неиспр. L пг 4
static const PvkByteBit meq2_Lkd_less460 = {p153az,	284	,	7	};	//	факт срабатывания схемы "≥2" для L кд < 460 см
static const PvkByteBit meq2_Lkd_neispr = {p153az,	285	,	0	};	//	факт срабатывания схемы "≥2" для неиспр. L кд
static const PvkByteBit meq2_dPgcn1_less4_0 = {p153az,	285	,	1	};	//	факт срабатывания схемы "≥2" для ΔР гцн 1 < 4,0 кгс
static const PvkByteBit meq2_dPgcn2_less4_0 = {p153az,	285	,	2	};	//	факт срабатывания схемы "≥2" для ΔР гцн 2 < 4,0 кгс
static const PvkByteBit meq2_dPgcn3_less4_0 = {p153az,	285	,	3	};	//	факт срабатывания схемы "≥2" для ΔР гцн 3 < 4,0 кгс
static const PvkByteBit meq2_dPgcn4_less4_0 = {p153az,	285	,	4	};	//	факт срабатывания схемы "≥2" для ΔР гцн 4 < 4,0 кгс
static const PvkByteBit meq2_dPgcn1_less2_5 = {p153az,	285	,	5	};	//	факт срабатывания схемы "≥2" для ΔР гцн 1 < 2,5 кгс
static const PvkByteBit meq2_dPgcn2_less2_5 = {p153az,	285	,	6	};	//	факт срабатывания схемы "≥2" для ΔР гцн 2 < 2,5 кгс
static const PvkByteBit meq2_dPgcn3_less2_5 = {p153az,	285	,	7	};	//	факт срабатывания схемы "≥2" для ΔР гцн 3 < 2,5 кгс
static const PvkByteBit meq2_dPgcn4_less2_5 = {p153az,	286	,	0	};	//	факт срабатывания схемы "≥2" для ΔР гцн 4 < 2,5 кгс
static const PvkByteBit meq2_Ppg1_less50 = {p153az,	286	,	1	};	//	факт срабатывания схемы "≥2" для Ppg1 < 50 кгс
static const PvkByteBit meq2_Ppg2_less50 = {p153az,	286	,	2	};	//	факт срабатывания схемы "≥2" для Ppg2 < 50 кгс
static const PvkByteBit meq2_Ppg3_less50 = {p153az,	286	,	3	};	//	факт срабатывания схемы "≥2" для Ppg3 < 50 кгс
static const PvkByteBit meq2_Ppg4_less50 = {p153az,	286	,	4	};	//	факт срабатывания схемы "≥2" для Ppg4 < 50 кгс
static const PvkByteBit meq2_Ppg1_more80 = {p153az,	286	,	5	};	//	факт срабатывания схемы "≥2" для Ppg1 > 80 кгс
static const PvkByteBit meq2_Ppg2_more80 = {p153az,	286	,	6	};	//	факт срабатывания схемы "≥2" для Ppg2 > 80 кгс
static const PvkByteBit meq2_Ppg3_more80 = {p153az,	286	,	7	};	//	факт срабатывания схемы "≥2" для Ppg3 > 80 кгс
static const PvkByteBit meq2_Ppg4_more80 = {p153az,	287	,	0	};	//	факт срабатывания схемы "≥2" для Ppg4 > 80 кгс
static const PvkByteBit meq2_Tgn1_more200 = {p153az,	287	,	1	};	//	факт срабатывания схемы "≥2" дляTgn1 > 200 C
static const PvkByteBit meq2_Tgn2_more200 = {p153az,	287	,	2	};	//	факт срабатывания схемы "≥2" дляTgn2 > 200 C
static const PvkByteBit meq2_Tgn3_more200 = {p153az,	287	,	3	};	//	факт срабатывания схемы "≥2" дляTgn3 > 200 C
static const PvkByteBit meq2_Tgn4_more200 = {p153az,	287	,	4	};	//	факт срабатывания схемы "≥2" дляTgn4 > 200 C
static const PvkByteBit meq2_Tgn1_more330 = {p153az,	287	,	5	};	//	факт срабатывания схемы "≥2" дляTgn1 > 330 C
static const PvkByteBit meq2_Tgn2_more330 = {p153az,	287	,	6	};	//	факт срабатывания схемы "≥2" дляTgn2 > 330 C
static const PvkByteBit meq2_Tgn3_more330 = {p153az,	287	,	7	};	//	факт срабатывания схемы "≥2" дляTgn3 > 330 C
static const PvkByteBit meq2_Tgn4_more330 = {p153az,	288	,	0	};	//	факт срабатывания схемы "≥2" дляTgn4 > 330 C
static const PvkByteBit meq2_Tgn1_more260 = {p153az,	288	,	1	};	//	факт срабатывания схемы "≥2" дляTgn1 > 260 C
static const PvkByteBit meq2_Tgn2_more260 = {p153az,	288	,	2	};	//	факт срабатывания схемы "≥2" дляTgn2 > 260 C
static const PvkByteBit meq2_Tgn3_more260 = {p153az,	288	,	3	};	//	факт срабатывания схемы "≥2" дляTgn3 > 260 C
static const PvkByteBit meq2_Tgn4_more260 = {p153az,	288	,	4	};	//	факт срабатывания схемы "≥2" дляTgn4 > 260 C
static const PvkByteBit meq2_Tgn1_less268 = {p153az,	288	,	5	};	//	факт срабатывания схемы "≥2" дляTgn1 < 268 C
static const PvkByteBit meq2_Tgn2_less268 = {p153az,	288	,	6	};	//	факт срабатывания схемы "≥2" дляTgn2 < 268 C
static const PvkByteBit meq2_Tgn3_less268 = {p153az,	288	,	7	};	//	факт срабатывания схемы "≥2" дляTgn3 < 268 C
static const PvkByteBit meq2_Tgn4_less268 = {p153az,	289	,	0	};	//	факт срабатывания схемы "≥2" дляTgn4 < 268 C
static const PvkByteBit meq2_Fgcn1_less46 = {p153az,	289	,	1	};	//	факт срабатывания схемы "≥2" для F гцн 1 < 46 Гц
static const PvkByteBit meq2_Fgcn2_less46 = {p153az,	289	,	2	};	//	факт срабатывания схемы "≥2" для F гцн 2 < 46 Гц
static const PvkByteBit meq2_Fgcn3_less46 = {p153az,	289	,	3	};	//	факт срабатывания схемы "≥2" для F гцн 3 < 46 Гц
static const PvkByteBit meq2_Fgcn4_less46 = {p153az,	289	,	4	};	//	факт срабатывания схемы "≥2" для F гцн 4 < 46 Гц
static const PvkByteBit meq2_Ptpn1_less0_408 = {p153az,	289	,	5	};	//	факт срабатывания схемы "≥2" для P тпн 1 < 0,408 кгс
static const PvkByteBit meq2_Ptpn2_less0_408 = {p153az,	289	,	6	};	//	факт срабатывания схемы "≥2" для P тпн 2 < 0,408 кгс
static const PvkByteBit meq2_Taz_pg1_more75 = {p153az,	289	,	7	};	//	факт срабатывания схемы "≥2" для (Ts а.з. - Ts п.пг. 1) > 75 C
static const PvkByteBit meq2_Taz_pg2_more75 = {p153az,	290	,	0	};	//	факт срабатывания схемы "≥2" для (Ts а.з. - Ts п.пг. 2) > 75 C
static const PvkByteBit meq2_Taz_pg3_more75 = {p153az,	290	,	1	};	//	факт срабатывания схемы "≥2" для (Ts а.з. - Ts п.пг. 3) > 75 C
static const PvkByteBit meq2_Taz_pg4_more75 = {p153az,	290	,	2	};	//	факт срабатывания схемы "≥2" для (Ts а.з. - Ts п.пг. 4) > 75 C
static const PvkByteBit meq2_N_more75 = {p153az,	290	,	3	};	//	факт срабатывания схемы "≥2" для N > 75%
static const PvkByteBit meq2_N_more5 = {p153az,	290	,	4	};	//	факт срабатывания схемы "≥2" для N > 5%
static const PvkByteBit meq2_PotPitSuz = {p153az,	290	,	5	};	//	факт срабатывания схемы "≥2" для Пот. Пит. СУЗ
static const PvkByteBit meq2_N_more40 = {p153az,	290	,	6	};	//	факт срабатывания схемы "≥2" для N > 40%
// подсветка "≥2" 155/156 ПЗ
static const PvkByteBit meq2_Preak_more170_3_pz = {p153pz,72,1}; //факт срабатывания схемы "≥2" для Р а.з. >170,3 кгс(выход алг. 2)	72	1
static const PvkByteBit meq2_Pgpk_more70_pz = {p153pz,72,2}; //факт срабатывания схемы "≥2" для Р гпк >70 кгс(выход алг. 3)	72	2
static const PvkByteBit meq2_Tpdrd_less20_pz = {p153pz,72,3};//факт срабатывания схемы "≥2" для T(пд.рд.) < 20 c(выход алг. 4)	72	3
static const PvkByteBit meq2_Npdrd_pz = {p153pz,72,4};//факт срабатывания схемы "≥2" для N(пд.рд.) > уст.(выход алг. 5)	72	4
static const PvkByteBit meq2_Razgruzka_pz = {p153pz,72,5};//факт срабатывания схемы "≥2" для Разгрузка(выход алг. 6)	72	5
static const PvkByteBit meq2_Npdrd_more10_15az_pz = {p153pz,72,6};//факт срабатывания схемы "≥2" для N(пд.рд.) > 10/15АЗ(выход алг. 7)	72	6
static const PvkByteBit meq2_Paz_more165_2_pz = {p153pz,72,7};//факт срабатывания схемы "≥2" для Р а.з. >165,2 кгс(выход алг. 8)	72	7
static const PvkByteBit meq2_padenie1OR_pz = {p153pz,73,0};//факт срабатывания схемы "≥2" для Падение одного ОР(выход алг. 9)	73	0
static const PvkByteBit meq2_QL_more350_pz = {p153pz,73,1};//факт срабатывания схемы "≥2" для QL>350(выход алг. 10)	73	1
static const PvkByteBit meq2_DNBR_less1_45_pz = {p153pz,73,2};//факт срабатывания схемы "≥2" для DNBR<1,45(выход алг. 11)
static const PvkByteBit meq2_QL_more400_pz = {p153pz,74,0};//факт срабатывания схемы "≥2" для QL>400(выход алг. 17)	73	1
static const PvkByteBit meq2_DNBR_less1_40_pz = {p153pz,74,1};//факт срабатывания схемы "≥2" для DNBR<1,40 (выход алг. 18)
static const PvkByteBit meq2_Tgn1_more325_pz = {p153pz,76,0};// факт срабатывания схемы "≥2" дляTgn1 > 325C
static const PvkByteBit meq2_Tgn2_more325_pz = {p153pz,76,1};// факт срабатывания схемы "≥2" дляTgn2 > 325C
static const PvkByteBit meq2_Tgn3_more325_pz = {p153pz,76,2};// факт срабатывания схемы "≥2" дляTgn3 > 325C
static const PvkByteBit meq2_Tgn4_more325_pz = {p153pz,76,3};// факт срабатывания схемы "≥2" дляTgn4 > 325C
static const PvkByteBit meq2_Ngcn1_less3000_pz = {p153pz,76,4};// факт срабатывания схемы "≥2" для N гцн 1 < 3000 кВт
static const PvkByteBit meq2_Ngcn2_less3000_pz = {p153pz,76,5};// факт срабатывания схемы "≥2" для N гцн 2 < 3000 кВт
static const PvkByteBit meq2_Ngcn3_less3000_pz = {p153pz,76,6};// факт срабатывания схемы "≥2" для N гцн 3 < 3000 кВт
static const PvkByteBit meq2_Ngcn4_less3000_pz = {p153pz,76,7};// факт срабатывания схемы "≥2" для N гцн 4 < 3000 кВт
static const PvkByteBit meq2_Ptpn1_less0_408_pz = {p153pz,77,0};// факт срабатывания схемы "≥2" для P тпн 1 < 0,408 кгс
static const PvkByteBit meq2_Ptpn2_less0_408_pz = {p153pz,77,1};// факт срабатывания схемы "≥2" для P тпн 2 < 0,408 кгс
static const PvkByteBit meq2_N_more75_pz = {p153pz,77,2}; // факт срабатывания схемы "≥2" для N > 75%
static const PvkByteBit meq2_otklEB_pz = {p153pz,77,3}; // факт срабатывания схемы "≥2" для Откл. ЭБ	77	3
static const PvkByteBit meq2_otlGen_pz = {p153pz,77,4}; // факт срабатывания схемы "≥2" для Откл. Генератора	77	4
static const PvkByteBit meq2_ZakrBolee1sk_pz = {p153pz,77,5}; // факт срабатывания схемы "≥2" для Закрыты более 1 СК	77	5

// индексы: факт сраб уставки 153/154 АЗ
static const PvkByteBitSign	faz_Paz_less150_A		= {p153az,243,0,0};              // факт срабатывания уставки Р а.з. <150 кгс A
static const PvkByteBitSign	faz_Paz_less150_B		= {p153az,243,1,0};              // факт срабатывания уставки Р а.з. <150 кгс B
static const PvkByteBitSign	faz_Paz_less150_C		= {p153az,243,2,0};              // факт срабатывания уставки Р а.з. <150 кгс C
static const PvkByteBitSign	faz_Paz_more178_5_A		= {p153az,243,3,1};              // факт срабатывания уставки Р а.з. >178,5 кгс A
static const PvkByteBitSign	faz_Paz_more178_5_B		= {p153az,243,4,1};              // факт срабатывания уставки Р а.з. >178,5 кгс B
static const PvkByteBitSign	faz_Paz_more178_5_C		= {p153az,243,5,1};              // факт срабатывания уставки Р а.з. >178,5 кгс C
static const PvkByteBitSign	faz_Paz_less140_A		= {p153az,243,6,0};              // факт срабатывания уставки Р а.з. <140 кгс A
static const PvkByteBitSign	faz_Paz_less140_B		= {p153az,243,7,0};              // факт срабатывания уставки Р а.з. <140 кгс B
static const PvkByteBitSign	faz_Paz_less140_C		= {p153az,244,0,0};              // факт срабатывания уставки Р а.з. <140 кгс C
static const PvkByteBitSign	faz_Paz_more150_A		= {p153az,244,1,1};              // факт срабатывания уставки Р а.з. >150 кгс A
static const PvkByteBitSign	faz_Paz_more150_B		= {p153az,244,2,1};              // факт срабатывания уставки Р а.з. >150 кгс B
static const PvkByteBitSign	faz_Paz_more150_C		= {p153az,244,3,1};              // факт срабатывания уставки Р а.з. >150 кгс C
static const PvkByteBitSign	faz_Ngcn1_less_3000_A	= {p153az,244,4,0};              // факт срабатывания уставки N гцн 1 < 3000 кВт A
static const PvkByteBitSign	faz_Ngcn1_less_3000_B	= {p153az,244,5,0};              // факт срабатывания уставки N гцн 1 < 3000 кВт B
static const PvkByteBitSign	faz_Ngcn1_less_3000_C	= {p153az,244,6,0};              // факт срабатывания уставки N гцн 1 < 3000 кВт C
static const PvkByteBitSign	faz_Ngcn2_less_3000_A	= {p153az,244,7,0};              // факт срабатывания уставки N гцн 2 < 3000 кВт A
static const PvkByteBitSign	faz_Ngcn2_less_3000_B	= {p153az,245,0,0};              // факт срабатывания уставки N гцн 2 < 3000 кВт B
static const PvkByteBitSign	faz_Ngcn2_less_3000_C	= {p153az,245,1,0};              // факт срабатывания уставки N гцн 2 < 3000 кВт C
static const PvkByteBitSign	faz_Ngcn3_less_3000_A	= {p153az,245,2,0};              // факт срабатывания уставки N гцн 3 < 3000 кВт A
static const PvkByteBitSign	faz_Ngcn3_less_3000_B	= {p153az,245,3,0};              // факт срабатывания уставки N гцн 3 < 3000 кВт B
static const PvkByteBitSign	faz_Ngcn3_less_3000_C	= {p153az,245,4,0};              // факт срабатывания уставки N гцн 3 < 3000 кВт C
static const PvkByteBitSign	faz_Ngcn4_less_3000_A	= {p153az,245,5,0};              // факт срабатывания уставки N гцн 4 < 3000 кВт A
static const PvkByteBitSign	faz_Ngcn4_less_3000_B	= {p153az,245,6,0};              // факт срабатывания уставки N гцн 4 < 3000 кВт B
static const PvkByteBitSign	faz_Ngcn4_less_3000_C	= {p153az,245,7,0};              // факт срабатывания уставки N гцн 4 < 3000 кВт C
static const PvkByteBitSign	faz_Lpg1_less_175_A		= {p153az,246,0,0};              // факт срабатывания уставки L пг 1 < 175 см A
static const PvkByteBitSign	faz_Lpg1_less_175_B		= {p153az,246,1,0};              // факт срабатывания уставки L пг 1 < 175 см B
static const PvkByteBitSign	faz_Lpg1_less_175_C		= {p153az,246,2,0};              // факт срабатывания уставки L пг 1 < 175 см C
static const PvkByteBitSign	faz_Lpg2_less_175_A		= {p153az,246,3,0};              // факт срабатывания уставки L пг 2 < 175 см A
static const PvkByteBitSign	faz_Lpg2_less_175_B		= {p153az,246,4,0};              // факт срабатывания уставки L пг 2 < 175 см B
static const PvkByteBitSign	faz_Lpg2_less_175_C		= {p153az,246,5,0};              // факт срабатывания уставки L пг 2 < 175 см C
static const PvkByteBitSign	faz_Lpg3_less_175_A		= {p153az,246,6,0};              // факт срабатывания уставки L пг 3 < 175 см A
static const PvkByteBitSign	faz_Lpg3_less_175_B		= {p153az,246,7,0};              // факт срабатывания уставки L пг 3 < 175 см B
static const PvkByteBitSign	faz_Lpg3_less_175_C		= {p153az,247,0,0};              // факт срабатывания уставки L пг 3 < 175 см C
static const PvkByteBitSign	faz_Lpg4_less_175_A		= {p153az,247,1,0};              // факт срабатывания уставки L пг 4 < 175 см A
static const PvkByteBitSign	faz_Lpg4_less_175_B		= {p153az,247,2,0};              // факт срабатывания уставки L пг 4 < 175 см B
static const PvkByteBitSign	faz_Lpg4_less_175_C		= {p153az,247,3,0};              // факт срабатывания уставки L пг 4 < 175 см C
static const PvkByteBitSign	faz_fail_Lpg1_A         = {p153az,247,4};              // факт срабатывания уставки неиспр. L пг 1 A
static const PvkByteBitSign	faz_fail_Lpg1_B         = {p153az,247,5};              // факт срабатывания уставки неиспр. L пг 1 B
static const PvkByteBitSign	faz_fail_Lpg1_C         = {p153az,247,6};              // факт срабатывания уставки неиспр. L пг 1 C
static const PvkByteBitSign	faz_fail_Lpg2_A         = {p153az,247,7};              // факт срабатывания уставки неиспр. L пг 2 A
static const PvkByteBitSign	faz_fail_Lpg2_B         = {p153az,248,0};              // факт срабатывания уставки неиспр. L пг 2 B
static const PvkByteBitSign	faz_fail_Lpg2_C         = {p153az,248,1};              // факт срабатывания уставки неиспр. L пг 2 C
static const PvkByteBitSign	faz_fail_Lpg3_A         = {p153az,248,2};              // факт срабатывания уставки неиспр. L пг 3 A
static const PvkByteBitSign	faz_fail_Lpg3_B         = {p153az,248,3};              // факт срабатывания уставки неиспр. L пг 3 B
static const PvkByteBitSign	faz_fail_Lpg3_C         = {p153az,248,4};              // факт срабатывания уставки неиспр. L пг 3 C
static const PvkByteBitSign	faz_fail_Lpg4_A         = {p153az,248,5};              // факт срабатывания уставки неиспр. L пг 4 A
static const PvkByteBitSign	faz_fail_Lpg4_B         = {p153az,248,6};              // факт срабатывания уставки неиспр. L пг 4 B
static const PvkByteBitSign	faz_fail_Lpg4_C         = {p153az,248,7};              // факт срабатывания уставки неиспр. L пг 4 C
static const PvkByteBitSign	faz_L_kd_less_460_A		= {p153az,249,0,0};              // факт срабатывания уставки L кд < 460 см A
static const PvkByteBitSign	faz_L_kd_less_460_B		= {p153az,249,1,0};              // факт срабатывания уставки L кд < 460 см B
static const PvkByteBitSign	faz_L_kd_less_460_C		= {p153az,249,2,0};              // факт срабатывания уставки L кд < 460 см C
static const PvkByteBitSign	faz_fail_Lkd_A          = {p153az,249,3};              // факт срабатывания уставки неиспр. L кд A
static const PvkByteBitSign	faz_fail_Lkd_B          = {p153az,249,4};              // факт срабатывания уставки неиспр. L кд B
static const PvkByteBitSign	faz_fail_Lkd_C          = {p153az,249,5};              // факт срабатывания уставки неиспр. L кд C
static const PvkByteBitSign	faz_Pgaz06_more0_306_A	= {p153az,249,6,0};              // факт срабатывания уставки Р под.об. > 0,306 кгс A
static const PvkByteBitSign	faz_Pgaz06_more0_306_B	= {p153az,249,7,0};              // факт срабатывания уставки Р под.об. > 0,306 кгс B
static const PvkByteBitSign	faz_Pgaz06_more0_306_C	= {p153az,250,0,0};              // факт срабатывания уставки Р под.об. > 0,306 кгс C
static const PvkByteBitSign	faz_dPgcn1_less_4_0_A	= {p153az,250,1,0};              // факт срабатывания уставки ΔР гцн 1 < 4,0 кгс A
static const PvkByteBitSign	faz_dPgcn1_less_4_0_B	= {p153az,250,2,0};              // факт срабатывания уставки ΔР гцн 1 < 4,0 кгс B
static const PvkByteBitSign	faz_dPgcn1_less_4_0_C	= {p153az,250,3,0};              // факт срабатывания уставки ΔР гцн 1 < 4,0 кгс C
static const PvkByteBitSign	faz_dPgcn2_less_4_0_A	= {p153az,250,4,0};              // факт срабатывания уставки ΔР гцн 2 < 4,0 кгс A
static const PvkByteBitSign	faz_dPgcn2_less_4_0_B	= {p153az,250,5,0};              // факт срабатывания уставки ΔР гцн 2 < 4,0 кгс B
static const PvkByteBitSign	faz_dPgcn2_less_4_0_C	= {p153az,250,6,0};              // факт срабатывания уставки ΔР гцн 2 < 4,0 кгс C
static const PvkByteBitSign	faz_dPgcn3_less_4_0_A	= {p153az,250,7,0};              // факт срабатывания уставки ΔР гцн 3 < 4,0 кгс A
static const PvkByteBitSign	faz_dPgcn3_less_4_0_B	= {p153az,251,0,0};              // факт срабатывания уставки ΔР гцн 3 < 4,0 кгс B
static const PvkByteBitSign	faz_dPgcn3_less_4_0_C	= {p153az,251,1,0};              // факт срабатывания уставки ΔР гцн 3 < 4,0 кгс C
static const PvkByteBitSign	faz_dPgcn4_less_4_0_A	= {p153az,251,2,0};              // факт срабатывания уставки ΔР гцн 4 < 4,0 кгс A
static const PvkByteBitSign	faz_dPgcn4_less_4_0_B	= {p153az,251,3,0};              // факт срабатывания уставки ΔР гцн 4 < 4,0 кгс B
static const PvkByteBitSign	faz_dPgcn4_less_4_0_C	= {p153az,251,4,0};              // факт срабатывания уставки ΔР гцн 4 < 4,0 кгс C
static const PvkByteBitSign	faz_dPgcn1_less_2_5_A	= {p153az,251,5,0};              // факт срабатывания уставки ΔР гцн 1 < 2,5 кгс A
static const PvkByteBitSign	faz_dPgcn1_less_2_5_B	= {p153az,251,6,0};              // факт срабатывания уставки ΔР гцн 1 < 2,5 кгс B
static const PvkByteBitSign	faz_dPgcn1_less_2_5_C	= {p153az,251,7,0};              // факт срабатывания уставки ΔР гцн 1 < 2,5 кгс C
static const PvkByteBitSign	faz_dPgcn2_less_2_5_A	= {p153az,252,0,0};              // факт срабатывания уставки ΔР гцн 2 < 2,5 кгс A
static const PvkByteBitSign	faz_dPgcn2_less_2_5_B	= {p153az,252,1,0};              // факт срабатывания уставки ΔР гцн 2 < 2,5 кгс B
static const PvkByteBitSign	faz_dPgcn2_less_2_5_C	= {p153az,252,2,0};              // факт срабатывания уставки ΔР гцн 2 < 2,5 кгс C
static const PvkByteBitSign	faz_dPgcn3_less_2_5_A	= {p153az,252,3,0};              // факт срабатывания уставки ΔР гцн 3 < 2,5 кгс A
static const PvkByteBitSign	faz_dPgcn3_less_2_5_B	= {p153az,252,4,0};              // факт срабатывания уставки ΔР гцн 3 < 2,5 кгс B
static const PvkByteBitSign	faz_dPgcn3_less_2_5_C	= {p153az,252,5,0};              // факт срабатывания уставки ΔР гцн 3 < 2,5 кгс C
static const PvkByteBitSign	faz_dPgcn4_less_2_5_A	= {p153az,252,6,0};              // факт срабатывания уставки ΔР гцн 4 < 2,5 кгс A
static const PvkByteBitSign	faz_dPgcn4_less_2_5_B	= {p153az,252,7,0};              // факт срабатывания уставки ΔР гцн 4 < 2,5 кгс B
static const PvkByteBitSign	faz_dPgcn4_less_2_5_C	= {p153az,253,0,0};              // факт срабатывания уставки ΔР гцн 4 < 2,5 кгс C
static const PvkByteBitSign	faz_Ppg1_less50_A		= {p153az,253,1,0};              // факт срабатывания уставки Ppg1 < 50 кгс A
static const PvkByteBitSign	faz_Ppg1_less50_B		= {p153az,253,2,0};              // факт срабатывания уставки Ppg1 < 50 кгс B
static const PvkByteBitSign	faz_Ppg1_less50_C		= {p153az,253,3,0};              // факт срабатывания уставки Ppg1 < 50 кгс C
static const PvkByteBitSign	faz_Ppg2_less50_A		= {p153az,253,4,0};              // факт срабатывания уставки Ppg2 < 50 кгс A
static const PvkByteBitSign	faz_Ppg2_less50_B		= {p153az,253,5,0};              // факт срабатывания уставки Ppg2 < 50 кгс B
static const PvkByteBitSign	faz_Ppg2_less50_C		= {p153az,253,6,0};              // факт срабатывания уставки Ppg2 < 50 кгс C
static const PvkByteBitSign	faz_Ppg3_less50_A		= {p153az,253,7,0};              // факт срабатывания уставки Ppg3 < 50 кгс A
static const PvkByteBitSign	faz_Ppg3_less50_B		= {p153az,254,0,0};              // факт срабатывания уставки Ppg3 < 50 кгс B
static const PvkByteBitSign	faz_Ppg3_less50_C		= {p153az,254,1,0};              // факт срабатывания уставки Ppg3 < 50 кгс C
static const PvkByteBitSign	faz_Ppg4_less50_A		= {p153az,254,2,0};              // факт срабатывания уставки Ppg4 < 50 кгс A
static const PvkByteBitSign	faz_Ppg4_less50_B		= {p153az,254,3,0};              // факт срабатывания уставки Ppg4 < 50 кгс B
static const PvkByteBitSign	faz_Ppg4_less50_C		= {p153az,254,4,0};              // факт срабатывания уставки Ppg4 < 50 кгс C
static const PvkByteBitSign	faz_Ppg1_more80_A		= {p153az,254,5,1};              // факт срабатывания уставки Ppg1 > 80 кгс A
static const PvkByteBitSign	faz_Ppg1_more80_B		= {p153az,254,6,1};              // факт срабатывания уставки Ppg1 > 80 кгс B
static const PvkByteBitSign	faz_Ppg1_more80_C		= {p153az,254,7,1};              // факт срабатывания уставки Ppg1 > 80 кгс C
static const PvkByteBitSign	faz_Ppg2_more80_A		= {p153az,255,0,1};              // факт срабатывания уставки Ppg2 > 80 кгс A
static const PvkByteBitSign	faz_Ppg2_more80_B		= {p153az,255,1,1};              // факт срабатывания уставки Ppg2 > 80 кгс B
static const PvkByteBitSign	faz_Ppg2_more80_C		= {p153az,255,2,1};              // факт срабатывания уставки Ppg2 > 80 кгс C
static const PvkByteBitSign	faz_Ppg3_more80_A		= {p153az,255,3,1};              // факт срабатывания уставки Ppg3 > 80 кгс A
static const PvkByteBitSign	faz_Ppg3_more80_B		= {p153az,255,4,1};              // факт срабатывания уставки Ppg3 > 80 кгс B
static const PvkByteBitSign	faz_Ppg3_more80_C		= {p153az,255,5,1};              // факт срабатывания уставки Ppg3 > 80 кгс C
static const PvkByteBitSign	faz_Ppg4_more80_A		= {p153az,255,6,1};              // факт срабатывания уставки Ppg4 > 80 кгс A
static const PvkByteBitSign	faz_Ppg4_more80_B		= {p153az,255,7,1};              // факт срабатывания уставки Ppg4 > 80 кгс B
static const PvkByteBitSign	faz_Ppg4_more80_C		= {p153az,256,0,1};              // факт срабатывания уставки Ppg4 > 80 кгс C
static const PvkByteBitSign	faz_Tgn1_more200_C_A	= {p153az,256,1,1};              // факт срабатывания уставкиTgn1 > 200 C A
static const PvkByteBitSign	faz_Tgn1_more200_C_B	= {p153az,256,2,1};              // факт срабатывания уставкиTgn1 > 200 C B
static const PvkByteBitSign	faz_Tgn1_more200_C_C	= {p153az,256,3,1};              // факт срабатывания уставкиTgn1 > 200 C C
static const PvkByteBitSign	faz_Tgn2_more200_C_A	= {p153az,256,4,1};              // факт срабатывания уставкиTgn2 > 200 C A
static const PvkByteBitSign	faz_Tgn2_more200_C_B	= {p153az,256,5,1};              // факт срабатывания уставкиTgn2 > 200 C B
static const PvkByteBitSign	faz_Tgn2_more200_C_C	= {p153az,256,6,1};              // факт срабатывания уставкиTgn2 > 200 C C
static const PvkByteBitSign	faz_Tgn3_more200_C_A	= {p153az,256,7,1};              // факт срабатывания уставкиTgn3 > 200 C A
static const PvkByteBitSign	faz_Tgn3_more200_C_B	= {p153az,257,0,1};              // факт срабатывания уставкиTgn3 > 200 C B
static const PvkByteBitSign	faz_Tgn3_more200_C_C	= {p153az,257,1,1};              // факт срабатывания уставкиTgn3 > 200 C C
static const PvkByteBitSign	faz_Tgn4_more200_C_A	= {p153az,257,2,1};              // факт срабатывания уставкиTgn4 > 200 C A
static const PvkByteBitSign	faz_Tgn4_more200_C_B	= {p153az,257,3,1};              // факт срабатывания уставкиTgn4 > 200 C B
static const PvkByteBitSign	faz_Tgn4_more200_C_C	= {p153az,257,4,1};              // факт срабатывания уставкиTgn4 > 200 C C
static const PvkByteBitSign	faz_Tgn1_more330_C_A	= {p153az,257,5,1};              // факт срабатывания уставкиTgn1 > 330 C A
static const PvkByteBitSign	faz_Tgn1_more330_C_B	= {p153az,257,6,1};              // факт срабатывания уставкиTgn1 > 330 C B
static const PvkByteBitSign	faz_Tgn1_more330_C_C	= {p153az,257,7,1};              // факт срабатывания уставкиTgn1 > 330 C C
static const PvkByteBitSign	faz_Tgn2_more330_C_A	= {p153az,258,0,1};              // факт срабатывания уставкиTgn2 > 330 C A
static const PvkByteBitSign	faz_Tgn2_more330_C_B	= {p153az,258,1,1};              // факт срабатывания уставкиTgn2 > 330 C B
static const PvkByteBitSign	faz_Tgn2_more330_C_C	= {p153az,258,2,1};              // факт срабатывания уставкиTgn2 > 330 C C
static const PvkByteBitSign	faz_Tgn3_more330_C_A	= {p153az,258,3,1};              // факт срабатывания уставкиTgn3 > 330 C A
static const PvkByteBitSign	faz_Tgn3_more330_C_B	= {p153az,258,4,1};              // факт срабатывания уставкиTgn3 > 330 C B
static const PvkByteBitSign	faz_Tgn3_more330_C_C	= {p153az,258,5,1};              // факт срабатывания уставкиTgn3 > 330 C C
static const PvkByteBitSign	faz_Tgn4_more330_C_A	= {p153az,258,6,1};              // факт срабатывания уставкиTgn4 > 330 C A
static const PvkByteBitSign	faz_Tgn4_more330_C_B	= {p153az,258,7,1};              // факт срабатывания уставкиTgn4 > 330 C B
static const PvkByteBitSign	faz_Tgn4_more330_C_C	= {p153az,259,0,1};              // факт срабатывания уставкиTgn4 > 330 C C
static const PvkByteBitSign	faz_Tgn1_more260_C_A	= {p153az,259,1,1};              // факт срабатывания уставкиTgn1 > 260 C A
static const PvkByteBitSign	faz_Tgn1_more260_C_B	= {p153az,259,2,1};              // факт срабатывания уставкиTgn1 > 260 C B
static const PvkByteBitSign	faz_Tgn1_more260_C_C	= {p153az,259,3,1};              // факт срабатывания уставкиTgn1 > 260 C C
static const PvkByteBitSign	faz_Tgn2_more260_C_A	= {p153az,259,4,1};              // факт срабатывания уставкиTgn2 > 260 C A
static const PvkByteBitSign	faz_Tgn2_more260_C_B	= {p153az,259,5,1};              // факт срабатывания уставкиTgn2 > 260 C B
static const PvkByteBitSign	faz_Tgn2_more260_C_C	= {p153az,259,6,1};              // факт срабатывания уставкиTgn2 > 260 C C
static const PvkByteBitSign	faz_Tgn3_more260_C_A	= {p153az,259,7,1};              // факт срабатывания уставкиTgn3 > 260 C A
static const PvkByteBitSign	faz_Tgn3_more260_C_B	= {p153az,260,0,1};              // факт срабатывания уставкиTgn3 > 260 C B
static const PvkByteBitSign	faz_Tgn3_more260_C_C	= {p153az,260,1,1};              // факт срабатывания уставкиTgn3 > 260 C C
static const PvkByteBitSign	faz_Tgn4_more260_C_A	= {p153az,260,2,1};              // факт срабатывания уставкиTgn4 > 260 C A
static const PvkByteBitSign	faz_Tgn4_more260_C_B	= {p153az,260,3,1};              // факт срабатывания уставкиTgn4 > 260 C B
static const PvkByteBitSign	faz_Tgn4_more260_C_C	= {p153az,260,4,1};              // факт срабатывания уставкиTgn4 > 260 C C
static const PvkByteBitSign	faz_Tgn1_less_268_C_A	= {p153az,260,5,0};              // факт срабатывания уставкиTgn1 < 268 C A
static const PvkByteBitSign	faz_Tgn1_less_268_C_B	= {p153az,260,6,0};              // факт срабатывания уставкиTgn1 < 268 C B
static const PvkByteBitSign	faz_Tgn1_less_268_C_C	= {p153az,260,7,0};              // факт срабатывания уставкиTgn1 < 268 C C
static const PvkByteBitSign	faz_Tgn2_less_268_C_A	= {p153az,261,0,0};              // факт срабатывания уставкиTgn2 < 268 C A
static const PvkByteBitSign	faz_Tgn2_less_268_C_B	= {p153az,261,1,0};              // факт срабатывания уставкиTgn2 < 268 C B
static const PvkByteBitSign	faz_Tgn2_less_268_C_C	= {p153az,261,2,0};              // факт срабатывания уставкиTgn2 < 268 C C
static const PvkByteBitSign	faz_Tgn3_less_268_C_A	= {p153az,261,3,0};              // факт срабатывания уставкиTgn3 < 268 C A
static const PvkByteBitSign	faz_Tgn3_less_268_C_B	= {p153az,261,4,0};              // факт срабатывания уставкиTgn3 < 268 C B
static const PvkByteBitSign	faz_Tgn3_less_268_C_C	= {p153az,261,5,0};              // факт срабатывания уставкиTgn3 < 268 C C
static const PvkByteBitSign	faz_Tgn4_less_268_C_A	= {p153az,261,6,0};              // факт срабатывания уставкиTgn4 < 268 C A
static const PvkByteBitSign	faz_Tgn4_less_268_C_B	= {p153az,261,7,0};              // факт срабатывания уставкиTgn4 < 268 C B
static const PvkByteBitSign	faz_Tgn4_less_268_C_C	= {p153az,262,0,0};              // факт срабатывания уставкиTgn4 < 268 C C
static const PvkByteBitSign	faz_Fgcn1_less_46_A		= {p153az,262,1,0};              // факт срабатывания  уставки F гцн 1 < 46 Гц A
static const PvkByteBitSign	faz_Fgcn1_less_46_B		= {p153az,262,2,0};              // факт срабатывания  уставки F гцн 1 < 46 Гц B
static const PvkByteBitSign	faz_Fgcn1_less_46_C		= {p153az,262,3,0};              // факт срабатывания  уставки F гцн 1 < 46 Гц C
static const PvkByteBitSign	faz_Fgcn2_less_46_A		= {p153az,262,4,0};              // факт срабатывания  уставки F гцн 2 < 46 Гц A
static const PvkByteBitSign	faz_Fgcn2_less_46_B		= {p153az,262,5,0};              // факт срабатывания  уставки F гцн 2 < 46 Гц B
static const PvkByteBitSign	faz_Fgcn2_less_46_C		= {p153az,262,6,0};              // факт срабатывания  уставки F гцн 2 < 46 Гц C
static const PvkByteBitSign	faz_Fgcn3_less_46_A		= {p153az,262,7,0};              // факт срабатывания  уставки F гцн 3 < 46 Гц A
static const PvkByteBitSign	faz_Fgcn3_less_46_B		= {p153az,263,0,0};              // факт срабатывания  уставки F гцн 3 < 46 Гц B
static const PvkByteBitSign	faz_Fgcn3_less_46_C		= {p153az,263,1,0};              // факт срабатывания  уставки F гцн 3 < 46 Гц C
static const PvkByteBitSign	faz_Fgcn4_less_46_A		= {p153az,263,2,0};              // факт срабатывания  уставки F гцн 4 < 46 Гц A
static const PvkByteBitSign	faz_Fgcn4_less_46_B		= {p153az,263,3,0};              // факт срабатывания  уставки F гцн 4 < 46 Гц B
static const PvkByteBitSign	faz_Fgcn4_less_46_C		= {p153az,263,4,0};              // факт срабатывания  уставки F гцн 4 < 46 Гц C
static const PvkByteBitSign	faz_Ptpn1_less_0_408_A	= {p153az,263,5,0};              // факт срабатывания уставки P тпн 1 < 0,408 кгс A
static const PvkByteBitSign	faz_Ptpn1_less_0_408_B	= {p153az,263,6,0};              // факт срабатывания уставки P тпн 1 < 0,408 кгс B
static const PvkByteBitSign	faz_Ptpn1_less_0_408_C	= {p153az,263,7,0};              // факт срабатывания уставки P тпн 1 < 0,408 кгс C
static const PvkByteBitSign	faz_Ptpn2_less_0_408_A	= {p153az,264,0,0};              // факт срабатывания уставки P тпн 2 < 0,408 кгс A
static const PvkByteBitSign	faz_Ptpn2_less_0_408_B	= {p153az,264,1,0};              // факт срабатывания уставки P тпн 2 < 0,408 кгс B
static const PvkByteBitSign	faz_Ptpn2_less_0_408_C	= {p153az,264,2,0};              // факт срабатывания уставки P тпн 2 < 0,408 кгс C
static const PvkByteBitSign	faz_Ts_az__Ts_pg1_more75_C_A	= {p153az,264,3,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 1) > 75 C A
static const PvkByteBitSign	faz_Ts_az__Ts_pg1_more75_C_B	= {p153az,264,4,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 1) > 75 C B
static const PvkByteBitSign	faz_Ts_az__Ts_pg1_more75_C_C	= {p153az,264,5,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 1) > 75 C C
static const PvkByteBitSign	faz_Ts_az__Ts_pg2_more75_C_A	= {p153az,264,6,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 2) > 75 C A
static const PvkByteBitSign	faz_Ts_az__Ts_pg2_more75_C_B	= {p153az,264,7,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 2) > 75 C B
static const PvkByteBitSign	faz_Ts_az__Ts_pg2_more75_C_C	= {p153az,265,0,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 2) > 75 C C
static const PvkByteBitSign	faz_Ts_az__Ts_pg3_more75_C_A	= {p153az,265,1,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 3) > 75 C A
static const PvkByteBitSign	faz_Ts_az__Ts_pg3_more75_C_B	= {p153az,265,2,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 3) > 75 C B
static const PvkByteBitSign	faz_Ts_az__Ts_pg3_more75_C_C	= {p153az,265,3,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 3) > 75 C C
static const PvkByteBitSign	faz_Ts_az__Ts_pg4_more75_C_A	= {p153az,265,4,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 4) > 75 C A
static const PvkByteBitSign	faz_Ts_az__Ts_pg4_more75_C_B	= {p153az,265,5,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 4) > 75 C B
static const PvkByteBitSign	faz_Ts_az__Ts_pg4_more75_C_C	= {p153az,265,6,1};              // факт срабатывания уставки = {p153az,Ts а.з. - Ts п.пг. 4) > 75 C C
static const PvkByteBitSign	faz_Ts_az__Tgn1_less_10_C_A	= {p153az,265,7,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn1) < 10 C A
static const PvkByteBitSign	faz_Ts_az__Tgn1_less_10_C_B	= {p153az,266,0,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn1) < 10 C B
static const PvkByteBitSign	faz_Ts_az__Tgn1_less_10_C_C	= {p153az,266,1,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn1) < 10 C C
static const PvkByteBitSign	faz_Ts_az__Tgn2_less_10_C_A	= {p153az,266,2,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn2) < 10 C A
static const PvkByteBitSign	faz_Ts_az__Tgn2_less_10_C_B	= {p153az,266,3,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn2) < 10 C B
static const PvkByteBitSign	faz_Ts_az__Tgn2_less_10_C_C	= {p153az,266,4,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn2) < 10 C C
static const PvkByteBitSign	faz_Ts_az__Tgn3_less_10_C_A	= {p153az,266,5,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn3) < 10 C A
static const PvkByteBitSign	faz_Ts_az__Tgn3_less_10_C_B	= {p153az,266,6,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn3) < 10 C B
static const PvkByteBitSign	faz_Ts_az__Tgn3_less_10_C_C	= {p153az,266,7,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn3) < 10 C C
static const PvkByteBitSign	faz_Ts_az__Tgn4_less_10_C_A	= {p153az,267,0,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn4) < 10 C A
static const PvkByteBitSign	faz_Ts_az__Tgn4_less_10_C_B	= {p153az,267,1,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn4) < 10 C B
static const PvkByteBitSign	faz_Ts_az__Tgn4_less_10_C_C	= {p153az,267,2,0};              // факт срабатывания уставки = {p153az,Ts а.з. -Tgn4) < 10 C C
static const PvkByteBitSign	faz_N_more75_A	= {p153az,267,3};          // факт срабатывания сигнала N > 75% A
static const PvkByteBitSign	faz_N_more75_B	= {p153az,267,4};          // факт срабатывания сигнала N > 75% B
static const PvkByteBitSign	faz_N_more75_C	= {p153az,267,5};          // факт срабатывания сигнала N > 75% C
static const PvkByteBitSign	faz_N_more5_A	= {p153az,267,6};          // факт срабатывания сигнала N > 5% A
static const PvkByteBitSign	faz_N_more5_B	= {p153az,267,7};          // факт срабатывания сигнала N > 5% B
static const PvkByteBitSign	faz_N_more5_C	= {p153az,268,0};          // факт срабатывания сигнала N > 5% C
static const PvkByteBitSign	faz_PotPitSuz_A	= {p153az,268,1};          // факт срабатывания сигнала Пот. Пит. СУЗ A
static const PvkByteBitSign	faz_PotPitSuz_B	= {p153az,268,2};          // факт срабатывания сигнала Пот. Пит. СУЗ B
static const PvkByteBitSign	faz_PotPitSuz_C	= {p153az,268,3};          // факт срабатывания сигнала Пот. Пит. СУЗ C
static const PvkByteBitSign	faz_N_more40_A	= {p153az,268,4};          // факт срабатывания сигнала N > 40% A
static const PvkByteBitSign	faz_N_more40_B	= {p153az,268,5};          // факт срабатывания сигнала N > 40% B
static const PvkByteBitSign	faz_N_more40_C	= {p153az,268,6};          // факт срабатывания сигнала N > 40% C
static const PvkByteBitSign	faz_Tpdrd_less10_A	= {p153az,268,7};      // факт срабатывания сигнала
static const PvkByteBitSign	faz_Tpdrd_less10_B	= {p153az,269,0};      // факт срабатывания сигнала
static const PvkByteBitSign	faz_Tpdrd_less10_C	= {p153az,269,1};      // факт срабатывания сигнала
static const PvkByteBitSign	faz_Npdrd_A     = {p153az,269,2};          // факт срабатывания сигнала N(пд.рд.) > плав.уст. A
static const PvkByteBitSign	faz_Npdrd_B     = {p153az,269,3};          // факт срабатывания сигнала N(пд.рд.) > плав.уст. B
static const PvkByteBitSign	faz_Npdrd_C     = {p153az,269,4};          // факт срабатывания сигнала N(пд.рд.) > плав.уст. C
static const PvkByteBitSign	faz_Seism_A     = {p153az,269,5};          // факт срабатывания сигнала Сейсмика A
static const PvkByteBitSign	faz_Seism_B     = {p153az,269,6};          // факт срабатывания сигнала Сейсмика B
static const PvkByteBitSign	faz_Seism_C     = {p153az,269,7};          // факт срабатывания сигнала Сейсмика C
static const PvkByteBitSign	faz_Oprob       = {p153az,277,6};          // Опробывание
//static const PvkByteBitSign	faz_Otkr        = {p153az,277,7};          // Открытие дверей

// индексы: факт сраб уставки 153/154 ПЗ
static const PvkByteBitSign	fpz_Tgn1_more325_C_A	= {p153pz,63,0,1};	// факт срабатывания уставки Tgn1 > 325 C A
static const PvkByteBitSign	fpz_Tgn1_more325_C_B	= {p153pz,63,1,1};	// факт срабатывания уставки Tgn1 > 325 C B
static const PvkByteBitSign	fpz_Tgn1_more325_C_C	= {p153pz,63,2,1};	// факт срабатывания уставки Tgn1 > 325 C C
static const PvkByteBitSign	fpz_Tgn2_more325_C_A	= {p153pz,63,3,1};	// факт срабатывания уставки Tgn2 > 325 C A
static const PvkByteBitSign	fpz_Tgn2_more325_C_B	= {p153pz,63,4,1};	// факт срабатывания уставкиTgn2 > 325 C B
static const PvkByteBitSign	fpz_Tgn2_more325_C_C	= {p153pz,63,5,1};	// факт срабатывания уставкиTgn2 > 325 C C
static const PvkByteBitSign	fpz_Tgn3_more325_C_A	= {p153pz,63,6,1};	// факт срабатывания уставкиTgn3 > 325 C A
static const PvkByteBitSign	fpz_Tgn3_more325_C_B	= {p153pz,63,7,1};	// факт срабатывания уставкиTgn3 > 325 C B
static const PvkByteBitSign	fpz_Tgn3_more325_C_C	= {p153pz,64,0,1};	// факт срабатывания уставкиTgn3 > 325 C C
static const PvkByteBitSign	fpz_Tgn4_more325_C_A	= {p153pz,64,1,1};	// факт срабатывания уставкиTgn4 > 325 C A
static const PvkByteBitSign	fpz_Tgn4_more325_C_B	= {p153pz,64,2,1};	// факт срабатывания уставкиTgn4 > 325 C B
static const PvkByteBitSign	fpz_Tgn4_more325_C_C	= {p153pz,64,3,1};	// факт срабатывания уставкиTgn4 > 325 C C
static const PvkByteBitSign	fpz_Ppz_more_170_3_A	= {p153pz,64,4,1};	// факт срабатывания уставки Р п.з. >170,3 кгс A
static const PvkByteBitSign	fpz_Ppz_more_170_3_B	= {p153pz,64,5,1};	// факт срабатывания уставки Р п.з. >170,3 кгс B
static const PvkByteBitSign	fpz_Ppz_more_170_3_C	= {p153pz,64,6,1};	// факт срабатывания уставки Р п.з. >170,3 кгс C
static const PvkByteBitSign	fpz_Ppz_more_165_2_A	= {p153pz,64,7,1};	// факт срабатывания уставки Р п.з. >165,2 кгс A
static const PvkByteBitSign	fpz_Ppz_more_165_2_B	= {p153pz,65,0,1};	// факт срабатывания уставки Р п.з. >165,2 кгс B
static const PvkByteBitSign	fpz_Ppz_more_165_2_C	= {p153pz,65,1,1};	// факт срабатывания уставки Р п.з. >165,2 кгс C
static const PvkByteBitSign	fpz_P_gpk_more_70_A     = {p153pz,65,2,1};	// факт срабатывания уставки Р гпк >70 кгс A
static const PvkByteBitSign	fpz_P_gpk_more_70_B     = {p153pz,65,3,1};	// факт срабатывания уставки Р гпк >70 кгс B
static const PvkByteBitSign	fpz_P_gpk_more_70_C     = {p153pz,65,4,1};	// факт срабатывания уставки Р гпк >70 кгс C
static const PvkByteBitSign	fpz_Ngcn1_less_3000_A	= {p153pz,65,5,0};	// факт срабатывания уставки N гцн 1 < 3000 кВт A
static const PvkByteBitSign	fpz_Ngcn1_less_3000_B	= {p153pz,65,6,0};	// факт срабатывания уставки N гцн 1 < 3000 кВт B
static const PvkByteBitSign	fpz_Ngcn1_less_3000_C	= {p153pz,65,7,0};	// факт срабатывания уставки N гцн 1 < 3000 кВт C
static const PvkByteBitSign	fpz_Ngcn2_less_3000_A	= {p153pz,66,0,0};	// факт срабатывания уставки N гцн 2 < 3000 кВт A
static const PvkByteBitSign	fpz_Ngcn2_less_3000_B	= {p153pz,66,1,0};	// факт срабатывания уставки N гцн 2 < 3000 кВт B
static const PvkByteBitSign	fpz_Ngcn2_less_3000_C	= {p153pz,66,2,0};	// факт срабатывания уставки N гцн 2 < 3000 кВт C
static const PvkByteBitSign	fpz_Ngcn3_less_3000_A	= {p153pz,66,3,0};	// факт срабатывания уставки N гцн 3 < 3000 кВт A
static const PvkByteBitSign	fpz_Ngcn3_less_3000_B	= {p153pz,66,4,0};	// факт срабатывания уставки N гцн 3 < 3000 кВт B
static const PvkByteBitSign	fpz_Ngcn3_less_3000_C	= {p153pz,66,5,0};	// факт срабатывания уставки N гцн 3 < 3000 кВт C
static const PvkByteBitSign	fpz_Ngcn4_less_3000_A	= {p153pz,66,6,0};	// факт срабатывания уставки N гцн 4 < 3000 кВт A
static const PvkByteBitSign	fpz_Ngcn4_less_3000_B	= {p153pz,66,7,0};	// факт срабатывания уставки N гцн 4 < 3000 кВт B
static const PvkByteBitSign	fpz_Ngcn4_less_3000_C	= {p153pz,67,0,0};	// факт срабатывания уставки N гцн 4 < 3000 кВт C
static const PvkByteBitSign	fpz_P_tpn1_less_0_408_A	= {p153pz,67,1,0};	// факт срабатывания уставки P тпн 1 < 0,408 кгс A
static const PvkByteBitSign	fpz_P_tpn1_less_0_408_B	= {p153pz,67,2,0};	// факт срабатывания уставки P тпн 1 < 0,408 кгс B
static const PvkByteBitSign	fpz_P_tpn1_less_0_408_C	= {p153pz,67,3,0};	// факт срабатывания уставки P тпн 1 < 0,408 кгс C
static const PvkByteBitSign	fpz_P_tpn2_less_0_408_A	= {p153pz,67,4,0};	// факт срабатывания уставки P тпн 2 < 0,408 кгс A
static const PvkByteBitSign	fpz_P_tpn2_less_0_408_B	= {p153pz,67,5,0};	// факт срабатывания уставки P тпн 2 < 0,408 кгс B
static const PvkByteBitSign	fpz_P_tpn2_less_0_408_C	= {p153pz,67,6,0};	// факт срабатывания уставки P тпн 2 < 0,408 кгс C
static const PvkByteBitSign	fpz_Tpdrd_less20_A  = {p153pz,67,7}; //факт срабатывания сигнала T(пд.рд.) < 20 c A
static const PvkByteBitSign	fpz_Tpdrd_less20_B  = {p153pz,68,0}; //факт срабатывания сигнала T(пд.рд.) < 20 c B
static const PvkByteBitSign	fpz_Tpdrd_less20_C  = {p153pz,68,1}; //факт срабатывания сигнала T(пд.рд.) < 20 c C
static const PvkByteBitSign	fpz_Npdrd_moreX_A   = {p153pz,68,2}; //факт срабатывания сигнала N(пд.рд.) > уст. A (ПЗ-1)
static const PvkByteBitSign	fpz_Npdrd_moreX_B   = {p153pz,68,3}; //факт срабатывания сигнала N(пд.рд.) > уст. B (ПЗ-1)
static const PvkByteBitSign	fpz_Npdrd_moreX_C   = {p153pz,68,4}; //факт срабатывания сигнала N(пд.рд.) > уст. C (ПЗ-1)
static const PvkByteBitSign	fpz_Razgr_A         = {p153pz,68,5}; //факт срабатывания сигнала Разгрузка A
static const PvkByteBitSign	fpz_Razgr_B         = {p153pz,68,6}; //факт срабатывания сигнала Разгрузка B
static const PvkByteBitSign	fpz_Razgr_C         = {p153pz,68,7}; //факт срабатывания сигнала Разгрузка C
static const PvkByteBitSign	fpz_Npdrd_more10_A  = {p153pz,69,0}; //факт срабатывания сигнала N(пд.рд.) > 10/15АЗ A (ПЗ-2)
static const PvkByteBitSign	fpz_Npdrd_more10_B  = {p153pz,69,1}; //факт срабатывания сигнала N(пд.рд.) > 10/15АЗ B (ПЗ-2)
static const PvkByteBitSign	fpz_Npdrd_more10_C  = {p153pz,69,2}; //факт срабатывания сигнала N(пд.рд.) > 10/15АЗ C (ПЗ-2)
static const PvkByteBitSign	fpz_Paden_A         = {p153pz,69,3}; //факт срабатывания сигнала Падение одного ОР A
static const PvkByteBitSign	fpz_Paden_B         = {p153pz,69,4}; //факт срабатывания сигнала Падение одного ОР B
static const PvkByteBitSign	fpz_Paden_C         = {p153pz,69,5}; //факт срабатывания сигнала Падение одного ОР C
static const PvkByteBitSign	fpz_QL_more350_A    = {p153pz,69,6}; //факт срабатывания сигнала QL>350  A
static const PvkByteBitSign	fpz_QL_more350_B    = {p153pz,69,7}; //факт срабатывания сигнала QL>350  B
static const PvkByteBitSign	fpz_QL_more350_C    = {p153pz,70,0}; //факт срабатывания сигнала QL>350  C
static const PvkByteBitSign	fpz_DNBR_less1_45_A = {p153pz,70,1}; //факт срабатывания сигнала DNBR<1,45  A
static const PvkByteBitSign	fpz_DNBR_less1_45_B = {p153pz,70,2}; //факт срабатывания сигнала DNBR<1,45  B
static const PvkByteBitSign	fpz_DNBR_less1_45_C = {p153pz,70,3}; //факт срабатывания сигнала DNBR<1,45  C
static const PvkByteBitSign	fpz_N_more75_A      = {p153pz,70,4}; //факт срабатывания сигнала N > 75% A
static const PvkByteBitSign	fpz_N_more75_B      = {p153pz,70,5}; //факт срабатывания сигнала N > 75% B
static const PvkByteBitSign	fpz_N_more75_C      = {p153pz,70,6}; //факт срабатывания сигнала N > 75% C
static const PvkByteBitSign	fpz_Otkl_EB_A       = {p153pz,70,7}; //факт срабатывания сигнала Откл. ЭБ A
static const PvkByteBitSign	fpz_Otkl_EB_B       = {p153pz,71,0}; //факт срабатывания сигнала Откл. ЭБ B
static const PvkByteBitSign	fpz_Otkl_EB_C       = {p153pz,71,1}; //факт срабатывания сигнала Откл. ЭБ C
static const PvkByteBitSign	fpz_Otkl_Gen_A      = {p153pz,71,2}; //факт срабатывания сигнала Откл. генератора A
static const PvkByteBitSign	fpz_Otkl_Gen_B      = {p153pz,71,3}; //факт срабатывания сигнала Откл. генератора B
static const PvkByteBitSign	fpz_Otkl_Gen_C      = {p153pz,71,4}; //факт срабатывания сигнала Откл. генератора C
static const PvkByteBitSign	fpz_Zakr_A          = {p153pz,71,5}; //факт срабатывания сигнала Закрыты более 1 СК A
static const PvkByteBitSign	fpz_Zakr_B          = {p153pz,71,6}; //факт срабатывания сигнала Закрыты более 1 СК B
static const PvkByteBitSign	fpz_Zakr_C          = {p153pz,71,7}; //факт срабатывания сигнала Закрыты более 1 СК C

//static const PvkByteBitSign	fpz_QL_more400_A    = {p153pz,74,0}; //факт срабатывания схемы "≥2" для QL>400(выход алг. 17)
//факт срабатывания схемы "≥2" для DNBR<1,4(выход алг. 18)

static const PvkByteBitSign	fpz_QL_more400_A = {p153pz,74,2}; //факт срабатывания сигнала QL>400 A
static const PvkByteBitSign	fpz_QL_more400_B = {p153pz,74,3}; //факт срабатывания сигнала QL>400 B
static const PvkByteBitSign	fpz_QL_more400_C = {p153pz,74,4}; //факт срабатывания сигнала QL>400 C
static const PvkByteBitSign	fpz_DNBR_less1_40_A = {p153pz,74,5}; //факт срабатывания сигнала DNBR<1,4  A
static const PvkByteBitSign	fpz_DNBR_less1_40_B = {p153pz,74,6}; //факт срабатывания сигнала DNBR<1,4  B
static const PvkByteBitSign	fpz_DNBR_less1_40_C = {p153pz,74,7}; //факт срабатывания сигнала DNBR<1,4  C



// 153, выходы алгоритмов 1..31
static const PvkByteBit algOut[31] = {
    {p153az,270,0},
    {p153az,270,1},
    {p153az,270,2},
    {p153az,270,3},
    {p153az,270,4},
    {p153az,270,5},
    {p153az,270,6},
    {p153az,270,7},
    {p153az,271,0},
    {p153az,271,1},
    {p153az,271,2},
    {p153az,271,3},
    {p153az,271,4},
    {p153az,271,5},
    {p153az,271,6},
    {p153az,271,7},
    {p153az,272,0},
    {p153az,272,1},
    {p153az,272,2},
    {p153az,272,3},
    {p153az,272,4},
    {p153az,272,5},
    {p153az,272,6},
    {p153az,272,7},
    {p153az,273,0},
    {p153az,273,1},
    {p153az,273,2},
    {p153az,273,3},
    {p153az,273,4},
    {p153az,273,5},
    {p153az,273,6},
};



// для слайда дискр. вх.сигналов, из 153_1 АЗ (алг.АЗ 1..19, 21)
static const PvkByteBitSign in153_1az[24] = {
    faz_N_more5_A, faz_N_more40_A, faz_N_more75_A, faz_Npdrd_A, fpz_Npdrd_moreX_A, /*fpz_Npdrd_more10_A*/ {blank}, faz_Tpdrd_less10_A, fpz_Tpdrd_less20_A, // 0..7
    {blank},{blank},{blank},{blank},{blank},{blank},{blank},{blank}, // 8..15
    faz_PotPitSuz_A, fpz_Paden_A, /*faz_Oprob*/{blank}, {blank},{blank},{blank}, {blank}, // 16..22
    //faz_Otkr // 23
    {blank}// 23
};
static const PvkByteBitSign in153_2az[24] = {
    {blank}, faz_Seism_A, //0..1
    {blank},{blank}, // 2,3
    fpz_Razgr_A, fpz_Otkl_EB_A, fpz_Otkl_Gen_A, fpz_Zakr_A, //4..7
    {blank},{blank},{blank},{blank},{blank}, // 8..12
    fpz_QL_more400_A, fpz_DNBR_less1_40_A, // 13,14
    fpz_QL_more350_A, fpz_DNBR_less1_45_A, // 15,16
    {blank},{blank},{blank},{blank},{blank},{blank},{blank} // 17..23
};
// для слайда дискр. вых.сигналов, из 153_1 АЗ (алг.АЗ 1..19, 21)
static const PvkByteBit out153_1az[24] = {
    {p153az,282,7},// ШАК1.1 АЗ = обобщ АЗ
    {p153az,282,7},// ШАК2.1 АЗ = обобщ АЗ
    {blank},
    {blank},
    {p153az,270,0},
    {p153az,270,1},
    {p153az,270,2},
    {p153az,270,3},
    {p153az,270,4},
    {p153az,270,5},
    {p153az,270,6},
    {p153az,270,7},
    {p153az,271,0},
    {p153az,271,1},
    {p153az,271,2},
    {p153az,271,3},
    {p153az,271,4},
    {p153az,271,5},
    {p153az,271,6},
    {p153az,271,7},
    {p153az,272,0},
    {p153az,272,1},
    {p153az,272,2},
    {p153az,272,4}
};

// для слайда дискр. вых.сигналов, из 153_2 АЗ (алг.АЗ 22..31, 20 )
static const PvkByteBit out153_2az[24] = {
    {p153az,282,7},// ШАК1.2 АЗ = обобщ АЗ
    {p153az,282,7},// ШАК2.2 АЗ = обобщ АЗ
    {p153az,272,5},// алг21
    {p153az,272,6},
    {p153az,272,7},
    {p153az,273,0},
    {p153az,273,1},
    {p153az,273,2},
    {p153az,273,3},
    {p153az,273,4},
    {p153az,273,5},
    {p153az,273,6},// алг31
    {p153az,272,3},// алг20
    {blank},
    {blank},
    {blank},
    {blank},
    {p153az,277,6},// ОБОБЩ ИСПРАВНОСТЬ
    {p153az,277,7},// ОБОБЩ ПРОВЕРКА
    {blank},
    {blank},
    {p153az,273,5},// выход алг30 = СЕЙСМИКА
    {p153az,273,5},// выход алг30 = СЕЙСМИКА
    {p153az,282,7},// СРАБАТЫВАНИЕ АЗ = обобщ АЗ = ШАК
};

// для слайда дискр. вых.сигналов, из 153_1 ПЗ (алг.АЗ 1..5)
static const PvkByteBit out153_1pz[24] = {
    {p153pz,80,2}, // 0 шак 1.1 ПЗ-1
    {p153pz,80,2}, // 1 шак 2.1 ПЗ-1
    {p153pz,80,3}, // 2 шак 1.2 ПЗ-2
    {p153pz,80,3}, // 3 шак 2.1 ПЗ-2
    {p153pz,80,4}, // 4 шак 1.1 УПЗ
    {p153pz,80,4}, // 5 шак 2.1 УПЗ
    {p153pz,65,5}, // 6 (7я строчка) откл гцн1 (АРОМ) ====факт срабатывания уставки N гцн 1 < 3000 кВт A
    {p153pz,66,0}, // 7 (8я строчка) откл гцн2 (АРОМ)
    {p153pz,66,3}, // 8 (9я строчка) откл гцн3 (АРОМ)
    {p153pz,66,6}, // 9 (10я строчка) откл гцн4 (АРОМ)
    {p153pz,67,1}, // 10 (11я строчка) откл тпн1 (АРОМ) ====факт срабатывания уставки P тпн 1 < 0,408 кгс A
    {p153pz,67,4}, // 11 (12я строчка) откл тпн2 (АРОМ) ====факт срабатывания уставки P тпн 2 < 0,408 кгс A
    {p153pz,70,7}, // 12 (13я строчка) откл ЭБ (АРОМ) ====факт срабатывания сигнала Откл. ЭБ A
    {p153pz,71,2}, // 13 (15я строчка) выкл Генер (АРОМ) ====факт срабатывания сигнала Откл. генератора A
    {p153pz,71,5}, // 14 (16я строчка) закр 2/4 СК (АРОМ)
    {blank}, // 15 (16я строчка) откл КЭН
    {blank}, // 16 (17я строчка)  откл ЦЭН
    {blank}, // 17 (18я строчка) <пусто>
    {blank}, // 18 (19я строчка) <пусто>
    {p153pz,72,0}, // 20 (21я строка), вых.канал 1 (Т ГРЧ > +3 C)
    {p153pz,72,1}, // 20 Р РЕАК > 170,3
    {p153pz,72,2}, // 21 Р ГПК > 65
    {p153pz,72,3}, // 22 Т ДР ДП ИД <20
    {p153pz,72,4}, // 23 N ДР > УСТ
};

// для слайда дискр. вых.сигналов, из 153_1 ПЗ (алг.ПЗ 6..19, 21)
static const PvkByteBit out153_2pz[24] = {
    {p153pz,80,2}, // шак 1.1 ПЗ
    {p153pz,80,2},
    {p153pz,80,3}, // шак 1.2 ПЗ
    {p153pz,80,3},
    {p153pz,80,4}, // шак 1.3 ПЗ
    {p153pz,80,4},
    {p153pz,72,5}, //6 (7 строчка) ===факт срабатывания схемы "≥2" для Разгрузка(выход алг. 6)
    {p153pz,74,0}, // факт срабатывания схемы "≥2" для QL>400(выход алг. 17)
    {p153pz,74,1}, // факт срабатывания схемы "≥2" для DNBR<1,4(выход алг. 18)
    {p153pz,72,6}, //9 (10я строчка) факт срабатывания схемы "≥2" для N(пд.рд.) > 10/15АЗ(выход алг. 7)
    {p153pz,72,7}, // факт срабатывания схемы "≥2" для Р а.з. >165,2 кгс(выход алг. 8)
    {p153pz,73,0}, //факт срабатывания схемы "≥2" для Падение одного ОР(выход алг. 9)
    {p153pz,73,1}, // факт срабатывания схемы "≥2" для QL>350(выход алг. 10)
    {p153pz,73,2}, // факт срабатывания схемы "≥2" для DNBR<1,45(выход алг. 11)
    {p153pz,73,3}, // выход схемы "&"  N>75&Откл. Гцн(выход алг. 12)
    {p153pz,73,4}, // выход схемы "&"  N>75&Откл. ЭБ(выход алг. 13)
    {p153pz,73,5}, // выход схемы "&"  N>75&Откл. Генератора(выход алг. 14)
    {p153pz,73,6}, // выход схемы "&"  N>75&P тпн < 0,408(выход алг. 15)
    {p153pz,73,7}, // выход схемы "&"  N>75&Закрыты более 1 СК(выход алг. 16)
    {p153pz,80,2}, // ОБОБЩ ПЗ1
    {p153pz,80,3}, // ОБОБЩ ПЗ2
    {p153pz,80,4}, // ОБОБЩ УПЗ
    {p153pz,80,5}  // СРАБ ПЗ
};

// для слайда дискр. вых.сигналов, из 153_1 ПЗ (алг.ПЗ 6..19, 21)
//static const PvkByteBit out151_1[24] = {
//  {p151,1}
//};


//// в имитации измеритель 157-158
//static PvkByteBit im157_Tpgn1 = {p157_1,436,4};// ТП ГРЧН1[T гн 1]
//static PvkByteBit im157_Tpgn4 = {p157_1,436,5};// ТП ГРЧН4[T гн 4]
//static PvkByteBit im157_Tspgn14 = {p157_1,436,6}; // ТСП ГРЧН1,4
//static PvkByteBit im157_Tsp_res1 = {p157_1,436,7}; // ТСП резерв
//static PvkByteBit im157_Tpgn2 = {p157_1,437,0}; // ТП ГРЧН2[T гн 2]
//static PvkByteBit im157_Tpgn3 = {p157_1,437,1}; // ТП ГРЧН3[T гн 3]
//static PvkByteBit im157_Tspgn23 = {p157_1,437,2};  // ТСП ГРЧН2,3
//static PvkByteBit im157_Tsp_res2 = {p157_1,437,3};  // ТСП резерв
//static PvkByteBit im157_Tphn = {p157_1,437,4};  // ТП ХН
//static PvkByteBit im157_Tp_res = {p157_1,437,5}; // ТП резерв
//static PvkByteBit im157_Tsphn = {p157_1,437,6};  // ТСП ХН
//static PvkByteBit im157_Tsp_res3 = {p157_1,437,7};  // ТСП резерв
//static PvkByteBit im157_Preak = {p157_1,438,0};  // Р РЕАК, кгс/см²
//static PvkByteBit im157_Pgpk = {p157_1,438,1};  // Ргпк, кгс/см²
//static PvkByteBit im157_Ts_reak_res = {p157_1,438,2};  // резерв (Р-Р РЕАК, кгс/см²)[Ts реак]
//static PvkByteBit im157_dP_reak_res = {p157_1,438,3};  // резерв (dР РЕАК, кгс/см²)
//static PvkByteBit im157_Fgcn1 = {p157_1,438,4};  // F ПИТ ГЦН1, Гц
//static PvkByteBit im157_Fgcn2 = {p157_1,438,5};  // F ПИТ ГЦН2, Гц
//static PvkByteBit im157_Fgcn3 = {p157_1,438,6};  // F ПИТ ГЦН3, Гц
//static PvkByteBit im157_Fgcn4 = {p157_1,438,7};  // F ПИТ ГЦН4, Гц
//static PvkByteBit im157_Ptrop1 = {p157_1,439,0};  // Р ТРОП ПГ1, кгс/см²
//static PvkByteBit im157_Ptrop2 = {p157_1,439,1}; // Р ТРОП ПГ2, кгс/см²
//static PvkByteBit im157_Ptrop3 = {p157_1,439,2}; // Р ТРОП ПГ3, кгс/см²
//static PvkByteBit im157_Ptrop4 = {p157_1,439,3}; // Р ТРОП ПГ4, кгс/см²
//static PvkByteBit im157_Ptpn1 = {p157_1,439,4}; // Р ЗКП ТПН1, кгс/см²
//static PvkByteBit im157_Ptpn2 = {p157_1,439,5}; // Р ЗКП ТПН2, кгс/см²
//static PvkByteBit im157_Pgaz06 = {p157_1,439,6}; // Р ГАЗ06/1, кгс/см²
//static PvkByteBit im157_Lkd = {p157_1,439,7}; // L КД, см
//static PvkByteBit im157_dPgcn1 = {p157_1,440,0}; // Р-Р ГЦН1, кгс/см²
//static PvkByteBit im157_dPgcn2 = {p157_1,440,1}; // Р-Р ГЦН2, кгс/см²
//static PvkByteBit im157_dPgcn3 = {p157_1,440,2}; // Р-Р ГЦН3, кгс/см²
//static PvkByteBit im157_dPgcn4 = {p157_1,440,3}; // Р-Р ГЦН4, кгс/см²
//static PvkByteBit im157_MedGcn1 = {p157_1,440,4}; // МЭД ГЦН1, кВт
//static PvkByteBit im157_MedGcn2 = {p157_1,440,5}; // МЭД ГЦН2, кВт
//static PvkByteBit im157_MedGcn3 = {p157_1,440,6}; // МЭД ГЦН3, кВт
//static PvkByteBit im157_MedGcn4 = {p157_1,440,7}; // МЭД ГЦН4, кВт
//static PvkByteBit im157_Lpg1 = {p157_1,441,0}; // L ПГ1, см
//static PvkByteBit im157_Lpg2 = {p157_1,441,1}; // L ПГ2, см
//static PvkByteBit im157_Lpg3 = {p157_1,441,2}; // L ПГ3, см
//static PvkByteBit im157_Lpg4 = {p157_1,441,3}; // L ПГ4, см
// резерв (dР ТРОП ПГ1, кгс/см²)[Ts ПГ1]
// резерв (dР ТРОП ПГ2, кгс/см²)[Ts ПГ2]
// резерв (dР ТРОП ПГ3, кгс/см²)[Ts ПГ3]
// резерв (dР ТРОП ПГ4, кгс/см²)[Ts ПГ4]
// резерв
// резерв
// резерв
// резерв
// ТП резерв
// ТП резерв
// ТСП резерв
// ТСП резерв



//узлы ППН отключение от датчиков/ПГХ и вывод в имитацию канала 0...63
static PvkByteBit imPkc_Tpgn1 = {pkc,71,0};// ТП ГРЧН1[T гн 1]
static PvkByteBit imPkc_Tpgn4 = {pkc,71,1};// ТП ГРЧН4[T гн 4]
static PvkByteBit imPkc_Tspgn14 = {pkc,71,2}; // ТСП ГРЧН1,4
static PvkByteBit imPkc_Tsp_res1 = {pkc,71,3}; // ТСП резерв
static PvkByteBit imPkc_Tpgn2 = {pkc,71,4}; // ТП ГРЧН2[T гн 2]
static PvkByteBit imPkc_Tpgn3 = {pkc,71,5}; // ТП ГРЧН3[T гн 3]
static PvkByteBit imPkc_Tspgn23 = {pkc,71,6};  // ТСП ГРЧН2,3
static PvkByteBit imPkc_Tsp_res2 = {pkc,71,7};  // ТСП резерв
static PvkByteBit imPkc_Tphn = {pkc,72,0};  // ТП ХН
static PvkByteBit imPkc_Tp_res = {pkc,72,1}; // ТП резерв
static PvkByteBit imPkc_Tsphn = {pkc,72,2};  // ТСП ХН
static PvkByteBit imPkc_Tsp_res3 = {pkc,72,3};  // ТСП резерв
static PvkByteBit imPkc_Preak = {pkc,72,4};  // Р РЕАК, кгс/см²
static PvkByteBit imPkc_Pgpk = {pkc,72,5};  // Ргпк, кгс/см²
static PvkByteBit imPkc_Ts_reak_res = {pkc,72,6};  // резерв (Р-Р РЕАК, кгс/см²)[Ts реак]
static PvkByteBit imPkc_dP_reak_res = {pkc,72,7};  // резерв (dР РЕАК, кгс/см²)
static PvkByteBit imPkc_Fgcn1 = {pkc,73,0};  // F ПИТ ГЦН1, Гц
static PvkByteBit imPkc_Fgcn2 = {pkc,73,1};  // F ПИТ ГЦН2, Гц
static PvkByteBit imPkc_Fgcn3 = {pkc,73,2};  // F ПИТ ГЦН3, Гц
static PvkByteBit imPkc_Fgcn4 = {pkc,73,3};  // F ПИТ ГЦН4, Гц
static PvkByteBit imPkc_Ptrop1 = {pkc,73,4};  // Р ТРОП ПГ1, кгс/см²
static PvkByteBit imPkc_Ptrop2 = {pkc,73,5}; // Р ТРОП ПГ2, кгс/см²
static PvkByteBit imPkc_Ptrop3 = {pkc,73,6}; // Р ТРОП ПГ3, кгс/см²
static PvkByteBit imPkc_Ptrop4 = {pkc,73,7}; // Р ТРОП ПГ4, кгс/см²
static PvkByteBit imPkc_Ptpn1 = {pkc,74,0}; // Р ЗКП ТПН1, кгс/см²
static PvkByteBit imPkc_Ptpn2 = {pkc,74,1}; // Р ЗКП ТПН2, кгс/см²
static PvkByteBit imPkc_Pgaz06 = {pkc,74,2}; // Р ГАЗ06/1, кгс/см²
static PvkByteBit imPkc_Lkd = {pkc,74,3}; // L КД, см
static PvkByteBit imPkc_dPgcn1 = {pkc,74,4}; // Р-Р ГЦН1, кгс/см²
static PvkByteBit imPkc_dPgcn2 = {pkc,74,5}; // Р-Р ГЦН2, кгс/см²
static PvkByteBit imPkc_dPgcn3 = {pkc,74,6}; // Р-Р ГЦН3, кгс/см²
static PvkByteBit imPkc_dPgcn4 = {pkc,74,7}; // Р-Р ГЦН4, кгс/см²
static PvkByteBit imPkc_MedGcn1 = {pkc,75,0}; // МЭД ГЦН1, кВт
static PvkByteBit imPkc_MedGcn2 = {pkc,75,1}; // МЭД ГЦН2, кВт
static PvkByteBit imPkc_MedGcn3 = {pkc,75,2}; // МЭД ГЦН3, кВт
static PvkByteBit imPkc_MedGcn4 = {pkc,75,3}; // МЭД ГЦН4, кВт
static PvkByteBit imPkc_Lpg1 = {pkc,75,4}; // L ПГ1, см
static PvkByteBit imPkc_Lpg2 = {pkc,75,5}; // L ПГ2, см
static PvkByteBit imPkc_Lpg3 = {pkc,75,6}; // L ПГ3, см
static PvkByteBit imPkc_Lpg4 = {pkc,75,7}; // L ПГ4, см



#endif // ALG_FACT_MEQ2

